import firebase from 'firebase/app';
//import 'firebase/firestore';
import 'firebase/functions';

// Initialize Firebase
const devConfig = {
  apiKey: 'AIzaSyCssHRiQ3PfgqK467d2exPgW1qTAg8kPZc',
  authDomain: 'repeat-a738a.firebaseapp.com',
  databaseURL: 'https://repeat-a738a.firebaseio.com',
  projectId: 'repeat-a738a',
  storageBucket: 'repeat-a738a.appspot.com',
  messagingSenderId: '811484102953',
};

firebase.initializeApp(devConfig);

export const functions = firebase.functions();

// https://firebase.googleblog.com/2017/04/easier-configuration-for-firebase-on-web.html
// https://firebase.google.com/docs/hosting/reserved-urls
