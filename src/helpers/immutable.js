/**
 * Метод обновляет элемент массива, не мутирует оригинальный массив
 *
 * @param {Array} array исходный массив
 * @param {Object} action объект с двумя ключами — index и item (новый элемент и индекс по-которому живет старый элемент)
 *
 * @return {Array} новый массив
 */
export const updateObjectInArray = (array, action) => {
  return array.map((item, index) => {
    console.log('nope');

    if (index !== action.index) {
      // This isn't the item we care about - keep it as-is
      return item;
    }

    // Otherwise, this is the one we want - return an updated value
    console.log('yep');
    return {
      ...item,
      ...action.item,
    };
  });
};
