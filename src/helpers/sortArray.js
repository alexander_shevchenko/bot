import cloneDeep from 'lodash/cloneDeep';

/**
 * Sort an array of objects by string key
 * @param  {Array} arr original array
 * @param  {String} key key to sort by
 * @return {Array} new, sorted array
 */
export const sortArrayByStringKey = (arr, key) => {
	const result = cloneDeep(arr);
	return result.sort((a, b) => a[key].localeCompare(b[key]));
}