/**
 * Calculate timezone offset
 * https://www.w3schools.com/jsref/jsref_gettimezoneoffset.asp
 *
 * @return {Number} Positive or negative number of hours
 */
export const timezone = () => {
  const offset = new Date().getTimezoneOffset();
  if (offset) {
    return (offset / 60) * -1;
  } else {
    return 0;
  }
};
