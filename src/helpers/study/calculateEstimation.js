/* Calculates estimated time */
export const calculateEstimation = (total) => {
  let estimated = 0;
  // коэффициент, добавляет больше времени для более длинных тренировок
  let k = 1;
  if (total > 10 && total <= 20) k = 1.1;
  if (total > 20 && total <= 30) k = 1.2;
  if (total > 30 && total <= 50) k = 1.3;
  if (total > 50 && total <= 75) k = 1.4;
  if (total > 75) k = 1.45;
  
  estimated = Math.max(1, Math.round(total/4 * k));

  return estimated;
}