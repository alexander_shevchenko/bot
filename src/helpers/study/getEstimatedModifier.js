/* Calculates estimated block's css modifier */
export const getEstimatedModifier = (total) => {
  if (total <= 34) return '--success';
  if (total > 34 && total <= 54) return '--warning';
  if (total > 54) return '--danger';
}