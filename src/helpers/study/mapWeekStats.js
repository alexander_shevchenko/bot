/**
 * Возвращает массив с данными в какие дни текущей недели тренировался пользователь
 * 
 * @param {Array} weekDays - массив с кол-вом карточек в тренировке за каждый из последних 7-ми дней
 * @param {Array} stats - массив с кол-вом карточек в тренировке за каждый из последних 7-ми дней
 * @returns {Array} - массив из объектов, с именем дня недели и флагом isPracticed
 */
export const mapWeekStats = (weekDays, stats) => {
  // текущий день недели
  // воскресенье === 0, меняю на 7-ку
  const currentWeekDay = new Date().getDay() ? new Date().getDay() : 7;
  
  // делаю shallow copy входящей статистики
  // делаю реверс массива (массив с днями недели идет из прошлого в будущее, а со статистикой наоборот)
  // отрезаю лишние дни в статистике
  // минус означает сколько брать элементов с конца
  const currentWeekStats = [...stats].reverse().slice( -currentWeekDay );

  // мерджу и возвращаю массив из имен дней недели со статистикой за текущую неделю
  return weekDays.map((weekDay, i) => {
    return {
      weekDay,
      isPracticed: !!currentWeekStats[i]
    }
  });
}