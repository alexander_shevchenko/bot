import { getPercentage } from '../../helpers/';
import { textAnswers }  from '../../actions/consts';

/**
 * Метод получает длину новой очереди и параметры для расчета прогресса,
 * возвращает обновленные параметры прогресса.
 * 
 * @param {Number} queueLength длина очереди
 * @param {String} Lt полная длина тренировки
 * @param {String} Lp длина тренировки в предыдущем шаге
 * @param {String} A  последняя оценка
 * @param {Boolean} isEdit было редактирование
 * @param {Boolean} isDelete было удаление
 * 
 * @return {Object} результат
 *    @param {String} Lt новая полная длина тренировки
 *    @param {String} Lp новая длина тренировки в предыдущем шаге
 *    @param {String} P  новый прогресс в процентах
 *    @param {String} A последняя оценка
 * 
 */
export const calculateProgress = ({
  queueLength, 
  Lt, 
  Lp,
  A, 
  isEdit, 
  isDelete
}) => {
  /* 6 КЕЙСОВ */
  /* 1. Карточка была отредактирована */
  if (isEdit) {
    return {
      Lt,
      Lp,
      P: getPercentage(Lt, queueLength),
      A,
    }
  }
  /* 2. Карточка была удалена */
  if (isDelete) {
    return {
      Lt: Lt -1,
      Lp: Lp -1,
      P: getPercentage(Lt - 1, queueLength),
      A,
    }
  }
  /* 3. Ответа еще нет */
  if (A === null) {
    // Модуль тренировки только инициализировался, пользователь не совершил никаких действий
    // FIXME: проверить этот момент на работоспособность с пустой очередью
    return {
      Lt: queueLength,
      Lp: queueLength,
      A,
    }
  }
  /* 4. Ответ again */
  if (A === textAnswers.again) {
    // на fail прогресс не меняется
    return {
      Lt,
      Lp,
      A,
    }
  }
  /* 5. Ответ есть и карточку выбило */
  if (Lp > queueLength) {
    // учитывая что ответ null я отсекаю раньше, то здесь я обхожусь только
    // тем условием, что очередь стала меньше
    return {
      Lt,
      Lp: queueLength,
      P: getPercentage(Lt, queueLength),
      A,
    }
  }
  /* 6. Ответ есть и карточка осталась */
  if (Lp === queueLength) {
    // учитывая что ответ null я отсекаю раньше, то здесь я обхожусь только
    // тем условием, что очередь стала меньше
    return {
      Lt: Lt + 1,
      Lp: queueLength,
      P: getPercentage(Lt + 1, queueLength),
      A,
    }
  }

  return {};
}