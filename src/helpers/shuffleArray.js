/**
 * Shuffles any array
 * @param  {Array} arr входящий массив
 * @return {Array} отшафленный массив
 */
export const shuffleArray = (arr) => {
	let j, temp;
	for(let i = arr.length - 1; i > 0; i--){
		j = Math.floor(Math.random()*(i + 1));
		temp = arr[j];
		arr[j] = arr[i];
		arr[i] = temp;
	}
	return arr;
}