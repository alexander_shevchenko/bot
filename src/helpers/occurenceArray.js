import cloneDeep from 'lodash/cloneDeep';

/**
 * Check if the array A contains all the elements of the array B in the same order
 * Returns index of the first B elemnt in A array.
 * ! Returns -1 if A doesn't contain B.
 * ! Returns only the first occurence.
 * ! Works only with the shallow arrays.
 * 
 * @param  {Array} A array A
 * @param  {Array} B array B
 * @return {Nmuber} index of the first occurence or -1
 */
export const occurenceArray = (A, B) => {
	// 1. взять первый элемент из массива б и найти все индексы этого элемента в массиве а
	const occurences = []; 
	A.forEach((word, index) => {
		if (word === B[0]) occurences.push(index);
	});

	// 2. если первый элемент Б нигде не встретился в A -- возвращаю -1
	if (!occurences.length) return -1;

	// 3. прохожу циклом по найденным индексам, 
	// проверяю все элементы массива б, совпадают ли последующими элементами в массиве а?
	for (let aIndex = 0; aIndex < occurences.length; aIndex++) {
		let isFullOccurence = true;

		// итерирую по элементам массива б
		// если все элементы совпали, то возвращаю индекс из occurences
		for (let bIndex = 0; bIndex < B.length; bIndex++) {
			isFullOccurence = B[bIndex] === A[occurences[aIndex] + bIndex] ? true : false;
		}

		if (isFullOccurence) return occurences[aIndex];
	}
	
	// если полных совпадений нет -- -1
	return -1;
}