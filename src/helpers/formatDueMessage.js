import { pluralize_ru } from './pluralize';

/**
 * Метод получает дату в милисекундах,
 * сравнивает полученную дату с текущей и
 * возвращает отформатированное сообщение вида:
 * просрочено, сегодня, N день/я/ей, N месяц/а/ей и N день/я/ей.
 * 
 * @param  {Number} dueDate искомый timestamp
 * @return {String} отформатированное сообщение (см. описание выше)
 */
const formatDueMessage = (dueDate) => {
  const now = new Date();
  const due = new Date(dueDate);

  const y1 = due.getFullYear();
  const m1 = due.getMonth();
  const d1 = due.getDate();

  const y2 = now.getFullYear();
  const m2 = now.getMonth();
  const d2 = now.getDate();

  const days = (y1 - y2) * 365 + (m1 - m2) * 30 + (d1 - d2);

  if (days < 0)   return 'просрочено';
  if (days === 0) return 'сегодня';
  if (days < 30)  return pluralize_ru(days, ['день', 'дня', 'дней']);

  return `${pluralize_ru(Math.floor(days / 30), ['месяц', 'месяца', 'месяцев'])} и ${pluralize_ru(days % 30, ['день', 'дня', 'дней'])}`;
}

export default formatDueMessage;