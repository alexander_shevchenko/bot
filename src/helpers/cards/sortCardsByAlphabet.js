import cloneDeep from 'lodash/cloneDeep';

/**
 * Gets raw data from the api — an object with card IDs as keys
 * and front (f) / part of speech (p) for every card ID.
 *
 * Creates an array with cards sorted alphabetically (sorted by 'front').
 * Cards with a 'front' that starts with a non-latin
 * character are stored under '#'.
 *
 * @param {Object} cards
 * @return {Array}
 */

export const sortCardsByAlphabet = (cards) => {
  let result = cloneDeep(resultTemplate);

  for (let key in cards) {
    const raw = cards[key];
    const letter = raw.f[0];
    const card = {
      id: key,
      word: raw.f,
      part: raw.p ? raw.p : '',
    };

    if (letter.search(/[a-z]/i) !== -1) {
      const index = result.findIndex((el) => el.key === letter.toUpperCase());
      result[index].list.push(card);
    } else {
      const index = result.findIndex((el) => el.key === '#');
      result[index].list.push(card);
    }
  }

  return result;
};

const resultTemplate = [
  { key: 'A', list: [] },
  { key: 'B', list: [] },
  { key: 'C', list: [] },
  { key: 'D', list: [] },
  { key: 'E', list: [] },
  { key: 'F', list: [] },
  { key: 'G', list: [] },
  { key: 'H', list: [] },
  { key: 'I', list: [] },
  { key: 'J', list: [] },
  { key: 'K', list: [] },
  { key: 'L', list: [] },
  { key: 'M', list: [] },
  { key: 'N', list: [] },
  { key: 'O', list: [] },
  { key: 'P', list: [] },
  { key: 'Q', list: [] },
  { key: 'R', list: [] },
  { key: 'S', list: [] },
  { key: 'T', list: [] },
  { key: 'U', list: [] },
  { key: 'V', list: [] },
  { key: 'W', list: [] },
  { key: 'X', list: [] },
  { key: 'Y', list: [] },
  { key: 'Z', list: [] },
  { key: '#', list: [] },
];
