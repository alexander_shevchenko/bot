/**
 * Function returns the first non-null member of an array
 * of characters.
 * If all are nulls — returns null.
 *
 * @param {Array}
 * @return {String} || @return {Null}
 */

export const findFirstActiveChar = (chars) => {
  const char = chars.find((ch) => ch !== null);
  return char || null;
};
