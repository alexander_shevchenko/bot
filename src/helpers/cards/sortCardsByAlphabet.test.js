import { sortCardsByAlphabet } from './sortCardsByAlphabet';

const testData = {
  "-Lzhy4pDXhpj8ZwMwHpe": {
    "f": "hefty",
    "p": "adjective"
  },
  "-LzmuXNT8eJmlAjiHgln": {
    "f": "misogyny",
    "p": "noun"
  },
  "-LzncYVR19jx6btUDZyR": {
    "f": "tilt",
    "p": "noun"
  },
  "-LzxKeLQelmg0Sk7nB3v": {
    "f": "tree"
  },
  "-LzxKgTfxW6GByEEdAfm": {
    "f": "яйцо"
  },
  "-LzzJ36KRjAXIhDpcl-i": {
    "f": "1",
    "p": "adjective"
  }
}

test('загоняется ли слово с латинской буквы в соответствующий слот', () => {
  const result = sortCardsByAlphabet( testData );
  expect( result[7].list.length ).toBe(1);
});

test('загоняются ли слова с нелатинской буквы в соответствующий слот', () => {
  const result = sortCardsByAlphabet( testData );
  expect( result[26].list.length ).toBe(2);
});

test('рассортированы ли все входящие карточки', () => {
  const result = sortCardsByAlphabet( testData );
  let resultCount = 0;
  result.forEach( r => resultCount += r.list.length);
  expect( resultCount ).toBe(6);
});

test('буква которой нет в входящих данных в итоге должна содержать пустой массив', () => {
  const result = sortCardsByAlphabet( testData );
  expect( result[0].list.length ).toBe(0);
});

test('соответствие входной и выходной даты для конкретного слова', () => {
  const result = sortCardsByAlphabet( testData );
  const expected = {
    id: '-Lzhy4pDXhpj8ZwMwHpe',
    word: 'hefty',
    part: 'adjective',
  }

  expect( result[7].list[0] ).toEqual(expected);
});

test('поле часть речи есть даже в том случае, если его нет в входящих данных', () => {
  const result = sortCardsByAlphabet( testData );
  expect( result[26].list[0] ).toHaveProperty('part', '');
  
});