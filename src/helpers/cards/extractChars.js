/**
 * Gets an array of {key: '', list: []} objects.
 * If there are cards in a list — a corresponding key gets
 * into the resulting array.
 * Otherwise — null is added.
 * The resulting array is used by a sidebar navigation
 * panel in Cards component.
 *
 * @param {Array} cards
 * @return {Array}
 */
export const extractChars = (cards) => {
  const result = [];

  cards.forEach((card) => {
    result.push(card.list.length ? card.key : null);
  });

  return result;
};
