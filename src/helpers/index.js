/**
 * Метод возвращает процентное соотношение между двумя величинами
 * 
 * @param  {Number} t полное
 * @param  {Number} c частное
 * 
 * @return {Number} процентное соотношение полного и частного
 */
export const getPercentage = (t, c) => ((t - c) / t) * 100;