import Page from '../../builder';
import LookupPage from '../../pages/LookupPage';

// Основные сценарии
describe('Lookup - fetch', () => {
  let page;
  let lookupPage;

  beforeEach(async () => {
    page = await Page.build('iPhone 6');
    lookupPage = new LookupPage(page);
  });

  afterEach(async () => {
    await page.close();
  })

  test('Слово успешно фетчится', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup('tree');
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
  });

  test('Слово успешно фетчится, есть перевод', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup('tree');
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.isAnyTranslationFetched() ).toBeTruthy();
  });

  test('Слово успешно фетчится, есть перевод и тезаурус', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup('fast');
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.isAnyTranslationFetched() ).toBeTruthy();

    await lookupPage.pickPartDefExample();
    expect( await lookupPage.isAnySynonym() ).toBeTruthy();
    expect( await lookupPage.isAnyAntonym() ).toBeTruthy();
  });

  test('Слово не найдено, но пришли результаты поиска', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup('treee');
    expect( await lookupPage.isSuggestions() ).toBeTruthy();
  });

  test('Слово не найдено и поиск ничего не вернул', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup('2reee2e');
    expect( await lookupPage.isNothingFound() ).toBeTruthy();
  });

  test('Дериватив успешно фетчится, есть ссылка', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup('jocularly');
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.isAnyDerivative() ).toBeTruthy();
  });
});





// основные сценарии -
        // слово без ошибок
        // слово без ошибок и с переводом
        // слово без ошибок и с переводом и тезаурус
        // пришли варианты из серча
        // вообще ничего не найдено

  // слово без определений - дериватив jocularly
  // слово без определений - кросс-референс juke (проблема с этими кейсами в том, что данные постепенно обновляются
  // и то, что было просто кросс-референсом сегодня -- завтра может быть полноценной записью) 