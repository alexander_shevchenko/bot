import Page from '../../builder';
import LookupPage from '../../pages/LookupPage';
import CardsPage from '../../pages/CardsPage';

describe('Lookup — post', () => {
  let page;
  let lookupPage;
  let cardsPage;

  beforeEach(async () => {
    page = await Page.build('iPhone 6');
    lookupPage = new LookupPage(page);
    cardsPage = new CardsPage(page);
  });

  afterEach(async () => {
    await page.close();
  })

  test('Карточка успешно создается', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup('ardent');
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.isAnyTranslationFetched() ).toBeTruthy();
    
    await lookupPage.pickPartDefExample();
    expect( await lookupPage.isAnySynonym() ).toBeTruthy();
    expect( await lookupPage.isAnyAntonym() ).toBeTruthy();

    await lookupPage.pickThesaurus();
    expect ( await lookupPage.isAnyTranslation() ).toBeTruthy();
    
    await lookupPage.pickTranslation();
    expect ( await lookupPage.isCardIncludesEverything() ).toBeTruthy();
    
    await lookupPage.submitCard();
    expect( await lookupPage.isSubmitSuccessfull() ).toBeTruthy();
  });

  test('Отрабатывает проверка на дубликат', async () => {
    // предыдущий тест создает карточку, данный находит дубликат
    await lookupPage.visit();
    await lookupPage.indexLookup('ardent');
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.isAnyTranslationFetched() ).toBeTruthy();
    
    await lookupPage.pickPartDefExample();
    expect( await lookupPage.isAnySynonym() ).toBeTruthy();
    expect( await lookupPage.isAnyAntonym() ).toBeTruthy();

    await lookupPage.pickThesaurus();
    expect ( await lookupPage.isAnyTranslation() ).toBeTruthy();
    
    await lookupPage.pickTranslation();
    expect ( await lookupPage.isCardIncludesEverything() ).toBeTruthy();
    
    await lookupPage.submitCard();
    expect( await lookupPage.isSubmitDuplicate() ).toBeTruthy();
  });
  
  test('Чистка, удаляю карточку', async () => {
    // предыдущие тесты создают карточку, данный удаляет, чистит
    await cardsPage.visit();
    await cardsPage.findDeleteCard('ardent');
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
});