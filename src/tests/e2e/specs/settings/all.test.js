import Page from '../../builder';
import SettingsPage from '../../pages/SettingsPage';
import LookupPage from '../../pages/LookupPage';

describe('Settings', () => {
  let page;
  let settingsPage;
  let lookupPage;
  
  const langs = {
    de: 'German',
    ru: 'Russian',
  };  
  
  const words = {
    en: 'tree',
    de: 'Baum',
    ru: 'дерево',
  };

  beforeAll(async () => {
    page = await Page.build('iPhone 6');
    settingsPage = new SettingsPage(page);
    lookupPage = new LookupPage(page);
  });

  afterAll(async () => {
    await page.close();
  });

  // отключить переводы - успех
  // сходить в лукап, проверить, что переводов нет
  // вернуть чекбокс, выбрать немецкий язык - успех
  // сходить в лукап, убедиться что перевод tree > Baum
  // выбрать русский язык - успех, 
  // сходить в лукап, убедиться, что перевод tree > дерево
  test('Отключить перевод — успех', async () => {
    await settingsPage.visit();
    await settingsPage.disableTranslations();
    await settingsPage.submit();
    expect( await settingsPage.isSuccess() ).toBeTruthy();
  });

  test('Сходить в лукап - перевода не должно быть', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup(words.en);
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.isAnyTranslationFetched() ).toBeFalsy();
  });

  test('Включить перевод, выбрать немецкий - успех', async () => {
    await settingsPage.visit();
    await settingsPage.enableTranslations();
    await settingsPage.setLanguage(langs.de);
    await settingsPage.submit();
    expect( await settingsPage.isSuccess() ).toBeTruthy();
  });

  test('Сходить в лукап - перевод tree > Baum', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup(words.en);
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.getTranslation() ).toBe(words.de);
  });

  test('Выбрать русский - успех', async () => {
    await settingsPage.visit();
    await settingsPage.setLanguage(langs.ru);
    await settingsPage.submit();
    expect( await settingsPage.isSuccess() ).toBeTruthy();
  });

  test('Сходить в лукап - перевод tree > дерево', async () => {
    await lookupPage.visit();
    await lookupPage.indexLookup(words.en);
    expect( await lookupPage.isAnyResult() ).toBeTruthy();
    expect( await lookupPage.getTranslation() ).toBe(words.ru);
  });
});