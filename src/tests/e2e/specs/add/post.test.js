import Page from '../../builder';
import AddPage from '../../pages/AddPage';
import CardsPage from '../../pages/CardsPage';
import SettingsPage from '../../pages/SettingsPage';

describe('Add card', () => {
  let page;
  let addPage;
  let cardsPage;
  let settingsPage;

  const card01 = {
    front: 'benevolent',
    definition: 'well meaning and kindly',
    example: 'a benevolent smile',
  }

  const card02 = {
    front: 'tedious',
    back: 'утомительный',
    definition: 'too long, slow, or dull; tiresome or monotonous',
    example: 'a tedious journey',
  }

  const card03 = {
    front: 'tacit',
    back: 'негласный',
    example: 'understood or implied without being stated',
  }

  const card04 = {
    front: 'tout',
    definition: 'attempt to sell (something), typically by a direct or persistent approach',
    example: 'touting wares',
  }

  beforeAll(async () => {
    page = await Page.build('iPhone 6');
    addPage = new AddPage(page);
    cardsPage = new CardsPage(page);
    settingsPage = new SettingsPage(page);
  });

  afterAll(async () => {
    await page.close();
  })

  // транслейшен отключен
  test('Отключить перевод — успех', async () => {
    await settingsPage.visit();
    await settingsPage.disableTranslations();
    await settingsPage.submit();
    expect( await settingsPage.isSuccess() ).toBeTruthy();
  });
  // создаю карточку 1 front + definition + example
  test('Создаю карточку номер 1 —- front + definition + example', async () => {
    await addPage.visit();
    await addPage.fillForm( card01 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });

  // первый блок проверки валидации, для отключенного перевода
  test('Перевод отключен, сабмит пустой формы — фронт и деф подсветились', async () => {
    await addPage.visit();
    await addPage.submit();
    expect( await addPage.isFrontValid() ).toBeFalsy();
    expect( await addPage.isDefinitionValid() ).toBeFalsy();
  });
  // транслейшен включен, русский
  test('Включить перевод, выбрать русский - успех', async () => {
    await settingsPage.visit();
    await settingsPage.enableTranslations();
    await settingsPage.setLanguage('Russian');
    await settingsPage.submit();
    expect( await settingsPage.isSuccess() ).toBeTruthy();
  });
  // второй блок проверки валидации, для включенного перевода
  // пока оставил все в одном тесте, возможно так неудобно и стоит разнести
  test('Перевод включен, проверка всех вариантов валидации', async () => {
    // 2 перевод есть
    // 21 сабмит пустой формы, убедиться что фронт/бэк/дэф подсветились
    // 22 ввести фронт, убедиться что подсветка с фронта пропала
    // 23 ввести бэк, убедиться что подсветка пропала с бэк и дэф
    // 24 убрать бэк, убедиться что подсветка на бэк и дэф
    // 25 ввести дэф, убедиться что подсветка с бэк и дэф ушла.
    await addPage.visit();
    await addPage.submit();
    expect( await addPage.isFrontValid() ).toBeFalsy();
    expect( await addPage.isBackValid() ).toBeFalsy();
    expect( await addPage.isDefinitionValid() ).toBeFalsy();
    await addPage.fillFront('something');
    expect( await addPage.isFrontValid() ).toBeTruthy();
    await addPage.fillBack('something');
    expect( await addPage.isBackValid() ).toBeTruthy();
    expect( await addPage.isDefinitionValid() ).toBeTruthy();
    await addPage.clearBack();
    expect( await addPage.isBackValid() ).toBeFalsy();
    expect( await addPage.isDefinitionValid() ).toBeFalsy();
    await addPage.fillDefinition('something');
    expect( await addPage.isBackValid() ).toBeTruthy();
    expect( await addPage.isDefinitionValid() ).toBeTruthy();
  });
  // создаю карточку 2 front + back + definition + example
  test('Создаю карточку номер 2 —- front + back + definition + example', async () => {
    await addPage.visit();
    await addPage.fillForm( card02 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  // создаю карточку 3 front + back + example
  test('Создаю карточку номер 3 —- front + back + example', async () => {
    await addPage.visit();
    await addPage.fillForm( card03 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  // создаю карточку 4 front + definition + example
  test('Создаю карточку номер 4 —- front + definition + example', async () => {
    await addPage.visit();
    await addPage.fillForm( card04 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  // есть и работает ли кнопка "добавить еще"
  test('Работает ли кнопка "добавить ещё"', async () => {
    // предыдущий тест создает карточку, данный продолжает с финального шага
    await addPage.addOneMore();
    expect( await addPage.isFormEmpty() ).toBeTruthy();
  });

  // попытка добавить дубликат, карточка 1 - front/def
  test('Попытка добавить дубликат - карточка 1, front/def', async () => {
    await addPage.fillForm( card01 );
    await addPage.submit();
    expect( await addPage.isDuplicate() ).toBeTruthy();
  });

  // попытка добавить дубликат, карточка 3 - front/back
  test('Попытка добавить дубликат - карточка 3, front/back', async () => {
    await addPage.fillForm( card03 );
    await addPage.submit();
    expect( await addPage.isDuplicate() ).toBeTruthy();
  });

  // уборка
  // удаляю карточку 1
  test('Уборка - удаляю карточку 1', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card01.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
  // удаляю карточку 2
  test('Уборка - удаляю карточку 2', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card02.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
  // удаляю карточку 3
  test('Уборка - удаляю карточку 3', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card03.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
  // удаляю карточку 4
  test('Уборка - удаляю карточку 4', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card04.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
});