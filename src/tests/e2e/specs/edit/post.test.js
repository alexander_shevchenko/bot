import Page from '../../builder';
import EditPage from '../../pages/EditPage';
import AddPage from '../../pages/AddPage';
import CardsPage from '../../pages/CardsPage';
import SettingsPage from '../../pages/SettingsPage';

describe('Edit custom card', () => {
  let page;
  let editPage;
  let addPage;
  let cardsPage;
  let settingsPage;

  const card01 = {
    front: 'tediousnessity',
    back: 'утомительный',
    definition: 'too long, slow, or dull; tiresome or monotonous',
    example: 'a tedious journey',
  }

  const card02 = {
    front: 'tacwerwerwert',
    back: 'негласный',
    example: 'understood or implied without being stated',
  }

  const card03 = {
    front: 'tacwerwerwert',
    definition: 'well meaning and kindly',
    example: 'a benevolent smile',
  }

  beforeAll(async () => {
    page = await Page.build('iPhone 6');
    editPage = new EditPage(page);
    addPage = new AddPage(page);
    cardsPage = new CardsPage(page);
    settingsPage = new SettingsPage(page);
  });

  afterAll(async () => {
    await page.close();
  })

  // транслейшен включен, русский
  test('Включить перевод, выбрать русский - успех', async () => {
    await settingsPage.visit();
    await settingsPage.enableTranslations();
    await settingsPage.setLanguage('Russian');
    await settingsPage.submit();
    expect( await settingsPage.isSuccess() ).toBeTruthy();
  });
  // создаю карточку 1
  test('Создаю карточку номер 1 —- front + back + definition + example', async () => {
    await addPage.visit();
    await addPage.fillForm( card01 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  // открыть карточку 1 в редактировании -- все 4 поля визибл
  test('Открыть карточку номер 1 —- front + back + definition + example visible', async () => {
    await cardsPage.visit();
    await cardsPage.findOpenCard(card01.front);
    expect( await editPage.isFrontVisible() ).toBeTruthy();
    expect( await editPage.isBackVisible() ).toBeTruthy();
    expect( await editPage.isDefinitionVisible() ).toBeTruthy();
    expect( await editPage.isExampleVisible() ).toBeTruthy();
  });
  
  // валидация
  test('Открыть карточку номер 1 —- валидация', async () => {
    await cardsPage.visit();
    await cardsPage.findOpenCard(card01.front);
    await editPage.clearFront();
    expect( await editPage.isFrontValid() ).toBeFalsy();
    // удалить поле фронт -- подсветка на фронт
    await editPage.clearFront();
    expect( await editPage.isFrontValid() ).toBeFalsy();
    // удалить поля транслейшен и дефинишн -- подсветка на обоих полях
    await editPage.clearBack();
    await editPage.clearDefinition();
    expect( await editPage.isBackValid() ).toBeFalsy();
    expect( await editPage.isDefinitionValid() ).toBeFalsy();
    // ввести что-либо в транслейшен -- подсветка уходит с обоих полей
    await editPage.fillBack(card01.back);
    expect( await editPage.isBackValid() ).toBeTruthy();
    expect( await editPage.isDefinitionValid() ).toBeTruthy();
    // убрать текст из транслейшена, ввести в деф -- подсветка уходит с обоих полей
    await editPage.clearBack();
    await editPage.fillDefinition(card01.definition);
    expect( await editPage.isBackValid() ).toBeTruthy();
    expect( await editPage.isDefinitionValid() ).toBeTruthy();
    // убрать значение из фронта/бэка/определения, сабмит -- подсветка на всех трех полях
    await editPage.clearDefinition();
    await editPage.submit();
    expect( await editPage.isFrontValid() ).toBeFalsy();
    expect( await editPage.isBackValid() ).toBeFalsy();
    expect( await editPage.isDefinitionValid() ).toBeFalsy();
  });
  // уборка 
  test('Уборка - удаляю карточку 1', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card01.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
  // БЭКЕНД
  // проверка на уникальность фронт/дэф
  // сделать 2 карточки, первая фронт/бэк, вторая фронт/дэф
  // сделать первую карточку идентичной второй и попробовать сохранить -- дубликат
  // создать кастомную карточку 2 (фронт (идентичный 3) + бэк)
  test('Создаю карточку 2 —- front + back', async () => {
    await addPage.visit();
    await addPage.fillForm( card02 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  test('Создаю карточку 3 —- front + def', async () => {
    await addPage.visit();
    await addPage.fillForm( card03 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  test('Открыть карточку 2 —- проверить дубликат front/def', async () => {
    await cardsPage.visit();
    await cardsPage.findOpenCard(card02.front);
    await editPage.clearBack();
    await editPage.fillDefinition(card03.definition);
    await editPage.submit();
    expect ( await editPage.isDuplicate() ).toBeTruthy();
  });
  test('Уборка - удаляю карточку 2', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card02.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
  test('Уборка - удаляю карточку 3', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card03.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
  // проверка на уникальность фронт/бэк
  // сделать 2 карточки в обратном порядке и тогда первой откроется (у меня 
  // нет механизма открытия второй идентичной карточки) 
  // уже последняя карточка
  // и опять же сделать последнюю идентичной первой и -- дубликат
  test('Создаю карточку 3 —- front + def', async () => {
    await addPage.visit();
    await addPage.fillForm( card03 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  test('Создаю карточку 2 —- front + back', async () => {
    await addPage.visit();
    await addPage.fillForm( card02 );
    await addPage.submit();
    expect( await addPage.isSuccess() ).toBeTruthy();
  });
  test('Открыть карточку 3 —- проверить дубликат front/back', async () => {
    await cardsPage.visit();
    await cardsPage.findOpenCard(card03.front);
    await editPage.clearDefinition();
    await editPage.fillBack(card02.back);
    await editPage.submit();
    expect ( await editPage.isDuplicate() ).toBeTruthy();
  });
  test('Уборка - удаляю карточку 2', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card02.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
  test('Уборка - удаляю карточку 3', async () => {
    await cardsPage.visit();
    await cardsPage.findDeleteCard( card03.front );
    expect( await cardsPage.isDeleteSuccessfull() ).toBeTruthy();
  });
});