import { paths } from '../../../consts/paths';

export default class SettingsPage {
  constructor(page) {
    this.page = page;
  }

  async visit() {
    await this.page.goto(`http://localhost:3000${paths.settings}`);
  }

  async submit() {
    await this.page.waitAndClick('[data-testid="submit"]');
  }

  async isSuccess() {
    return await this.page.isElementVisible('[data-testid="success"]');
  }

  async checkCheckbox() {
    return await this.page.getCheckboxState('[data-testid="enableTranslationCheckbox"]')
  }

  async disableTranslations() {
    const isChecked = await this.page.getCheckboxState('[data-testid="enableTranslationCheckbox"]');
    if (isChecked) {
      await this.page.waitAndClick('[data-testid="enableTranslationLabel"]');
    };
  }

  async enableTranslations() {
    const isChecked = await this.page.getCheckboxState('[data-testid="enableTranslationCheckbox"]');
    if (!isChecked) {
      await this.page.waitAndClick('[data-testid="enableTranslationLabel"]');
    };
  }

  async setLanguage(title) {
    await this.page.waitAndClick('[data-testid="targetLanguageSelect"]');
    await this.page.waitAndClickXPath(`//li[contains(text(), "${title}")]`);
  }
}