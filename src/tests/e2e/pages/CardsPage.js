import { paths } from '../../../consts/paths';

export default class CardsPage {
  constructor(page) {
    this.page = page;
  }

  async visit() {
    await this.page.goto(`http://localhost:3000${paths.cards}`);
  }

  async navigate(letter) {
    await this.page.waitAndClick(`[data-testid="${letter}"]`);
  }

  async isCardShown(word) {
    return await this.page.isElementVisible(`[data-testid="${word}"]`);
  }

  async findDeleteCard(word) {
    const letter = word.charAt(0).toUpperCase();
    await this.page.waitAndClick(`[data-testid="${letter}"]`);
    await this.page.waitAndClick(`[data-testid="${word}"]`);
    await this.page.waitAndClick('[data-testid="deleteSubmit"]');
    await this.page.waitAndClick('[data-testid="confirmSubmit"]');
  }

  // если карточки с одинаковым фронтом -- то беда будет
  async findOpenCard(word) {
    const letter = word.charAt(0).toUpperCase();
    await this.page.waitAndClick(`[data-testid="${letter}"]`);
    await this.page.waitAndClick(`[data-testid="${word}"]`);
  }

  async isDeleteSuccessfull() {
    return await this.page.isElementVisible('[data-testid="success"]');
  }
}