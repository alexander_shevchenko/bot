import { paths } from '../../../consts/paths';

export default class LookupPage {
  constructor(page) {
    this.page = page;
  }

  async visit() {
    await this.page.goto(`http://localhost:3000${paths.addCard}`);
  }

  async indexLookup(query) {
    await this.page.waitAndType('[data-testid="indexSearchInput"]', query);
    await this.page.waitAndClick('[data-testid="indexSearchSubmit"]');
  }

  async isAnyResult() {
    return await this.page.isElementVisible('[data-testid="lookupResults"]');
  }

  async isSuggestions() {
    return await this.page.isElementVisible('[data-testid="suggestions"]');
  }

  async isNothingFound() {
    return await this.page.isElementVisible('[data-testid="nothingFound"]');
  }

  async isAnyTranslationFetched() {
    return await this.page.isElementVisible('[data-testid="lookupTranslations"]');
  }

  async getTranslation() {
    return await this.page.getText('[data-testid="lookupTranslations"]');
  }

  async isAnySynonym() {
    return await this.page.isElementVisible('[data-testid="synonyms"]');
  }

  async isAnyAntonym() {
    return await this.page.isElementVisible('[data-testid="antonyms"]');
  }

  async isAnyDerivative() {
    return await this.page.isElementVisible('[data-testid="linkToDerivative"]');
  }

  async pickPartDefExample() {
    await this.page.waitAndClick('[data-testid="pickPartSpeechButton"]');
    await this.page.waitAndClick('[data-testid="pickDefinitionButton"]');
    await this.page.waitAndClick('[data-testid="pickExampleButton"]');
  }

  async pickThesaurus() {
    await this.page.waitAndClick('[data-testid="pickSynonym"]');
    await this.page.waitAndClick('[data-testid="pickAntonym"]');
    await this.page.waitAndClick('[data-testid="thesaurusSubmit"]');
  }

  async isAnyTranslation() {
    return await this.page.isElementVisible('[data-testid="translations"]');
  }

  async pickTranslation() {
    await this.page.waitAndClick('[data-testid="pickTranslation"]');
    await this.page.waitAndClick('[data-testid="translationSubmit"]');
  }

  async isCardIncludesEverything() {
    let is = true;
    if ( await this.page.isElementVisible('[data-testid="finalStep-front"]') === false ) is = false;
    if ( await this.page.isElementVisible('[data-testid="finalStep-part"]') === false ) is = false;
    if ( await this.page.isElementVisible('[data-testid="finalStep-audio"]') === false ) is = false;
    if ( await this.page.isElementVisible('[data-testid="finalStep-definition"]') === false ) is = false;
    if ( await this.page.isElementVisible('[data-testid="finalStep-example"]') === false ) is = false;
    if ( await this.page.isElementVisible('[data-testid="finalStep-synonym"]') === false ) is = false;
    if ( await this.page.isElementVisible('[data-testid="finalStep-antonym"]') === false ) is = false;
    if ( await this.page.isElementVisible('[data-testid="finalStep-translation"]') === false ) is = false;
    await setTimeout(() => {}, 2000);
    return is;
  }

  async submitCard() {
    await this.page.waitAndClick('[data-testid="finaStep-submit"]');
  }

  async isSubmitSuccessfull() {
    return await this.page.isElementVisible('[data-testid="finalStep-success"]');
  }

  async isSubmitDuplicate() {
    return await this.page.isElementVisible('[data-testid="finalStep-duplicate"]');
  }
}