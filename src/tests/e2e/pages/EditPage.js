import { paths } from '../../../consts/paths';

export default class AddPage {
  constructor(page) {
    this.page = page;
  }

  async submit() {
    await this.page.waitAndClick('[data-testid="submit"]');
  }

  async isSuccess() {
    return await this.page.isElementVisible('[data-testid="success"]');
  }

  async isDuplicate() {
    return await this.page.isElementVisible('[data-testid="duplicate"]');
  }

  async isFrontVisible() {
    return await this.page.isElementVisible('[data-testid="frontField"]');
  }

  async isBackVisible() {
    return await this.page.isElementVisible('[data-testid="backField"]');
  }
  
  async isDefinitionVisible() {
    return await this.page.isElementVisible('[data-testid="definitionField"]');
  }

  async isExampleVisible() {
    return await this.page.isElementVisible('[data-testid="exampleField"]');
  }

  async isFrontValid() {
    const hasClass = await this.page.isElementHasClass('[data-testid="frontParent"]', 'form-control__invalid');
    return !hasClass;
  }

  async isBackValid() {
    const hasClass = await this.page.isElementHasClass('[data-testid="backParent"]', 'form-control__invalid');
    return !hasClass;
  }

  async isDefinitionValid() {
    const hasClass = await this.page.isElementHasClass('[data-testid="definitionParent"]', 'form-control__invalid');
    return !hasClass;
  }

  async fillFront(text) {
    await this.page.waitAndType('[data-testid="frontField"]', text);
  }

  async fillBack(text) {
    await this.page.waitAndType('[data-testid="backField"]', text);
  }

  async fillDefinition(text) {
    await this.page.waitAndType('[data-testid="definitionField"]', text);
  }

  async clearFront() {
    return await this.page.clearInputValue('[data-testid="frontField"]');
  }

  async clearBack() {
    return await this.page.clearInputValue('[data-testid="backField"]');
  }

  async clearDefinition() {
    return await this.page.clearInputValue('[data-testid="definitionField"]');
  }
}