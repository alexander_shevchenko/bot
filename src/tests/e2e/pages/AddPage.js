import { paths } from '../../../consts/paths';

export default class AddPage {
  constructor(page) {
    this.page = page;
  }

  async visit() {
    await this.page.goto(`http://localhost:3000${paths.addCustomCard}`);
  }

  async fillForm(card) {
    await this.page.waitAndType('[data-testid="frontField"]', card.front);
    if (card.back) {
      await this.page.waitAndType('[data-testid="backField"]', card.back);
    }
    if (card.definition) {
      await this.page.waitAndType('[data-testid="definitionField"]', card.definition);
    }
    await this.page.waitAndType('[data-testid="exampleField"]', card.example);
  }

  async submit() {
    await this.page.waitAndClick('[data-testid="submit"]');
  }

  async addOneMore() {
    await this.page.waitAndClick('[data-testid="addOneMore"]');
  }

  async isSuccess() {
    return await this.page.isElementVisible('[data-testid="success"]');
  }

  async isDuplicate() {
    return await this.page.isElementVisible('[data-testid="duplicate"]');
  }

  async isFrontValid() {
    const hasClass = await this.page.isElementHasClass('[data-testid="frontParent"]', 'form-control__invalid');
    return !hasClass;
  }

  async isBackValid() {
    const hasClass = await this.page.isElementHasClass('[data-testid="backParent"]', 'form-control__invalid');
    return !hasClass;
  }

  async isDefinitionValid() {
    const hasClass = await this.page.isElementHasClass('[data-testid="definitionParent"]', 'form-control__invalid');
    return !hasClass;
  }

  async fillFront(text) {
    await this.page.waitAndType('[data-testid="frontField"]', text);
  }

  async fillBack(text) {
    await this.page.waitAndType('[data-testid="backField"]', text);
  }

  async fillDefinition(text) {
    await this.page.waitAndType('[data-testid="definitionField"]', text);
  }

  async clearBack() {
    return await this.page.clearInputValue('[data-testid="backField"]');
  }

  async isFormEmpty() {
    let empty = true;
    // поскольку back это поле которые зависят от пользовательских настроек
    // и оно не всегда есть, делаю проверку
    const isBackField = await this.page.isElementVisible('[data-testid="backField"]')
    
    if ( await this.page.getInputValue('[data-testid="frontField"]') !== '' ) empty = false;
    if (isBackField) {
      if ( await this.page.getInputValue('[data-testid="backField"]') !== '' ) empty = false;
    }
    if ( await this.page.getInputValue('[data-testid="definitionField"]') !== '' ) empty = false;
    if ( await this.page.getInputValue('[data-testid="exampleField"]') !== '' ) empty = false;
    return empty;
  }
}