import puppeteer from 'puppeteer';

export default class Builder {
  static async build(viewport) {
    const launchOptions = {
      headless: true,
      slowMo: 0,
      args: [
        '--no-sandbox',
        '--disable-setui-sandbox',
        '--disable-web-security',
      ]
    };

    const browser = await puppeteer.launch(launchOptions);
    const page = await browser.newPage();
    const extendedPage = new Builder(page);
    await page.setDefaultTimeout(60000);

    switch (viewport) {
      // https://github.com/puppeteer/puppeteer/blob/master/docs/api.md#puppeteerdevices
      // https://github.com/puppeteer/puppeteer/blob/master/src/DeviceDescriptors.ts
      // есть landscape сетапы
      case 'iPhone SE':
        const seViewport = puppeteer.devices['iPhone SE'];
        await page.emulate(seViewport);
        break;
      case 'iPhone 6':
        const iphone6Viewport = puppeteer.devices['iPhone 6'];
        await page.emulate(iphone6Viewport);
        break;
      default: 
        throw new Error('Supported devices — iPhone SE / iPhone 6');
    }

    return new Proxy(extendedPage, {
      get: function(_target, property) {
        return extendedPage[property] || browser[property] || page[property];
      }
    });
  }

  constructor(page) {
    this.page = page;
  }

  async waitAndClickXPath(xpath) {
    const element = await this.page.waitForXPath(xpath);
    await element.click();
  }

  async waitAndClick(selector) {
    await this.page.waitForSelector(selector);
    await this.page.click(selector);
  }

  async waitAndType(selector, text) {
    await this.page.waitForSelector(selector);
    await this.page.type(selector, text);
  }

  async getText(selector) {
    await this.page.waitForSelector(selector);
    const text = await this.page.$eval(selector, e => e.innerHTML);
    return text;
  }

  async getInputValue(selector) {
    await this.page.waitForSelector(selector);
    const value = await this.page.$eval(selector, e => e.value);
    return value;
  }

  async clearInputValue(selector) {
    await this.page.waitForSelector(selector);
    await this.page.click(selector);
    const inputValue = await this.page.$eval(selector, el => el.value);
    for (let i = 0; i < inputValue.length; i++) {
      await this.page.keyboard.press('Backspace');
    }
  }

  async getCheckboxState(selector) {
    await this.page.waitForSelector(selector);
    const checkbox = await this.page.$(selector);
    return await (await checkbox.getProperty('checked')).jsonValue();
  }

  async getCount(selector) {
    await this.page.waitForSelector(selector);
    const count = await this.page.$$eval(selector, items => items.length);
    return count;
  }

  async isElementVisible(selector) {
    let visible = true;
    await this.page.waitForSelector(selector, { visible: true, timeout: 15000 }).catch(() => {
      visible = false;
    });
    return visible;
  }

  async isElementHasClass(selector, klass) {
    const el = await this.page.waitForSelector(selector);
    const classes = await el.getProperty('className');
    const classesString = await classes.evaluate(data => data);
    return classesString.includes(klass);
  }
}