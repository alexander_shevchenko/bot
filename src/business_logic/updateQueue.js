import {textAnswers} from '../actions/consts';
import {pluralize_en} from '../helpers/pluralize';
const cloneDeep = require('lodash/cloneDeep')

// Формирование новой очереди
//
// Входыне параметры:
//		oldqueue - исходная очередь
//		grade - оценка первой карточки в очереди
// 
// Выходные параметры:
//		объект со свойствами:
//		queue - измененная очередь
//		card - карточка с новыми параметрами
//
const updateQueue = (oldqueue, grade = null) => {

	// сделаю копию массива объектов, чтобы не менять входную информацию
	let queue = cloneDeep(oldqueue);

	// чтобы не выбрал пользователь - удалим карточку с первого места очереди 
	let card1 = queue.shift();

	// создадим поле для нового лога (не хочу смешивать со старым, так проще вносить в базу)
	// card1.newlogs = new Array();

	// проверим, если у нас попалась карточка inactive - то сменим ей статус на new
	if( card1.status === "inactive") {
		// проверка на наличие newlogs
		if ( !card1.newlogs ) card1.newlogs = [];
		
		card1.status = "new";
		card1.newlogs.push( { "string": "new", "timestamp": Date.now() } );
	}

	return calcCard( card1, grade, queue);
}

// Функция расчета параметров карточки в случае ее оценки.
// Используется для двух случаев: 
// 1. формирования новой очереди и вноса информации в базу, в случае если карточку действительно оценили
// 2. оценки времени следующего показа карточки если будет выбран тот или иной ответ на карточку
//
// Входные данные:
//		card - объект карточка 
//		grade - ответ на карточку
//		queue - очередь, если очередь не задана то, ее не изменяем (это значит что функция используется для случая 2)
//
// Выходные данные:
//		card1 - копия исходной карточки с рассчитанными параметрами
//		queue1 - копия исходной очереди с изменением в ней, которое вызвал ответ grade
//
// Алгоритм работы
//		Входящие объекты card и queue клонируеются, дабы не портить исходники
//		Если queue не задана, то очередь не изменяется
//		case "inactive" используется только для случая 2
//		Подробно алгоритм оценки времени следующего показа карточки и изменения ее статуса расписан тут
//		https://www.draw.io/?state=%7B%22ids%22:%5B%221VPGHGPr_v9yaTJpW6zbeYpu9bnmjZKwJ%22%5D,%22action%22:%22open%22,%22userId%22:%22117483809825009545294%22%7D#G1VPGHGPr_v9yaTJpW6zbeYpu9bnmjZKwJ
//		Подробный алгоритм расчета Ef фактора расписан тут
//		https://docs.google.com/document/d/1zXOWixHcIaDN5f7DD4u29epCcrXTP6nwDWyfoX0RNpw/edit
function calcCard ( card, grade, queue ) {

	let card1 = cloneDeep( card );
	let queue1 = cloneDeep( queue );

	// проверка на наличие newlogs
	if ( !card1.newlogs ) card1.newlogs = [];

	switch ( card1.status ) {
		// карточка ни разу не изучалась
		// данный case не используется для расчета реальных параметров карточки, только для предварительной оценки времени следующего показа
		case "inactive":
			if ( grade === textAnswers.again ) {
				
				card1.again = 1 ;
				card1.periodText = "~1 min";
				if( queue ) queue1.splice( 1, 0, card1 );
				card1.newlogs.push( { "string": "again", "timestamp": Date.now() } );
			}
			
			if ( grade === textAnswers.good ) {

				card1.good = 1;
				card1.periodText = "~1 min";
				card1.newlogs.push( { "string": "good", "timestamp": Date.now() } );
				if( queue ) queue1.push( card1 );
			}

			if ( grade === textAnswers.easy ) {
				
				card1.status = "review";
				card1.period = 4;
				card1.periodText = "4 days";
				card1.dueDate = dueDate( card1.period );
				card1.newlogs.push( { "string": "easy", "timestamp": Date.now() } );
				card1.newlogs.push( { "string": "review", "timestamp": Date.now() } );

				// запомним, что сегодня тренировали new карточку, это необходимо в случае повторной тренировки
				// и для подсчета счетчика статистики
				card1.todayNewCard = true;
			}
		break;

		// обработка карточки, которую изучают впервые
		case "new": 

			// пока не использую - если человек прервал тренировку, то пусть начинает сначала? 
			// если не существует card1.good, то поищем в логе, вдруг ответ good уже когда то был дан, например, с предыдущей боршенной треннировки
			card1.good = card1.good === undefined ? Object.values(card1.logs).filter( element => element.string === "good" ).length : card1.good;
			card1.again = card1.again === undefined ? Object.values(card1.logs).filter( element => element.string === "again" ).length : card1.again;
			
			// карточка уходит в review E -> R
			if( grade === textAnswers.easy ) {

				card1.status = "review";
				card1.period = 4;
				card1.periodText = "4 days";
				card1.dueDate = dueDate( card1.period );
				card1.newlogs.push( { "string": "easy", "timestamp": Date.now() } );
				card1.newlogs.push( { "string": "review", "timestamp": Date.now() } );

				// запомним, что сегодня тренировали new карточку, это необходимо в случае повторной тренировки
				// и для подсчета счетчика статистики
				card1.todayNewCard = true;
			} 

			// карточка уходит в review G+G -> R, A+A+G -> R
			if( ( grade === textAnswers.good && card1.good > 0 ) || ( grade === textAnswers.good && card1.again > 1 ) ) {

				card1.status = "review";
				card1.period = 1;
				card1.periodText = "1 day";
				card1.dueDate = dueDate( card1.period );
				card1.newlogs.push( { "string": "good", "timestamp": Date.now() } );
				card1.newlogs.push( { "string": "review", "timestamp": Date.now() } );

				// запомним, что сегодня тренировали new карточку, это необходимо для подсчета количества новых карточек, которые изучали сегодня, дабы не выйти за лимит
				// и для подсчета счетчика статистики
				card1.todayNewCard = true;
			}

			// первый раз ответили good - карточка в конец очереди
			if( card1.status === "new" && grade === textAnswers.good ) {

				card1.good = 1;
				card1.periodText = "~1 min";
				card1.newlogs.push( { "string": "good", "timestamp": Date.now() } );
				if( queue ) queue1.push( card1 );
			}

			// ответили again - 
			if( card1.status === "new" && grade === textAnswers.again ) {

				card1.newlogs.push( { "string": "again", "timestamp": Date.now() } );
				switch (card1.again) {

					// первый раз выбрали again - смещаем карточку на 1 позицию в очереди
					case 0:

						card1.again = 1 ;
						card1.periodText = "~1 min";
						if( queue ) queue1.splice( 1, 0, card1 );
					break;

					// второй раз выбрали again - смещаем карточку на 3 позиции в очереди
					case 1:

						card1.again = 2 ;
						card1.periodText = "~1 min";
						if( queue ) queue1.splice( 3, 0, card1 );
					break;

					// again выбрали более двух раз - смещаем карточку в конец очереди
					default:

						card1.periodText = "~5 min";
						if( queue ) queue1.push( card1 );
					break;

				}
			}
		break;

		// обработка карточки, которую повторяют из предыдущих уроков
		case "review":
			
			// ответ easy - увеличить Ef и удалить карточку из очереди
			if( grade === textAnswers.easy ) {

				card1.period = ( card1.period + delay(card1.dueDate) ) * card1.Ef * 1.3 / 100;
				card1.periodText = `${pluralize_en(Math.floor(card1.period), 'day')}`
				card1.dueDate = dueDate( card1.period + fuzz( card1.period ) );
				card1.Ef = ( card1.Ef + 15 > 250 ) ? 250 : card1.Ef + 15 ;
				card1.newlogs.push( { "string": "easy", "timestamp": Date.now() } );

				// зафикисируем, что карточка изучена, это необходимо для подсчета счетчика статистики
				card1.todayTotlaCard = true;
			}

			// ответ good - удалить карточку из очереди
			if( grade === textAnswers.good ) {

				card1.period  = ( card1.period + delay(card1.dueDate)/2 ) * card1.Ef / 100;
				card1.periodText = `${pluralize_en(Math.floor(card1.period), 'day')}`
				card1.dueDate = dueDate( card1.period + fuzz( card1.period ) );
				card1.newlogs.push( { "string": "good", "timestamp": Date.now() } );

				// зафикисируем, что карточка изучена, это необходимо для подсчета счетчика статистики
				card1.todayTotlaCard = true;
			}

			// ответ hard - понизить Ef и удалить карточку из очереди 
			if( grade === textAnswers.hard ) {

				card1.period  = ( card1.period + delay(card1.dueDate)/4 ) * 1.2;
				card1.periodText = `${pluralize_en(Math.floor(card1.period), 'day')}`
				card1.dueDate = dueDate( card1.period + fuzz( card1.period ) );
				card1.Ef = ( card1.Ef - 15 < 130 ) ? 130 : card1.Ef - 15 ;
				card1.newlogs.push( { "string": "hard", "timestamp": Date.now() } );

				// зафикисируем, что карточка изучена, это необходимо для подсчета счетчика статистики
				card1.todayTotlaCard = true;
			}

			// ответ again - понизить Ef, сменить статус на relearning, поместить в конец очереди
			if( grade === textAnswers.again ) {
				
				card1.Ef = ( card1.Ef - 20 < 130 ) ? 130 : card1.Ef - 20 ;
				card1.status = "relearning";
				card1.periodText = "~5 min";
				card1.newlogs.push( { "string": "again", "timestamp": Date.now() } );
				card1.newlogs.push( { "string": "relearning", "timestamp": Date.now() } );
				if( queue ) queue1.push( card1 );
			}
		break;

		// обработка карточки, которую повторяют в рамках текущего урока
		case "relearning":

			// ответ again - карточку в конец очереди
			if( grade === textAnswers.again ) {

				card1.periodText = "~5 min";
				card1.newlogs.push( { "string": "again", "timestamp": Date.now() } );
				if( queue ) queue1.push( card1 );
			}

			// ответ hard - карточка уходит из очереди
			if( grade === textAnswers.hard ) {

				card1.status = "review";
				card1.periodText = "1 day";
				card1.period = 1;
				card1.dueDate = dueDate( card1.period );
				card1.newlogs.push( { "string": "hard", "timestamp": Date.now() } );
				card1.newlogs.push( { "string": "review", "timestamp": Date.now() } );

				// зафикисируем, что карточка изучена, это необходимо для подсчета счетчика статистики
				card1.todayTotlaCard = true;
			}
		break;

		// на всякий случай
		default:
			throw new Error ( "Unexpected situatin in caclCrad." );
		break;

	}

	//console.log(card1)

	return { card: card1, queue: queue1 }
}

// вычисление dueDate - это день в числовом формате yymmdd, когда нужно показать карточку
// next - смещение в днях, через сколько стоит показать опять карточку к изучению
const dueDate = ( next ) => {

	// текущее время на сервере 
	let date = new Date();

	// выполним смещение на нужно число дней
	date.setDate(date.getDate() + next);

	// переведем дату в нужный формат yymmdd и вернем ее 
    const dd = (date.getDate() < 10 ) ? `0${date.getDate()}` : date.getDate();
    const mm = (date.getMonth()+1 < 10 ) ? `0${date.getMonth()+1}` : date.getMonth()+1;
    const yy = date.getFullYear() % 100;

	return 1*`${yy}${mm}${dd}`;
}

// вычисление delay - вернет число дней прошедших со времени day2
// day2 это день в числовом формате yymmdd
const delay = ( day2 ) => {

	const day1 = dueDate( 0 );
	const yyyy1 = Math.floor( day1/10000 );
	const mm1 = Math.floor( ( day1 - yyyy1*10000 ) / 100 ) - 1;
    const dd1 = Math.floor( ( day1 - yyyy1*10000 - (mm1+1)*100 ) );

	const yyyy2 = Math.floor( day2/10000 );
	const mm2 = Math.floor( ( day2 - yyyy2*10000 ) / 100 ) - 1;
    const dd2 = Math.floor( ( day2 - yyyy2*10000 - (mm2+1)*100 ) );

	const difference = new Date( yyyy1, mm1, dd1) - new Date( yyyy2, mm2, dd2);

	return difference/24/60/60/1000;
}

export { updateQueue, calcCard };


// рандомный коэффициент, который определит смещение показа карточки в днях
// необходимо, чтобы карточки, добавленные в одно время, постепенно перемешимвались с другими карточками
//
// входные параметры: 
//					next - через сколько стоит показать опять карточку к изучению
// выходные параметры:
//					рандомное смещение в днях 
//
// логика работы: (исходник тут https://github.com/ankitects/anki/blob/master/pylib/anki/schedv2.py)
//			next == 1    - смещение 0
//					2    - смещение либо 0 либо 1
//					3-6  - смещение от -1 до 1
//					7-29 - смещение минимум от -2 до 2 либо до -next*15% до next*15%, смотря что больше
//					30+  - смещение от ..-4 до 4..
//				
const fuzz = ( next ) => {
	
	if ( next == 1 ) return 0;

	if ( next == 2 ) return Math.round( Math.random() );

	if ( next < 7 ) return Math.round( 2 * Math.random() - 1 );

	let k = Math.max( 4, Math.round( next * .05 ) )
	
	if ( next < 30 ) k = Math.max( 2, Math.round( next * .15 ) );
	
	return Math.round( 2*k * Math.random() - k );
}