// Функция создает очередь карточек.
// Входные параметры:
// 		queue - "сырая очередь", объект, содержащий результат запросов из БД 
// Выходные параметры:
//		res - готовая к работе очередь, массив объектов, выстраенных по правилу L + R + ...

const createQueue = (queue = {}) => {
	
	// сформировать массив из объекта
	let arr = new Array();
	for (let key in queue) {
		arr.push(queue[key]);
	}

	// сортируем по правилу Review + New + ...
	let res = new Array();
	const len = arr.length;
	// повторим эти операции столько раз, сколько элементов в массиве, поскольку крайний случай - это все элементы review или new
	for (let i = 0; i < len; i++) {

		// первым ищем элементы со статусом review, его заносим в новый массив, а в старом удаляем найденный элемент
		arr.find( (item, indx) => {
			if ( item.status === "review" ) {
				res.push( item );
				arr.splice( indx, 1 );
				return 1; 
			}
		});

		// аналогично поступаем с эелементами у которых статус new, relearning и inactive
		arr.find( (item, indx) => {
			if ( item.status === "new" || item.status === "relearning" || item.status === "inactive" ) {
				res.push( item );
				arr.splice( indx, 1 );
				return 1; 
			}
		});
	}
	return res;
	
}
export default createQueue;