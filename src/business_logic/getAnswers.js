/**
 * Метод получает статус карточки, возвращает массив из 
 * ключей, по которым затем можно получить тексты кнопок 
 * из {textAnswers} from '../actions/consts';
 * @param {String} status статус карточки
 * @return {Array} массив с ключами
 */

const getAnswers = ( status ) => {
	switch ( status ) {
		case "inactive": 
		case "new": 
			return ['again', 'good', 'easy'];
		case "review":
			return ['again', 'hard', 'good', 'easy'];
		case "relearning":
			return ['again', 'hard'];
		default:
			throw new Error ( "Status " + status + " — appears to be invalid" );
	}
}

export default getAnswers;