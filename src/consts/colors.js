/**
 * Цвета для анимаций
 */
export const $gray1 = '#1B2529';
export const $gray2 = '#32454B';
export const $gray3 = '#3E5259';
export const $gray4 = '#52666D';
export const $gray5 = '#61757C';
export const $gray6 = '#7B8E94';
export const $gray7 = '#9AABB1';
export const $gray8 = '#CBD5D9';
export const $gray9 = '#E4E9EB';
export const $gray95 = '#F3F6F7';
export const $gray10 = '#F5F8FA';