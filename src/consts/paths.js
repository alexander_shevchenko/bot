/**
 * Пути к модулям у меня используются и как пути и как условия в разных модулях.
 * Поскольку пути могут в какой-то момент измениться, я выношу их в файлик и буду импортировать там где нужно.
 */

export const paths = {
  study: '/study',
  addCard: '/add',
  addCustomCard: '/addCustom',
  editCard: '/edit',
  cards: '/cards',
  settings: '/settings',
  references: '/references',
  playground: '/playground',
};
