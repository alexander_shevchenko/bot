/**
 * Поддерживаемые языки перевода
 */

 export const targetLanguages = [
  {code: 'fa', title: 'Farsi', status: 'available'},
  {code: 'ka', title: 'Georgian', status: 'available'},
  {code: 'ha', title: 'Hausa', status: 'available'},
  {code: 'hi', title: 'Hindi', status: 'available'},
  {code: 'ru', title: 'Russian', status: 'available'},
  {code: 'es', title: 'Spanish', status: 'available'},
  {code: 'de', title: 'German', status: 'available'},
  {code: 'pt', title: 'Portuguese', status: 'available'},
  {code: 'it', title: 'Italian', status: 'available'},
  {code: 'ms', title: 'Malay', status: 'available'},
  {code: 'id', title: 'Indonesian', status: 'available'},
  {code: 'tn', title: 'Setswana', status: 'available'},
  {code: 'ro', title: 'Romanian', status: 'available'},
  {code: 'el', title: 'Greek', status: 'available'},
  {code: 'tg', title: 'Tajik', status: 'available'},
  {code: 'tk', title: 'Turkmen', status: 'available'},
  {code: 'tpi', title: 'Tok Pisin', status: 'available'},
  {code: 'tt', title: 'Tatar', status: 'available'},
  {code: 'zh', title: 'Chinese', status: 'available'},
  {code: 'ig', title: 'Igbo', status: 'available'},
  {code: 'yo', title: 'Yoruba', status: 'available'},
  {code: 'mr', title: 'Marathi', status: 'available'},
  {code: 'ar', title: 'Arabic', status: 'available'},
  {code: 'nso', title: 'Northern Sotho', status: 'available'},
  {code: 'xh', title: 'isiXhosa', status: 'available'},
  {code: 'zu', title: 'isiZulu', status: 'available'},
  {code: 'other', title: 'Other', status: 'other'},
 ];