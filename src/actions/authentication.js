import { fetchUserAttributes } from './fetchUserAttributes';
import { setInitialSearchQuery } from './setInitialSearchQuery';
import { parseUrl } from '../api/telegram';
import { timezone } from '../helpers/timezone';
import { testTgID } from '../config/telegram';

// У нас есть 2 ветки авторизации, телеграм и фейсбук (от второго мы постепенно уходим)
// + опциональный сценарий, предустановленный search query
//
// все данные приходят в параметрах урла
// разбираю параметры и развожу аутентификацию + устанавливаю query, если есть
//
// 1. есть параметры auth_client=tg и auth_tgid — это телеграм
// форваржу tgid в фетчюзераттрибутс и готово
//
// 2. все остальные варианты — делаю вывод что это фб мобильный или десктопный
// гет сдк, взять psid из sdk и дальше фетчюзераттрибутс с psid

// 1. Авторизацию через фб мы убили.
// 2. Авторизация через тг —
// все данные приходят в параметрах урла
// разбираю параметры + устанавливаю query, если есть
//
// параметры —
// auth_client=tg (стал ненужным с отвязкой от фб)
// auth_tgid
//
// форваржу tgid в фетчюзераттрибутс и готово
//
// 3. Авторизация тестового пользователя
// если продакшн — то идем по стандартной ветке TG
// иначе — фетчу аттрибуты для тестового tgid (импортирую выше testTgID из tg конфига)

export const authentication = () => (dispatch) => {
  // распарсить url
  let params = parseUrl();

  // 1. есть ли search query?
  if (
    Object.keys(params).length &&
    Object.keys(params).includes('search_wordid')
  ) {
    dispatch(setInitialSearchQuery({ wordid: params.search_wordid }));
  }

  // 2. если мы не в продакшене
  // то — добавляю требуемые аттрибуты ручками
  if (process.env.NODE_ENV !== 'production') {
    params.auth_client = 'tg';
    params.auth_tgid = testTgID;
  }

  // 3. в итоге развожу аутентификацию
  if (
    Object.keys(params).length &&
    Object.keys(params).includes('auth_client') &&
    Object.keys(params).includes('auth_tgid') &&
    params.auth_client === 'tg'
  ) {
    // стандартный TG
    authTG(dispatch, params);
  } else {
    console.log('Authentication failed. Missing expected parameters.');
  }
};

function authTG(dispatch, params) {
  // у тг нет сдк, tgid пришел в урле
  // я делаю фетч аттрибутов и отдаю туда айди и пользовательскую таймзону
  dispatch(
    fetchUserAttributes({
      tgid: params.auth_tgid,
      timezone: timezone(),
    })
  );
}
