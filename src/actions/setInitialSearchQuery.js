/**
 * Сохраняю входящий пользовательский
 * поисковый запрос
 */
export const setInitialSearchQuery = (data) => ({
  type: 'SET_INITIAL_SEARCH_QUERY',
  wordid: data.wordid,
});
