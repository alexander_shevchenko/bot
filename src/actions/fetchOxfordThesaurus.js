import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Экшен, запрашивает данные о слове из Thesaurus Oxford API
 * 
 * @param {Object} data полезная нагрузка экшена
 * @param {String} data.word id слова
 * @param {Array}  data.senseIds опционально, айди определений в тезаурусе, !!!данное поле не отдается на сервер!!!, 
 * а передается дальше в экшен success
 * откуда его берет парсер и с его помощью достает требуемое определение 
 * 
 * в ответ могут прийти три типа ответов
 * 1. успешный ответ
 * 2. исключение с кодом и сообщением (все еще успешный ответ)
 * 3. server error (от нашего бэкенда) 
 * 
 * @success ответ от endpoints
 *    @param {String} statusCode для успешного ответа будет равен 200
 *    @param {Object} results результаты поиска
 * 
 * @success исключение
 *    @param {String} statusCode код исключения
 *    @param {String} message ответ
 * 
 *    подкейсы исключений для Entries (развожу исходя из statusCode)
 *    @successcase 404 - слово не найдено (не найдено в entries и в search)
 *    @successcase 500 - внутренняя ошибка оксфорда
 *    @successcase 502 - сервис оксфорда недоступен
 *    @successcase 503 - сервис оксфорда перегружен
 *    @successcase 504 - таймаут оксфорда
 *    @successcase 601 - в запросе были нелатинские буквы
 * 
 * Server error
 * @errorcase NNN - все исключения, что не попали в кейсы выше — падают сюда
 * @errorcase любая ошибка на нашем бэкенде, error.message === 'Server Error' ?
 * 
 * Connection Error
 * @errorcase клиентский промис сам сваливается в catch
 
 * @exception если запрашивается пустое слово, data.word === ''
 * то, для экономии ресурсов — экшен сразу возвращает 404
 */

export const fetchOxfordThesaurus = (data) => (dispatch, getState) => {
  dispatch({
    type: 'FETCH_OXFORDTHESAURUS_REQUEST',
    data,
  });

  // смотри @exception выше
  if ( !data.word ) {
    dispatch({
      type: 'FETCH_OXFORDTHESAURUS_FAILURE',
      statusCode: 404,
      message: 'empty request',
    });

    return;
  }
  
  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('fetchOxfordThesaurus');

  callable(data).then(
    response => {
      // ответ от endpoints
      if (response.data.statusCode === 200) {
        dispatch({
          type: 'FETCH_OXFORDTHESAURUS_SUCCESS',
          data: response.data.results,
          senseIds: data.senseIds,
        })
      } else {
        // ответ с исключением
        dispatch({
          type: 'FETCH_OXFORDTHESAURUS_FAILURE',
          statusCode: response.data.statusCode,
          message: response.data.message,
        })
      }
    },
    error => {
      // case server error
      if (error.message && error.message === ioMessages.connection.serverError) {
        dispatch({
          type: 'FETCH_OXFORDTHESAURUS_FAILURE',
          message: ioMessages.connection.serverError,
        });
      } else {
        // case connection error
        dispatch({
          type: 'FETCH_OXFORDTHESAURUS_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
}

/**
 * Экшен, просто обнуляет данные тезауруса в сторе
 * 
 */
export const resetThesaurus = () => {
  return {
    type: 'RESET_OXFORDTHESAURUS',
  }
}