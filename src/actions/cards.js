export const changeChar = (char) => ({
  type: 'CARDS_CHANGE_ACTIVE_CHAR',
  char,
});