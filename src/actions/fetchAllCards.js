import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Запрос списка пользовательских карточек с сервера
 * 
 * @param {String} userId ID пользователя
 * 
 * @successcase успешный фетч
 *    @param {Object} cards объект из карточек
 * @successcase у пользователя нет карточек
 *    @param {String} строка со значением "Empty"
 * 
 * @errorcase Server Error любая ошибка на сервере
 * @errorcase Connection Failed клиентский промис сам сваливается в catch
 */

export const fetchAllCards = (data) => (dispatch) => {
  dispatch({
    type: 'FETCH_ALLCARDS_REQUEST',
    data,
  });
  
  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('fetchAllCards');
  
  // стандартный промис
  callable(data).then(
    response => {
      // case 1 — у пользователя есть карточки
      if (response.data && (typeof response.data) === 'object') {
        dispatch({
          type: 'FETCH_ALLCARDS_SUCCESS',
          cards: response.data,
        });
      }
      
      // case 2 — у пользователя НЕТ карточек
      if (response.data && response.data === 'Empty') {
        dispatch({
          type: 'FETCH_ALLCARDS_EMPTY',
        });
      }
    },
    error => {
      if (error.message && error.message === ioMessages.connection.serverError) {
        // case 4
        dispatch({
          type: 'FETCH_ALLCARDS_FAILURE',
          message: ioMessages.connection.serverError,
        })
      } else {
        // case 5
        dispatch({
          type: 'FETCH_ALLCARDS_FAILURE',
          message: ioMessages.connection.connectionError,
        })
      };
    }
  );
}