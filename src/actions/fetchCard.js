import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Запрос карточки с сервера
 * 
 * @param {Object} data полезная нагрузка экшена
 * @param {String} data.userId ID пользователя
 * @param {String} data.cardId ID требуемого слова
 * 
 * @successcase успешный фетч
 *    @param {Object} объект карточки
 * 
 * @errorcase Server Error любая ошибка на сервере (ПОКА ДАЖЕ ВКЛЮЧАЕТ КЕЙС КОГДА КАРТОЧКИ С ТАКИМ АЙДИ НЕТ)
 * @errorcase Connection Failed клиентский промис сам сваливается в catch
 */

export const fetchCard = (data) => (dispatch) => {
  dispatch({
    type: 'FETCH_CARD_REQUEST',
    data,
  });
  
  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('fetchCard');
  
  // стандартный промис
  callable(data).then(
    response => {
      // case 1
      if (response.data && (typeof response.data) === 'object') {
        dispatch({
          type: 'FETCH_CARD_SUCCESS',
          card: response.data,
        });
      }
    },
    error => {
      if (error.message && error.message === ioMessages.connection.serverError) {
        // case 4
        dispatch({
          type: 'FETCH_CARD_FAILURE',
          message: ioMessages.connection.serverError,
        })
      } else {
        // case 5
        dispatch({
          type: 'FETCH_CARD_FAILURE',
          message: ioMessages.connection.connectionError,
        })
      };
    }
  );
}