import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

// #region Запрос карточек с сервера
// ------------------------------------------------------------
// отдаю 
//    data.userId (id пользователя)
//    data.currentDayStart (timestamp 4-х утра текущего дня на клиенте)
// ------------------------------------------------------------
// ------------------------------------------------------------
// ожидаю в ответ
//    case 1. объект вида
//            {cards: {...}, stats: {...}}
//    case 2. string 'Done' (если пользователь сегодня уже 
//            оттренировался и новых карточек не добавлял)
//            !!UI показывает стандартный DONE!!
//    case 3. string 'Empty' (если на сегодня в принципе
//            ничего нет и не было)
//            !!UI показывает Nothing for Today!!
// ------------------------------------------------------------
// ошибки
//    case 4. error из промиса (любая ошибка на сервере),
//            error.message = 'Server Error'?
//            !!UI показывает 'Server Error'!!
//    case 5. клиентский промис сам сваливается в catch
//            (любая ошибка на клиенте, например нет соединения)
//            !!UI показывает Connection Failed!!
// ------------------------------------------------------------
// #endregion

// ????????????????????????????????????????????????????????????
// вопрос такой, как все эти кейсы тестировать
// может быть сделать так, что на определенный объект data с моей
// стороны сервер возвращает определенный ответ?
// то есть будет пара исключительных кейсов на которые сервер
// вернет done, empty или выбросит ошибку?
// ????????????????????????????????????????????????????????????

export const fetchCards = (data) => (dispatch) => {
  dispatch({
    type: 'FETCH_CARDS_REQUEST',
    data,
  });
  
  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('fetchCards');
  
  // стандартный промис
  callable(data).then(
    response => {
      // case 1
      if (response.data && (typeof response.data) === 'object') {
        dispatch({
          type: 'FETCH_CARDS_SUCCESS',
          cards: response.data.cards,
          stats: response.data.stat,
        });
      }
      
      // case 2
      if (response.data && response.data === 'Done') {
        dispatch({
          type: 'FETCH_CARDS_DONE_FOR_TODAY',
        });
      }
      
      // case 3
      if (response.data && response.data === 'Empty') {
        dispatch({
          type: 'FETCH_CARDS_EMPTY_FOR_TODAY',
        });
      }
    },
    error => {
      if (error.message && error.message === ioMessages.connection.serverError) {
        // case 4
        dispatch({
          type: 'FETCH_CARDS_FAILURE',
          message: ioMessages.connection.serverError,
        })
      } else {
        // case 5
        dispatch({
          type: 'FETCH_CARDS_FAILURE',
          message: ioMessages.connection.connectionError,
        })
      };
    }
  );
}