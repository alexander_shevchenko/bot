import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Экшен, отправляет обновленные настройки карточки
 * пока фактически заточен под одну настройку — defFirst
 *
 * @param {String} userId айди пользователя
 * @param {String} cardId айди карточки
 * @param {Boolean} defFirst дефинишн ферст
 *
 * @success успешный резолв
 *    @param {Object} data объект с айди карточки и свойством деф ферст?
 *
 * @errorcase любая ошибка на сервере, error.message === 'Server Error'
 * @errorcase клиентский промис сам сваливается в catch
 */

export const postCardPreferences = (data) => (dispatch, getState) => {
  dispatch({
    type: 'POST_CARDPREFERENCES_REQUEST',
    data,
  });

  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('postCardPreferences');

  // стандартный промис
  // если всё здорово — то экшен саксесс
  // если всё плохо - то экшен failure

  // TODO: UI пока никак не реагирует на саксесс или ошибку
  callable(data).then(
    (response) => {
      dispatch({
        type: 'POST_CARDPREFERENCES_SUCCESS',
        data: response,
      });
    },
    (error) => {
      // case server error
      if (
        error.message &&
        error.message === ioMessages.connection.serverError
      ) {
        dispatch({
          type: 'POST_CARDPREFERENCES_FAILURE',
          message: ioMessages.connection.serverError,
        });
      } else {
        // case connection error
        dispatch({
          type: 'POST_CARDPREFERENCES_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
};
