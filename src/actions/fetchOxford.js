import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Экшен, запрашивает данные о слове из Entries и Translations
 * если в entries пусто, то запрос в Search
 * 
 * @param {Object} data полезная нагрузка экшена
 * @param {String} data.word id слова
 * @param {Boolean} data.translationEnabled флаг, нужен ли перевод
 * @param {String} data.targetLanguage язык на который необходим перевод (пока по умолчанию ru)
 * 
 * @success ответ 200 (успех в entries и translations)
 *    @param {String} endpoint идентификатор сервиса давшего ответ — entries/search/translations
 *    @param {String} statusCode для успешного ответа будет равен 200
 *    @param {Object} entries результаты поиска в entries
 *    @param {Object} translations результаты поиска в translations
 *    @param {Object} search результаты поиска в search
 * 
 * @success исключение
 *    @param {String} statusCode код исключения
 *    @param {String} message ответ
 * 
 *    подкейсы исключений для Entries
 *    @successcase 200 + entries — кейс когда не нужен перевод (я отдаю стасу translationEnabled false или targetLanguage = other)
 *    @successcase 404 + translations — есть entries, нет translations
 *    @successcase 200 + search — нет entries, есть search
 *    @successcase 404 + search — нет entries, нет search
 *    @successcase 500 - внутренняя ошибка оксфорда
 *    @successcase 502 - сервис оксфорда недоступен
 *    @successcase 503 - сервис оксфорда перегружен
 *    @successcase 504 - таймаут оксфорда
 *    @successcase 601 - в запросе были нелатинские буквы
 * 
 * Server error
 * @errorcase NNN - все исключения, что не попали в кейсы выше — падают сюда
 * @errorcase любая ошибка на нашем бэкенде, error.message === 'Server Error' ?
 * 
 * Connection Error
 * @errorcase клиентский промис сам сваливается в catch
 
 * @exception если запрашивается пустое слово, data.word === '' (что возможно при запросе тезауруса) 
 * то, для экономии ресурсов — экшен сразу возвращает 404
 */

export const fetchOxford = (data) => (dispatch, getState) => {
  dispatch({
    type: 'FETCH_OXFORD_REQUEST',
    data,
  });

  // смотри @exception выше
  if (!data.word) {
    dispatch({
      type: 'FETCH_OXFORD_FAILURE',
      statusCode: 404,
      message: 'empty request',
    });

    return;
  }

  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('fetchOxfordAll');

  callable(data).then(
    (response) => {
      console.log(response);

      if (
        response.data.statusCode === 200 &&
        response.data.endpoint === 'entries'
      ) {
        // 0 - экстра-кейс, перевод не нужен, 200 в entries
        dispatch({
          type: 'FETCH_OXFORDENTRIES_SUCCESS',
          data: response.data,
        });
      } else if (
        response.data.statusCode === 200 &&
        response.data.endpoint === 'translations'
      ) {
        // 1 - успех в entries и translations
        dispatch({
          type: 'FETCH_OXFORDALL_SUCCESS',
          data: response.data,
        });
      } else if (
        response.data.statusCode === 404 &&
        response.data.endpoint === 'translations'
      ) {
        // 2 - успех в entries и 404 в translations
        dispatch({
          type: 'FETCH_OXFORDENTRIES_SUCCESS',
          data: response.data,
        });
      } else if (
        response.data.statusCode === 200 &&
        response.data.endpoint === 'search'
      ) {
        // 3 - 404 в entries и успех в search
        dispatch({
          type: 'FETCH_OXFORDSEARCH_SUCCESS',
          data: response.data,
        });
      } else if (response.data.statusCode && response.data.message) {
        // 4 - 404 в entries и search
        // 5 - 601
        // 6 - 5NN
        dispatch({
          type: 'FETCH_OXFORD_FAILURE',
          statusCode: response.data.statusCode,
          message: response.data.message,
        });
      } else {
        // 7 - тут одни server error по идее должны остаться
        dispatch({
          type: 'FETCH_OXFORD_FAILURE',
          statusCode: response.data.statusCode,
          message: response.data.message,
        });
      }
    },
    (error) => {
      // case connection error
      dispatch({
        type: 'FETCH_OXFORD_FAILURE',
        message: ioMessages.connection.connectionError,
      });
    }
  );
};
