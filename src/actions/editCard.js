import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

// TODO: убрать из документации поля, которые пользователь не может редактировать

/**
 * Экшен, редактирует карточку
 * 2 типа карточек, кастомные и оксфорд, валидация для обоих типов отличается
 * тип карточки передается в поле type
 
 * *** CUSTOM КАРТОЧКА ***
 * @param {String} data.front слово
 * @param {String} data.back перевод
 * @param {String} data.definition определение
 * @param {String} data.example пример - опционально
 * 
 * *** OXFORD КАРТОЧКА ***
 * @param {String} data.defId айди определения - обязательно
 * @param {String} data.example определение - опционально
 * @param {String} data.synonym синоним - опционально
 * @param {String} data.antonym антоним - опционально
 * @param {String} data.back перевод - опционально
 * @param {String} data.hint подсказка - опционально
 *  
 * *** + ОБА ТИПА ***
 * @param {String} data.cardId айди карточки
 * @param {String} data.type тип карточки, custom / oxford
 * @param {String} data.userId ID пользователя
 * 
 * @success успешный резолв, слово изменено
 *    @param {Object} измененная карточка целиком
 * 
 * ошибки для оксфорд-карточки
 * @errorcase дубликат — такой defId у данного пользователя уже существует (исключая саму изменяемую карточку)
 * @errorcase валидация - не все обязательные поля присутствуют
 * 
 * ошибки для кастомной карточки
 * @errorcase дубликат — такое сочетание front/back или front/def у данного пользователя уже существуют (исключая саму изменяемую карточку)
 * @errorcase валидация - не все обязательные поля присутствуют
 * 
 * общие ошибки
 * @errorcase валидация — тип карточки не oxford || custom
 * @errorcase любая другая ошибка на сервере, error.type === 'Server Error', error.message = {сообщение}
 * @errorcase клиентский промис сам сваливается в catch
 */

export const editCard = (data) => (dispatch, getState) => {
  dispatch({
    type: 'POST_EDITWORD_REQUEST',
    data,
  });

  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('postUpdateCard');

  // стандартный промис
  // если всё здорово — то экшен саксесс
  // если всё плохо - то экшен failure
  callable(data).then(
    response => {
      // case 1
      // кроме самой карточки, я возвращаю на саксесс и 
      // часть стора с карточками, чтобы cards > list редюсер
      // смог обновить текущие карточки в сторе
      dispatch({
        type: 'POST_EDITWORD_SUCCESS',
        response: response.data,
      });
    },
    error => {
      // validation error
      if (error.message && error.message === ioMessages.validation.validationError) {
        dispatch({
          type: 'POST_EDITWORD_FAILURE',
          message: ioMessages.validation.validationError,
        });  
      } else if (error.message && error.message === ioMessages.validation.duplicate) {
        // duplicate
        dispatch({
          type: 'POST_EDITWORD_FAILURE',
          message: ioMessages.validation.duplicate,
        });  
      } else if (error.message && error.message === ioMessages.connection.serverError) {
        // server error
        dispatch({
          type: 'POST_EDITWORD_FAILURE',
          message: ioMessages.connection.serverError,
        });
      } else {
        dispatch({
          // connection error
          type: 'POST_EDITWORD_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
}