import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Экшен, создает карточку слова
 * 
 * у нас два типа карточек:
 * — кастомные (это по сути то, что есть сейчас, пользователь сам создает произвольную карточку — слово/перевод/пример);
 * — Oxford-карточки
 * + оба типа карточек будут иметь общие технические поля — userId/timestamp
 * 
 * в итоге 
 * *** CUSTOM КАРТОЧКА ***
 * @param {String} data.front слово
 * @param {String} data.back перевод (перевод и определение условно опциональны, одно из двух полей должно быть)
 * @param {String} data.definition определение (перевод и определение условно опциональны, одно из двух полей должно быть)
 * @param {String} data.example пример - опционально
 * 
 * *** OXFORD КАРТОЧКА ***
 * @param {String} data.front слово
 * @param {String} data.wordId айди слова (может отличаться от самого слова, 
 * используется во всех сервисах оксфорда как основной идентификатор)
 * @param {String} data.part часть речи
 * @param {String} data.definition определение
 * @param {String} data.defId айди определения
 * @param {String} data.example определение - опционально
 * @param {String} data.transcription транскрипция - опционально
 * @param {String} data.audioUrl ссылка на произношение - опционально
 * @param {Array}  data.domains массив из доменов и регистров - опционально
 * @param {Array}  data.synonyms массив всех синонимов подходящих к определению - опционально
 * @param {String} data.synonym выбранный синоним - опционально
 * @param {Array}  data.antonyms массив всех антонимов подходящих к определению - опционально
 * @param {String} data.antonym выбранный антоним - опционально
 * @param {String} data.back перевод - опционально
 * @param {String} data.hint подсказка - опционально
 *  
 * *** + ОБА ТИПА ***
 * @param {String} data.type тип карточки, custom / oxford
 * @param {String} data.userId ID пользователя
 * 
 * @success успешный резолв, карточка создана
 *    @param {String} idCard айди созданной карточки
 * 
 * ошибки для оксфорд-карточки
 * @errorcase дубликат — такой defId у данного пользователя уже существует
 * @errorcase валидация - не все обязательные поля присутствуют
 * 
 * ошибки для кастомной карточки
 * @errorcase дубликат — такое сочетание front/back или front/def у данного пользователя уже существуют
 * @errorcase валидация - не все обязательные поля присутствуют
 * 
 * общие ошибки
 * @errorcase любая другая ошибка на сервере, error.type === 'Server Error', error.message = {сообщение}
 * @errorcase клиентский промис сам сваливается в catch
 */

export const addCard = (data) => (dispatch, getState) => {
  dispatch({
    type: 'POST_ADDWORD_REQUEST',
    data,
  });

  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('postAddCard');

  // стандартный промис
  // если всё здорово — то экшен саксесс
  // если всё плохо - то экшен failure
  callable(data).then(
    response => {
      // case 1
      dispatch({
        type: 'POST_ADDWORD_SUCCESS',
        response,
      });
    },
    error => {
      if (error.message && error.message === ioMessages.validation.validationError) {
        // validation error
        dispatch({
          type: 'POST_ADDWORD_FAILURE',
          message: ioMessages.validation.validationError,
        })
      } else if (error.message && error.message === ioMessages.validation.duplicate) {
        // duplicate
        dispatch({
          type: 'POST_ADDWORD_FAILURE',
          message: ioMessages.validation.duplicate,
        })
      } else if (error.message && error.message === ioMessages.connection.serverError) {
        // server error
        dispatch({
          type: 'POST_ADDWORD_FAILURE',
          message: ioMessages.connection.serverError,
        })
      } else {
        // connection error
        dispatch({
          type: 'POST_ADDWORD_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
}