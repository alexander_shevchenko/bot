import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Обновление пользовательских настроек
 *
 * @param {String} userId пользовательский айди
 * @param {Boolean} translationEnabled включены ли переводы в принципе
 * @param {String} targetLanguage язык перевода
 * @param {Boolean} autoPlayEnabled авто-проигрыш в тренировке
 *
 * @success
 *    @param {String} userId пользовательский id
 *    @param {Boolean} translationEnabled обновленный флаг
 *    @param {String} targetLanguage обновленный язык
 *
 * @errorcase Server Error любая ошибка на сервере
 * @errorcase Connection Failed клиентский промис сам сваливается в catch
 */
export const postUserAttributes = (data) => (dispatch) => {
  dispatch({
    type: 'POST_USER_ATTRIBUTES_REQUEST',
    data,
  });

  const callable = functions.httpsCallable('postUserAttributes');

  // стандартный промис
  return callable(data).then(
    (response) => {
      // case 1
      dispatch({
        type: 'POST_USER_ATTRIBUTES_SUCCESS',
        data: response.data,
      });
    },
    (error) => {
      if (
        error.message &&
        error.message === ioMessages.connection.serverError
      ) {
        // case 2
        dispatch({
          type: 'POST_USER_ATTRIBUTES_FAILURE',
          message: ioMessages.connection.serverError,
        });
      } else if (
        error.message &&
        error.message === ioMessages.validation.validationError
      ) {
        // validation error
        dispatch({
          type: 'POST_USER_ATTRIBUTES_FAILURE',
          message: ioMessages.validation.validationError,
        });
      } else {
        // case 3
        dispatch({
          type: 'POST_USER_ATTRIBUTES_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
};
