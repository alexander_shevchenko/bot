// TODO: переименовать в io messages
// дополнить всеми недостающими кейсами
// использовать как дикчионари для overlayActions
// и заодно можно будет использовать в оверлее

// Error messages
export const errorMessages = {
  server: 'Server Error',
  messenger: 'Messenger Error',
  connection: 'Connection Failed',
  duplicate: 'Duplicate Found',
  validation: 'Validation Error',
};

// Возможные ответы на карточки (при изменении необходимо синхронизировать с таким же объектом на сервере)
export const textAnswers = {
  again: 'Fail',
  hard: 'Hard',
  good: 'Good',
  easy: 'Easy',
};

/*
export const textAnswers = {
  again: 'Fail',
  hard: 'Hard',
  good: 'Good',
  easy: 'Easy',
};
*/

// IO messages
export const ioMessages = {
  messenger: {
    down: 'Messenger Error',
    downSecondary: 'Something went wrong. Try again later.',
  },
  oxford: {
    apiDown: 'Oxford API Down',
    apiDownSecondary: 'Oxford Dictionary API is down. Try again later.',
    addedPrimary: 'Done',
    addedSecondary: 'The card has been added',
  },
  custom: {
    addedPrimary: 'Done',
    addedSecondary: 'The card has been added',
  },
  connection: {
    serverError: 'Server Error',
    serverErrorSecondary: 'Something went wrong. Try again later.',
    connectionError: 'Connection Failed',
    connectionErrorSecondary: 'Something went wrong. Try again later.',
    messengerError: 'Messenger Error',
    sdkFailure: 'Something went wrong. Try again later.',
  },
  validation: {
    duplicate: 'Duplicate Found',
    duplicateWordSecondary: 'The card you’ve just edited duplicates an already existing card.',
    validationError: 'Validation Error',
    validationErrorSecondary: 'All fields are required. Please check the card and submit again.',
    validationErrorOxfordSecondary: 'Something went wrong. Try again.',
    validationErrorUserSettings: 'Something went wrong. Try again.',
  },
  study: {
    alreadyDone: 'Done for Today',
    alreadyDoneSecondary: 'Come back tomorrow!',
    empty: 'Nothing for Today',
    emptySecondary: 'Seems like you don’t have anything scheduled.',
    done: 'Done for Today',
    doneSecondary: 'Way to go! Come back tomorrow!',
  },
  // FIXME: вот тут потенциальная жопка, поскольку ключи разные, а значение одинаковое
  // а я в overrlayActions значение использую в качестве ключа
  // в итоге если например в одном и том же кейсе будут оба экшена (update и add)
  // то в overlayAction всегда будет улетать первый из них
  // то есть я не смогу сделать два разных экшена на главной кнопке в этом случае
  // есть правда мнение, что такой кейс может никогда не настать
  // но есть и мнение, что если он настанет, я фиг найду эту ошибку
  update: {
    updated: 'Updated',
    updatedSecondary: 'The word has been updated',
    added: 'Done',
    deleted: 'Deleted',
    deletedSecondary: 'The word has been deleted',
    settings: 'Done',
    settingsSecondary: 'Updated successfully',
  },
};