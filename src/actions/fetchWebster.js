import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Экшен, запрашивает данные о слове из Merriam Webster словаря
 * 
 * @param {Object} data полезная нагрузка экшена
 * @param {String} data.word слово
 * @param {String} data.dictionary типа словаря (collegiate или learners)
 * 
 * @success успешный резолв, словарь обновлен
 *    @param {Object} data ответ от API
 * 
 * @errorcase любая ошибка на сервере, error.message === 'Server Error'
 * @errorcase клиентский промис сам сваливается в catch
 */

export const fetchWebster = (data) => (dispatch, getState) => {
  dispatch({
    type: 'FETCH_WEBSTER_REQUEST',
    data,
  });
  
  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('postMerriamWebster');

  // стандартный промис
  // если всё здорово — то экшен саксесс
  // если всё плохо - то экшен failure

  // TODO: UI пока никак не реагирует на саксесс или ошибку
  callable(data).then(
    response => {
      dispatch({
        type: 'FETCH_WEBSTER_SUCCESS',
        data: response,
      });
    },
    error => {
      // case server error
      if (error.message && error.message === ioMessages.connection.serverError) {
        dispatch({
          type: 'FETCH_WEBSTER_FAILURE',
          message: ioMessages.connection.serverError,
        });
      } else {
        // case connection error
        dispatch({
          type: 'FETCH_WEBSTER_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
}