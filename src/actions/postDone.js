import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

// #region Пост уведомления о конце тренировки на сервер
// ------------------------------------------------------------
// отдаю 
//    userId (id пользователя)
//    timestamp (клиентский timestamp)
//    length - сколько было карточек пройдено
// ------------------------------------------------------------
// ------------------------------------------------------------
// ожидаю в ответ на успех объект с флагом первой тренировки
//    case 1. {isFirstDoneEver: true/false}
// ------------------------------------------------------------
// ошибки
//    case 2. error из промиса (любая ошибка на сервере),
//            error.message = 'Server Error'?
//            TODO: !!UI показывает 'Server Error'!!
//    case 3. клиентский промис сам сваливается в catch
//            (любая ошибка на клиенте, например нет соединения)
//            !!UI показывает Connection Failed!!
// ------------------------------------------------------------
// #endregion
export const postDone = () => (dispatch, getState) => {
  const data = {
    userId: getState().user.id,
    timestamp: Date.now(),
    length: getState().study.queue.rawTotal,
  };

  // выбросить экшен с обновленной очередью
  dispatch({
    type: 'POST_DONE_REQUEST',
    ...data,
  });
  
  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('postDone');

  // стандартный промис
  return callable({
    ...data,
  }).then(
    response => {
      dispatch({
        type: 'POST_DONE_SUCCESS',
        response: response,
      });
    },
    error => {
      if (error.message && error.message === ioMessages.connection.serverError) {
        dispatch({
          type: 'POST_DONE_FAILURE',
          message: ioMessages.connection.serverError,
        })  
        
      } else {
        dispatch({
          type: 'POST_DONE_FAILURE',
          message: ioMessages.connection.connectionError,
        })  
      };
    }
  );
}