import { updateQueue } from '../business_logic/updateQueue';
import { postGrade } from './postGrade';
import { postDone } from './postDone';

// TODO: свести все три экшена в один файл?
export const gradeCard = (grade) => (dispatch) => {
  // получить обновленные очередь и карточку (на базе текущей очереди и оценки)
  const { queue, card } = updateQueue(grade.queue, grade.grade);
  
  // если очередь не закончилась (это не последняя карточка в тренировке) — я иду по стандартной ветке
  // оценка отправляется асинхронно в фоне, стэйт обновляется новой очередью, 
  // компонент Study показывает следующую карточку
  // саксесс или ошибка приходят в фоне, я на них никак не реагирую
  if (queue.length) {
    return dispatch( postGrade(grade, queue, card, 'POST_GRADE', grade.progress ) );
  } else {
    // если новая очередь пуста, значит тренировка закончилась — 
    // я выбрасываю альтернативный экшен реквеста оценки (он запускает isFetching)
    // отправляю оценку, жду ответ
    // получаю
    // отправляю postDone
    // жду ответ
    // получаю — сбрасываю isFetching, Study показывает Done
    
    return dispatch( postGrade(grade, queue, card, 'POST_LASTGRADE'), grade.progress ).then(() => {
      return dispatch( postDone() );
    })
  }
};