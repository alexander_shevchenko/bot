import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Запрос универсального пользовательского айди и пользовательских настроек
 *
 * @param {Object}
 *    @param {String} tgid
 *    @param {Number} timezone
 *
 * @success
 *    @param {String} userId пользовательский id
 *    @param {Boolean} translationEnabled включены ли переводы
 *    @param {String} targetLanguage на какой язык
 *
 * @errorcase Server Error любая ошибка на сервере
 * @errorcase Connection Failed клиентский промис сам сваливается в catch
 */
export const fetchUserAttributes = (data) => (dispatch) => {
  dispatch({
    type: 'FETCH_USER_ATTRIBUTES_REQUEST',
    data,
  });

  const callable = functions.httpsCallable('fetchUserAttributes');

  // стандартный промис
  return callable(data).then(
    (response) => {
      // case 1
      dispatch({
        type: 'FETCH_USER_ATTRIBUTES_SUCCESS',
        data: response.data,
      });
    },
    (error) => {
      if (
        error.message &&
        error.message === ioMessages.connection.serverError
      ) {
        // case 2
        dispatch({
          type: 'FETCH_USER_ATTRIBUTES_FAILURE',
          message: ioMessages.connection.serverError,
        });
      } else {
        // case 3
        dispatch({
          type: 'FETCH_USER_ATTRIBUTES_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
};
