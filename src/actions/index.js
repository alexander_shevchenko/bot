export const resetFailure = () => ({
  type: 'FAILURE_RESET',
});

export const resetGeneral = () => ({
  type: 'RESET',
});