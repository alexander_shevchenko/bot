import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

export const postGrade = (grade, queue, card, actionPrefix, progress) => (dispatch) => {
  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('postGrade');

  dispatch({
    type: actionPrefix + '_REQUEST',
    ...grade,
    nextQueue: queue,
    progress,
  });

  // стандартный промис, отправить оценку на сервер
  return callable({
    userId: grade.userId,
    card,
    timestamp: grade.timestamp,
  }).then(
    response => {
      dispatch({
        type: actionPrefix + '_SUCCESS',
        response: response,
      });
    },
    error => {
      let message;
      
      if (error.message && error.message === ioMessages.connection.serverError) {
        message = ioMessages.connection.serverError
      } else {
        message = ioMessages.connection.connectionError
      };

      dispatch({
        type: actionPrefix +  + '_FAILURE',
        message,
      })
    }
  );
}