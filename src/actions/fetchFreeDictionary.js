import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Экшен, запрашивает данные о слове из Entries и Translations
 * если в entries пусто, то запрос в Search
 * 
 * @param {Object} data полезная нагрузка экшена
 * @param {String} data.word id слова
 * @param {Boolean} data.translationEnabled флаг, нужен ли перевод
 * @param {String} data.targetLanguage язык на который необходим перевод (пока по умолчанию ru)
 * 
 * @success ответ 200 (успех в entries и translations)
 *    @param {String} endpoint идентификатор сервиса давшего ответ — entries/search/translations
 *    @param {String} statusCode для успешного ответа будет равен 200
 *    @param {Object} entries результаты поиска в entries
 *    @param {Object} translations результаты поиска в translations
 *    @param {Object} search результаты поиска в search
 * 
 * @success исключение
 *    @param {String} statusCode код исключения
 *    @param {String} message ответ
 * 
 *    подкейсы исключений для Entries
 *    @successcase 200 + entries — кейс когда не нужен перевод (я отдаю стасу translationEnabled false или targetLanguage = other)
 *    @successcase 404 + translations — есть entries, нет translations
 *    @successcase 200 + search — нет entries, есть search
 *    @successcase 404 + search — нет entries, нет search
 *    @successcase 500 - внутренняя ошибка оксфорда
 *    @successcase 502 - сервис оксфорда недоступен
 *    @successcase 503 - сервис оксфорда перегружен
 *    @successcase 504 - таймаут оксфорда
 *    @successcase 601 - в запросе были нелатинские буквы
 * 
 * Server error
 * @errorcase NNN - все исключения, что не попали в кейсы выше — падают сюда
 * @errorcase любая ошибка на нашем бэкенде, error.message === 'Server Error' ?
 * 
 * Connection Error
 * @errorcase клиентский промис сам сваливается в catch
 
 * @exception если запрашивается пустое слово, data.word === '' (что возможно при запросе тезауруса) 
 * то, для экономии ресурсов — экшен сразу возвращает 404
 */

export const fetchFreeDictionary = (data) => (dispatch, getState) => {
  dispatch({
    type: 'FETCH_FREEDICTIONARY_REQUEST',
    data,
  });

  // смотри @exception выше
  if (!data.word) {
    dispatch({
      type: 'FETCH_FREEDICTIONARY_FAILURE',
      statusCode: 404,
      message: 'empty request',
    });

    return;
  }

  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('fetchFreeDictionary');
  console.log(callable);
  callable(data).then(
    (response) => {
      dispatch({
        type: 'FETCH_FREEDICTIONARY_SUCCESS',
        data: response.data,
      });
    },
    (error) => {
      console.log(error);
      // case connection error
      dispatch({
        type: 'FETCH_FREEDICTIONARY_FAILURE',
        message: ioMessages.connection.connectionError,
        error,
      });
    }
  );
};
