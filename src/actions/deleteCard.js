import { functions } from '../config/firebaseConfig';
import { ioMessages } from './consts';

/**
 * Экшен, удаляет слово
 * 
 * @param {Object} data полезная нагрузка экшена
 * @param {String} data.userId ID пользователя
 * @param {String} data.cardId id удаляемого слова
 * 
 * @success успешный резолв, словарь удален
 *    @param {String} айди удаленной карточки
 * 
 * @errorcase любая ошибка на сервере, error.message === 'Server Error'
 * @errorcase клиентский промис сам сваливается в catch
 */

export const deleteCard = (data) => (dispatch, getState) => {
  dispatch({
    type: 'POST_DELETEWORD_REQUEST',
    data,
  });

  // создаю референс на нужную мне callable функцию
  const callable = functions.httpsCallable('postDeleteCard');
  
  // стандартный промис
  // если всё здорово — то экшен саксесс
  // если всё плохо - то экшен failure
  callable(data).then(
    response => {
      // case 1
      dispatch({
        type: 'POST_DELETEWORD_SUCCESS',
        response: response.data,
      });
    },
    error => {
      // server error
      if (error.message && error.message === ioMessages.connection.serverError) {
        dispatch({
          type: 'POST_DELETEWORD_FAILURE',
          message: ioMessages.connection.serverError,
        })
      } else {
        // connection error
        dispatch({
          type: 'POST_DELETEWORD_FAILURE',
          message: ioMessages.connection.connectionError,
        });
      }
    }
  );
}