import { combineReducers } from 'redux';
import { user } from './user';
import { study } from './study';
import { lookup } from './lookup';
import { addCustom } from './addCustom';
import { edit } from './edit';
import { cards } from './cards';

const rootReducer = combineReducers({
  /* уже переработанные редюсеры */
  cards,
  lookup,
  addCustom,
  edit,
  study,
  user,
});

export default rootReducer;