import { combineReducers } from 'redux';
import { sortCardsByAlphabet } from '../helpers/cards/sortCardsByAlphabet';
import { extractChars } from '../helpers/cards/extractChars';
import { findFirstActiveChar } from '../helpers/cards/findFirstActiveChar';
import cloneDeep from 'lodash/cloneDeep';

/**
 * @param {String} isFetching
 * @param {String} isFailure
 * @param {Object} list
 * @param {Array} cards
 */

const isFetching = (state = false, action) => {
  switch (action.type) {
    case 'FETCH_ALLCARDS_REQUEST':
    case 'FETCH_CARD_REQUEST':
      return true;
    default:
      return false;
  }
};

const isFailure = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_ALLCARDS_FAILURE':
      return {
        type: action.message,
        action: 'fetchAllCards',
      };
    case 'FETCH_CARD_FAILURE':
      return {
        type: action.message,
        action: 'fetchCard',
      };
    case 'FAILURE_RESET':
    default:
      return null;
  }
};

const list = (
  state = {
    raw: null,
    list: null,
    chars: null,
    activeChar: null,
    total: 0,
    isFetchDone: false,
  },
  action
) => {
  switch (action.type) {
    case 'CARDS_CHANGE_ACTIVE_CHAR':
      return {
        ...state,
        activeChar: action.char,
      };
    case 'POST_DELETEWORD_SUCCESS':
      // Same as 'POST_EDITWORD_SUCCESS' (see below).
      // 2 cases:
      // 1. no action needed;
      // 2. update raw, regenerate 'list', 'chars', 'activeChar'.

      if (state.total) {
        const raw = cloneDeep(state.raw);

        // remove deleted card (id returned by API)
        delete raw[action.response];

        // regenerate list
        const list = sortCardsByAlphabet(raw);

        // regenerate chars
        const chars = extractChars(list);

        // if activeChar still has cards it remains active,
        // otherwise — regenerate activeChar
        const activeChar = chars.includes(state.activeChar)
          ? state.activeChar
          : findFirstActiveChar(chars);

        return {
          raw,
          list,
          chars,
          activeChar,
          total: Object.keys(raw).length,
        };
      } else {
        return state;
      }

    case 'POST_EDITWORD_SUCCESS':
      // I mirror updates locally (to avoid refetch).
      //
      // There are 2 use cases:
      // 1. The user is studying and chooses to edit a card;
      // 2. The user is in the Cards component and chooses to edit a card.
      //
      // in case 1, there are no stored cards (state.total === 0),
      // hence — no need to update them locally.
      //
      // in case 2, I update the stored raw collection with the returned
      // value from the API, regenerate 'list', 'chars', 'activeChar'
      // and update the state.
      //

      if (state.total) {
        // updated card is returned by API
        const updatedCard = cloneDeep(action.response);
        const raw = cloneDeep(state.raw);

        // update corresponding card in raw data
        raw[updatedCard.id].f = updatedCard.front;
        if (updatedCard.part) {
          raw[updatedCard.id].p = updatedCard.part;
        }

        // regenerate list
        const list = sortCardsByAlphabet(raw);

        // regenerate chars
        const chars = extractChars(list);

        // if activeChar still has cards it remains active,
        // otherwise — regenerate activeChar
        const activeChar = chars.includes(state.activeChar)
          ? state.activeChar
          : findFirstActiveChar(chars);

        return {
          raw,
          list,
          chars,
          activeChar,
          total: Object.keys(raw).length,
        };
      } else {
        return state;
      }

    case 'FETCH_ALLCARDS_EMPTY':
      return {
        ...state,
        isFetchDone: true,
      };
    case 'FETCH_ALLCARDS_SUCCESS':
      const raw = action.cards;
      const list = sortCardsByAlphabet(raw);
      const chars = extractChars(list);
      const activeChar = findFirstActiveChar(chars);

      return {
        raw,
        list,
        chars,
        activeChar,
        total: Object.keys(raw).length,
        isFetchDone: true,
      };
    default:
      return state;
  }
};

// In case the user needs to edit a card,
// we fetch it in Cards component
// and pass it as a parameter with Router state
const card = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_CARD_SUCCESS':
      return action.card;
    default:
      // I don't need this card to be stored,
      // I just want to fetch it and pass to Edit component
      return null;
  }
};

export const cards = combineReducers({
  isFetching,
  isFailure,
  list,
  card,
});
