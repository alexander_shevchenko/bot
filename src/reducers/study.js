import { combineReducers } from 'redux';
import cloneDeep from 'lodash/cloneDeep';
import createQueue from '../business_logic/createQueue';
import { calculateProgress } from '../helpers/study/calculateProgress';

/**
 * Структура хранилища для очередей карточек
 * @param {String} isFetching
 *
 * запрос очереди
 * @param {String} fetchError ошибка запроса карточек
 * @param {String} fetchEmpty в очереди пусто
 * @param {String} fetchAlreadyDone всё оттренировано до нас
 *
 * отправка флага о конце тренировки
 * @param {String} doneError
 * @param {string} doneSuccess
 *
 * @param {Array} activeQueue текущая очередь, если есть
 * @param {Array} rawQueue «сырая» очередь, очередь которую сервер прислал
 * в ответ на запрос на тренировку
 *
 */

const isFetching = (state = false, action) => {
  switch (action.type) {
    case 'FETCH_CARDS_REQUEST':
    case 'POST_LASTGRADE_REQUEST':
    case 'POST_DONE_REQUEST':
      return true;
    default:
      return false;
  }
};

const isFetchingDone = (state = false, action) => {
  switch (action.type) {
    case 'POST_LASTGRADE_REQUEST':
    case 'POST_DONE_REQUEST':
      return true;
    default:
      return false;
  }
};

const fetchError = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_CARDS_FAILURE':
      return action.message;
    default:
      return null;
  }
};

const fetchEmpty = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_CARDS_EMPTY_FOR_TODAY':
      return true;
    default:
      return null;
  }
};

const fetchAlreadyDone = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_CARDS_DONE_FOR_TODAY':
      return true;
    default:
      return null;
  }
};

const doneError = (state = null, action) => {
  switch (action.type) {
    case 'POST_DONE_FAILURE':
      return action.message;
    default:
      return null;
  }
};

const doneSuccess = (state = null, action) => {
  switch (action.type) {
    case 'POST_DONE_SUCCESS':
      return true;
    default:
      return null;
  }
};

const isFirstDoneEver = (state = null, action) => {
  switch (action.type) {
    case 'POST_DONE_SUCCESS':
      if (action.response.data && action.response.data.isFirstDoneEver) {
        return true;
      } else {
        return false;
      }
    default:
      return null;
  }
};

const queue = (
  state = { active: [], rawTotal: null, Lt: 0, Lp: 0, P: 0, A: null },
  action
) => {
  switch (action.type) {
    case 'FETCH_CARDS_SUCCESS':
      const active = createQueue(action.cards);

      // TODO: с calculateProgress сейчас беда, есть куча переменных на этой стороне
      // и куча переменных на стороне метода
      // часть из низ нужно перезаписывать по результатам работы, часть нет
      // ощущаю некоторый хаос, логика плохо читается

      return {
        ...state,
        active,
        rawTotal: active.length,
        ...calculateProgress({
          queueLength: active.length,
          Lt: state.Lt,
          Lp: state.Lp,
          A: state.A,
          isEdit: false,
          isDelete: false,
        }),
      };
    case 'POST_GRADE_REQUEST':
      return {
        ...state,
        active: action.nextQueue,
        ...calculateProgress({
          queueLength: action.nextQueue.length,
          Lt: state.Lt,
          Lp: action.progress.Lp,
          A: action.progress.A,
          isEdit: false,
          isDelete: false,
        }),
      };
    case 'POST_LASTGRADE_REQUEST':
      // на последнюю оценку я обнуляю прогресс
      // возможно нужно обнулять в целом стор
      return {
        ...state,
        // я оставляю последнюю карточку в стэйте на last_grade_request
        // карточку я оставляю, чтобы компонент с карточкой не падал в ошибки из-за отсутствия карточки в стэйте на анмаунт
        // (поскольку анмаунт асинхронный и занимает время, а в сторе карточка уже тю-тю)
        // хочешь поправить этот момент?
        // убери анимацию с карточек и раскомментируй код ниже
        // АПДЕЙТ — АНИМАЦИИ НЕТ И КОД РАСКОМЕНТИРОВАН ( все переменные ниже были закомментированы )
        active: [],
        Lt: 0,
        Lp: 0,
        P: 0,
        A: null,
      };
    case 'POST_EDITWORD_SUCCESS':
      // в данном случае, реакция нужна только если очередь уже есть в стэйте
      // т.е. предположительно, пользователь редактировал карточку перейдя из тренировки
      if (state.rawTotal) {
        const updatedCard = Object.assign({}, action.response);
        const active = cloneDeep(state.active);

        // нахожу карточку по айди в текущей очереди, заменяю её на обновленную карточку
        active[
          active.findIndex((card) => card.id === updatedCard.id)
        ] = updatedCard;

        return {
          ...state,
          active,
          ...calculateProgress({
            queueLength: active.length,
            Lt: state.Lt,
            Lp: state.Lp,
            A: state.A,
            isEdit: true,
            isDelete: false,
          }),
        };
      } else {
        return state;
      }
    case 'POST_DELETEWORD_SUCCESS':
      // и в данном случае, реакция нужна только если очередь уже есть в стэйте
      // т.е. предположительно, пользователь редактировал карточку перейдя из тренировки
      if (state.rawTotal) {
        const activeRaw = cloneDeep(state.active);
        const index = activeRaw.findIndex(
          (card) => card.id === action.response
        );
        const active = [
          ...activeRaw.slice(0, index),
          ...activeRaw.slice(index + 1),
        ];

        return {
          ...state,
          active,
          // уменьшаю исходный тотал, чтобы в postDone ушла аккуратная цифра
          rawTotal: state.rawTotal - 1,
          ...calculateProgress({
            queueLength: active.length,
            Lt: state.Lt,
            Lp: state.Lp,
            A: state.A,
            isEdit: false,
            isDelete: true,
          }),
        };
      } else {
        return state;
      }
    default:
      return state;
  }
};

const stats = (
  state = {
    totalToday: null,
    newToday: null,
    reviewToday: null,
    streak: null,
    bestStreak: null,
    newWeek: null,
    totalWeek: null,
  },
  action
) => {
  switch (action.type) {
    case 'FETCH_CARDS_SUCCESS':
      const totalToday = Object.keys(action.cards).length;
      const newToday = Object.keys(action.cards).filter(
        (el) =>
          action.cards[el].status === 'new' ||
          action.cards[el].status === 'inactive'
      ).length;
      const reviewToday = totalToday - newToday;
      return {
        totalToday,
        newToday,
        reviewToday,
        streak: action.stats.currentStreak,
        bestStreak: action.stats.bestStreak,
        newWeek: action.stats.trainedNew,
        totalWeek: action.stats.trainedTotal,
        allCards: action.stats.totalCards,
      };
    default:
      return state;
  }
};

export const study = combineReducers({
  isFetching,
  isFetchingDone,
  fetchError,
  fetchEmpty,
  fetchAlreadyDone,
  doneError,
  doneSuccess,
  isFirstDoneEver,
  queue,
  stats,
  // active
  // rawTotal
});

/*
// взять объект из карточек, пройтись по ключам,
// скопировать вложенные объекты с помощью Object.assign
// запушить в массив
// вернуть массив
const toArray = (raw) => {
  return Object.keys(raw).map(key => {
    return Object.assign({}, raw[key])
  })
}
*/
