import { combineReducers } from 'redux';

const isFetching = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_USER_ATTRIBUTES_REQUEST':
    case 'POST_USER_ATTRIBUTES_REQUEST':
      return true;
    default:
      return null;
  }
};

const isFailure = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_USER_ATTRIBUTES_FAILURE':
    case 'POST_USER_ATTRIBUTES_FAILURE':
      return action.message;
    case 'FAILURE_RESET':
      return null;
    default:
      return null;
  }
};

const isDone = (state = null, action) => {
  switch (action.type) {
    case 'POST_USER_SETTINGS_SUCCESS':
    case 'POST_USER_ATTRIBUTES_SUCCESS':
      return true;
    default:
      return null;
  }
};

const id = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_USER_ATTRIBUTES_SUCCESS':
      return action.data.userId;
    default:
      return state;
  }
};

const settings = (state = null, action) => {
  switch (action.type) {
    // а не забирать ли мне пользовательские настройки сразу на фетч psid?
    // экономия времени но проблема с лишним траффиком
    // может быть редакс сага и там можно запускать параллельно экшены?
    case 'FETCH_USER_ATTRIBUTES_SUCCESS':
    case 'POST_USER_ATTRIBUTES_SUCCESS':
      return Object.assign({}, action.data);
    default:
      return state;
  }
};

export const user = combineReducers({
  isFetching,
  isFailure,
  isDone,
  id,
  settings,
});
