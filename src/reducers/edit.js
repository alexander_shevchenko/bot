import { combineReducers } from 'redux';

/**
 * Структура хранилища для редактирования карточек
 * @param {String} isFetching
 * @param {String} isFailure
 * @param {String} isDone
 */

const isFetching = (state = false, action) => {
  switch(action.type) {
    case 'POST_EDITWORD_REQUEST':
    case 'POST_DELETEWORD_REQUEST':
      return true;
    default: 
      return false;
  }
}

const isFailure = (state = null, action) => {
  switch(action.type) {
    case 'POST_EDITWORD_FAILURE':
      return {
        type: action.message,
        action: 'edit',
      };
    case 'POST_DELETEWORD_FAILURE':
      return {
        type: action.message,
        action: 'delete',
      };
    case 'FAILURE_RESET':
    default:
      return null;
  }
}

const isDone = (state = null, action) => {
  switch(action.type) {
    case 'POST_EDITWORD_SUCCESS':
      return 'edit';
    case 'POST_DELETEWORD_SUCCESS':
      return 'delete';
    default: 
      return null;
  }
}

export const edit = combineReducers({
  isFetching,
  isFailure,
  isDone,
});