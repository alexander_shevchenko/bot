import { combineReducers } from 'redux';
import {
  parseEntries,
  parseSearch,
  parseThesaurus,
  parseTranslations,
} from '../api/oxford';

//import { parseEntries as parseEntries2 } from '../api/oxfordServer';

/**
 * Структура хранилища для добавления слова из Оксфорда (Entries > Search)
 * @param {String} isFetching
 * @param {String} isFailure
 * @param {Object} lookupData ответ из entries / search
 * @param {Object} thesaurusData ответ из entries / search
 * @param {String} isDone
 *
 */

const isFetching = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_OXFORD_REQUEST':
    case 'FETCH_OXFORDTHESAURUS_REQUEST':
    case 'POST_ADDWORD_REQUEST':
      return true;
    default:
      return null;
  }
};

const isFailure = (state = null, action) => {
  switch (action.type) {
    // сдублировал код для FETCH_OXFORD_FAILURE и FETCH_OXFORDTHESAURUS_FAILURE
    // весь смысл дублирования был в том, что я хочу развести nothing found ошибку,
    // чтобы ui знал что именно сломалось, тезаурус или энтрайс
    // код сдублирован целиком потому, что мне не хочется разбираться в том, какие
    // кейсы нужны, а какие не нужны в обоих случаях — устал я от этого всего
    case 'FETCH_OXFORD_FAILURE':
      // есть статус код
      if (action.statusCode) {
        const code = action.statusCode;
        // 404 и, временно, 601
        if (code === 404 || code === 601) {
          return 'Nothing Found Entries';
        } else if (
          // все 500
          code === 500 ||
          code === 502 ||
          code === 503 ||
          code === 504
        ) {
          return 'API Down';
        } else {
          // все оставшиеся исключения
          return 'API Error';
        }
      } else {
        // серверная ошибка и нет связи
        return action.message;
      }
    case 'FETCH_OXFORDTHESAURUS_FAILURE':
      // есть статус код
      if (action.statusCode) {
        const code = action.statusCode;
        // 404 и, временно, 601
        if (code === 404 || code === 601) {
          return 'Nothing Found Thesaurus';
        } else if (
          // все 500
          code === 500 ||
          code === 502 ||
          code === 503 ||
          code === 504
        ) {
          return 'API Down';
        } else {
          // все оставшиеся исключения
          return 'API Error';
        }
      } else {
        // серверная ошибка и нет связи
        return action.message;
      }
    case 'POST_ADDWORD_FAILURE':
      return action.message;
    case 'FAILURE_RESET':
    default:
      return null;
  }
};

const isDone = (state = null, action) => {
  switch (action.type) {
    case 'POST_ADDWORD_SUCCESS':
      // айди карточки
      return action.response.data;
    default:
      return null;
  }
};

const entriesData = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_OXFORDALL_SUCCESS':
    case 'FETCH_OXFORDENTRIES_SUCCESS':
      //parseEntries2(action.data.entries, action.data.translations);
      return [...parseEntries(action.data.entries.results)];
    case 'FETCH_OXFORD_REQUEST':
    case 'FETCH_OXFORD_FAILURE':
      return null;
    default:
      return state;
  }
};

const translationsData = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_OXFORDALL_SUCCESS':
      return [...parseTranslations(action.data.translations.results)];
    case 'FETCH_OXFORD_REQUEST':
    case 'FETCH_OXFORD_FAILURE':
      return null;
    default:
      return state;
  }
};

const searchData = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_OXFORDSEARCH_SUCCESS':
      return [...parseSearch(action.data.search.results)];
    case 'FETCH_OXFORD_REQUEST':
    case 'FETCH_OXFORD_FAILURE':
      return null;
    default:
      return state;
  }
};

const thesaurusData = (state = null, action) => {
  switch (action.type) {
    case 'FETCH_OXFORDTHESAURUS_SUCCESS':
      return {
        ...parseThesaurus(action.data, action.senseIds),
      };
    case 'FETCH_OXFORDTHESAURUS_REQUEST':
    case 'FETCH_OXFORDTHESAURUS_FAILURE':
    case 'RESET_OXFORDTHESAURUS':
      return null;
    default:
      return state;
  }
};

const initialQuery = (state = null, action) => {
  switch (action.type) {
    case 'SET_INITIAL_SEARCH_QUERY':
      return action.wordid;
    default:
      return state;
  }
};

export const lookup = combineReducers({
  isFetching,
  isDone,
  isFailure,
  entriesData,
  translationsData,
  searchData,
  thesaurusData,
  initialQuery,
});
