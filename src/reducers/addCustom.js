import { combineReducers } from 'redux';

/**
 * Структура хранилища для добавления карточек
 * @param {String} isFetching
 * @param {String} isFailure
 * @param {String} isDone
 */

const isFetching = (state = false, action) => {
  switch(action.type) {
    case 'POST_ADDWORD_REQUEST':
      return true;
    default: 
      return false;
  }
}

const isFailure = (state = null, action) => {
  switch(action.type) {
    case 'POST_ADDWORD_FAILURE':
      return action.message;
    case 'FAILURE_RESET':
    default:
      return null;
  }
}

const isDone = (state = null, action) => {
  switch(action.type) {
    case 'POST_ADDWORD_SUCCESS':
      // айди карточки
      return action.response.data;
    default: 
      return null;
  }
}

export const addCustom = combineReducers({
  isFetching,
  isFailure,
  isDone,
});