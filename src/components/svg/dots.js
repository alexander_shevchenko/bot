import React from 'react';

const IconDots = () => {
  return (
    <svg viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="10" cy="24" r="4" fill="#3E5259" />
      <circle cx="24" cy="24" r="4" fill="#3E5259" />
      <circle cx="38" cy="24" r="4" fill="#3E5259" />
    </svg>
  );
};

export default IconDots;
