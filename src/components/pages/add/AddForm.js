import React from 'react';
import PropTypes from 'prop-types';
import Heroinput from '../../misc/heroinput';
import Notice from '../../misc/notice';

/**
 * @param {Object} front
 * @param {Object} back
 * @param {Object} definition
 * @param {Object} example
 * @param {Boolean} isTranslationEnabled
 * @param {Function} handleChange
 */

function AddForm({
  front,
  back,
  definition,
  example,
  handleChange,
  isTranslationEnabled,
}) {
  return (
    <div className="edit__form">
      <form action="#">
        <div
          className={
            'form-control form-control--emphasized required ' +
            (front.isValid ? '' : 'form-control__invalid')
          }
        >
          {!front.isValid && (
            <span className="form-control__errorMsg">
              <Notice type="danger" size="small" title="Required field" />
            </span>
          )}
          <label htmlFor="front">
            Word
            <span className="form-control__marker"></span>
          </label>
          <Heroinput
            id="front"
            name="front"
            value={front.value}
            handleChange={handleChange}
          />
        </div>
        {!isTranslationEnabled && (
          <>
            <div
              className={
                'form-control form-control--emphasized required ' +
                (definition.isValid ? '' : 'form-control__invalid')
              }
            >
              {!definition.isValid && (
                <span className="form-control__errorMsg">
                  <Notice type="danger" size="small" title="Required field" />
                </span>
              )}
              <label htmlFor="back">
                Definition
                <span className="form-control__marker"></span>
              </label>
              <textarea
                value={definition.value}
                name="definition"
                id="definition"
                maxLength="200"
                onChange={handleChange}
              ></textarea>
            </div>
          </>
        )}
        {isTranslationEnabled && (
          <>
            <div
              className={
                'form-control form-control--emphasized required ' +
                (back.isValid ? '' : 'form-control__invalid')
              }
            >
              {!back.isValid && (
                <span className="form-control__errorMsg">
                  <Notice
                    type="danger"
                    size="small"
                    title="At least one required"
                  />
                </span>
              )}
              <label htmlFor="back">
                Translation
                <span className="form-control__marker"></span>
                {back.isValid && (
                  <span className="form-control__at_least_one">
                    at least one
                  </span>
                )}
              </label>
              <Heroinput
                id="back"
                name="back"
                value={back.value}
                handleChange={handleChange}
              />
            </div>
            <div
              className={
                'form-control form-control--emphasized required ' +
                (definition.isValid ? '' : 'form-control__invalid')
              }
            >
              {!definition.isValid && (
                <span className="form-control__errorMsg">
                  <Notice
                    type="danger"
                    size="small"
                    title="At least one required"
                  />
                </span>
              )}
              <label htmlFor="back">
                Definition
                <span className="form-control__marker"></span>
                {definition.isValid && (
                  <span className="form-control__at_least_one">
                    at least one
                  </span>
                )}
              </label>
              <textarea
                value={definition.value}
                name="definition"
                id="definition"
                maxLength="200"
                onChange={handleChange}
              ></textarea>
            </div>
          </>
        )}
        <div className="form-control">
          <label htmlFor="example">
            Example
            <span className="form-control__marker"></span>
          </label>
          <textarea
            value={example.value}
            name="example"
            id="example"
            maxLength="200"
            onChange={handleChange}
          ></textarea>
          <p className="form-control__note">
            <strong>E.g.</strong> “She reached in and pulled out a <b>solar</b>{' '}
            map, with one star highlighted.”
          </p>
        </div>
      </form>
    </div>
  );
}

AddForm.propTypes = {
  front: PropTypes.object.isRequired,
  back: PropTypes.object,
  definition: PropTypes.object,
  example: PropTypes.object,
  isTranslationEnabled: PropTypes.bool.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default AddForm;
