import React from 'react';
import PropTypes from 'prop-types';
import OverlayAlert from '../../layout/OverlayAlert';
import Alert from '../../misc/alert';
import { ioMessages } from '../../../actions/consts';

/**
 * @param {Function} handleReset resets the form
 * @param {Object} history react-router 'history' object
 */

function AddDone({ history, handleReset }) {
  return (
    <OverlayAlert history={history} action="close">
      <Alert
        type="done"
        title={ioMessages.custom.addedPrimary}
        description={ioMessages.custom.addedSecondary}
        actionTitle="Add one more"
        actionType="secondary--gray"
        actionHandler={handleReset}
      />
    </OverlayAlert>
  );
}

AddDone.propTypes = {
  history: PropTypes.object.isRequired,
  handleReset: PropTypes.func.isRequired,
};

export default AddDone;
