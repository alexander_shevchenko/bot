import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { paths } from '../../../consts/paths';

/**
 * @param {Function} handleSubmit
 */

function AddFooter({ handleSubmit }) {
  return (
    <footer className="footer--static">
      <button className="btn btn--primary btn--block" onClick={handleSubmit}>
        Save Card
      </button>
      <Link
        className="btn btn--link btn--medium btn--pseudo footer__cancel"
        to={paths.addCard}
      >
        Cancel
      </Link>
    </footer>
  );
}

AddFooter.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default AddFooter;
