import React from 'react';
import PropTypes from 'prop-types';
import OverlayAlert from '../../layout/OverlayAlert';
import Alert from '../../misc/alert';
import { ioMessages } from '../../../actions/consts';

/**
 * @param {String} error
 * @param {Object} history react-router 'history' object
 */

function AddErrors({ error, history }) {
  return (
    <OverlayAlert history={history} action="reset">
      {error === ioMessages.connection.connectionError && (
        <Alert
          type="connectionFailed"
          title={ioMessages.connection.connectionError}
          description={ioMessages.connection.connectionErrorSecondary}
        />
      )}
      {error === ioMessages.connection.serverError && (
        <Alert
          type="serverError"
          title={ioMessages.connection.serverError}
          description={ioMessages.connection.serverErrorSecondary}
        />
      )}
      {error === ioMessages.validation.duplicate && (
        <Alert
          type="duplicateFound"
          title={ioMessages.validation.duplicate}
          description={ioMessages.validation.duplicateWordSecondary}
        />
      )}
      {error === ioMessages.validation.validationError && (
        <Alert
          type="validationError"
          title={ioMessages.validation.validationError}
          description={ioMessages.validation.validationErrorSecondary}
        />
      )}
    </OverlayAlert>
  );
}

AddErrors.propTypes = {
  error: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default AddErrors;
