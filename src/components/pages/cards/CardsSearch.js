import React from 'react';
import PropTypes from 'prop-types';

/**
 * @param {String} query
 * @param {Function} handler
 */

function CardsSearch({ query, handler }) {
  return (
    <div className="cards__search">
      <div className="form-control">
        <input
          type="text"
          maxLength="60"
          id="search"
          name="search"
          placeholder="Search"
          value={query}
          onChange={handler}
        />
      </div>
    </div>
  );
}

CardsSearch.propTypes = {
  query: PropTypes.string.isRequired,
  handler: PropTypes.func.isRequired,
};

export default CardsSearch;
