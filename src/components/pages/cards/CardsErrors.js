import React from 'react';
import PropTypes from 'prop-types';
import OverlayAlert from '../../layout/OverlayAlert';
import Alert from '../../misc/alert';
import { ioMessages } from '../../../actions/consts';

/**
 * @param {String} error
 * @param {String} action
 * @param {Object} history react-router 'history' object
 */

function CardsErrors({ error, action, history }) {
  return (
    <OverlayAlert
      history={history}
      action={action === 'fetchAllCards' ? 'close' : 'reset'}
    >
      {error === ioMessages.connection.connectionError && (
        <Alert
          type="connectionFailed"
          title={ioMessages.connection.connectionError}
          description={ioMessages.connection.connectionErrorSecondary}
        />
      )}
      {error === ioMessages.connection.serverError && (
        <Alert
          type="serverError"
          title={ioMessages.connection.serverError}
          description={ioMessages.connection.serverErrorSecondary}
        />
      )}
    </OverlayAlert>
  );
}

CardsErrors.propTypes = {
  error: PropTypes.string.isRequired,
  action: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default CardsErrors;
