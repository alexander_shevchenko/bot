import React from 'react';
import PropTypes from 'prop-types';

/**
 * @param {Array} chars
 * @param {String} activeChar
 * @param {Function} handler
 */

function CardsNav({ chars, activeChar, handler }) {
  const items = chars.map((char, i) => {
    return (
      <li key={i}>
        {char && char === activeChar && (
          <span data-testid={char} className="icon__wrapper">
            <span className="icon__wrapper__container">{char}</span>
          </span>
        )}
        {char && char !== activeChar && (
          <button
            data-testid={char}
            className="btn btn--link btn--xs"
            onClick={() => handler(char)}
          >
            <strong>{char}</strong>
          </button>
        )}
        {!char && <em>&sdot;</em>}
      </li>
    );
  });

  return (
    <div className="cards__nav">
      <ul>{items}</ul>
    </div>
  );
}

CardsNav.propTypes = {
  chars: PropTypes.array.isRequired,
  activeChar: PropTypes.string.isRequired,
  handler: PropTypes.func.isRequired,
};

export default CardsNav;
