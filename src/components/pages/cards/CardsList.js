import React from 'react';
import PropTypes from 'prop-types';

/**
 * @param {Array} words
 * @param {Function} handler
 */

function CardsList({ words, handler }) {
  const items = words.map((word) => {
    return (
      <li key={word.id}>
        <span onClick={() => handler(word.id)} data-testid={word.word}>
          <span>{word.word}</span>
          {word.part && (
            <span className={'pill pill--' + word.part}>{word.part}</span>
          )}
        </span>
      </li>
    );
  });

  return (
    <div className="cards__list">
      <ul>{items}</ul>
    </div>
  );
}

CardsList.propTypes = {
  words: PropTypes.array.isRequired,
  handler: PropTypes.func.isRequired,
};

export default CardsList;
