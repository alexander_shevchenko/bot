import React from 'react';
import PropTypes from 'prop-types';
import OverlayAlert from '../../layout/OverlayAlert';
import Alert from '../../misc/alert';
import { ioMessages } from '../../../actions/consts';

/**
 * Component renders various error states of the App component
 *
 * @param {String} error
 * @param {Object} history react-router 'history' object
 */

function AppErrors({ error, history }) {
  return (
    <OverlayAlert history={history} action="close">
      {(error === ioMessages.connection.sdkFailure ||
        error === ioMessages.connection.messengerError) && (
        <Alert
          type="serverError"
          title={ioMessages.messenger.down}
          description={ioMessages.messenger.downSecondary}
        />
      )}
      {error === ioMessages.connection.serverError && (
        <Alert
          type="serverError"
          title={ioMessages.connection.serverError}
          description={ioMessages.connection.serverErrorSecondary}
        />
      )}
      {error === ioMessages.connection.connectionError && (
        <Alert
          type="connectionFailed"
          title={ioMessages.connection.connectionError}
          description={ioMessages.connection.connectionErrorSecondary}
        />
      )}
    </OverlayAlert>
  );
}

AppErrors.propTypes = {
  error: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default AppErrors;
