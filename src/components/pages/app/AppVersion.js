import React from 'react';
import PropTypes from 'prop-types';
import { releaseId, users } from '../../../config/version';

/**
 * Component renders product version
 * for the specified list of user ids
 *
 * @param {String} userId
 */

export default function AppVersion({ user }) {
  if (users.includes(user)) {
    return <div id="v">v. {releaseId}</div>;
  }

  return null;
}

AppVersion.propTypes = {
  user: PropTypes.string.isRequired,
};
