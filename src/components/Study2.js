import React from 'react';
import Progress from './layout/study/progress';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Test } from './tests/';
import { AssembleWord } from './tests/noticing/01assembleWord';
import { AssembleTranslations } from './tests/noticing/02assembleTranslations';
import { PickSynonym } from './tests/noticing/03pickSynonym';


const Study2View = ({
  
}) => {
  return (
    <div className="page page--zero-pb study">
      
      <div className="study__header">
        <Progress />
        <Link 
          className="btn btn--link btn--pseudo btn--pseudo--dark btn--xs"
          role="button"
          to="/"
        >Изменить</Link>
      </div>

      {/* Слово-перевод > Синоним / Часть речи */}
      <div className="test hidden">
        <div className="test__title">Часть речи</div>
        <div className="test__description">Выбери часть речи подходящую к слову и переводу.</div>
        <div className="test__content">
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>or</span>
          </div>
          <div className="test__content__blank">
            <div className="word --primary --active --l"></div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">pleased</button>
          <button className="word-btn --primary">striking</button>
          <button className="word-btn --primary">half-baked</button>
          <button className="word-btn --primary">bright</button>
          <button className="word-btn --primary">blockish</button>
        </div>
      </div>
      {/* Слово-перевод > Синоним / Часть речи --успех */}
      <div className="test hidden">
        <div className="test__title">Часть речи</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>or</span>
          </div>
          <div className="test__content__blank">
            <div className="word --primary">half-baked</div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Слово-перевод > Определение сборка */}
      <div className="test hidden">
        <div className="test__title">Определение</div>
        <div className="test__description">Собери определение соответствующее слову.</div>
        <div className="test__content">
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --l"></div>
              <div className="word --primary --inactive --l"></div>
              <div className="word --primary --inactive --xs"></div>
              <div className="word --primary --inactive --s"></div>
              <div className="word --primary --inactive --xs"></div>
              <div className="word --primary --inactive --l"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">insensitive</button>
          <button className="word-btn --primary">understand</button>
          <button className="word-btn --primary">slow</button>
          <button className="word-btn --primary">or</button>
          <button className="word-btn --primary">to</button>
          <button className="word-btn --primary">annoyingly</button>
        </div>
      </div>
      {/* Слово-перевод > Определение сборка --успех */}
      <div className="test hidden">
        <div className="test__title">Определение</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            {/* Вот тут я буду, в зависимости от длины блока, скриптом расставлять toothy модификаторы */}
            <div className="sentence">
              <div className="word --primary --clean-right">annoyingly</div>
              <div className="word --primary --clean-both">insensitive</div>
              <div className="word --primary --clean-both">or</div>
              <div className="word --primary --clean-left --toothy-right">slow</div>
              <br />
              <div className="word --primary --clean-right --toothy-left">to</div>
              <div className="word --primary --clean-left">understand</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Слово-перевод > Определение выбор */}
      <div className="test hidden">
        <div className="test__title">Определение</div>
        <div className="test__description">Выбери определение соответствующее слову.</div>
        <div className="test__content">
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --empty-block"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary --block">annoyingly insensitive or slow to understand</button>
          <button className="word-btn --primary --block">skilled at doing or achieving something; talented</button>
        </div>
      </div>
      {/* Слово-перевод > Определение выбор --успех */}
      <div className="test hidden">
        <div className="test__title">Определение</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --content-block">annoyingly insensitive or slow to understand</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Определение > Слово сборка */}
      <div className="test hidden">
        <div className="test__title">Слово</div>
        <div className="test__description">Собери слово соответствующее определению.</div>
        <div className="test__content">
          <div className="test__content__definition">
            <span>annoyingly insensitive or slow to understand</span>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="letter --primary --empty --active"></div>
              <div className="letter --primary --empty --inactive"></div>
              <div className="letter --primary --empty --inactive"></div>
              <div className="letter --primary --empty --inactive"></div>
              <div className="letter --primary --empty --inactive"></div>
              <div className="letter --primary --empty --inactive"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="letter-btn --primary">s</button>
          <button className="letter-btn --primary">o</button>
          <button className="letter-btn --primary">t</button>
          <button className="letter-btn --primary">u</button>
          <button className="letter-btn --primary">b</button>
          <button className="letter-btn --primary">e</button>
        </div>
      </div>
      {/* Определение > Слово сборка --успех */}
      <div className="test hidden">
        <div className="test__title">Слово</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__definition">
            <span>annoyingly insensitive or slow to understand</span>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="word --primary">obtuse</div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Определение > Слово выбор */}
      <div className="test hidden">
        <div className="test__title">Слово</div>
        <div className="test__description">Выбери слово соответствующее определению.</div>
        <div className="test__content">
          <div className="test__content__definition">
            <span>annoyingly insensitive or slow to understand</span>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --l"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">aspersions</button>
          <button className="word-btn --primary">obtuse</button>
          <button className="word-btn --primary">blatant</button>
        </div>
      </div>
      {/* Определение > Слово выбор --успех */}
      <div className="test hidden">
        <div className="test__title">Слово</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__definition">
            <span>annoyingly insensitive or slow to understand</span>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="word --primary">obtuse</div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Определение + Слово > Часть речи */}
      <div className="test hidden">
        <div className="test__title">Часть речи</div>
        <div className="test__description">Выбери часть речи подходящую к определению.</div>
        <div className="test__content">
          <div className="test__content__word-definition">
            {/* если больше N символов совокупно, то убираю --compact */}
            <div className="test__content__word-translation --compact --nounderline --small">
              <div className="test__content__word-translation__word">obtuse</div>
            </div>
            <div className="test__content__word-definition__definition">annoyingly insensitive or slow to understand</div>
          </div>
          <div className="test__content__or">
            <span>is</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --l"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">adverb</button>
          <button className="word-btn --primary">noun</button>
          <button className="word-btn --primary">adjective</button>
        </div>
      </div>
      {/* Определение + Слово > Часть речи --успех */}
      <div className="test hidden">
        <div className="test__title">Часть речи</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__word-definition">
            {/* если больше N символов совокупно, то убираю --compact */}
            <div className="test__content__word-translation --compact --nounderline --small">
              <div className="test__content__word-translation__word">obtuse</div>
            </div>
            <div className="test__content__word-definition__definition">annoyingly insensitive or slow to understand</div>
          </div>
          <div className="test__content__or">
            <span>is</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary">adjective</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>        
      </div>
      {/* Определение + Слово > Синоним */}
      <div className="test hidden">
        <div className="test__title">Синоним</div>
        <div className="test__description">Выбери синоним подходящий к слову и определению.</div>
        <div className="test__content">
          <div className="test__content__word-definition">
            {/* если больше N символов совокупно, то убираю --compact */}
            <div className="test__content__word-translation --compact --nounderline --small">
              <div className="test__content__word-translation__word">obtuse</div>
            </div>
            <div className="test__content__word-definition__definition">annoyingly insensitive or slow to understand</div>
          </div>
          <div className="test__content__or">
            <span>or</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --l"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">pleased</button>
          <button className="word-btn --primary">striking</button>
          <button className="word-btn --primary">half-baked</button>
          <button className="word-btn --primary">bright</button>
          <button className="word-btn --primary">blockish</button>
        </div>
      </div>
      {/* Определение + Слово > Синоним --успех */}
      <div className="test hidden">
        <div className="test__title">Синоним</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__word-definition">
            {/* если больше N символов совокупно, то убираю --compact */}
            <div className="test__content__word-translation --compact --nounderline --small">
              <div className="test__content__word-translation__word">obtuse</div>
            </div>
            <div className="test__content__word-definition__definition">annoyingly insensitive or slow to understand</div>
          </div>
          <div className="test__content__or">
            <span>or</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary">half-baked</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>        
      </div>
      {/* Определение > Пример выбор */}
      <div className="test hidden">
        <div className="test__title">Пример</div>
        <div className="test__description">Выбери пример соответствующий данному определению.</div>
        <div className="test__content">
          <div className="test__content__definition">
            <span>annoyingly insensitive or slow to understand</span>
          </div>
          <div className="test__content__or">
            <span>matches</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --empty-block"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary --block --quote"><span>he wondered if the doctor was being deliberately obtuse</span></button>
          <button className="word-btn --primary --block --quote">The charity said over half of adults in the UK are overweight, with one in five classed as obese</button>
        </div>
      </div>
      {/* Определение > Пример --успех */}
      <div className="test hidden">
        <div className="test__title">Пример</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__definition">
            <span>annoyingly insensitive or slow to understand</span>
          </div>
          <div className="test__content__or">
            <span>matches</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --content-block --quote">he wondered if the doctor was being deliberately <strong>obtuse</strong></div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      
      {/* NOTICING */}
      {/* Кубики, Собрать слово (Noticing) */}
      <AssembleWord 
        words={['half-baked']}
        handleSuccess={() => true}
        isHidden={true}
      />
      {/* Кубики, Собрать слово (Noticing) --успех */}
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__blank --only">
            <div className="sentence">
              <div className="letter --primary">o</div>
              <div className="letter --primary">b</div>
              <div className="letter --primary">t</div>
              <div className="letter --primary">u</div>
              <div className="letter --primary">s</div>
              <div className="letter --primary">e</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Кубики, Слово > Собрать перевод (Noticing) */}
      <AssembleTranslations 
        words={['obtuse']}
        translations={['тупой', 'оболваненный']}
        handleSuccess={() => true}
        isHidden={true}
      />
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__description">Собери перевод слова.</div>
        <div className="test__content">
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
          </div>
          <div className="test__content__or">
            <span>translates</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="letter --primary --active">т</div>
              <div className="letter --primary --inactive">у</div>
              <div className="letter --primary --inactive">п</div>
              <div className="letter --primary --inactive">о</div>
              <div className="letter --primary --inactive">й</div>
              <div className="letter --punctuation hidden">-</div>
              <div className="letter --punctuation --comma">,</div>
              <br />
              <div className="letter --primary --inactive">о</div>
              <div className="letter --primary --inactive">б</div>
              <div className="letter --primary --inactive">о</div>
              <div className="letter --primary --inactive">л</div>
              <div className="letter --primary --inactive">в</div>
              <div className="letter --primary --inactive">а</div>
              <div className="letter --primary --inactive">н</div>
              <div className="letter --primary --inactive">е</div>
              <div className="letter --primary --inactive">н</div>
              <div className="letter --primary --inactive">н</div>
              <div className="letter --primary --inactive">ы</div>
              <div className="letter --primary --inactive">й</div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="letter-btn --primary">о</button>
          <button className="letter-btn --primary">б</button>
          <button className="letter-btn --primary">о</button>
          <button className="letter-btn --primary">л</button>
          <button className="letter-btn --primary">в</button>
          <button className="letter-btn --primary">а</button>
          <button className="letter-btn --primary">н</button>
          <button className="letter-btn --primary">п</button>
          <button className="letter-btn --primary">у</button>
          <button className="letter-btn --primary">й</button>
          <button className="letter-btn --primary">т</button>
          <button className="letter-btn --primary">о</button>
          <button className="letter-btn --primary">е</button>
          <button className="letter-btn --primary">н</button>
          <button className="letter-btn --primary">н</button>
          <button className="letter-btn --primary">ы</button>
          <button className="letter-btn --primary">й</button>
        </div>
      </div>
      {/* Кубики, Слово > Собрать перевод (Noticing) --успех */}
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
          </div>
          <div className="test__content__or">
            <span>translates</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="letter --primary">т</div>
              <div className="letter --primary">у</div>
              <div className="letter --primary">п</div>
              <div className="letter --primary">о</div>
              <div className="letter --primary">й</div>
              <div className="letter --punctuation --comma">,</div>
              <br />
              <div className="letter --primary">о</div>
              <div className="letter --primary">б</div>
              <div className="letter --primary">о</div>
              <div className="letter --primary">л</div>
              <div className="letter --primary">в</div>
              <div className="letter --primary">а</div>
              <div className="letter --primary">н</div>
              <div className="letter --primary">е</div>
              <div className="letter --primary">н</div>
              <div className="letter --primary">н</div>
              <div className="letter --primary">ы</div>
              <div className="letter --primary">й</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Слово-перевод > Синоним (Noticing) */}
      <PickSynonym 
        isHidden={false}
      />
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__description">Выбери подходящую часть речи из списка внизу</div>
        <div className="test__content">
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>is</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active">adjective</div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">adverb</button>
          <button className="word-btn --primary">noun</button>
          <button className="word-btn --primary">adjective</button>
        </div>
      </div>
      {/* Слово-перевод > Синоним (Noticing) --успех */}
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>is</span>
          </div>
          <div className="test__content__blank">
            <div className="word --primary">adjective</div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Слово-перевод > Определение сборка (Noticing) */}
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__description">Собери определение соответствующее слову.</div>
        <div className="test__content">
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active">annoyingly</div>
              <div className="word --primary --inactive">insensitive</div>
              <div className="word --primary --inactive">or</div>
              <div className="word --primary --inactive">slow</div>
              <div className="word --primary --inactive">to</div>
              <div className="word --primary --inactive">understand</div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">insensitive</button>
          <button className="word-btn --primary">understand</button>
          <button className="word-btn --primary">slow</button>
          <button className="word-btn --primary">or</button>
          <button className="word-btn --primary">to</button>
          <button className="word-btn --primary">annoyingly</button>
        </div>
      </div>
      {/* Слово-перевод > Определение сборка (Noticing) --успех */}
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__word-translation --compact">
            <div className="test__content__word-translation__word">obtuse</div>
            <div className="test__content__word-translation__separator"><span></span></div>
            <div className="test__content__word-translation__translation">тупой</div>
          </div>
          <div className="test__content__or">
            <span>means</span>
          </div>
          <div className="test__content__blank">
            {/* Вот тут я буду, в зависимости от длины блока, скриптом расставлять toothy модификаторы */}
            <div className="sentence">
              <div className="word --primary --clean-right">annoyingly</div>
              <div className="word --primary --clean-both">insensitive</div>
              <div className="word --primary --clean-both">or</div>
              <div className="word --primary --clean-left --toothy-right">slow</div>
              <br />
              <div className="word --primary --clean-right --toothy-left">to</div>
              <div className="word --primary --clean-left">understand</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Определение + Слово-Перевод > Пример (Noticing) */}
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__description">Собери пример соответствующий слову и его определению.</div>
        <div className="test__content">
          <div className="test__content__word-definition">
            {/* если больше N символов совокупно, то убираю --compact */}
            <div className="test__content__word-translation --compact --nounderline --small">
              <div className="test__content__word-translation__word">obtuse</div>
              <div className="test__content__word-translation__separator"><span></span></div>
              <div className="test__content__word-translation__translation">тупой</div>
            </div>
            <div className="test__content__word-definition__definition">annoyingly insensitive or slow to understand</div>
          </div>
          <div className="test__content__or">
            <span>matches</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active">he</div>
              <div className="word --primary --inactive">wondered</div>
              <div className="word --primary --inactive">if</div>
              <div className="word --primary --inactive">the</div>
              <div className="word --primary --inactive">doctor</div>
              <div className="word --primary --inactive">was</div>
              <div className="word --primary --inactive">being</div>
              <div className="word --primary --inactive">deliberately</div>
              <div className="word --primary --inactive --bold">obtuse</div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">if</button>
          <button className="word-btn --primary">wondered</button>
          <button className="word-btn --primary">he</button>
          <button className="word-btn --primary">doctor</button>
          <button className="word-btn --primary">was</button>
          <button className="word-btn --primary">being</button>
          <button className="word-btn --primary">the</button>
          <button className="word-btn --primary --bold">obtuse</button>
          <button className="word-btn --primary">deliberately</button>
        </div>
      </div>
      {/* Определение + Слово-Перевод > Пример (Noticing) --успех */}
      <div className="test hidden">
        <div className="test__title">Знакомство</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__word-definition">
            {/* если больше N символов совокупно, то убираю --compact */}
            <div className="test__content__word-translation --compact --nounderline --small">
              <div className="test__content__word-translation__word">obtuse</div>
              <div className="test__content__word-translation__separator"><span></span></div>
              <div className="test__content__word-translation__translation">тупой</div>
            </div>
            <div className="test__content__word-definition__definition">annoyingly insensitive or slow to understand</div>
          </div>
          <div className="test__content__or">
            <span>matches</span>
          </div>
          <div className="test__content__blank">
          <div className="sentence">
              <div className="word --primary --content-block --quote">he wondered if the doctor was being deliberately <strong>obtuse</strong></div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>        
      </div>
      
      
      {/* Кубики, Исправь ошибку */}
      <div className="test hidden">
        <div className="test__title">Исправь ошибку</div>
        <div className="test__description">Найди и нажми одну из букв не на своем месте</div>
        <div className="test__content">
          <div className="test__content__blank --only">
            <div className="sentence">
              <div className="letter --primary --warning">o</div>
              <div className="letter --primary --warning">t</div>
              <div className="letter --primary --warning">b</div>
              <div className="letter --primary --error">u</div>
              <div className="letter --primary --warning">s</div>
              <div className="letter --primary --warning">e</div>
            </div>
          </div>
        </div>
        <div className="test__footer"></div>
      </div>
      {/* Кубики, Исправь ошибку --успех */}
      <div className="test hidden">
        <div className="test__title">Исправь ошибку</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__blank --only">
            <div className="sentence --underlined">
              <div className="sentence__underline-wrapper">
                <div className="letter --primary">o</div>
                <div className="letter --primary">b</div>
                <div className="letter --primary">t</div>
                <div className="letter --primary">u</div>
                <div className="letter --primary">s</div>
                <div className="letter --primary">e</div>
                <div className="letter --primary">o</div>
                <div className="letter --primary">b</div>
                <div className="letter --primary">t</div>
                <div className="letter --primary">u</div>
                <div className="letter --primary">s</div>
                <div className="letter --primary">e</div>
              </div>
            </div>
          </div>
          {/* Если есть перевод */}
          <div className="test__content__or">
            <span>translates</span>
          </div>
          <div className="test__content__blank">
            <div className="word --info">тупой</div>
          </div>
          {/* Если только дефинишн */}
          <div className="test__content__or hidden">
            <span>means</span>
          </div>
          <div className="test__content__blank hidden">
            <div className="word --info --content-block">he wondered if the doctor was being deliberately <strong>obtuse</strong></div>
          </div>
          
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Кубики, Собери слово, Аудио */}
      <div className="test hidden">
        <div className="test__title">Слово на слух</div>
        <div className="test__description">Прослушай аудио и собери слово используя буквы внизу.</div>
        <div className="test__content">
          {/** --active на проигрыш */}
          <div className="test__content__audio">
            <button>
              <span className="icon icon-volume-up"></span>
            </button>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="letter --primary --active --empty"></div>
              <div className="letter --primary --inactive --empty"></div>
              <div className="letter --primary --inactive --empty"></div>
              <div className="letter --primary --inactive --empty"></div>
              <div className="letter --primary --inactive --empty"></div>
              <div className="letter --primary --inactive --empty"></div>
            </div>
          </div>
          <div className="test__content__skip">
              <Link 
              className="btn btn--link btn--pseudo btn--medium"
              role="button"
              to="/"
            >Пропустить упражнение</Link>
          </div>
        </div>
        <div className="test__footer">
          <button className="letter-btn --primary">o</button>
          <button className="letter-btn --primary">s</button>
          <button className="letter-btn --primary">t</button>
          <button className="letter-btn --primary">u</button>
          <button className="letter-btn --primary">b</button>
          <button className="letter-btn --primary">e</button>
        </div>
      </div>
      {/* Кубики, Собери слово, Аудио --успех */}
      <div className="test hidden">
        <div className="test__title">Слово на слух</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon icon-check"></span>
          </div>
          <div className="test__content__blank --only">
            <div className="sentence --underlined">
              <div className="sentence__underline-wrapper">
                <div className="letter --primary">o</div>
                <div className="letter --primary">b</div>
                <div className="letter --primary">t</div>
                <div className="letter --primary">u</div>
                <div className="letter --primary">s</div>
                <div className="letter --primary">e</div>
              </div>
            </div>
          </div>
          {/* Если есть перевод */}
          <div className="test__content__or">
            <span>translates</span>
          </div>
          <div className="test__content__blank">
            <div className="word --info">тупой</div>
          </div>
          {/* Если только дефинишн */}
          <div className="test__content__or hidden">
            <span>means</span>
          </div>
          <div className="test__content__blank hidden">
            <div className="word --info --content-block">he wondered if the doctor was being deliberately <strong>obtuse</strong></div>
          </div>
          
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>


      {/* Пример - подстановка */}
      <div className="test hidden">
        <div className="test__title">Пример</div>
        <div className="test__description">Подставь подходящее к примеру слово.</div>
        <div className="test__content">
          {/* если больше N символов совокупно, то убираю --compact */}
          <div className="test__content__blank --only">
            {/* Вот тут я буду, в зависимости от длины блока, скриптом расставлять toothy модификаторы */}
            <div className="sentence">
              <div className="word --primary --clean-right">He</div>
              <div className="word --primary --clean-both">wondered</div>
              <div className="word --primary --clean-both">if</div>
              <div className="word --primary --clean-both">the</div>
              <div className="word --primary --clean-left --toothy-right">doctor</div>
              <br />
              <div className="word --primary --clean-right --toothy-left">was</div>
              <div className="word --primary --clean-both">being</div>
              <div className="word --primary --clean-both">deliberately</div>
              <div className="word --active --s"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary">aspersions</button>
          <button className="word-btn --primary">obtuse</button>
          <button className="word-btn --primary">blatant</button>
        </div>
      </div>
      {/* Пример - подстановка --успех */}
      <div className="test hidden">
        <div className="test__title">Пример</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__blank">
            {/* Вот тут я буду, в зависимости от длины блока, скриптом расставлять toothy модификаторы */}
            <div className="sentence">
              <div className="word --primary --clean-right">He</div>
              <div className="word --primary --clean-both">wondered</div>
              <div className="word --primary --clean-both">if</div>
              <div className="word --primary --clean-both">the</div>
              <div className="word --primary --clean-left --toothy-right">doctor</div>
              <br />
              <div className="word --primary --clean-right --toothy-left">was</div>
              <div className="word --primary --clean-both">being</div>
              <div className="word --primary --clean-both">deliberately</div>
              <div className="word --primary --clean-left --bold">obtuse</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Пример > определение, выбор */}
      <div className="test hidden">
        <div className="test__title">Определение</div>
        <div className="test__description">Выбери определение соответствующее приведенному примеру.</div>
        <div className="test__content">
          <div className="test__content__example">
            <span>he wondered if the doctor was being deliberately <strong>obtuse</strong></span>
          </div>

          <div className="test__content__or">
            <span>matches</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --empty-block"></div>
            </div>
          </div>
        </div>
        <div className="test__footer">
          <button className="word-btn --primary --block">he wondered if the doctor was being deliberately obtuse</button>
          <button className="word-btn --primary --block">The charity said over half of adults in the UK are overweight, with one in five classed as obese</button>
        </div>
      </div>
      {/* Пример > определение --успех */}
      <div className="test hidden">
        <div className="test__title">Определение</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__example">
            <span>he wondered if the doctor was being deliberately <strong>obtuse</strong></span>
          </div>
          <div className="test__content__or">
            <span>matches</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --content-block">annoyingly insensitive or slow to understand</div>
            </div>
          </div>
        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Пример > часть речи, выбор */}
      <div className="test hidden">
        <div className="test__title">Часть речи</div>
        <div className="test__description">Выбери часть речи соответствующую слову в контексте.</div>
        <div className="test__content">
          <div className="test__content__example">
            he wondered if the doctor was being deliberately <span><strong>obtuse</strong></span>
          </div>

          {/** Как позиционировать or и placeholder? */}
          <div className="test__content__or">
            <span>is</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --l"></div>
            </div>
          </div>

        </div>
        <div className="test__footer">
          <button className="word-btn --primary">adverb</button>
          <button className="word-btn --primary">noun</button>
          <button className="word-btn --primary">adjective</button>
        </div>
      </div>
      {/* Пример > часть речи --успех */}
      <div className="test hidden">
        <div className="test__title">Часть речи</div>
        <div className="test__content">
          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__example">
            he wondered if the doctor was being deliberately <span><strong>obtuse</strong></span>
          </div>

          {/** Как позиционировать or и placeholder? */}
          <div className="test__content__or">
            <span>is</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary">adjective</div>
            </div>
          </div>

        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      {/* Пример > синоним, выбор */}
      <div className="test hidden">
        <div className="test__title">Синоним</div>
        <div className="test__description">Выбери подходящий синоним.</div>
        <div className="test__content">
          <div className="test__content__example">
            he wondered if the doctor was being deliberately <span><strong>obtuse</strong></span>
          </div>

          {/** Как позиционировать or и placeholder? */}
          <div className="test__content__or">
            <span>or</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary --active --l"></div>
            </div>
          </div>

        </div>
        <div className="test__footer">
          <button className="word-btn --primary">pleased</button>
          <button className="word-btn --primary">striking</button>
          <button className="word-btn --primary">half-baked</button>
          <button className="word-btn --primary">noun</button>
          <button className="word-btn --primary">blockish</button>
        </div>
      </div>
      {/* Пример > синоним --успех */}
      <div className="test hidden">
        <div className="test__title">Синоним</div>
        <div className="test__content">
          <Test />

          <div className="test__content__success">
            <span className="icon-check"></span>
          </div>
          <div className="test__content__example">
            he wondered if the doctor was being deliberately <span><strong>obtuse</strong></span>
          </div>

          {/** Как позиционировать or и placeholder? */}
          <div className="test__content__or">
            <span>or</span>
          </div>
          <div className="test__content__blank">
            <div className="sentence">
              <div className="word --primary">half-baked</div>
            </div>
          </div>

        </div>
        <div className="test__footer --success">
          <button className="btn btn--primary btn--block">Продолжить</button>
        </div>
      </div>
      


    </div>
  );
};
// #endregion

class Study2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
  }

  componentDidMount() {
    
  }

  render() {
    return (
      <Study2View />
    )
  }
}

const mapStateToProps = state => {
  return {}
};

Study2 = connect(mapStateToProps, null)(Study2);
export default Study2;