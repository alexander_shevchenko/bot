import React, { useState, useLayoutEffect, useRef } from 'react';
/**
 * Рендерит слово или предложение
 * у каждого слова есть состояние; 
 * есть подсказка или нет,
 * если подсказки нет то компонент расставляет словам примерные размеры;
 * болдом хайлайтед контент;
 * расставляет классы clean и toothy
 * 
 * @param {Array}   content массив из контента (одно или множество слов)
 * @param {String}  content[i].title само слово
 * @param {String}  content[i].id уникальный айди
 * @param {Boolean} content[i].hasHint показывать ли подсказку
 * @param {Boolean} content[i].isHighlighted выделение искомого слова
 * @param {String}  content[i].status active/inactive/''
 * 
 */

export const ContentSentence = ({
  content,
}) => {
  // расставить референсы для useLayout
  const sentenceRef = useRef(null);
  const wordsRefs = content.map(el => {
    const ref = React.createRef();
    return ref;
  })

  useLayoutEffect(() => {
    // после рендера, расставляем зубчики в зависимости от размера блока sentence
    window.addEventListener('resize', applyToothy);
    applyToothy();

    function applyToothy() {
      // беру все доне
      const done = content.filter(el => el.status === '');
      const sentenceWidth = sentenceRef.current.getBoundingClientRect().width;
      // магический коэффициент
      const k = 10;

      let rowWidth = 0;
      for (let i = 0; i < done.length; i++) {
        // сначала почистить все зубчики
        // затем наложить новые
        wordsRefs[i].current.classList.remove('--toothy-right', '--toothy-left');
        // поскольку все done элементы сначала
        // то i-ый элемент done будет соответствовать i-му элементу wordsRefs
        const currentWidth = wordsRefs[i].current.getBoundingClientRect().width;
        // если длина предложения больше чем аккумулированная длина строки -
        // то пока ничего не делаю
        // если уже меньше, тогда расставляю зубцы
        // и сбрасываю аккумулированную длину строки до размера последнего слова
        rowWidth = rowWidth + currentWidth;
        if (rowWidth + k <= sentenceWidth) {
          // ничего не делаю
        } else {
          wordsRefs[i - 1].current.classList.add('--toothy-right');
          wordsRefs[i].current.classList.add('--toothy-left');
          rowWidth = currentWidth;
        }
      }
    }

    // отписаться!
    return () => {
      window.removeEventListener('resize', applyToothy);
    }
  });

  return (
    <div className="sentence" ref={sentenceRef}>
      {content.map((el, i) => {
        // состояние - активное, неактивное и done
        let status = '';
        if (el.status === 'active') status = '--active';
        if (el.status === 'inactive') status = '--inactive';

        // для легкости чтения добавлю экстра-переменную done
        let isDone = status ? false : true;

        // если контент не скрыт (есть подсказка) и еще не done — расставить размер: 
        // 1-3  =  --xs; 
        // 4-7  =  --s;
        // 8-11 =  --m;
        // 12+  =  --l;
        let size = '';
        if (el.hasHint && !isDone) {
          if (el.title.length <= 3) {
            size = '--xs';
          } else if (el.title.length <= 7) {
            size = '--s';
          } else if (el.title.length <= 11) {
            size = '--m';
          } else {
            size = '--l';
          }
        }

        // расставляю clean, 
        // если done то первому слову --clean-right, последнему --clean-left, остальным --clean-both
        let clean = '';
        if (isDone) {
          if (i === 0) {
            clean = '--clean-right';
          } else if (i === content.length - 1) {
            clean = '--clean-left';
          } else {
            clean = '--clean-both';
          }
        }

        return (
          <div 
            className={
              "word --primary " 
              + status + ' ' 
              + size + ' ' 
              + (el.isHighlighted ? '--bold' : '') + ' '
              + clean
            }
            key={el.id}
            ref={wordsRefs[i]}
          >
            {!el.isValueHidden && el.title}
          </div>
        )
      })}
    </div>
  );
}

