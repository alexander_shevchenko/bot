import React from 'react';
/**
 * Саб-компонент — стандартные экшены в футере
 * 
 * @param {Array} actions массив из экшенов
 * @param {String} actions[i].title содержимое кнопки
 * @param {String} actions[i].id уникальный айди кнопки
 * @param {Boolean} actions[i].hasError флаг ошибки
 * @param {Function} onActionClick хэндлер отрабатывающий на нажатие кнопки
 */

export const StandardActions = ({
  actions,
  onActionClick,
}) => {
  return (
    <React.Fragment>
      {actions.map((el) => {
        let modifier = el.hasError ? '--warning' : '';
        return (
          <button 
            className={"letter-btn --primary " + modifier}
            key={el.id}
            onClick={() => onActionClick(el.id, el.title)}
        >{el.title}</button>
        )
      })}
    </React.Fragment>
  );
}