import React from 'react';
/**
 * Саб-компонент — стандартные экшены в футере
 * 
 * @param {Array} content массив из контента
 * @param {String} content[i].char символ
 * @param {String} content[i].charType тип символа
 * @param {String} content[i].status статус // letter, comma, other
 * @param {String} content[i].id уникальный айди символа
 */

export const ContentChars = ({
  content,
}) => {
  return (
    <div className="sentence">
      {content.map(el => {
        // в зависимости от charType у меня 3 разных варианта рендера
        // буква
        if (el.charType === 'letter') {
          let modifier = '';
          if (el.status === 'active') modifier = '--active';
          if (el.status === 'inactive') modifier = '--inactive';
          return (
            <div 
              className={"letter --primary " + modifier}
              key={el.id}
            >
              {el.char}
            </div>
          )
        }
        // запятая 
        // FIXME: потенциальная проблема
        // запятая в слове наверное может быть органической, не исключено (возможно в идиомах), но я пока не встречал таких случаев
        // пока запятые вставляются методом formatInitialContent, который втыкает их как разделитель между словами (если слов больше одного)
        // если встретится проблема с этим моментом, то нужно будет перепродумать организацию поставки контента, сделать более сложную структуру
        // не линейную, а массив и переписать все методы завязанные на контент, в общем откладываю тебе это Саша в долгий ящик на будущее
        // если это будущее придёт
        if (el.charType === 'comma') {
          return (
            <React.Fragment key={el.id}>
              <div 
                className="letter --punctuation --comma"
              >
                {el.char}
              </div>
              <br />
            </React.Fragment>
          )
        }
        // остальные символы
        // сделал пока этот вариант дефолтным
        //if (el.charType === 'other') {
          return (
            <div 
              className="letter --punctuation"
              key={el.id}
            >
              {el.char}
            </div>
          )
        //}
      })}
    </div>
  );
}