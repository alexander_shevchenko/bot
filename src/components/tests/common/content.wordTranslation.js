import React from 'react';
/**
 * Саб-компонент — рендерит блок со словом-переводами
 * изменяет вью в зависимости от количества контента
 * 
 * @param {String} word само слово
 * @param {Array} translations массив переводов слова
 */

export const WordTranslation = ({
  word = '',
  translations = [],
}) => {
  const isTranslations = translations.length ? true : false;
  const joinedTranslations = isTranslations ? translations.join(', ') : '';
  
  // если кол-во символов в слове и переводе совокупно превышает 20
  // то --compact
  const compactCount = 20;
  const isCompact = word.length + joinedTranslations.length <= compactCount ? true : false;

  return (
    <div className={"test__content__word-translation " + (isCompact ? '--compact' : '')}>
      <div className="test__content__word-translation__word">{word}</div>
      {isTranslations && 
        <React.Fragment>
          <div className="test__content__word-translation__separator"><span></span></div>
          <div className="test__content__word-translation__translation">
            {joinedTranslations}
          </div>
        </React.Fragment>
      }
    </div>
  );
}