import { uuid } from 'uuidv4';
import cloneDeep from 'lodash/cloneDeep';

/**
 * Форматирует данные для блока с кубиками
 * буквы становятся кубиками, остальные символы — инлайн текстом
 * если есть больше одного слова, то слова разделяются запятой
 * 
 * @param {Array} words массив из слов
 * 
 * @returns {Array} массив с отформатированными данными
 * @param {String} content[i].char символ
 * @param {String} content[i].charType тип символа // letter, comma, other
 * @param {String} content[i].id уникальный айди
 * @param {String} content[i].status статус, всегда null
 */
export const formatInitialContent = (words) => {
  const content = words.join(',').split('').map(char => {
    return {
      char,
      charType: getCharType(char),
      id: uuid(),
      status: null,
    }
  });

  return content;
}


/**
 * Определяет тип символа, 
 * поддерживает — letter, comma, other
 * 
 * @param {String} char символ
 * 
 * @returns {String} тип символа
 */
export const getCharType = (char) => {
  // letter, comma, other
  // FIXME: потенциальная проблема, Несмотря на то, что это часть стандарта с 2018 года, 
  // юникодные свойства не поддерживаются в Firefox и Edge.
  // Существует библиотека XRegExp, которая реализует «расширенные» регулярные выражения с кросс-браузерной поддержкой юникодных свойств. 
  // https://learn.javascript.ru/regexp-character-sets-and-ranges
  if ( /[\p{Alpha}]/u.test(char) ) return 'letter';
  if ( /,/.test(char) ) return 'comma';
  return 'other';
}

/**
 * Берет массив контента и айди текущего активного элемента (опционально)
 * возвращает айди следующего активного элемента (игнорирует не буквы)
 *  
 * @param {Array} content массив с контентом
 * @param {String} currentId айди текущего активвного эл-та
 * 
 * @returns {String} айди следующего активного эл-та
 */
export const pickCurrentChar = (content, currentId) => {
  // два сценария — 
  // *первый прогон*
  // предыдущего current не существует
  // current-ом становится первый letter из массива content
  // *остальные прогоны*
  // нахожу по какому индексу живет currentId и отрезаю эту часть, 
  // затем итерирую оставшееся и забираю id первой буквы
  let clonedContent = cloneDeep(content);
  let nextElement;

  // если есть currentId — значит это уже не первый прогон, отрезаю лишнее
  // если нет, значит сразу нахожу буковку и возвращаю
  if (currentId) {
    const currentIndex = clonedContent.findIndex(el => el.id === currentId);
    clonedContent = clonedContent.slice(currentIndex + 1);
  }

  nextElement = clonedContent.find( el => el.charType === 'letter' );

  return nextElement ? nextElement.id : undefined;
}


/**
 * Берет массив контента и возвращает его копию 
 * со статусами установленными в null
 *  
 * @param {Array} content массив с контентом
 * @returns {Array} массив контента с обнуленными статусами
 */
export const contentResetStatuses = (content) => {
  return content.map(el => {
    el.status = null;
    return el;
  });
}

/**
 * Получает массив с контентом и айди следующего активного элемента
 * переустанавливает соответствующие статусы
 *  
 * @param {Array} content массив с контентом
 * @param {String} nextCurrentId айди следующего активного элемента
 * @returns {Array} массив контента с новыми статусами
 */
export const contentUpdateStatuses = (content, nextCurrentId) => {
  const nextIndex = content.findIndex(el => el.id === nextCurrentId);
  const nextContent = content.map( (el, i) => {
    if ( (i < nextIndex) || el.charType !== 'letter' ) {
      el.status = null;
    } else if (i === nextIndex) {
      el.status = 'active';
    } else if (i > nextIndex) {
      el.status = 'inactive';
    };

    return el;
  });

  return nextContent;
}