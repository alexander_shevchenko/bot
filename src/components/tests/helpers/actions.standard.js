import { uuid } from 'uuidv4';
import cloneDeep from 'lodash/cloneDeep';

/**
 * Форматирует данные для стандартного блока с экшенами
 * попутно отфильтровывает все символы не принадлежащие к английскому алфавиту (кроме _)
 * 
 * @param {Array} words массив из слов
 * 
 * @returns {Array} массив с отформатированными данными
 * @param {String} content[i].title заголовок экшена
 * @param {String} content[i].hasError тип символа // letter, comma, other
 * @param {String} content[i].id уникальный айди
 */
export const formatInitialActions = (words) => {
  // FIXME: потенциальная проблема, Несмотря на то, что это часть стандарта с 2018 года, 
  // юникодные свойства не поддерживаются в Firefox и Edge.
  // Существует библиотека XRegExp, которая реализует «расширенные» регулярные выражения с кросс-браузерной поддержкой юникодных свойств. 
  // https://learn.javascript.ru/regexp-character-sets-and-ranges
  const actions = words.join('').split('')
    .filter(title => /[\p{Alpha}]/u.test(title))
    .map(title => {
      return {
        title,
        hasError: false,
        id: uuid(),
      }
    });

  return actions;
}

/**
 * Получает массив из экшенов и айди экшена который 
 * нужно пометить как ошибочный
 * Возвращает копию массива экшенов с отмеченным ошибочным элементом
 * 
 * @param {Array} words массив из экшенов
 * 
 * @returns {Array} копия массива экшенов с обновленными данными
 */
export const setErrorOnAction = (actions, id) => {
  const nextActions = cloneDeep(actions);
  const index = nextActions.findIndex(action => action.id === id);
  nextActions[index].hasError = true;
  return nextActions;
}

/**
 * Получает массив из экшенов и обнуляет все ошибки
 * Возвращает копию массива экшенов с обнуленными ошибками
 * 
 * @param {Array} actions массив из экшенов
 * @returns {Array} копия массива экшенов с обнуленными ошибками
 */
export const resetErrors = (actions) => {
  return actions.map(el => {
    el.hasError = false;
    return el;
  });
}

/**
 * Получает массив из экшенов и айди экшена, вырезает экшен с искомым айди
 * Возвращает копию массива экшенов
 * 
 * @param {Array} actions массив из экшенов
 * @param {String} id искомый айди
 * @returns {Array} копия массива экшенов
 */
export const deleteAction = (actions, id) => {
  return actions.filter(el => el.id !== id);
}