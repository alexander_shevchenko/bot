import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';

export const Test = () => {
  const contentRef = useRef(null);
  const orRef = useRef(null);
  const shift = useShiftCalculation(contentRef, orRef);

  return (
    <React.Fragment>
      <div className="test__content__example">
        he wondered if the doctor was being deliberately <span ref={contentRef}><strong>obtuse</strong></span>
      </div>
      <div id="wrapper" ref={orRef} style={{
        position: 'relative',
        left: shift,
      }}>
        <div className="test__content__or">
          <span>or</span>
        </div>
        <div className="test__content__blank">
          <div className="sentence">
            <div className="word --primary">half-baked</div>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

const useShiftCalculation = (contentRef, orRef) => {
  const [shift, setShift] = useState(0);

  useLayoutEffect(() => {
    // подписываюсь
    window.addEventListener('resize', calculateShift);
    // выполняю по умолчанию
    calculateShift();
    // сам метод
    function calculateShift() {
      const contentRect = contentRef.current.getBoundingClientRect();
      const orRect = orRef.current.getBoundingClientRect();
      // расстояние слева до середины блока с подчеркнутым словом
      const p1 = contentRect.left + contentRect.width / 2;
      // расстояние слева до середины блока с "or" (вычитаю свойство left, если есть)
      const p2 = orRect.left + orRect.width / 2 - parseInt( orRef.current.style.left );
      // даю поправку на разницу между расстояними p1 и p2
      setShift( Math.round( p1 -  p2 ) + 'px' );
    }
    // отписываюсь
    return () => {
      window.removeEventListener('resize', calculateShift);
    }
  }, []);
  return shift;
}