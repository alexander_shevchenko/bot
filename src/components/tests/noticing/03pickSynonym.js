import React from 'react';
import { uuid } from 'uuidv4';
import { ContentSentence } from '../common/content.sentence';

// TODO: пока никак взаимодействует с хэндлером успеха, механизм непродуман

/**
 * 
 * @param {Array} words массив из слов, в данном случае не может быть длиннее одного элемента
 * @param {Array} translations массив переводов
 * @param {Array} actions массив частей речи (тут его держать или дергать из родителя?)
 * @param {Number} answerId индекс верной части речи? где это держать?
 * @param {Function} handleSuccess хэндлер успеха
 * 
 * @param {String} isHidden костылек для меня, чтобы легко было скрывать компоненты 
 * // TODO: удалить костыль кругом (класс, вьюха, родительский компонент, эта спека)
 */

const oneWord = [
  {
    title: 'adjective', 
    id: uuid(),
    hasHint: true,
    isHighlighted: false,
    status: 'active',
  }
];

const multipleWords = [
  {
    title: 'annoyingly', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'insensitive', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'or', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'slow', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'to', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'to', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'a', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'a', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'a', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'a', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'a', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'a', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'understand', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'understand', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'understand', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'understand', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: '',
  },
  {
    title: 'understand', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: 'active',
  },
  {
    title: 'understand', 
    id: uuid(),
    hasHint: false,
    isHighlighted: false,
    status: 'inactive',
  }
]


const View = ({
  isDone,
  isHidden,
}) => {
  return (
    <div className={"test " + (isHidden ? 'hidden' : '')}>
      <div className="test__title">Знакомство</div>
      <div className="test__description">Выбери подходящую часть речи из списка внизу</div>
      <div className="test__content">
        <div className={"test__content__success --s " + (isDone ? '' : 'hidden')}>
          <span className="icon-check"></span>
        </div>
        <div className="test__content__blank --only">
          <ContentSentence
            content={multipleWords}
          />
        </div>
      </div>
      <div className="test__footer">
        {/*
        <StandardActions 
          actions={actions}
          onActionClick={onActionClick}
        />
        */}
      </div>
    </div>
  )
}

export class PickSynonym extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: null,
      content: [],
      actions: [],
      isDone: null,
    }
  }

  componentDidMount() {
    
  }


  render() {
    return (
      <View

        //content={this.state.content}
        //actions={this.state.actions}
        //onActionClick={this.onActionClick}
        isDone={this.state.isDone}
        isHidden={this.props.isHidden}
      />
    )
  }
}