import React from 'react';
import cloneDeep from 'lodash/cloneDeep';
import { WordTranslation } from '../common/content.wordTranslation';
import { shuffleArray } from '../../../helpers/shuffleArray';
import { StandardActions } from '../common/actions.standard';
import { ContentChars } from '../common/content.chars';
import { formatInitialContent, pickCurrentChar, contentResetStatuses, contentUpdateStatuses } from '../helpers/content.chars';
import { formatInitialActions, setErrorOnAction, resetErrors, deleteAction, } from '../helpers/actions.standard';

// TODO: пока никак взаимодействует с хэндлером успеха, механизм непродуман

/**
 * Упражнение на noticing, сборка переводов из кубиков (с подсказкой)
 * принимает слово, набор переводов и хэндлер успеха
 * после того как все пользователь пройдет тест — отрабатывает хэндлер
 * 
 * @param {String} words массив из слов, в реальности тут всегда будет только один элемент, даже если это идиома
 * @param {Array} translations массив переводов
 * @param {Function} handleSuccess хэндлер успеха
 * 
 * @param {String} isHidden костылек для меня, чтобы легко было скрывать компоненты 
 * // TODO: удалить костыль кругом (класс, вьюха, родительский компонент, эта спека)
 */

const View = ({
  word,
  content,
  actions,
  onActionClick,
  isDone,
  isHidden,
}) => {
  return (
    <div className={"test " + (isHidden ? 'hidden' : '')}>
      <div className="test__title">Знакомство</div>
      <div className="test__description">Собери перевод слова.</div>
      <div className="test__content">
        <div className={"test__content__success --s " + (isDone ? '' : 'hidden')}>
          <span className="icon-check"></span>
        </div>
        <WordTranslation 
          word={word}
        />
        <div className="test__content__or">
          <span>translates</span>
        </div>
        <div className="test__content__blank">
          <ContentChars 
            content={content}
          />
        </div>
      </div>
      <div className="test__footer">
        <StandardActions 
          actions={actions}
          onActionClick={onActionClick}
        />
      </div>
    </div>
  )
}

export class AssembleTranslations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: null,
      content: [],
      actions: [],
      isDone: null,
    }
  }

  componentDidMount() {
    // готовлю контент
    let content = formatInitialContent(this.props.translations);
    // выбираю текущий активный элемент контент
    let currentId = pickCurrentChar(content);
    // обновляю статусы в контенте
    content = contentUpdateStatuses(content, currentId);
    // готовлю экшены
    let actions = formatInitialActions(this.props.translations);
    console.log(this.props.translations)

    this.setState({
      current: currentId,
      content,
      // + рандомно тасую кнопочки, чтобы не так легко было бездумно набирать
      actions: shuffleArray(actions),
    })
  }

  // логика сдублирована для первых двух упражнений в noticing
  onActionClick = (clickedId, clickedTitle) => {
    let actions = cloneDeep(this.state.actions);
    let content = cloneDeep(this.state.content);
    let prevCurrent = this.state.content.find(el => el.id === this.state.current);
    
    // первым делом убрать все ошибки
    actions = resetErrors(actions);
    
    // правильный ответ?
    if (prevCurrent.char === clickedTitle) {
      // вырезаю нажатую кнопочку из экшенов
      actions = deleteAction(actions, clickedId);
      
      // проверяю, не последняя ли у меня была буква на повестке дня
      const lastRound = actions.length >= 1 ? false : true;
      
      if (lastRound) {
        // если последний раунд, 
        // то просто обнулить все статусы в content
        // и, для порядка, обнулить current
        content = contentResetStatuses(content);

        this.setState({
          current: null,
          content,
          actions,
          // плюс пометить isDone
          isDone: true,
        });
      } else {
        // если не последний раунд —
        // мне нужно в state.current вбить айди следующего элемента в content
        // и перебить все статусы контента соответствующе
        const newCurrentId = pickCurrentChar(content, this.state.current);
        content = contentUpdateStatuses(content, newCurrentId)

        this.setState({
          current: newCurrentId,
          content,
          actions,
        });
      }
    } else {
      // на ошибку
      // найти элемент с нужным индексом и пометить ошибку на нём
      actions = setErrorOnAction(actions, clickedId);
      // обновить state
      this.setState({
        actions,
      });
    }
  }

  render() {
    return (
      <View
        word={this.props.words[0]}
        content={this.state.content}
        actions={this.state.actions}
        onActionClick={this.onActionClick}
        isDone={this.state.isDone}
        isHidden={this.props.isHidden}
      />
    )
  }
}