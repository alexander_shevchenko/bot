import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { paths } from '../consts/paths';
import Dummy from './Dummy.js';
import Add from './Add.js';
import Edit from './Edit.js';
import Cards from './Cards.js';
import Study from './Study.js';
import Lookup from './Lookup.js';
import Settings from './Settings.js';
import References from './_references/';

// Playground for the new exercises
import Study2 from './Study2.js';

const Routes = () => (
  <Switch>
    {/* Playground and references */}
    <Route exact path="/" component={Dummy} />
    <Route path={paths.references} component={References} />
    <Route path={paths.playground} component={Study2} />

    {/* APP itself */}
    <Route path={paths.study} component={Study} />
    <Route path={paths.addCard} component={Lookup} />
    <Route path={paths.addCustomCard} component={Add} />
    <Route path={paths.editCard} component={Edit} />
    <Route path={paths.cards} component={Cards} />
    <Route path={paths.settings} component={Settings} />
  </Switch>
);

export default Routes;
