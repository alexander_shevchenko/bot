import React from 'react';
import { connect } from 'react-redux';
import { ioMessages } from '../actions/consts';
import Dropdown from './misc/dropdown';
import Toggle from './misc/toggle';
import { targetLanguages } from '../consts/languages';
import { postUserAttributes } from '../actions/postUserAttributes';
import Notice from './misc/notice';

import Overlay from './layout/Overlay';
import OverlayAlert from './layout/OverlayAlert';
import Spinner from './misc/spinner';
import Alert from './misc/alert';

const SettingsView = ({
  isFetching,
  isFailure,
  isDone,
  history,

  isTranslationsEnabled,
  toggleTranslationsCheckbox,
  toggleAutoPlayCheckbox,
  isAutoPlayEnabled,
  onFormSubmit,

  available,
  pending,
  other,
  current,
  handler,
  isValid,
}) => {
  return (
    <div className="page page--zero-pp page--flex settings">
      {isFetching && (
        <Overlay>
          <Spinner />
        </Overlay>
      )}
      {isFailure && (
        <OverlayAlert history={history} action="reset">
          {isFailure === ioMessages.connection.connectionError && (
            <Alert
              type="connectionFailed"
              title={ioMessages.connection.connectionError}
              description={ioMessages.connection.connectionErrorSecondary}
            />
          )}
          {isFailure === ioMessages.connection.serverError && (
            <Alert
              type="serverError"
              title={ioMessages.connection.serverError}
              description={ioMessages.connection.serverErrorSecondary}
            />
          )}
          {isFailure === ioMessages.validation.validationError && (
            <Alert
              type="validationError"
              title={ioMessages.validation.validationError}
              description={ioMessages.validation.validationErrorUserSettings}
            />
          )}
        </OverlayAlert>
      )}
      {isDone && (
        <OverlayAlert history={history} action="close">
          <Alert
            type="done"
            title={ioMessages.update.settings}
            description={ioMessages.update.settingsSecondary}
          />
        </OverlayAlert>
      )}

      <div className="settings__form">
        <form action="#">
          <section className="hr">
            <h3 className="--uppercased">Translations</h3>
            <Toggle
              title="Enable translations"
              handleChange={toggleTranslationsCheckbox}
              isChecked={isTranslationsEnabled}
              labelTestId="enableTranslationLabel"
              inputTestId="enableTranslationCheckbox"
            />
            {isTranslationsEnabled && (
              <Dropdown
                available={available}
                pending={pending}
                other={other}
                current={current}
                handler={handler}
                disabled={isTranslationsEnabled ? false : true}
                isValid={isValid}
                data-testid="targetLanguageSelect"
              />
            )}
            {isTranslationsEnabled && current && current.status === 'other' && (
              <Notice
                type="warning"
                title="We’ll let you know when new bilingual dictionaries added."
              />
            )}
          </section>
          <section className="hr">
            <h3 className="--uppercased">Practice</h3>
            <Toggle
              title="Auto-play audio"
              handleChange={toggleAutoPlayCheckbox}
              isChecked={isAutoPlayEnabled}
            />
          </section>
        </form>
      </div>
      <footer className="footer--static">
        <button
          className="btn btn--primary btn--block"
          onClick={onFormSubmit}
          data-testid="submit"
        >
          Save
        </button>
      </footer>
      <div className="settings__border"></div>
    </div>
  );
};

class Settings extends React.Component {
  constructor(props) {
    super(props);

    const current = targetLanguages.find(
      (l) => l.code === props.userSettings.targetLanguage
    );

    this.state = {
      isTranslationsEnabled: props.userSettings.translationEnabled,
      isAutoPlayEnabled: props.userSettings.autoPlayEnabled
        ? props.userSettings.autoPlayEnabled
        : false,
      // dropdown
      available: targetLanguages.filter((l) => l.status === 'available'),
      pending: targetLanguages.filter((l) => l.status === 'pending'),
      other: targetLanguages.find((l) => l.status === 'other'),
      current: current ? current : null,
      isValid: true,
    };
  }

  toggleTranslationsCheckbox = (e) => {
    this.setState({
      isTranslationsEnabled: this.state.isTranslationsEnabled ? false : true,
    });
  };

  toggleAutoPlayCheckbox = (e) => {
    this.setState({
      isAutoPlayEnabled: this.state.isAutoPlayEnabled ? false : true,
    });
  };

  selectHandler = (el) => {
    // el - это сам выбранный язык (либо объект язык, либо строка 'Other')
    this.setState({
      current: el ? el : null,
      isValid: true,
    });
  };

  onFormSubmit = () => {
    const current = this.state.current;

    // если переводы включены, то проверяю форму на валидность (есть ли current элемент)
    // и если невалидно, обламываю
    if (this.state.isTranslationsEnabled && !current) {
      this.setState({
        isValid: false,
      });

      return true;
    }

    // в остальных случаях сабмит
    this.props.postUserAttributes({
      userId: this.props.userId,
      translationEnabled: this.state.isTranslationsEnabled,
      targetLanguage:
        current && this.state.isTranslationsEnabled
          ? this.state.current.code
          : null,
      autoPlayEnabled: this.state.isAutoPlayEnabled,
    });
  };

  render() {
    const { isFetching, isFailure, isDone } = this.props;

    return (
      <SettingsView
        isFetching={isFetching}
        isFailure={isFailure}
        isDone={isDone}
        isTranslationsEnabled={this.state.isTranslationsEnabled}
        toggleTranslationsCheckbox={this.toggleTranslationsCheckbox}
        isAutoPlayEnabled={this.state.isAutoPlayEnabled}
        toggleAutoPlayCheckbox={this.toggleAutoPlayCheckbox}
        onFormSubmit={this.onFormSubmit}
        history={this.props.history}
        // dropdown
        available={this.state.available}
        pending={this.state.pending}
        other={this.state.other}
        current={this.state.current}
        handler={this.selectHandler}
        isValid={this.state.isValid}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isFetching: state.user.isFetching,
    isFailure: state.user.isFailure,
    isDone: state.user.isDone,
    userSettings: state.user.settings,
    userId: state.user.id,
  };
};

Settings = connect(mapStateToProps, { postUserAttributes })(Settings);

export default Settings;
