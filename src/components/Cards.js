import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { fetchAllCards } from '../actions/fetchAllCards';
import { fetchCard } from '../actions/fetchCard';
import { paths } from '../consts/paths';
import { changeChar } from '../actions/cards';
import CardsError from './pages/cards/CardsErrors';
import CardsNav from './pages/cards/CardsNav';
import CardsList from './pages/cards/CardsList';
import CardsSearch from './pages/cards/CardsSearch';
import Overlay from './layout/Overlay';
import Spinner from './misc/spinner';
import Alert from './misc/alert';

class Cards extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
      isRedirect: false,
    };
  }

  componentDidMount() {
    // To avoid fetch on every mount
    if (!this.props.isFetchDone) {
      this.props.fetchAllCards({
        userId: this.props.userId,
      });
    }
  }

  componentDidUpdate() {
    // If there is a card in the store it means we've just fetched it.
    // That means only one thing — the user clicked on a word, we fetched the card
    // and user needs to be redirected to 'edit' component in order to edit the card.
    if (this.props.card) {
      this.setState({
        isRedirect: true,
      });
    }
  }

  handleQueryChange = (e) => {
    this.setState({
      query: e.target.value,
    });
  };

  handleCharClick = (key) => {
    this.props.changeChar(key);
  };

  handleWordClick = (id) => {
    this.props.fetchCard({
      userId: this.props.userId,
      cardId: id,
    });
  };

  render() {
    // If card has been fetched — redirect to 'edit'
    if (this.state.isRedirect) {
      return (
        <Redirect
          push
          to={{
            pathname: paths.editCard,
            state: {
              origin: 'list',
              word: this.props.card,
            },
          }}
        />
      );
    }

    const {
      total,
      list,
      chars,
      activeChar,
      isFetching,
      failureType,
      failureAction,
      history,
    } = this.props;

    const { query } = this.state;

    let wordsToDisplay = [];

    if (total) {
      if (query) {
        // TODO: If query — filtered by query
      } else {
        // No query — filtered by 'activeChar'
        wordsToDisplay = list
          .find((el) => el.key === activeChar)
          .list.sort((a, b) => a.word.localeCompare(b.word));
      }
    }

    return (
      <>
        <div className="page page--zero-pp page--flex page--overflow cards">
          {isFetching && (
            <Overlay>
              <Spinner />
            </Overlay>
          )}
          {failureType && (
            <CardsError
              error={failureType}
              action={failureAction}
              history={history}
            ></CardsError>
          )}
          {total === 0 && (
            <div className="cards__exception">
              {/* TODO: 'add card' handler is missing */}
              <Alert
                type="plain"
                size="small"
                title="No Cards Yet"
                description="You can start adding words using the button below"
                actionTitle="Add card"
                actionHandler={() => true}
                actionType="secondary"
              />
            </div>
          )}
          {total > 0 && (
            <>
              <div className="cards__content">
                <CardsSearch query={query} handler={this.handleQueryChange} />
                <CardsList
                  words={wordsToDisplay}
                  handler={this.handleWordClick}
                />
              </div>

              <CardsNav
                chars={chars}
                activeChar={activeChar}
                handler={this.handleCharClick}
              />
            </>
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isFetching: state.cards.isFetching,
    failureType: state.cards.isFailure ? state.cards.isFailure.type : false,
    failureAction: state.cards.isFailure ? state.cards.isFailure.action : false,

    list: state.cards.list.list,
    chars: state.cards.list.chars,
    activeChar: state.cards.list.activeChar,
    total: state.cards.list.total,
    isFetchDone: state.cards.list.isFetchDone,
    card: state.cards.card,

    userId: state.user.id,
  };
};

Cards = connect(mapStateToProps, { fetchAllCards, fetchCard, changeChar })(
  Cards
);

export default Cards;
