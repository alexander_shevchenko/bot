import React from 'react';

/**
 * Псевдо-ссылка с плюсиком слева
 * 
 * @param {Function} handleOnClick метод который кнопка должна триггерить
 * @param {String} title текст кнопки
 */

const AddCustomBtn = (props) => {
  return (
    <button 
      className='btn btn--add'
      onClick={props.handleOnClick}
    >
      <span className="icon__wrapper icon__wrapper--tertiary">
        <span className="icon__wrapper__container">
          <span className="icon icon--add icon--m icon--secondary"></span>
        </span>
      </span>
      <span className="btn__titleWrapper">{props.title}</span>
    </button>
  )
}

export default AddCustomBtn;