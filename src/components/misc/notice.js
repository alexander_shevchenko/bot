import React from 'react';

/**
 * Notice-v3
 * 
 * @param {String} type info, warning... (если нет === default)
 * @param {String} size small... (если нет === default)
 * @param {String} title текст (обычно короткий, несколько слов)
 * @param {String} extra опциональный экстра-текст (детальнее, до нескольких строчек)
 */

// TODO: type пока дефолтится только в info

function Notice(props) {
  let type = '';
  let size = '';

  switch(props.type) {
    case 'info':
      type = 'notice-v3--info';
      break;
    case 'warning':
      type = 'notice-v3--warning';
      break;
    case 'danger':
      type = 'notice-v3--danger';
      break;
      default:
      break;
  }

  switch(props.size) {
    case 'small':
      size = 'notice-v3--s';
      break;
    default:
      break;
  }

  return (
    <div className={
      'notice-v3 ' 
      + type + ' ' 
      + size + ' '
      + (props.extra ? '--extra ' : '')
    }>
      <div className="notice-v3__topLine-wrapper">
        <span className="notice-v3__icon"></span>
        <span className="notice-v3__title">{props.title}</span>
      </div>
      {props.extra &&
        <div className="notice-v3__extra">{props.extra}</div>
      }
    </div>
  )
}

export default Notice;