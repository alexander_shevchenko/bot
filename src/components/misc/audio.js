import React from 'react';
import { useSpring, animated, interpolate } from 'react-spring';
import Notice from './notice';
import IconAudio from '../svg/audio';
import * as colors from '../../consts/colors';

// https://www.w3schools.com/tags/ref_av_dom.asp
//
// вот тут есть пример аудио-плеера
// подсматриваю
// https://github.com/justinmc/react-audio-player/blob/master/src/index.jsx
// документация про MediaError
// https://developer.mozilla.org/en-US/docs/Web/API/MediaError

/**
 * Обертка над <audio> тегом
 *
 * @param {String} url ссылка на аудио-файл
 * @param {String} transcription транскрипция слова
 * @param {Boolean} autoPlay проиграть один раз аудио на маунт
 */

const AudioView = React.forwardRef((props, ref) => {
  const {
    handlePlay,
    isLoading,
    isError,
    isPlaying,
    transcription,
    isTranscriptionShown,
  } = props;

  const { l, p, t, backgroundColor, borderColor } = useSpring({
    l: isLoading ? 1 : 0,
    t: isTranscriptionShown ? 1 : 0,
    backgroundColor: isPlaying ? '#fff' : colors.$gray10,
    borderColor: isPlaying ? colors.$gray6 : colors.$gray10,
  });

  return (
    <div className="audio">
      <div className="audio__wrapper">
        <div className="audio__content">
          <animated.div
            className="spinner spinner--plain --l"
            style={{
              opacity: l,
              zIndex: l.interpolate((l) => (l ? 3 : -1)),
            }}
          ></animated.div>
          <button className="btn btn--plain btn--air-m" onClick={handlePlay}>
            <animated.span
              className="icon__wrapper icon__wrapper--l"
              style={{
                opacity: l.interpolate([0, 1], [1, 0]),
                backgroundColor,
                borderColor,
              }}
            >
              <span className="icon__wrapper__container">
                <IconAudio />
              </span>
            </animated.span>
          </button>
          <animated.div
            className="audio__transcription"
            style={{
              opacity: t,
              transform: t
                .interpolate([0, 1], [25, 0])
                .interpolate((t) => `translate3d(${t}px, 0, 0)`),
            }}
          >
            {transcription && <span>{transcription}</span>}
          </animated.div>
          <audio ref={ref} type="audio/mpeg"></audio>
        </div>
        {isError && (
          <div className="audio__error">
            <Notice type="warning" title="audio file unavailable" />
          </div>
        )}
      </div>
    </div>
  );
});

class Audio extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isLoaded: false,
      isError: false,
      isPlaying: false,
      isTranscriptionShown: false,
    };
    this.player = React.createRef();
  }

  componentDidMount() {
    this.player.current.addEventListener('error', this.onError);
    this.player.current.addEventListener('canplaythrough', this.onLoad);
    this.player.current.addEventListener('playing', this.onPlaying);
    this.player.current.addEventListener('ended', this.onEnded);
    this.player.current.src = this.props.url;

    // меня примаунтили, значит проверяю, есть ли автоплэй, если есть — играю
    if (this.props.autoPlay) this.handlePlay();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.url !== this.props.url) {
      // если урл сменился, то делаю ресет
      this.setState({
        isLoading: false,
        isLoaded: false,
        isError: false,
        isPlaying: false,
        isTranscriptionShown: false,
      });
      // и записываю новый урл в плеер
      this.player.current.src = this.props.url;
    }
  }

  componentWillUnmount() {
    this.player.current.removeEventListener('error', this.onError);
    this.player.current.removeEventListener('canplaythrough', this.onLoad);
    this.player.current.removeEventListener('playing', this.onPlaying);
    this.player.current.removeEventListener('ended', this.onEnded);
  }

  onLoad = (e) => {
    this.setState({
      isLoading: false,
      isLoaded: true,
    });
  };

  onError = (error) => {
    this.setState({
      isError: true,
    });

    console.log('audio error');
    console.log(error);
  };

  onPlaying = () => {
    this.setState({
      isPlaying: true,
      isTranscriptionShown: true,
    });
  };

  onEnded = () => {
    this.setState({
      isPlaying: false,
      isTranscriptionShown: false,
    });
  };

  handlePlay = (e) => {
    if (e) e.preventDefault();

    // если файл уже загружен, то я сбрасываю ошибки, показываю транскрипцию и затем play
    // если файл еше не загружен, то + isLoading и затем play
    let state = {};
    if (this.state.isLoaded) {
      state = {
        isError: false,
        //isTranscriptionShown: true,
        isLoading: false,
      };
    }
    if (!this.state.isLoaded) {
      state = {
        isError: false,
        //isTranscriptionShown: true,
        isLoading: true,
      };
    }

    this.setState(
      {
        ...state,
      },
      () => {
        // Show loading animation.
        // https://developers.google.com/web/updates/2017/06/play-request-was-interrupted
        const playPromise = this.player.current.play();

        if (playPromise !== undefined) {
          playPromise
            .then((_) => {
              /*this.onPlay();
          console.log('then')*/
            })
            .catch((error) => {
              this.onError(error);
            });
        }
      }
    );
  };

  render() {
    return (
      <AudioView
        ref={this.player}
        handlePlay={this.handlePlay}
        isLoading={this.state.isLoading}
        isError={this.state.isError}
        isPlaying={this.state.isPlaying}
        transcription={this.props.transcription}
        isTranscriptionShown={this.state.isTranscriptionShown}
      />
    );
  }
}

export default Audio;
