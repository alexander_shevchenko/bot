import React from 'react';

// TODO: перепроектировать модуль
// с учетом показа в лукапе и редактировании, несколько модов

/**
 * Отвечает за вывод определения слова в разных форматах (параметр mode)
 * 
 * @param {String} mode формат определения
 * @param {String} title определение
 * @param {Array} examples примеры использования -- опциональны
 * @param {String} domain домен определения -- опционален
 * @param {String} id айди определения -- опционален ТОЛЬКО для presentation режимов
 * @param {Function} handler хэндлер выбора определения
 */

// TODO: перекинуть handler с кнопки на всю панель, как в результатах
// сейчас disabled выключает кнопку, нужно будет решить видимо этот момент в самом хэндлере

const Definition = ({
  mode,
  title,

  examples,
  domains,
  handler,
  id,
}) => {
  // тип карточки, дефолтный/выбранный/компактный
  let modifier;
  let iconWrapper;
  let iconType;
  // кнопка отключена в компактном режиме
  let isDisabled = false;
  
  // домены
  let domainsList = domains && domains.length ? domains : null;

  // примеры
  let examplesList = examples && examples.length ? examples : null;

  // показана ли ссылка изменить
  let isChangeShown = false;

  switch(mode) {
    case 'selected':
      modifier = '--selected';
      iconWrapper = 'icon__wrapper--cyan';
      iconType = 'icon--check icon-check';
      break;
    case 'presentation':
      modifier = '--selected'; // --compact пока убрал
      iconWrapper = 'icon__wrapper--ss icon__wrapper--cyan';
      iconType = 'icon--check icon--check--xs icon-check';
      isDisabled = true;
      break;
    case 'editPresentation': 
      modifier = '--selected'; 
      iconWrapper = 'icon__wrapper--ss icon__wrapper--cyan';
      iconType = 'icon--check icon--check--xs icon-check';
      isDisabled = false;
      isChangeShown = true;
      break;
    default:
      modifier = '';
      iconWrapper = 'icon__wrapper--secondary';
      iconType = 'icon--add icon--m icon--primary';
  }

  return (
    <div 
      className={'definition ' + modifier}
      onClick={isDisabled ? null : (e) => handler(e, id)}
    >
      <div className="definition__contentCol">
        {domainsList && 
          <div className="definition__domains">
            {domainsList.map((domain, index) => {
              return (
                <span key={index}>{domain}</span>
              )
            })}
          </div>
        }
        {title && 
          <h4 className="definition__title">
            {title}
          </h4>
        }
        {examplesList && 
          <ul className="definition__examples">
            {examplesList.map((tokenizedExample, index) => {
              return (
                <li 
                  key={index}
                >
                  {tokenizedExample.map(word => {
                    return (
                      <span 
                        key={word.id}
                        className={word.isActive ? '--active' : ''}
                      >{word.word}</span>
                    )
                  })}
                </li>
              )
            })}
          </ul>
        }
      </div>
      <div className={'definition__btnCol'}>
        {isChangeShown && 
          <button className={'btn btn--link btn--medium btn--pseudo btn--pseudo--dark definition__change'}>
            Update
          </button>
        }
        {!isChangeShown && 
          <button 
            className={'btn btn--plain icon__wrapper icon__wrapper--secondary definition__pick ' + iconWrapper}
            disabled = {isDisabled}
            data-testid='pickDefinitionButton'
          >
            <span className="icon__wrapper__container">
              <span className={'icon ' + iconType}></span>
            </span>
          </button>
        }
      </div>
    </div>
  );
};

export default Definition;