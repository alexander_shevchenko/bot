import React from 'react';


// инпут меняет свой размер в зависимости от количества текста
// 0 - 19 символов — обычный размер
// 19 - 40 символов — увеличенный
const Heroinput = (props) => {
  const { handleChange, value, name, id, placeholder, modifier } = props;
  const testid = props['data-testid'];
  const packed = (typeof value === 'string' && value.length >= 28) ? true : false;
  return (
    <textarea className={
        "hero-input" + (packed ? " hero-input__packed" : "" + modifier ? ` ${modifier}` : "")
      }
      maxLength="120"
      onChange={handleChange}
      value={value}
      id={id}
      name={name}
      placeholder={placeholder}
      data-testid={testid}
    ></textarea>
  );
};

export default Heroinput;