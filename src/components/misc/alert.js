import React from 'react';
import PropTypes from 'prop-types';
import IconCheck from '../svg/check';
import IconAlert from '../svg/alert';
import IconDB from '../svg/db';
import IconDuplicate from '../svg/duplicate';
import IconEmpty from '../svg/empty';
import IconWifi from '../svg/wifi';

/**
 * Renders an alert component
 * (big fat icon + title + description + optional action button with a tiny icon).
 *
 * Supports variety of sizes and styles.
 *
 * @param {String} size — normal, small
 * @param {String} type - plain, done, nothingFound, connectionFailed, serverError, messengerError, validationError, duplicateFound
 * @param {String} title
 * @param {String} description
 * @param {String} actionTitle a title for an optional action button inside the alert
 * @param {Function} actionHandler required if actionTitle
 * @param {String} actionType required if actionTitle — primary, secondary, secondary--gray
 * @returns {Object}
 */

export default function Alert({
  size,
  type,
  title,
  description,
  actionTitle,
  actionHandler,
  actionType,
}) {
  let componentType = getTypeClass(type);
  let componentSize = getSizeClass(size);
  let componentActionType = getButtonClass(actionType);
  let componentActionIconType = getButtonIconClass(actionType);
  let componentPrimaryIcon = getPrimaryIcon(type);

  return (
    <div className={'alert ' + componentSize + ' ' + componentType}>
      {type !== 'plain' && (
        <div className="alert__icon">{componentPrimaryIcon}</div>
      )}

      {title && <div className="alert__primary">{title}</div>}

      <div className="alert__secondary">{description}</div>

      {actionTitle && (
        <div className="alert__action">
          <button
            className={'btn btn--small ' + componentActionType}
            onClick={actionHandler}
          >
            <span
              className={'icon icon--add icon--s ' + componentActionIconType}
            ></span>
            {actionTitle}
          </button>
        </div>
      )}
    </div>
  );
}

Alert.propTypes = {
  size: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  actionTitle: PropTypes.string,
  actionHandler: PropTypes.func,
  actionType: PropTypes.string,
};

Alert.defaultProps = {
  size: 'normal',
  type: 'done',
};

/**
 * Matches user defined 'type' parameter
 * with less extensive range of actual classNames of the component.
 *
 * @param {String} type user defined type
 * @returns {String} a className matching 'type'
 */
function getTypeClass(type) {
  let className;

  switch (type) {
    case 'plain':
      className = 'alert--text';
      break;
    case 'done':
    case 'nothingFound':
      className = 'alert--done';
      break;
    case 'connectionFailed':
    case 'serverError':
    case 'messengerError':
      className = 'alert--error';
      break;
    case 'validationError':
    case 'duplicateFound':
      className = 'alert--warning';
      break;
    default:
      className = '';
  }

  return className;
}

/**
 * Matches user defined 'size' parameter
 * with the range of actual classNames of the component
 *
 * @param {String} size user defined size
 * @returns {String} a className matching 'size'
 */
function getSizeClass(size) {
  let className;

  switch (size) {
    case 'small':
      className = 'alert--s';
      break;
    case 'normal':
    default:
      className = '';
  }

  return className;
}

/**
 * Matches user defined actionType
 * with a corresponding submit button's className.
 *
 * @param {String} actionType user defined actionType
 * @returns {String} a className matching actionType
 */
function getButtonClass(actionType) {
  let className;

  switch (actionType) {
    case 'primary':
      className = 'btn--primary';
      break;
    case 'secondary':
      className = 'btn--secondary';
      break;
    case 'secondary--gray':
      className = 'btn--secondary btn--gray';
      break;
    default:
      className = '';
  }

  return className;
}

/**
 * Matches user defined actionType with
 * a corresponding svg icon's className
 *
 * @param {String} actionType user defined actionType
 * @returns {String} a className matching actionType
 */
function getButtonIconClass(actionType) {
  let className;

  switch (actionType) {
    case 'primary':
      className = 'icon--primary-white';
      break;
    case 'secondary':
      className = 'icon--primary-muted';
      break;
    case 'secondary--gray':
    default:
      className = '';
  }

  return className;
}

/**
 * Matches user defined 'type' with
 * component's primary icon
 *
 * @param {String} type user defined type
 * @returns {Object} specific icon
 */
function getPrimaryIcon(type) {
  let icon;

  switch (type) {
    case 'done':
      icon = <IconCheck />;
      break;
    case 'nothingFound':
      icon = <IconEmpty />;
      break;
    case 'connectionFailed':
      icon = <IconWifi />;
      break;
    case 'serverError':
    case 'messengerError':
      icon = <IconDB />;
      break;
    case 'validationError':
      icon = <IconAlert />;
      break;
    case 'duplicateFound':
      icon = <IconDuplicate />;
      break;
    default:
      icon = null;
  }

  return icon;
}
