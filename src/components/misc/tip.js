import React from 'react';

const Tip = ({
  action,
  actionTitle,
  title,
  primary,
  secondary,
}) => {
  return (
      <div className="tip">
        <div className="tip__content">
          <h2>{title}</h2>
          <p className="tip__primary">{primary}</p>
          <p className="tip__secondary"><small>{secondary}</small></p>
          {/*<div className="tip__checkbox">
            <div className="form-control checkbox checkbox--muted">
              <label>Всё понятно. Не нужно показывать эту подсказку в будущем.
                <input 
                  type="checkbox" 
                  value="regular"
                />
                <span className="checkmark"></span>
              </label>
            </div>
          </div>
          */}
          <button 
            className="btn btn--secondary btn--medium btn--gray"
            onClick={action}
          >{actionTitle}</button>
        </div>
      </div>
    
  );
};

export default Tip;