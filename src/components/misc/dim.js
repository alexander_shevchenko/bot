import React from 'react';
import { useLayoutEffect } from 'react';

/**
 * Затемненная подложка под попапами
 * 
 * @param {Function} handleClose закрывает попап по клику в dim
 */

const Dim = ({
  handleClose
}) => {
  useLayoutEffect(() => {
    // на mount повесить modal-open
    document.body.classList.add('modal-open');
    // на unmount убрать modal-open
    return () => {
      document.body.classList.remove('modal-open');
    }
  });

  return (
    <div 
      className="dim"
      onClick={handleClose}
    ></div>
  )
}

export default Dim;