import React, { Component } from 'react';
import { sortArrayByStringKey } from '../../helpers/sortArray';
import Notice from './notice';

/**
 * Псевдо-селект компонент.
 * Имитирует поведение дропдауна, на клик открывает overlay во весь экран.
 * Пока заточен исключительно под один кейс (у нас всего один select в приложении, в настройках).
 * Принимает списки языков, хэндлер и требуемые флаги, возвращает выбранный язык и тип языка (из какой группы))
 *
 * @param {Array} available список available языков
 * @param {Array} pending список pending языков
 * @param {String} current текущий язык, если есть
 * @param {Function} handler
 * @param {Boolean} disabled отключено ли поле
 * @param {Boolean} isValid валидно ли поле
 *
 * @return
 * @param {String} el - язык
 */

const placeholder = 'Choose Language';
// сколько языков в массиве должно быть, чтобы UI начал разбивать контент на две колонки
const overlayContentMaxToBreak = 5;

// принимает массив значений и максимальное количество после которого
// разбивает контент на два списка
const Content = ({ elements, current, max, handler }) => {
  const content = [];
  if (elements.length > max) {
    const half = Math.ceil(elements.length / 2);
    content.push(elements.slice(0, half));
    content.push(elements.slice(half));
  } else {
    content.push(elements);
  }

  return content.map((list, i) => {
    return (
      <ul key={i}>
        {list.map((el, j) => {
          return (
            <li
              key={j + el}
              className={current && el.code === current.code ? '--active' : ''}
            >
              <span onClick={(e) => handler(el)}>
                {current && el.code === current.code && (
                  <span className="icon__wrapper__container">
                    <span className="icon icon-check icon--check icon--check--xs"></span>
                  </span>
                )}
                {el.title}
              </span>
            </li>
          );
        })}
      </ul>
    );
  });
};

const DropdownOverlay = ({
  available,
  pending,
  other,
  current,
  handler,
  closeHandler,
}) => {
  return (
    <div className="dropdown-overlay" data-testid="dropdownOverlay">
      <div className="dropdown-overlay__wrapper">
        <button
          className="btn btn--plain icon__wrapper icon__wrapper--transparent dropdown-overlay__close"
          onClick={closeHandler}
        >
          <span className="icon__wrapper__container">
            <span className="icon icon--add icon--primary-white icon--l --clear"></span>
          </span>
        </button>
        {available && available.length && (
          <section>
            <h4>Available Languages</h4>
            <div className="dropdown-overlay__content">
              <Content
                elements={available}
                current={current}
                max={overlayContentMaxToBreak}
                handler={handler}
              />
            </div>
          </section>
        )}
        {pending && !!pending.length && (
          <section>
            <h4>We are currently working on</h4>
            <div className="dropdown-overlay__content">
              <Content
                elements={pending}
                current={current}
                max={overlayContentMaxToBreak}
                handler={handler}
              />
            </div>
          </section>
        )}
        <button
          className={
            'btn btn--link --pure-link dropdown-overlay__not-listed ' +
            (current && current.code === 'other' ? '--active' : '')
          }
          onClick={(e) => handler(other)}
        >
          {(current && current.code) === 'other' && (
            <span className="icon__wrapper__container">
              <span className="icon icon-check icon--check icon--check--xs"></span>
            </span>
          )}
          Not listed above
        </button>
      </div>
    </div>
  );
};

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDropdownShown: false,
      current: null,
      // available, pending, other, placeholder
      placeholder,
    };
  }

  componentDidMount() {
    this.setState({
      current: this.props.current,
    });
  }

  onDropdownClose = () => {
    this.setState({
      isDropdownShown: false,
    });
  };

  onSelectClick = () => {
    this.setState({
      isDropdownShown: true,
    });
  };

  onOptionClick = (el) => {
    this.setState({
      current: el,
      isDropdownShown: false,
    });

    this.props.handler(el);
  };

  render() {
    const {
      available,
      pending,
      other,
      isValid,
      disabled,
      'data-testid': testid,
    } = this.props;

    return (
      <React.Fragment>
        <div
          className={
            'form-control required ' +
            (isValid ? '' : 'form-control__invalid') +
            ' ' +
            (disabled ? '--disabled' : '')
          }
        >
          {!isValid && (
            <span className="form-control__errorMsg">
              <Notice type="danger" size="small" title="Required field" />
            </span>
          )}
          <label>
            Target Language
            <span className="form-control__marker"></span>
          </label>
          <div
            className={
              'pseudo-select ' + (this.state.current ? '' : '--placeholder')
            }
            onClick={disabled ? (e) => true : this.onSelectClick}
            data-testid={testid}
          >
            {this.state.current
              ? this.state.current.title
              : this.state.placeholder}
            <span className="icon icon--angle icon--angle-bottom icon--s pseudo-select__angle"></span>
            {this.state.current &&
              (this.state.current.status === 'pending' ||
                this.state.current.status === 'other') && (
                <span className="icon icon-exclamation-triangle pseudo-select__warning"></span>
              )}
          </div>
        </div>
        {this.state.isDropdownShown && (
          <DropdownOverlay
            available={sortArrayByStringKey(available, 'title')}
            pending={sortArrayByStringKey(pending, 'title')}
            other={other}
            current={this.state.current}
            handler={this.onOptionClick}
            closeHandler={this.onDropdownClose}
          />
        )}
      </React.Fragment>
    );
  }
}

export default Dropdown;
