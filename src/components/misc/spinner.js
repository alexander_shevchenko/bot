import React, { useRef } from 'react';
import { useLayoutEffect } from 'react';

/**
 * Renders animated spinner
 */

const Spinner = ({ children }) => {
  // анимация появления тени, использую setTimeout, это ок?
  const spinner = useRef(null);
  useLayoutEffect(() => {
    const timer = setTimeout(() => {
      if (spinner.current) spinner.current.classList.add('--shadow');
    }, 1);
    return () => clearTimeout(timer);
  }, []);

  return (
    <div className="spinner overlay__spinner">
      <div className="spinner__spinner" ref={spinner}>
        <div className="spinner__spinner__inner"></div>
        <div className="spinner__spinner__dot"></div>
      </div>
      {children && <div className="spinner__text">{children}</div>}
    </div>
  );
};

export default Spinner;
