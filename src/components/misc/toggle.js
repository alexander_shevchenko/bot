import React from 'react';

/**
 * IOS style toggle control
 *
 * @param {String} title label text
 * @param {Boolean} isChecked
 * @param {Function} handleChange handles on change
 * @param {String} labelTestId label's test id
 * @param {String} inputTestId input's test id
 * @param {String} theme color theme, white/dark (white - default)
 */

const Toggle = ({
  title,
  isChecked,
  handleChange,
  labelTestId,
  inputTestId,
  theme,
}) => {
  return (
    <div
      className={
        'toggle ' +
        (isChecked ? '--active' : '') +
        ' ' +
        (theme === 'dark' ? '--dark' : '')
      }
    >
      <label data-testid={labelTestId}>
        <span className="toggle__text">{title}</span>
        <div className="toggle__switch">
          <input
            type="checkbox"
            data-testid={inputTestId}
            checked={isChecked}
            onChange={handleChange}
          />
        </div>
      </label>
    </div>
  );
};

export default Toggle;
