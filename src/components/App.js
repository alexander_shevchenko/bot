import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { authentication } from '../actions/authentication';
import Routes from './Routes';
import AppVersion from './pages/app/AppVersion';
import AppErrors from './pages/app/AppErrors';
import Overlay from './layout/Overlay';
import Spinner from './misc/spinner';

class App extends Component {
  render() {
    let { history, userId, isFailure, isFetching } = this.props;

    return (
      <div className="pseudoLayout">
        <div className="container">
          {isFetching && (
            <Overlay>
              <Spinner />
            </Overlay>
          )}
          {isFailure && <AppErrors error={isFailure} history={history} />}
          {userId && <AppVersion user={userId} />}
          {userId && <Routes />}
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.setCSSViewportHeight();
    window.addEventListener('resize', this.setCSSViewportHeight);

    this.props.authentication();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setCSSViewportHeight);
  }

  setCSSViewportHeight() {
    // get viewport height and assign to a css custom property
    document
      .getElementById('root')
      .style.setProperty(
        '--viewportHeight',
        document.documentElement.clientHeight + 'px'
      );
  }
}

const mapStateToPorps = (state) => {
  return {
    isFetching: state.user.isFetching,
    isFailure: state.user.isFailure,
    userId: state.user.id,
  };
};

App = withRouter(connect(mapStateToPorps, { authentication })(App));
export default App;
