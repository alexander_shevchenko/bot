import React from 'react';
import { uuid } from 'uuidv4';
import getAnswers from '../business_logic/getAnswers';
import { connect } from 'react-redux';
import { gradeCard } from '../actions/gradeCard';
import { fetchCards } from '../actions/fetchCards';
import { postDone } from '../actions/postDone';
import { textAnswers } from '../actions/consts';
import { postCardPreferences } from '../actions/postCardPreferences';
import { calcCard } from '../business_logic/updateQueue';
import { ioMessages } from '../actions/consts';
import StudyIntro from './layout/study/intro';
import StudyDone from './layout/study/done';
import StudyWrapper from './layout/study/wrapper';

import Overlay from './layout/Overlay';
import OverlayAlert from './layout/OverlayAlert';
import Spinner from './misc/spinner';
import Alert from './misc/alert';

const StudyView = ({
  isFetching,
  isFetchingDone,
  fetchError,
  fetchEmpty,
  fetchAlreadyDone,
  doneError,
  history,
  stats,
  isFirstDoneEver,
  mode,

  card,
  answers,
  step,

  handleStart,

  handleOnShow,
  handleGradeCard,
  progress,

  isAncillaryShown,
  handleAncillaryShow,
  handleCardSettingsShow,
  handleCardSettingsHide,
  isSettingsShown,
  handleDefFirstToggle,

  targetLanguage,
  frameId,
  autoPlay,
}) => {
  const isFailure = fetchError || fetchEmpty || fetchAlreadyDone || doneError;

  return (
    <div className="page page--flex study">
      {isFetching && (
        <Overlay>
          <Spinner>{isFetchingDone ? 'SAVING YOUR PROGRESS' : null}</Spinner>
        </Overlay>
      )}

      {isFailure && (
        <OverlayAlert history={history} action="close">
          {fetchError === ioMessages.connection.connectionError && (
            <Alert
              type="connectionFailed"
              title={ioMessages.connection.connectionError}
              description={ioMessages.connection.connectionErrorSecondary}
            />
          )}
          {fetchError === ioMessages.connection.serverError && (
            <Alert
              type="serverError"
              title={ioMessages.connection.serverError}
              description={ioMessages.connection.serverErrorSecondary}
            />
          )}
          {fetchEmpty && (
            <Alert
              type="nothingFound"
              title={ioMessages.study.empty}
              description={ioMessages.study.emptySecondary}
            />
          )}
          {fetchAlreadyDone && (
            <Alert
              type="done"
              title={ioMessages.study.alreadyDone}
              description={ioMessages.study.alreadyDoneSecondary}
            />
          )}
          {doneError === ioMessages.connection.connectionError && (
            <Alert
              type="connectionFailed"
              title={ioMessages.connection.connectionError}
              description={ioMessages.connection.connectionErrorSecondary}
            />
          )}
          {doneError === ioMessages.connection.serverError && (
            <Alert
              type="serverError"
              title={ioMessages.connection.serverError}
              description={ioMessages.connection.serverErrorSecondary}
            />
          )}
        </OverlayAlert>
      )}

      {mode === 'intro' && (
        <StudyIntro
          newCards={stats.newToday}
          reviewCards={stats.reviewToday}
          totalCards={stats.totalToday}
          handleStart={handleStart}
        />
      )}
      {mode === 'done' && (
        <StudyDone
          statsTotalWeek={stats.totalWeek}
          statsTotalToday={stats.totalToday}
          statsCurrentStreak={stats.streak}
          statsBestStreak={stats.bestStreak}
          isFirstDoneEver={isFirstDoneEver}
        />
      )}

      {mode === 'card' && (
        <StudyWrapper
          targetLanguage={targetLanguage}
          frameId={frameId}
          progress={progress}
          card={card}
          step={step}
          answers={answers}
          handleGradeCard={handleGradeCard}
          handleOnShow={handleOnShow}
          isAncillaryShown={isAncillaryShown}
          handleAncillaryShow={handleAncillaryShow}
          autoPlay={autoPlay}
          handleCardSettingsShow={handleCardSettingsShow}
          handleCardSettingsHide={handleCardSettingsHide}
          isSettingsShown={isSettingsShown}
          handleDefFirstToggle={handleDefFirstToggle}
        />
      )}
    </div>
  );
};

class Study extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // показывается ли интро
      isIntro: true,
      isAncillaryShown: false,
      isSettingsShown: false,

      // карточка и очередь переехали в стэйт, поскольку
      // я локально меняю настройки карточки
      // остальные связанные с очередью св-ва остались как и были, в props
      queue: props.queue,
      currentCard: props.currentCard,

      // frame id
      // уникальный айди который я генерирую для каждой показываемой карточки
      // то есть даже если одна и таже карточка в очереди покажется больше одного раза
      // у каждого из инстансов будет свой айди
      // используется чтобы ререндерить компонент Actions (с иконками аудио и транскрипции)
      // генерирую первый айди сразу
      // и затем на оценку
      frameId: uuid(),

      step: null, // front > full > front > full ...
    };

    this.handleOnShow = this.handleOnShow.bind(this);
    this.handleGradeCard = this.handleGradeCard.bind(this);
    this.handleTipOpen = this.handleTipOpen.bind(this);
    this.handleTipClose = this.handleTipClose.bind(this);
  }

  componentDidMount() {
    // TODO: компонент хочет чтобы его зарефакторили, интро я вставил относительным костылем, а потом еще будет и outro

    // Данный компонент может маунтиться несколько раз в рамках одной сессии
    // (тренировка > редактирование/удаление > тренировка)
    // В целях оптимизации я не делаю fetch каждый раз на mount
    // я проверяю, был ли уже фетч (проверяя длину очереди в props)
    let { queueLength } = this.props;

    // всего поддержано три сценария
    // 1. основной сценарий — прямой переход к тренировке (в этом сценарии автоматом показывается intro)
    // 2. пользователь из тренировки сходил, отредактировал/удалил карточку и продолжил тренировку (в 2-3 я отключаю руками intro)
    // 3. аналогично сценарию выше, но, пользователь удалил последнюю карточку в тренировке
    // (таким образом, по возвращении, мы должны завершить тренировку)

    // СЦЕНАРИЙ 2 — обычное редактирование/удаление
    if (queueLength) {
      this.setState({
        ...this.state,
        step: 'front',
        isIntro: false,
      });
    } else if (typeof this.props.rawTotal === 'number') {
      // СЦЕНАРИЙ 3 — тренировка была, но пользователь удалил последнюю карточку
      this.setStat({
        isIntro: false,
      });
      this.props.postDone();
    } else {
      // СЦЕНАРИЙ 1 — обычная тренировка
      this.fetchData();
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // реакция на оценку карточки (очередь в сторе обновляется и сюда приходят новые пропс)
    this.setState({
      queue: nextProps.queue,
      currentCard: nextProps.currentCard,
      step: 'front',
      fullView: false,
      audioEverClicked: false,
      audioError: false,
    });
  }

  /* FETCH КАРТОЧЕК И POST ОЦЕНОК */
  /* Fetch карточек */
  fetchData() {
    // в качестве timestamp отправляю 4 утра текущего дня у клиента
    let now = new Date();
    if (now.getHours() < 4) {
      now.setDate(now.getDate() - 1);
    }
    now.setHours(4, 0, 0, 0);

    this.props.fetchCards({
      userId: this.props.userId,
      currentDayStart: now.getTime(),
    });
  }

  /* Post оценки */
  handleGradeCard(id, grade) {
    // генерирую новый айди для фрейма/карточки (смотри описание выше)
    this.setState({
      frameId: uuid(),
    });

    this.props.gradeCard({
      id,
      grade,
      userId: this.props.userId,
      timestamp: Date.now(),
      queue: this.state.queue,
      progress: {
        Lp: this.props.queueLength,
        A: grade,
      },
    });
  }

  /* ШАГИ И КОМПАКТНЫЙ ВИД */
  /* Закрыть intro */
  handleStart = () => {
    this.setState({
      isIntro: false,
    });
  };

  /* Переход к ответу */
  handleOnShow() {
    this.setState({
      fullView: false,
      step: 'full',
      audioEverClicked: false,
      isAncillaryShown: false,
    });
  }

  /* Показать второстепенную информацию */
  handleAncillaryShow = () => {
    // показываю и скрываю, аналогично как транскрипцию (только там завязка на конец ролика)
    this.setState(
      {
        isAncillaryShown: true,
      },
      () => {
        setTimeout(() => {
          this.setState({
            isAncillaryShown: false,
          });
        }, 1250);
      }
    );
  };

  /* НАСТРОЙКИ КАРТОЧКИ */
  /* Показать настройки карточки */
  handleCardSettingsShow = () => {
    this.setState({
      isSettingsShown: true,
    });
  };

  /* Скрыть настройки карточки */
  handleCardSettingsHide = () => {
    this.setState({
      isSettingsShown: false,
    });
  };

  /* Обновить defFirst */
  handleDefFirstToggle = () => {
    /* пока метод заточен только под одну единственную настройку */
    /* станет больше настроек — мутирует в более общий метод */
    /* 
      1. обновляю this.state.currentCard (тогглю defFirst)
      2. обнволяю this.state.queue (беру нулевую карточку и обновляю defFirst аналогично значению в currentCard)
      3. дальше отправляю экшен postCardPreferences (userId, cardId, defFirst: {value})
      экшен уходит в пустоту, локально в компоненте очередь и карточка обновлены,
      когда будет экшен оценки, то обновленные карточка и очередь улетят в экшен и 
      превратятся в новую очередь и будут записаны в стор редюсером
    */

    const current = Object.assign({}, this.state.currentCard);
    current.defFirst = current.defFirst ? false : true;

    this.setState({
      currentCard: current,
      queue: [current, ...this.state.queue.slice(1)],
    });

    this.props.postCardPreferences({
      userId: this.props.userId,
      cardId: current.id,
      defFirst: current.defFirst,
    });
  };

  /* ПОДСКАЗКИ */
  /* Открытие подсказок */
  handleTipOpen(id) {
    // метод показывает подсказку (получая тип подсказки как аргумент)
    // поскольку подсказок больше одной, то я не завязываюсь на простое true/false,
    // а получаю айди той подсказки которую необходимо показать
    this.setState({
      isTip: id,
    });
  }

  /* Скрытие подсказки */
  handleTipClose() {
    this.setState({
      isTip: false,
    });
  }

  render() {
    // mode -- эксперимент, выбираю что показывать — интро, карточку, done
    let mode;
    // если есть isIntro и статистика (по крайней мере total)
    if (this.state.isIntro && this.props.stats.totalToday) mode = 'intro';
    if (
      !this.state.isIntro &&
      !this.props.doneSuccess &&
      this.props.currentCard
    )
      mode = 'card';
    // done на последнем месте, потому что я оставляю последнюю карточку в стэйте на last_grade_request (см редюсер queue в study)
    // карточку я оставляю, чтобы компонент с карточкой не падал в ошибки из-за отсутствия карточки в стэйте на анмаунт
    // (поскольку анмаунт асинхронный и занимает время, а в сторе карточка уже тю-тю)
    // хочешь поправить этот момент?
    // убери анимацию с карточек и раскомментируй код в редюсере
    // АПДЕЙТ — АНИМАЦИИ НЕТ И КОД РАСКОМЕНТИРОВАН
    if (this.props.doneSuccess) mode = 'done';

    // все что ниже -- карточка
    const { step } = this.state;
    const { currentCard } = this.state;

    // создаю пустой массив с кнопками
    const answers = [];

    // получаю список кнопок исходя из статуса карточки (в виде ключей)
    const answersKeys = currentCard ? getAnswers(currentCard.status) : '';

    // если ключи есть, забираю тексты кнопок на локальном языке
    // забираю будущие интервалы
    // наполняю массив кнопок
    if (answersKeys) {
      answersKeys.forEach((key) => {
        const answer = {};
        answer.key = key;
        answer.text = textAnswers[key];
        answer.periodText = calcCard(
          currentCard,
          textAnswers[key]
        ).card.periodText;
        answers.push(answer);
      });
    }

    return (
      <StudyView
        isFetching={this.props.isFetching}
        isFetchingDone={this.props.isFetchingDone}
        fetchError={this.props.fetchError}
        fetchEmpty={this.props.fetchEmpty}
        fetchAlreadyDone={this.props.fetchAlreadyDone}
        doneError={this.props.doneError}
        history={this.props.history}
        stats={this.props.stats}
        isFirstDoneEver={this.props.isFirstDoneEver}
        mode={mode}
        card={currentCard}
        answers={answers}
        step={step}
        handleStart={this.handleStart}
        handleOnShow={this.handleOnShow}
        handleGradeCard={this.handleGradeCard}
        handleTipOpen={this.handleTipOpen}
        handleTipClose={this.handleTipClose}
        progress={this.props.P}
        isTip={this.state.isTip}
        isAncillaryShown={this.state.isAncillaryShown}
        handleAncillaryShow={this.handleAncillaryShow}
        targetLanguage={this.props.userTargetLanguage}
        frameId={this.state.frameId}
        autoPlay={this.props.userAutoPlay}
        handleCardSettingsShow={this.handleCardSettingsShow}
        handleCardSettingsHide={this.handleCardSettingsHide}
        isSettingsShown={this.state.isSettingsShown}
        handleDefFirstToggle={this.handleDefFirstToggle}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isFetching: state.study.isFetching,
    isFetchingDone: state.study.isFetchingDone,
    fetchError: state.study.fetchError,
    fetchEmpty: state.study.fetchEmpty,
    fetchAlreadyDone: state.study.fetchAlreadyDone,
    doneError: state.study.doneError,
    doneSuccess: state.study.doneSuccess,
    isFirstDoneEver: state.study.isFirstDoneEver,
    queue: state.study.queue.active,
    queueLength: state.study.queue.active.length,
    currentCard: state.study.queue.active[0],
    rawTotal: state.study.queue.rawTotal,
    P: state.study.queue.P,
    stats: state.study.stats,
    userId: state.user.id,
    userTargetLanguage: state.user.settings.targetLanguage,
    userAutoPlay: state.user.settings.autoPlayEnabled
      ? state.user.settings.autoPlayEnabled
      : null,
  };
};

Study = connect(mapStateToProps, {
  fetchCards,
  gradeCard,
  postDone,
  postCardPreferences,
})(Study);
export default Study;
