import React from 'react';
import { connect } from 'react-redux';
import { resetFailure } from '../actions/';
import { addCard } from '../actions/addCard';
import Overlay from './layout/Overlay';
import Spinner from './misc/spinner';
import AddErrors from './pages/add/AddErrors';
import AddDone from './pages/add/AddDone';
import AddForm from './pages/add/AddForm';
import AddFooter from './pages/add/AddFooter';

const fieldsInitialState = {
  front: {
    value: '',
    isRequired: true,
    isValid: true,
  },
  definition: {
    value: '',
    isRequired: true,
    isValid: true,
  },
  back: {
    value: '',
    isRequired: true,
    isValid: true,
  },
  example: {
    value: '',
    isRequired: false,
    isValid: true,
  },
};

class Add extends React.Component {
  constructor(props) {
    super(props);

    // TODO: validate case with user coming from Lookup

    // In case user came from Lookup
    const frontValue = props.location.state?.query
      ? props.location.state.query
      : '';

    this.state = {
      isTranslationEnabled: props.userSettings.translationEnabled,
      isFormValid: false,
      fields: Object.assign(
        {},
        {
          ...fieldsInitialState,
          front: {
            ...fieldsInitialState.front,
            value: frontValue,
          },
        }
      ),
    };
  }

  render() {
    const { isFetching, isFailure, isDone, history } = this.props;
    const {
      isTranslationEnabled,
      fields: { front, back, definition, example },
    } = this.state;

    return (
      <div className="page edit page--zero-pb">
        {isFetching && (
          <Overlay>
            <Spinner>Saving Card</Spinner>
          </Overlay>
        )}
        {isFailure && <AddErrors history={history} error={isFailure} />}
        {isDone && <AddDone history={history} handleReset={this.handleReset} />}
        <AddForm
          front={front}
          back={back}
          definition={definition}
          example={example}
          isTranslationEnabled={isTranslationEnabled}
          handleChange={this.handleChange}
        />
        <AddFooter handleSubmit={this.handleSubmit} />
      </div>
    );
  }

  handleChange = (e) => {
    const value = e.target.value.trim();
    const fieldName = e.target.name;

    // update state
    this.setState({
      fields: {
        ...this.state.fields,
        [fieldName]: {
          ...this.state.fields[fieldName],
          value,
        },
      },
    });

    // validate field
    this.validateField(fieldName);
  };

  validateField = (fieldName) => {
    const field = this.state.fields[fieldName];

    // if not required — no need to validate
    if (!field.isRequired) return;

    // if 'back' or 'definition' — validate both (if any has value — both are good)
    if (fieldName === 'back' || fieldName === 'definition') {
      this.setState((state) => {
        const isValid = !!(
          state.fields.back.value || state.fields.definition.value
        );

        return {
          fields: {
            ...state.fields,
            back: {
              ...state.fields.back,
              isValid,
            },
            definition: {
              ...state.fields.definition,
              isValid,
            },
          },
        };
      });

      return;
    }

    // any other field
    this.setState((state) => ({
      fields: {
        ...state.fields,
        [fieldName]: {
          ...state.fields[fieldName],
          isValid: !!state.fields[fieldName].value,
        },
      },
    }));
  };

  handleSubmit = () => {
    // Force validate all fields
    Object.keys(this.state.fields).forEach((fieldName) =>
      this.validateField(fieldName)
    );

    // Check if every field is valid
    this.setState(
      (state) => {
        return {
          isFormValid: Object.values(state.fields).every(
            (field) => field.isValid
          ),
        };
      },
      () => {
        // Send data
        if (this.state.isFormValid) {
          this.props.addCard({
            type: 'custom',
            userId: this.props.userId,
            timestamp: Date.now(),
            front: this.state.fields.front.value,
            back: this.state.fields.back.value,
            definition: this.state.fields.definition.value,
            example: this.state.fields.example.value,
          });
        }
      }
    );
  };

  handleReset = () => {
    this.setState(
      // Reset state
      {
        isFormValid: false,
        fields: Object.assign({}, fieldsInitialState),
      },
      // Reset isDone in redux store
      this.props.resetFailure
    );
  };
}

const mapStateToProps = (state) => {
  return {
    isFetching: state.addCustom.isFetching,
    isFailure: state.addCustom.isFailure,
    isDone: state.addCustom.isDone,

    userId: state.user.id,
    userSettings: state.user.settings,
  };
};

Add = connect(mapStateToProps, { addCard, resetFailure })(Add);

export default Add;
