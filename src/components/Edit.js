import React from 'react';
import Heroinput from './misc/heroinput';
import cloneDeep from 'lodash/cloneDeep';
import SingleTextareaTitle from './layout/overlayForm/singleTextareaTitle';
import { connect } from 'react-redux';
import { editCard } from '../actions/editCard';
import { deleteCard } from '../actions/deleteCard';
import { resetFailure } from '../actions/';
import { ioMessages } from '../actions/consts';
import { Link } from 'react-router-dom';
import { paths } from '../consts/paths';
import Dim from './misc/dim';
import Notice from './misc/notice';

import Overlay from './layout/Overlay';
import OverlayAlert from './layout/OverlayAlert';
import Alert from './misc/alert';
import Spinner from './misc/spinner';

const Confirm = ({ handleCancel, handleDelete }) => {
  return (
    <div className="actions">
      <Dim handleClose={handleCancel} />
      <div className="popup popup--confirmation">
        <div className="popup__section">
          <h6>Delete card?</h6>
          <ul>
            <li>
              <button
                className="btn btn--link btn--block"
                onClick={handleCancel}
                data-testid="confirmCancel"
              >
                <strong>Cancel</strong>
              </button>
            </li>
            <li>
              <button
                className="btn btn--link btn--red btn--block"
                onClick={handleDelete}
                data-testid="confirmSubmit"
              >
                Delete
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

// TODO: в оверлей просто отдавать resetForm (когда isDone)

const EditView = ({
  isFetching,
  isFailure,
  isDone,

  history,
  origin,

  handleOnSubmit,
  handleOnChange,

  mode,

  front,
  back,
  definition,
  example,

  domains,
  synonym,
  antonym,
  hint,
  part,

  handleExampleCancel,
  handleExampleSubmit,
  handlePicker,
  isPickerShown,

  isTranslationEnabled,

  isConfirmShown,
  handleConfirmShow,
  handleConfirmHide,
  handleDelete,
}) => {
  return (
    <div className="page edit page--zero-pb">
      {isConfirmShown && (
        <Confirm handleCancel={handleConfirmHide} handleDelete={handleDelete} />
      )}
      {isFetching && (
        <Overlay>
          <Spinner />
        </Overlay>
      )}
      {isFailure && (
        <OverlayAlert history={history} action="reset">
          {isFailure === ioMessages.connection.connectionError && (
            <Alert
              type="connectionFailed"
              title={ioMessages.connection.connectionError}
              description={ioMessages.connection.connectionErrorSecondary}
            />
          )}
          {isFailure === ioMessages.connection.serverError && (
            <Alert
              type="serverError"
              title={ioMessages.connection.serverError}
              description={ioMessages.connection.serverErrorSecondary}
            />
          )}
          {isFailure === ioMessages.validation.duplicate && (
            <Alert
              type="duplicateFound"
              title={ioMessages.validation.duplicate}
              description={ioMessages.validation.duplicateWordSecondary}
            />
          )}
          {isFailure === ioMessages.validation.validationError && (
            <Alert
              type="validationError"
              title={ioMessages.validation.validationError}
              description={ioMessages.validation.validationErrorSecondary}
            />
          )}
        </OverlayAlert>
      )}
      {isDone && (
        <OverlayAlert
          history={history}
          action="router"
          pathname={
            origin === 'study' ? paths.study : paths.cards // origin === 'study' || 'list'
          }
        >
          <Alert
            type="done"
            title={
              isDone === 'edit'
                ? ioMessages.update.updated
                : ioMessages.update.deleted
            }
            description={
              isDone === 'edit'
                ? ioMessages.update.updatedSecondary
                : ioMessages.update.deletedSecondary
            }
          />
        </OverlayAlert>
      )}

      <div className="edit__form">
        <div className="btn--back">
          <Link
            className="btn btn--link"
            to={{
              pathname: origin === 'study' ? paths.study : paths.cards,
            }}
          >
            <span className="icon icon--angle icon--angle-left icon--primary2"></span>
            Back
          </Link>
        </div>
        {mode === 'custom' && (
          <form action="#">
            <div
              className={
                'form-control form-control--emphasized required ' +
                (front.valid ? '' : 'form-control__invalid')
              }
              data-testid="frontParent"
            >
              {!front.valid && (
                <span className="form-control__errorMsg">
                  <Notice type="danger" size="small" title="Required field" />
                </span>
              )}
              <label htmlFor="front">
                Word
                <span className="form-control__marker"></span>
              </label>
              <Heroinput
                id="front"
                name="front"
                value={front.value}
                handleChange={handleOnChange}
                data-testid="frontField"
              />
            </div>
            {!isTranslationEnabled && (
              <>
                <div
                  className={
                    'form-control form-control--emphasized required ' +
                    (definition.valid ? '' : 'form-control__invalid')
                  }
                  data-testid="definitionParent"
                >
                  {!definition.valid && (
                    <span className="form-control__errorMsg">
                      <Notice
                        type="danger"
                        size="small"
                        title="Required field"
                      />
                    </span>
                  )}
                  <label htmlFor="back">
                    Definition
                    <span className="form-control__marker"></span>
                  </label>
                  <textarea
                    value={definition.value}
                    name="definition"
                    id="definition"
                    maxLength="200"
                    onChange={handleOnChange}
                    data-testid="definitionField"
                  ></textarea>
                </div>
              </>
            )}
            {isTranslationEnabled && (
              <>
                <div
                  className={
                    'form-control form-control--emphasized required ' +
                    (back.valid ? '' : 'form-control__invalid')
                  }
                  data-testid="backParent"
                >
                  {!back.valid && (
                    <span className="form-control__errorMsg">
                      <Notice
                        type="danger"
                        size="small"
                        title="At least one required"
                      />
                    </span>
                  )}
                  <label htmlFor="back">
                    Translation
                    <span className="form-control__marker"></span>
                    {back.valid && (
                      <span className="form-control__at_least_one">
                        at least one
                      </span>
                    )}
                  </label>
                  <Heroinput
                    id="back"
                    name="back"
                    value={back.value}
                    handleChange={handleOnChange}
                    data-testid="backField"
                  />
                </div>
                <div
                  className={
                    'form-control form-control--emphasized required ' +
                    (definition.valid ? '' : 'form-control__invalid')
                  }
                  data-testid="definitionParent"
                >
                  {!definition.valid && (
                    <span className="form-control__errorMsg">
                      <Notice
                        type="danger"
                        size="small"
                        title="At least one required"
                      />
                    </span>
                  )}
                  <label htmlFor="back">
                    Definition
                    <span className="form-control__marker"></span>
                    {definition.valid && (
                      <span className="form-control__at_least_one">
                        at least one
                      </span>
                    )}
                  </label>
                  <textarea
                    value={definition.value}
                    name="definition"
                    id="definition"
                    maxLength="200"
                    onChange={handleOnChange}
                    data-testid="definitionField"
                  ></textarea>
                </div>
              </>
            )}
            <div className="form-control">
              <label htmlFor="example">
                EXAMPLE
                <span className="form-control__marker"></span>
              </label>
              <textarea
                value={example.value}
                name="example"
                id="example"
                maxLength="150"
                onChange={handleOnChange}
                data-testid="exampleField"
              ></textarea>
              <p className="form-control__note">
                <strong>E.g.</strong> “She reached in and pulled out a{' '}
                <b>solar</b> map, with one star highlighted.”
              </p>
            </div>
          </form>
        )}
        {mode === 'oxford' && (
          <form action="#">
            <h1>
              <span>{front}</span>
              {part && <span className={'pill pill--' + part}>{part}</span>}
            </h1>
            <section className="edit__definition">
              <button
                className="btn btn--plain icon__wrapper edit__definition__edit"
                onClick={handlePicker}
              >
                <span className="icon__wrapper__container">
                  <span className="icon icon-pencil"></span>
                </span>
              </button>
              {domains && domains.length && (
                <div className="edit__definition__domains">
                  {domains.map((domain, index) => {
                    return <span key={index}>{domain}</span>;
                  })}
                </div>
              )}
              <div className="edit__definition__def">{definition}</div>
              {example.value && (
                <div className="edit__definition__example">{example.value}</div>
              )}
            </section>
            <div>
              <div className="form-control">
                <label htmlFor="example">Synonym</label>
                <Heroinput
                  id="synonym"
                  name="synonym"
                  value={synonym.value}
                  handleChange={handleOnChange}
                  data-testid="synonym"
                />
              </div>
              <div className="form-control">
                <label htmlFor="antonym">Antonym</label>
                <Heroinput
                  id="antonym"
                  name="antonym"
                  value={antonym.value}
                  handleChange={handleOnChange}
                  data-testid="antonym"
                />
              </div>
            </div>
            {isTranslationEnabled && (
              <div className="form-control">
                <label htmlFor="back">Translation</label>
                <Heroinput
                  id="back"
                  name="back"
                  value={back.value}
                  handleChange={handleOnChange}
                  data-testid="back"
                />
              </div>
            )}
            <div className="form-control">
              <label htmlFor="hint">Hint</label>
              <Heroinput
                id="hint"
                name="hint"
                value={hint.value}
                handleChange={handleOnChange}
                data-testid="hint"
              />
            </div>
          </form>
        )}
      </div>
      <footer className="footer--static">
        <button
          className="btn btn--primary btn--block"
          onClick={handleOnSubmit}
          data-testid="submit"
        >
          Update
        </button>
        <button
          className="btn btn--red btn--block"
          onClick={handleConfirmShow}
          data-testid="deleteSubmit"
        >
          Delete
        </button>
      </footer>
      {isPickerShown && (
        <SingleTextareaTitle
          title={definition}
          label="Example"
          value={example.value ? example.value : null}
          submitTitle="Continue"
          handleOnCancel={handleExampleCancel}
          handleOnSubmit={handleExampleSubmit}
        />
      )}
    </div>
  );
};

class Edit extends React.Component {
  constructor(props) {
    super(props);
    const data = cloneDeep(props.location.state.word);

    this.state = {
      // искомое слово пришедшее из state
      data,

      // включен ли перевод у пользователя (скрываю поле translation у oxford-карточки)
      isTranslationEnabled: props.userSettings.translationEnabled
        ? props.userSettings.translationEnabled
        : null,

      // откуда пришло слово — study/list
      origin: props.location.state.origin,

      // режим в котором работает модуль — custom/oxford
      mode: data.type,

      // picker
      isPickerShown: false,

      // показать конфирмейшен удаления
      isConfirmShown: false,

      /* поля кастомной карточки */
      custom: {
        front: {
          value: data.front ? data.front : '',
          touched: false,
          isRequired: true,
          valid: true,
        },
        back: {
          value: data.back ? data.back : '',
          touched: false,
          isRequired: true,
          valid: true,
        },
        definition: {
          value: data.definition ? data.definition : '',
          touched: false,
          isRequired: true,
          valid: true,
        },
        example: {
          value: data.example ? data.example : '',
          touched: false,
          isRequired: false,
          valid: true,
        },
      },

      /* поля oxford-карточки */
      oxford: {
        example: {
          value: data.example ? data.example : '',
          touched: false,
          isRequired: false,
          valid: true,
        },
        synonym: {
          value: data.synonym ? data.synonym : '',
          touched: false,
          isRequired: false,
          valid: true,
        },
        antonym: {
          value: data.antonym ? data.antonym : '',
          touched: false,
          isRequired: false,
          valid: true,
        },
        back: {
          value: data.back ? data.back : '',
          touched: false,
          isRequired: false,
          valid: true,
        },
        hint: {
          value: data.hint ? data.hint : '',
          touched: false,
          isRequired: false,
          valid: true,
        },
      },
    };
  }

  componentDidMount() {}

  /* ВВОД */
  /* Ввод в любое поле */
  handleOnChange = (e) => {
    // записываю в стэйт изменения инпутов
    // правила, редактирование —
    // если поле было раньше заполнено, то на change или blur — если пустое -- ошибка
    // если поле было пустым, пользователь сделал фокус, никогда не менял value и заблюрил -- нет ошибки
    // если поле было пустым, пользователь сделал фокус, изменял value и заблюрил - если пустое -- ошибка
    // в итоге пока все сводится к простой проверке на наличие value на onChange
    // subcase — если включены переводы, то валидность меняется одновременно у пары полей Translation/Definition
    const value = e.target.value;
    const field = e.target.name;
    const mode = this.state.mode;
    const fields = cloneDeep(this.state[mode]);

    // у кастомной карточки есть обязательные поля и валидация
    if (mode === 'custom') {
      // если переводы отключены то все ок, обычный режим
      if (!this.state.isTranslationEnabled) {
        updateField();
      }

      // если переводы включены, то, если поле translation или definition, то валидириую их оба одновременно
      if (this.state.isTranslationEnabled) {
        // если это не перевод и не определение (остается только back)
        if (field === 'front') {
          updateField();
        } else {
          // обновляю value и валидность искомого поля (перевод/определение)
          updateField();
          // скопом ставлю в такое же положение валидность второго поля (перевод/определение)
          updateGroup();
        }
      }
    } else if (mode === 'oxford') {
      updateField();
    }

    this.setState({
      [mode]: fields,
    });

    function updateField() {
      fields[field] = Object.assign(fields[field], {
        value: value,
        touched: true,
        valid: !!value,
      });
    }

    function updateGroup() {
      // если у одного из полей есть value, то оба поля валидны,
      // если у обоих нет value, то невалидны
      let isValid =
        fields['back'].value || fields['definition'].value ? true : false;

      fields['back'].valid = isValid;
      fields['definition'].valid = isValid;
    }
  };

  /* ВАЛИДАЦИЯ */
  // валидация на моей стороне необходима только для кастомных карточек
  // обязательные оксфорд-карточки не редактируемы

  /* Валидация отдельного поля */
  forceValidate = (fieldname, mode) => {
    // валидирует поле принудительно
    // изменяет state
    const field = this.state[mode][fieldname];
    this.setState({
      [field]: Object.assign(field, {
        valid: !!field.value.trim(),
      }),
    });
  };

  /* Валидация группы полей */
  forceValidateGroup = (f1, f2, mode) => {
    // валидирует пару полей принудительно
    // одно из полей должно быть валидно, тогда все валидны
    // изменяет state
    const field1 = this.state[mode][f1];
    const field2 = this.state[mode][f2];

    // если у одного из полей есть value, то оба поля валидны,
    // если у обоих нет value, то невалидны
    let isValid = field1.value.trim() || field2.value.trim() ? true : false;

    this.setState({
      [field1]: Object.assign(field1, {
        valid: isValid,
      }),
      [field2]: Object.assign(field2, {
        valid: isValid,
      }),
    });
  };

  /* Проверить валидность формы */
  isFormValid = () => {
    // итерирует по required полям
    // валидирует их и
    // возвращает true или false
    // в реальности нет необходимости итерировать оксфорд-карточку
    // все required поля у неё нередактируемы
    const mode = this.state.mode;

    // если оксфорд — значит все здорово и форма всегда валидна
    if (mode === 'oxford') {
      return true;
    }

    // если кастомная, то поля могут быть невалидны
    // кроме этого, есть еще два сабкейса —
    // переводы включены (front и либо back либо definition обязательны)
    // переводы отключены (front и definition обязательны)
    if (this.state.isTranslationEnabled) {
      // валидирую поля принудительно
      this.forceValidate('front', mode);
      this.forceValidateGroup('back', 'definition', mode);

      // проверяю на валидность
      // и возвращаю true/false
      if (
        this.state[mode].front.valid &&
        this.state[mode].back.valid &&
        this.state[mode].definition.valid
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      // валидирую поля принудительно
      this.forceValidate('front', mode);
      this.forceValidate('definition', mode);

      // проверяю на валидность
      // и возвращаю true/false
      if (this.state[mode].front.valid && this.state[mode].definition.valid) {
        return true;
      } else {
        return false;
      }
    }
  };

  /* МОДАЛЬНОЕ С ПРИМЕРОМ */
  /* Ввод примера, сабмит */
  handleExampleSubmit = (e, value) => {
    e.preventDefault();

    // просто получаю значение
    const mode = this.state.mode;
    const fields = cloneDeep(this.state[mode]);

    fields.example = Object.assign(fields.example, {
      value: value,
      touched: true,
      valid: !!value,
    });

    this.setState({
      [mode]: fields,
      isPickerShown: false,
    });
  };

  /* Ввод примера, cancel */
  handleExampleCancel = (e) => {
    e.preventDefault();
    this.setState({
      isPickerShown: false,
    });
  };

  /* ПОКАЗ ЭКСТРА ПОЛЕЙ */
  /* Handler окошка с примером */
  handlePicker = (e) => {
    e.preventDefault();

    this.setState({
      isPickerShown: true,
    });
  };

  /* ОСНОВНЫЕ ЭКШЕНЫ */
  /* Сабмит формы */
  handleOnSubmit = () => {
    // на сабмит валидирую все поля принудительно
    const isValid = this.isFormValid();
    const mode = this.state.mode;

    if (isValid) {
      // кастомная карточка
      if (mode === 'custom') {
        this.props.editCard({
          type: mode,
          userId: this.props.userId,
          timestamp: Date.now(),
          cardId: this.state.data.id,

          front: this.state[mode].front.value.trim(),
          back: this.state[mode].back.value.trim(),
          definition: this.state[mode].definition.value.trim(),
          example: this.state[mode].example.value.trim(),
        });
      }
      // оксфорд-карточка
      if (mode === 'oxford') {
        this.props.editCard({
          type: mode,
          userId: this.props.userId,
          timestamp: Date.now(),
          cardId: this.state.data.id,
          defId: this.state.data.defId,

          example: this.state[mode].example.value.trim(),
          synonym: this.state[mode].synonym.value.trim(),
          antonym: this.state[mode].antonym.value.trim(),
          back: this.state[mode].back.value.trim(),
          hint: this.state[mode].hint.value.trim(),
        });
      }
    }
  };

  /* УДАЛЕНИЕ */
  /* Показать конфирм */
  handleConfirmShow = () => {
    this.setState({
      isConfirmShown: true,
    });
  };
  /* Спрятать конфирм */
  handleConfirmHide = () => {
    this.setState({
      isConfirmShown: false,
    });
  };
  /* Удалить карточку */
  handleDelete = () => {
    this.props.deleteCard({
      userId: this.props.userId,
      cardId: this.state.data.id,
    });

    this.setState({
      isConfirmShown: false,
    });
  };

  render() {
    if (this.state.mode === 'custom') {
      return (
        <EditView
          isFetching={this.props.isFetching}
          isFailure={this.props.isFailure}
          isDone={this.props.isDone}
          history={this.props.history}
          origin={this.state.origin}
          handleOnChange={this.handleOnChange}
          handleOnSubmit={this.handleOnSubmit}
          mode={this.state.mode}
          example={this.state.custom.example}
          back={this.state.custom.back}
          front={this.state.custom.front}
          definition={this.state.custom.definition}
          isTranslationEnabled={this.state.isTranslationEnabled}
          isConfirmShown={this.state.isConfirmShown}
          handleConfirmShow={this.handleConfirmShow}
          handleConfirmHide={this.handleConfirmHide}
          handleDelete={this.handleDelete}
        />
      );
    }

    if (this.state.mode === 'oxford') {
      return (
        <EditView
          isFetching={this.props.isFetching}
          isFailure={this.props.isFailure}
          isDone={this.props.isDone}
          history={this.props.history}
          origin={this.state.origin}
          handleOnChange={this.handleOnChange}
          handleOnSubmit={this.handleOnSubmit}
          handleExampleCancel={this.handleExampleCancel}
          handleExampleSubmit={this.handleExampleSubmit}
          handlePicker={this.handlePicker}
          mode={this.state.mode}
          definition={this.state.data.definition}
          domains={this.state.data.domains}
          example={this.state.oxford.example}
          synonym={this.state.oxford.synonym}
          antonym={this.state.oxford.antonym}
          back={this.state.oxford.back}
          hint={this.state.oxford.hint}
          part={this.state.data.part}
          front={this.state.data.front}
          isPickerShown={this.state.isPickerShown}
          isTranslationEnabled={this.state.isTranslationEnabled}
          isConfirmShown={this.state.isConfirmShown}
          handleConfirmShow={this.handleConfirmShow}
          handleConfirmHide={this.handleConfirmHide}
          handleDelete={this.handleDelete}
        />
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isFetching: state.edit.isFetching,
    isFailure: state.edit.isFailure ? state.edit.isFailure.type : false,

    isDone: state.edit.isDone,
    userId: state.user.id,
    userSettings: state.user.settings,
  };
};

Edit = connect(mapStateToProps, { editCard, deleteCard, resetFailure })(Edit);

export default Edit;
