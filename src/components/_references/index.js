import React from 'react';
import Definition from '../misc/definition';

const References = () => {
    
  return (
    <div className="references">
      <section>
        <h2>Кнопки-буквы</h2>
        <button className="letter-btn --primary">o</button>
        <button className="letter-btn --primary --muted">o</button>
        <button className="letter-btn --warning">o</button>
      </section>
      <section>
        <h2>Буквы</h2>
        <div className="letter --primary">o</div>
        <div className="letter --primary --active">o</div>
        <div className="letter --primary --inactive">o</div>
        <div className="letter --warning">o</div>
        <div className="letter --warning --active">o</div>
        <div className="letter --error">o</div>
      </section>
      <section>
        <h2>Кнопки-слова</h2>
        <button className="word-btn --primary">
          adverb
        </button>
        &nbsp;
        <button className="word-btn --primary --bold">
          adverb
        </button>
        &nbsp;
        <button className="word-btn --warning">
          adverb
        </button>
      </section>
      <section>
        <h2>Слова</h2>
        <div className="word --primary">
          adjective
        </div>
        <div className="word --primary --bold">
          adjective
        </div>
        <div className="word --primary --active">
          adjective
        </div>
        <div className="word --primary --active --bold">
          adjective
        </div>
        <div className="word --inactive">
          adjective
        </div>
        <div className="word --inactive --bold">
          adjective
        </div>
        <div className="word --primary --clean-right">
          adjective
        </div>
        <div className="word --primary --clean-left">
          adjective
        </div>
        <div className="word --primary --clean-both">
          adjective
        </div>
        <div className="word --primary --toothy-right">
          adjective
        </div>
        <div className="word --primary --toothy-left">
          adjective
        </div>
        <div className="word --primary --toothy-both">
          adjective
        </div>
        <div className="word --primary --clean-left --toothy-right">
          adjective
        </div>
        <div className="word --primary --clean-right --toothy-left">
          adjective
        </div>
      </section>

      <section>
        <h2>Элементы форм</h2>
        <div className="form-control checkbox">
          <label>a fast car
            <input 
              type="checkbox"
              name="check01"
            />
            <span className="checkmark"></span>
          </label>
        </div>
        <div className="form-control radio">
          <label>a fast car
            <input 
              type="radio"
              value="ex01"
              name="example"
            />
            <span className="checkmark"></span>
          </label>
        </div>
        <div className="form-control radio radio--inverted">
          <label>a fast car
            <input 
              type="radio"
              value="ex02"
              name="example"
            />
            <span className="checkmark"></span>
          </label>
        </div>
        <div className="form-control checkbox checkbox--inverted">
          <label>a fast car
            <input 
              type="checkbox"
              name="check02"
            />
            <span className="checkmark"></span>
          </label>
        </div>
        <div className="form-control checkbox checkbox--l checkbox--muted">
          <label>a fast car
            <input 
              type="checkbox"
              name="check03"
            />
            <span className="checkmark"></span>
          </label>
        </div>
      </section>
      <section>
        <h2>Определения</h2>
        <Definition
            mode="selectedCompact"
            title="a false or misleading charge meant to harm someone's reputation"
            examples={
              [
                'casting aspersions on her integrity',
              ]
            }
          />
          <Definition
            mode="selected"
            title="a false or misleading charge meant to harm someone's reputation"
            examples={
              [
                'casting aspersions on her integrity',
              ]
            }
          />
          <Definition
            title="a false or misleading charge meant to harm someone's reputation"
            examples={
              [
                'casting aspersions on her integrity',
              ]
            }
          />
          <Definition
            title="moving or able to move quickly"
            domain="of a clock or watch"
            examples={
              [
                'She\'s a very fast runner.',
                'a fast car',
                'a fast pitch',
                'maintaining a fast pace',
                'blazingly/blindingly fast',
              ]
            }
          />
      </section>
      <section>
        <footer className="footer--static">
          <button 
            className="btn btn--primary btn--block" 
          >
            Продолжить
          </button>
          <button 
            className="btn btn--link btn--medium btn--pseudo footer__cancel"
          >
            Назад
          </button>
        </footer>
        <footer className="footer--static">
          <button 
            className="btn btn--secondary btn--block" 
          >
            Продолжить
          </button>
          <button 
            className="btn btn--link btn--medium btn--pseudo footer__cancel"
          >
            Назад
          </button>
        </footer>
      </section>
    </div>
  );
};

export default References;