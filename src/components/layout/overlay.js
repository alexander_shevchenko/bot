import React from 'react';

export default class Overlay extends React.Component {
  render() {
    return (
      <div
        className="overlay"
        data-testid={this.props.testId ? this.props.testId : null}
      >
        {this.props.children}
      </div>
    );
  }

  componentDidMount() {
    document.body.classList.add('modal-open');
  }

  componentWillUnmount() {
    document.body.classList.remove('modal-open');
  }
}
