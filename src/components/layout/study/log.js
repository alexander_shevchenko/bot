import React, { useRef } from 'react';
import { useSpring, animated } from 'react-spring';

/**
 * Лог оценов в карточке
 *
 * @param {Object} data
 * @param {Boolean} isOpen
 */

const Log = ({ data, isOpen }) => {
  /* Высота блока */
  /* анимирую от нуля до размера контента */
  const contentRef = useRef();
  const contentHeight = contentRef.current
    ? contentRef.current.clientHeight
    : 0;

  /* Cпринг */
  /* анимирую height и marginBottom */
  const { h, m } = useSpring({
    h: isOpen ? contentHeight : 0,
    m: isOpen ? 16 : 0,
  });

  /* Перевариваю входящие данные */
  /* объект в массив, обратная сортировка, таймстемпы в читабельную дату */
  const formattedData = [];
  Object.keys(data)
    .reverse()
    .forEach((key) => {
      const date = new Date(+key);
      const formatted = date.toString().slice(4, 15);
      formattedData.push({
        date: formatted,
        title: data[key],
      });
    });

  return (
    <animated.div
      className="log"
      style={{
        marginBottom: m,
        height: h,
      }}
    >
      <div className="log__wrapper" ref={contentRef}>
        <dl className="--flex --reversed">
          {formattedData.map((el, i) => {
            return (
              <React.Fragment key={i}>
                <dt>{el.date}</dt>
                <dd>{el.title}</dd>
              </React.Fragment>
            );
          })}
        </dl>
      </div>
    </animated.div>
  );
};

export default Log;
