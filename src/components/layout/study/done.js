import React, { useRef } from 'react';
import { animated, useChain, useSpring } from 'react-spring';
import { ioMessages } from '../../../actions/consts';
import Week from './week';
import AppearanceTop from '../../animations/appearanceTop';
import AppearanceOpacity from '../../animations/appearanceOpacity';
import Alert from '../../misc/alert';
import OnFire from './onfire';
import Streak from './streak';

/**
 * View шага Done в тренировке
 *
 * @param {Boolean} isFirstDoneEver - флаг первого done тренировки (ever)
 * @param {Array} statsTotalWeek - сколько карточек было в тренировке каждый день за последние 7 дней
 * @param {Number} statsCurrentStreak - сколько дней подряд человек тренировлся, не считая сегодня
 * @param {Number} statsBestStreak - личный рекорд тренировок подряд
 * @param {Number} statsTotalToday - сколько карточек было в текущей тренировке
 */
const StudyDone = ({
  isFirstDoneEver,
  statsTotalWeek,
  statsTotalToday,
  statsCurrentStreak,
  statsBestStreak,
}) => {
  // у меня есть статистика за неделю,
  // но она не включает текущую тренировку
  // поэтому я добавляю текущий тотал в недельную статистику
  // и отдаю дальше в week
  const joinedStats = statsTotalWeek.map((s, i) => {
    if (i !== 0) {
      return s;
    } else {
      return s + statsTotalToday;
    }
  });
  // имея на руках joinedStats я должен решить
  // отзанимался ли человек всю неделю подряд
  // два условия — сегодня вск (вск 4am минимум или пн до 4am) и все семь дней в joinedStats не равны нулю
  const isSunday =
    (new Date().getDay() === 0 && new Date().getHours() >= 4) ||
    (new Date().getDay() === 1 && new Date().getHours() < 4);

  const isWholeWeekTrained =
    !joinedStats.includes(0) && isSunday ? true : false;

  /* Animations */
  const doneRef = useRef();
  const contentRef = useRef();
  const weekRef = useRef();
  const streakRef = useRef();
  const btnRef = useRef();
  const allDoneRef = useRef();
  const allAchievementRef = useRef();
  const disappearSpring = useSpring({
    from: {
      position: 'absolute',
      top: '50%',
      left: 0,
      right: 0,
      opacity: 1,
      transform: 'translate3d(0%,-50%,0)',
    },
    to: async (next) => {
      await next({
        opacity: 0,
        transform: 'translate3d(-100%,-50%,0)',
      });
      await next({
        zIndex: -1,
      });
    },
    ref: allDoneRef,
  });
  const appearSpring = useSpring({
    from: {
      position: 'absolute',
      top: '50%',
      left: 0,
      right: 0,
      opacity: 0,
      transform: 'translate3d(100%,-50%,0)',
      zIndex: -1,
    },
    to: async (next) => {
      await next({ zIndex: 1 });
      await next({ opacity: 1, transform: 'translate3d(0%,-50%,0)' });
    },
    ref: allAchievementRef,
  });
  // see the explanation for the 'current' trick
  // https://stackoverflow.com/questions/61149605/how-to-execute-two-animations-sequentially-using-react-spring
  // https://spectrum.chat/react-spring/general/unable-to-get-usechain-to-work-with-two-springs~90f925a7-8233-4b27-afb0-ca07dea2a235
  const doneRefCurrent = doneRef.current
    ? { current: doneRef.current }
    : doneRef;

  // теперь в зависимости от того, прозанимался ли человек всю неделю или нет,
  // у меня две разные анимации, одна с ачивкой, другая без
  useChain(
    isWholeWeekTrained
      ? [
          doneRefCurrent,
          contentRef,
          weekRef,
          streakRef,
          allDoneRef,
          allAchievementRef,
          btnRef,
        ]
      : [doneRefCurrent, contentRef, weekRef, streakRef, btnRef],
    isWholeWeekTrained
      ? [0, 0.35, 0.35, 0.9, 2.43, 2.05, 4.2]
      : [0, 0.35, 0.35, 0.9, 1.45]
  );

  return (
    <div className="overlay-v2 backgrounds--cards study__done">
      <div className="overlay-v2__content">
        <animated.div style={appearSpring} ref={allAchievementRef}>
          <OnFire />
        </animated.div>

        <animated.div style={disappearSpring}>
          <AppearanceTop ref={doneRef}>
            <Alert
              type="done"
              title={ioMessages.study.done}
              description={ioMessages.study.doneSecondary}
            />
          </AppearanceTop>
          {isFirstDoneEver ? (
            <AppearanceOpacity ref={contentRef} className="study__done__note">
              <p>
                Congrats on your first practice!
                <br />
                I recommend adding 2-3 new words
                <br />
                and practice once a day, every day.
                <br />
                Slowly but steadily.
              </p>
            </AppearanceOpacity>
          ) : (
            <>
              <AppearanceOpacity
                ref={contentRef}
                className="study__done__stats"
              >
                <Week stats={joinedStats} ref={weekRef} />
              </AppearanceOpacity>
              <AppearanceOpacity ref={streakRef}>
                <Streak
                  bestStreak={statsBestStreak}
                  currentStreak={statsCurrentStreak}
                />
              </AppearanceOpacity>
            </>
          )}
        </animated.div>
      </div>
      <div className="overlay-v2__footer">
        <AppearanceOpacity ref={btnRef}>
          <button
            onClick={(e) => console.log('close')}
            className="btn btn--secondary btn--m"
          >
            Close
          </button>
        </AppearanceOpacity>
      </div>
    </div>
  );
};

export default StudyDone;
