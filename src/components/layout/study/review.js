import React from 'react';
import Notice from '../../misc/notice';
import Audio from '../../misc/audio';

/**
 * View шага Review в тренировке
 *
 * @param {String} wordTitle
 * @param {String} wordPart
 * @param {String} wordAudio
 * @param {String} wordDef
 * @param {String} wordDomains
 * @param {String} wordTranslation
 * @param {String} wordExample
 * @param {Boolean} wordDefFirst
 *
 * @param {Boolean} isAudioPlay
 * @param {Boolean} isAudioLoading
 * @param {Boolean} isAudioError
 * @param {Function} handleAudioLoaded
 * @param {Function} handleAudioError
 * @param {Function} handleAudioPlay
 *
 * @param {String} targetLanguage язык перевода, если есть
 */
const Review = ({
  wordTitle,
  wordPart,
  wordDef,
  wordDomains,
  wordExample,
  wordTranslation,
  wordDefFirst,

  targetLanguage,
}) => {
  const defHighlight = wordDefFirst || !wordTranslation;
  const defFirst = wordDefFirst;

  return (
    <div className="review">
      <div className="review__front">
        {wordPart && (
          <>
            <span className={'pill pill--' + wordPart}>{wordPart}</span>
            <br />
          </>
        )}
        <h1 className="--xl">
          <span>{wordTitle}</span>
        </h1>
      </div>
      <div
        className={
          'review__definition ' + (defHighlight ? '--defHighlight' : '')
        }
      >
        {!defFirst && wordTranslation && (
          <h2
            className="review__definition__translation"
            lang={targetLanguage ? targetLanguage : ''}
          >
            {wordTranslation}
          </h2>
        )}
        {wordDef && (
          <div className="review__definition__def">
            {wordDomains && (
              <p className="review__definition__domains">
                {wordDomains.join(', ')}
              </p>
            )}
            {wordDef}
          </div>
        )}
        {defFirst && wordTranslation && (
          <div
            className="review__definition__translation"
            lang={targetLanguage ? targetLanguage : ''}
          >
            {wordTranslation}
          </div>
        )}
      </div>
      {wordExample && (
        <div className="review__example">
          <p>{wordExample}</p>
        </div>
      )}
    </div>
  );
};

export default Review;
