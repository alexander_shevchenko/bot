import React from 'react';
import { textAnswers }  from '../../../actions/consts';

const Answers = ({
  card,
  answers,
  handleGradeCard,
}) => {
  const list = answers.map((answer) => 
    <button 
      className={'btn btn--secondary btn--xs btn--answer ' + (answer.text === textAnswers.again ? 'btn--yellow' : '')}
      onClick={ e => handleGradeCard(card.id, answer.text) }
      key={answer.key}
    >
      {answer.text}
      <small className="small_xxs">{answer.periodText}</small>
    </button>
  );

  return (
    <div className="footer__grid">
      {list}
    </div>
  )
}

export default Answers;