import React from 'react';
import IconStar from '../../svg/star';
import { mapWeekStats } from '../../../helpers/study/mapWeekStats';
import { useTrail, animated } from 'react-spring';

/**
 * View недельной статистики в шаге Done в тренировке
 *
 * @param {Array} stats - сколько карточек было в тренировке каждый день за последние 7 дней
 */
const weekDays = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

const Week = React.forwardRef((props, ref) => {
  const currentStats = mapWeekStats(weekDays, props.stats);
  const trail = useTrail(currentStats.length, {
    ref,
    opacity: 1,
    x: 0,
    from: { opacity: 0, x: -90 },
  });

  return (
    <div className="week">
      <ul className="week__list">
        {trail.map(({ x, ...rest }, index) => (
          <li
            key={index}
            className={
              'week__item ' +
              (currentStats[index].isPracticed ? '--active' : '')
            }
          >
            <span>{currentStats[index].weekDay}</span>
            <animated.span
              style={{
                ...rest,
                transform: x.interpolate((x) => `rotate(${x}deg)`),
              }}
            >
              <IconStar />
            </animated.span>
          </li>
        ))}
      </ul>
    </div>
  );
});

export default Week;
