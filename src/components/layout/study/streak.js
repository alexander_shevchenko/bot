import React from 'react';
import IconStar from '../../svg/star';

/**
 * View страйка
 *
 * @param {Number} currentStreak - текущий страйк, не считая сегодня
 * @param {Number} bestStreak - персональный рекорд
 */

const Streak = ({ currentStreak, bestStreak }) => {
  // у меня есть страйк, кол-во дней не включая сегодня
  // раз дошли до done, то добавляю сегодняшний день
  currentStreak++;

  let copy;
  if (currentStreak === 1) {
    copy = <>Here’s to new beginnings!</>;
  } else if (currentStreak >= bestStreak) {
    copy = <>Your best streak so far!</>;
  } else if (currentStreak < bestStreak) {
    copy = (
      <>
        <strong>{bestStreak}X</strong> is your personal best
      </>
    );
  }

  return (
    <div className="streak">
      <strong className="streak__title">
        <IconStar />
        {currentStreak}X STREAK
      </strong>
      <p className="streak__copy">{copy}</p>
    </div>
  );
};

export default Streak;
