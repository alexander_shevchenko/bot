import React, { useRef } from 'react';
import { useChain } from 'react-spring';
import { pluralize_en } from '../../../helpers/pluralize';
import { getEstimatedModifier } from '../../../helpers/study/getEstimatedModifier';
import { calculateEstimation } from '../../../helpers/study/calculateEstimation';
import AppearanceTop from '../../animations/appearanceTop';
import Counter from '../../animations/counter';
import AppearanceOpacity from '../../animations/appearanceOpacity';

/**
 * View шага Review в тренировке
 * 
 * @param {Number} newCards - кол-во новых карточек в тренировке
 * @param {Number} reviewCards - кол-во ревью карточек в тренировке
 * @param {Number} totalCards - полное карточек в тренировке
 * @param {Function} handleStart - хэндлер для кнопки start
 */
const StudyIntro = ({
  newCards,
  reviewCards,
  totalCards,
  handleStart,
}) => {
  /* Springs */
  const estimatedRef = useRef();
  const buttonRef = useRef();
  
  // три каунтера стартуют от нуля по умолчанию, эстимейтэд и кнопка подключаются позже
  useChain( [estimatedRef, buttonRef], [.85, 1.25] );

  return (
    <div 
      className="overlay-v2 backgrounds--cards study__intro"
    >
      <div className="overlay-v2__content">
        <div className="study__intro__stats">
          <ul>
            <li>New Cards <Counter start={0} end={newCards} /></li>
            <li>Review Cards <Counter start={0} end={reviewCards} /></li>
          </ul>
          <h1>
            <span>Total Cards</span>
            <Counter start={0} end={totalCards} className="study__intro__stats__total" />
          </h1>
        </div>
        <AppearanceTop ref={estimatedRef}>
          <div className={'study__intro__estimated ' + getEstimatedModifier(totalCards)}>
            <div className="study__intro__estimated__wrapper">
              Estimated practice time
              <strong>{pluralize_en( calculateEstimation( totalCards ), 'minute' )}</strong>
            </div>
          </div>
        </AppearanceTop>
      </div>
      <div className="overlay-v2__footer">
        <AppearanceOpacity ref={buttonRef}>
          <button 
            className="btn btn--primary btn--m"
            onClick={handleStart}
          >
            Start
          </button>
        </AppearanceOpacity>
      </div>
    </div>
  )
}

export default StudyIntro;