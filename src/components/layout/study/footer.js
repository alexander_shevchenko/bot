import React from 'react';
import Answers from './answers';

const Footer = ({
  step,
  card,
  answers,
  handleGradeCard,
  handleOnShow,
}) => {
  return (
    <footer className="study__footer">
      {step === 'front' && 
        <button className="btn btn--secondary btn--block" onClick={handleOnShow}>Show Answer</button>
      }
      {step === 'full' &&
        <Answers
          card={card}
          answers={answers}
          handleGradeCard={handleGradeCard}
        />
      }
    </footer>
  )
}

export default Footer;