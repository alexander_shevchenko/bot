import React, { useRef } from 'react';
import { useSpring, animated } from 'react-spring'


const Front = ({ card, isAncillaryShown }) => {
  const ancillaryRef = useRef();
  const ancillaryHeight = ancillaryRef.current ? ancillaryRef.current.clientHeight : 0;
  const { x, h } = useSpring({ 
    x: isAncillaryShown ? 1 : 0, 
    h: isAncillaryShown ? ancillaryHeight : 0,
  });

  return (
    <div className="study__front">
      {card.part && 
        <span className={'study__front__pill pill pill--' + card.part}>
          {card.part}
        </span>
      }
      <div className="study__front__title">{card.front}</div>
      {(card.domains && card.domains.length) &&
        <div className="study__domain study__front__domains">
          {card.domains.join(', ')}
        </div>
      }
      <animated.div className="study__front__ancillary" style={{
        opacity: x,
        height: h,
        transform: x.interpolate( [0, 1], [50, 0] ).interpolate( x => `translate3d(0, ${x}px, 0)` ),
        overflow: x.interpolate( x => x ? 'visible' : 'hidden' ),
      }}>
        <div className="study__front__ancillary__wrapper" ref={ancillaryRef}>
          <div className="study__thesaurus">
            {card.synonym && 
              <dl>
                <dt>Synonym</dt>
                <dd>{card.synonym}</dd>
              </dl>
            }
            {card.antonym &&
              <dl>
                <dt>Antonym</dt>
                <dd>{card.antonym}</dd>
              </dl>
            }
          </div>
          {card.hint &&
            <p className="study__hint"><span className="icon-lightbulb-o"></span>{card.hint}</p>
          }
        </div>
      </animated.div>
    </div>
  )
}

export default Front;