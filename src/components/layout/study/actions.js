import React from 'react';
import Audio from '../../misc/audio';
import IconHint from '../../svg/hint';
import IconDots from '../../svg/dots';

const Actions = ({
  step,
  card,
  handleCardSettingsShow,
  isSettingsShown,
  handleAncillaryShow,
  isAncillaryShown,
  autoPlay,
}) => {
  return (
    <div className="study__actions">
      {step === 'full' && (
        <button
          className="btn btn--plain btn--air-m"
          onClick={handleCardSettingsShow}
        >
          <span
            className={
              'icon__wrapper icon__wrapper--l --transition ' +
              (isSettingsShown ? '--active' : '')
            }
          >
            <span className="icon__wrapper__container">
              <IconDots />
            </span>
          </span>
        </button>
      )}
      {(card.synonym || card.antonym || card.hint) && step !== 'full' && (
        <button
          className="btn btn--plain btn--air-m"
          onClick={() => handleAncillaryShow(!isAncillaryShown)}
        >
          <span
            className={
              'icon__wrapper icon__wrapper--l --transition ' +
              (isAncillaryShown ? '--active' : '')
            }
          >
            <span className="icon__wrapper__container">
              <IconHint />
            </span>
          </span>
        </button>
      )}
      {card.audioUrl && (
        <Audio
          url={card.audioUrl}
          transcription={card.transcription}
          autoPlay={autoPlay}
        />
      )}
    </div>
  );
};

export default Actions;
