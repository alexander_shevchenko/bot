import React from 'react';

const Progress = ({
  progress
}) => {
  const style = {
    width: `${progress}%`
  }
  return (
    <div className="progress">
      <div className="progress__bar" style={style}></div>
    </div>
  );
};

export default Progress;