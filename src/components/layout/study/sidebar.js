import React, { useState } from 'react';
import { useSpring, useTransition, animated } from 'react-spring';
import Toggle from '../../misc/toggle';
import Dim from '../../misc/dim';
import IconChevronDown from '../../svg/chevronDown';
import { Link } from 'react-router-dom';
import { paths } from '../../../consts/paths';
import Log from './log';

/**
 * Панель настроек карточки
 *
 * @param {Object} card карточка со всеми потрохами
 * @param {Boolean} isShown мама, меня показывают!
 * @param {Function} closeHandler даю знать вышестоящему компоненту, что нажали на крестик
 * @param {Function} handleDefFirstToggle метод для тоггла деф ферст
 */

const Sidebar = ({ card, isShown, closeHandler, handleDefFirstToggle }) => {
  /* Log */
  /* и анимация разворота иконки лога */
  const [isLogOpen, setIsLogOpen] = useState(false);
  const logSpring = useSpring({
    from: {
      display: 'inline-block',
      transform: 'rotate(0deg)',
    },
    to: {
      transform: isLogOpen ? 'rotate(-180deg)' : 'rotate(0deg)',
    },
  });

  /* Dim */
  /* появление dim */
  const dimTransition = useTransition(isShown, null, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });

  /* Sidebar */
  /* выезд справа */
  const sidebarTransition = useTransition(isShown, null, {
    from: { transform: 'translate(100%,0)' },
    enter: { transform: 'translate(0%,0)' },
    leave: { transform: 'translate(100%, 0)' },
  });

  /* Close wrapper */
  /* закрывает компонент и лог */
  const closeWrapper = () => {
    setIsLogOpen(false);

    // использую setTimeout хак, чтобы развести во времени анимации закрытия лога и поворота иконки
    // и leave сайдбара.
    // проблема была в том, что анимации игрались одновременно и сайдбар успевал отмаунтиться к тому
    // времени как анимация закрытия лога заканчивалась и какие-то пропсы пытались еще прийти в компоненты
    // но их уже не было, в результате хром ругался на утечку.
    // пока костыль с таймаутом работает.
    setTimeout(() => {
      closeHandler();
    }, 1);
  };

  return (
    <>
      {dimTransition.map(
        ({ item, key, props }) =>
          item && (
            <animated.div
              key={key}
              style={props}
              className="dim__transition-wrapper"
            >
              <Dim handleClose={closeWrapper} />
            </animated.div>
          )
      )}
      {sidebarTransition.map(
        ({ item, key, props }) =>
          item && (
            <animated.div className="sidebar" key={key} style={props}>
              <button
                className="btn btn--plain icon__wrapper icon__wrapper--transparent sidebar__close"
                onClick={closeWrapper}
              >
                <span className="icon__wrapper__container">
                  <span className="icon icon--add icon--primary-white icon--l --clear"></span>
                </span>
              </button>
              <div className="sidebar__wrapper">
                <div className="sidebar__form">
                  <div className="sidebar__form__wrapper">
                    <section className="hr --dark">
                      <h3 className="--uppercased">Preferences</h3>
                      <Toggle
                        title="Definition First"
                        handleChange={handleDefFirstToggle}
                        isChecked={card.defFirst ? true : false}
                        theme="dark"
                      />
                    </section>
                    <section className="hr --dark">
                      <h3 className="--uppercased">Statistics</h3>
                      <dl className="--flex">
                        <dt>Easiness Factor</dt>
                        <dd>{card.Ef}</dd>
                        <dt>
                          <span
                            className="sidebar__logLink"
                            onClick={() => setIsLogOpen(!isLogOpen)}
                          >
                            Log
                            <animated.span style={logSpring}>
                              <IconChevronDown />
                            </animated.span>
                          </span>
                        </dt>
                      </dl>
                      <Log isOpen={isLogOpen} data={card.logs} />
                    </section>
                  </div>
                </div>
                <div className="sidebar__action">
                  <Link
                    className="btn btn--tertiary btn--block"
                    role="button"
                    to={{
                      pathname: paths.editCard,
                      state: {
                        origin: 'study',
                        word: card,
                      },
                    }}
                  >
                    Edit Card
                  </Link>
                </div>
              </div>
            </animated.div>
          )
      )}
    </>
  );
};

export default Sidebar;
