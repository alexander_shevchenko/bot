import React from 'react';
import Header from './header';
import Footer from './footer';
import Front from './front';
import Back from './back';
import Actions from './actions';
import Sidebar from './sidebar';
// import Tip from './misc/tip';

const StudyWrapper = ({
  targetLanguage,
  frameId,
  progress,
  card,
  step,

  answers,
  handleGradeCard,
  handleOnShow,

  isAncillaryShown,
  handleAncillaryShow,

  handleCardSettingsShow,
  handleCardSettingsHide,
  isSettingsShown,
  handleDefFirstToggle,

  autoPlay,
}) => {
  const isActionsShown =
    step === 'full' ||
    card.audioUrl ||
    card.synonym ||
    card.antonym ||
    card.hint
      ? true
      : false;
  // модуль Card
  return (
    <div className="study__content backgrounds--cards">
      <Header progress={progress} />
      {step === 'full' && (
        <Sidebar
          isShown={isSettingsShown}
          closeHandler={handleCardSettingsHide}
          card={card}
          handleDefFirstToggle={handleDefFirstToggle}
        />
      )}

      <div className="study__card">
        <div className="study__card__wrapper">
          {step === 'front' && (
            <Front card={card} isAncillaryShown={isAncillaryShown} />
          )}
          {step === 'full' && (
            <Back card={card} targetLanguage={targetLanguage} />
          )}
          {isActionsShown && (
            <Actions
              step={step}
              card={card}
              handleCardSettingsShow={handleCardSettingsShow}
              isSettingsShown={isSettingsShown}
              handleAncillaryShow={handleAncillaryShow}
              isAncillaryShown={isAncillaryShown}
              // для каждого нового инстанса в очереди создается уникальный айди
              // тут я его передаю как ключ, что приводит к ремаунту компонента actions каждый
              // раз для новой карточки
              // что дает мне например автоплэй в аудио-компоненте на маунт
              key={frameId}
              autoPlay={autoPlay}
            />
          )}
        </div>
      </div>
      <Footer
        step={step}
        card={card}
        answers={answers}
        handleGradeCard={handleGradeCard}
        handleOnShow={handleOnShow}
      />

      {/* на этом уровне жили типы и ссылки на них, смотри ниже */}
    </div>
  );
};

export default StudyWrapper;

/*isTip === 'front' && 
  <Tip 
    action={handleTipClose}
    actionTitle='Закрыть подсказку'
    title='Флеш-карточка'
    primary='Представь себе бумажную карточку, на одной стороне которой написано слово, а на другой его перевод. Твоя задача вспомнить перевод слова, затем перевернуть карточку и оценить свой ответ.'
    secondary={<span>Для эффективного удержания слов в памяти используется принцип <strong>интервальных повторений</strong> (техника повторения учебного материала по определенным, постоянно возрастающим интервалам).</span>}
  />
}
{isTip === 'full' && 
  <Tip 
    action={handleTipClose}
    actionTitle='Закрыть подсказку'
    title='Оценка ответа'
    primary='Если тебе не удалось вспомнить перевод — жми «Плохо». Если вспомнил с трудом — «Сложно». Вспомнил с небольшой паузой — «Средне». Даже не пришлось вспоминать — «Легко».'
    secondary='Широкий диапазон градации ответов позволяет более эффективно рассчитать  интервал через который слово будет показано в следующий раз. Таким образом можно избежать ненужных повторений и уменьшить время потраченное на тренировки.'
  />
}
{isTip === 'done' && 
  <Tip 
    action={handleTipClose}
    actionTitle='Закрыть подсказку'
    title='Тренировка'
    primary='Каждое слово, по результатам тренировки, отодвигается на некоторый интервал в будущее. Ежедневно мы проверяем, есть ли слова интервал у которых истек или истекает сегодня и добавляем их в сегодняшнюю тренировку.'
    secondary={<span>Метод интервальных повторений работает эффективно при условии ежедневных коротких тренировок. Бывают ситуации когда в очереди собирается много слов, в таких случаях мы рекомендуем разбить тренировку (не бойся прерывать тренировку, уже сделанный прогресс будет сохранен) на несколько коротких подходов в течении дня.</span>}
  />
}
{(card && step === 'front') && 
  <div className="study__header__help">
    <button 
      className="btn btn--plain icon__wrapper icon__wrapper--s icon__wrapper--quaternary"
      onClick={() => handleTipOpen('front')}
    >
      <span className="icon__wrapper__container">
        <span className="icon icon--help">?</span>
      </span>
    </button>
  </div>
}
{(card && step === 'full') && 
  <div className="study__header__help">
    <button 
      className="btn btn--plain icon__wrapper icon__wrapper--s icon__wrapper--quaternary"
      onClick={() => handleTipOpen('full')}
    >
      <span className="icon__wrapper__container">
        <span className="icon icon--help">?</span>
      </span>
    </button>
  </div>
*/
