import React, { useState } from 'react';
import IconOnFire from '../../svg/onfire';

const synonyms = [
  'perfect',
  'incredible',
  'stunning',
  'splendid',
  'outstanding',
  'glorious',
  'exemplary',
  'EXCELLENT',
  'marvelous',
  'remarkable',
  'spectacular',
  'terrific',
  'brilliant',
  'impressive',
  'astonishing',
  'phenomenal',
  'transcendental',
];

const OnFire = () => {
  const [synonym] = useState(
    synonyms[Math.floor(Math.random() * synonyms.length)]
  );

  return (
    <div className="study__onfire">
      <h2>You are on fire!</h2>
      <IconOnFire />
      <h3>{synonym} week</h3>
    </div>
  );
};

export default OnFire;
