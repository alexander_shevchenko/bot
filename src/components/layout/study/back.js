import React from 'react';
import Review from './review';

const Back = ({ card, targetLanguage }) => {
  return (
    <div className="study__back">
      <Review
        wordTitle={card.front}
        wordPart={card.part}
        wordAudio={card.audioUrl}
        wordDef={card.definition}
        wordDomains={card.domains}
        wordExample={card.example}
        wordTranslation={card.back}
        wordDefFirst={card.defFirst}
        targetLanguage={targetLanguage}
      />
    </div>
  );
};

export default Back;
