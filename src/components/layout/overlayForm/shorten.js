import React from 'react';
import OverlayForm from './form';
import cloneDeep from 'lodash/cloneDeep';

/**
 * @param {String} tokenizedExample токены примера
 * @param {Number} exampleIndex индекс примера в рамках определения
 * @param {String} wordTitle само искомое слово
 * @param {String} submitTitle название кнопки сабмита
 * @param {Function} handleOnCancel хэндлер отмены
 * @param {Function} handleOnSubmit хэндлер сабмита
 */

class Shorten extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      words: props.tokenizedExample,
    }
  }

  toggleWord(e, word) {
    // на тоггл меняю isActive у слова
    e.preventDefault();
    const words = cloneDeep(this.state.words);
    const clickedWord = words[ words.findIndex(el => el.id === word.id) ];
    clickedWord.isActive = clickedWord.isActive === true ? false : true;

    this.setState({
      words,
    })
  }

  render() {
    return (
      <OverlayForm
        {...this.props}
        handleOnSubmit={e => this.props.handleOnSubmit(e, this.state.words, this.props.exampleIndex)}
      >
        <div className="picker__shorten">
          <ul>
            {this.state.words.map(word => {
              return (
                <li key={word.id}>
                  {word.isWordTitle && 
                    <button 
                      className="btn btn--link --wordTitle"
                      onClick={e => e.preventDefault()}
                    >
                      {word.word}
                    </button>
                  }
                  {!word.isWordTitle && 
                    <button 
                      className={
                        'btn btn--link ' 
                        + (word.isActive ? '' : '--inactive')
                      }
                      onClick={e => this.toggleWord(e, word)}
                    >
                      {word.word}
                    </button>
                  }
                </li>
              );
            })}
          </ul>
        </div>
      </OverlayForm>
    )
  }
}
export default Shorten;