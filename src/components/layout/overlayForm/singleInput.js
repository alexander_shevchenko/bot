import React from 'react';
import OverlayForm from './form';

/**
 * @param {String} note подсказка (опционально)
 * @param {String} label лэйбл инпута
 * @param {String} value initial value if any
 * @param {String} submitTitle название кнопки сабмита
 * @param {String} cancelTitle название кнопки отмены
 * @param {Function} handleOnCancel хэндлер отмены
 * @param {Function} handleOnSubmit хэндлер сабмита
 */

class SingleInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: props.value ? props.value : '',
    }
  }

  handleOnInput = (e) => {
    this.setState({
      input: e.target.value,
    })
  }

  render() {
    const {
      label,
      handleOnSubmit,
    } = this.props;

    return (
      <OverlayForm
        {...this.props}
        handleOnSubmit={(e) => handleOnSubmit(e, this.state.input)}
      >
        <div className="form-control picker__input">
          <label htmlFor="overlayInput">
            {label}
          </label>
          <input 
            type="text"
            maxLength="60"
            id="overlayInput"
            name="overlayInput"
            value={this.state.input}
            onChange={(e) => this.handleOnInput(e)}
          />
        </div>
      </OverlayForm>
    )
  }
}
export default SingleInput;