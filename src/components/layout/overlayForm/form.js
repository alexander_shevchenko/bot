import React from 'react';
import Notice from '../../misc/notice';

/**
 * Компонент показывает произвольную форму в самом приоритетном оверлее
 * принимает подсказку + два экшена и произвольный контент формы (props.children)
 * 
 * @param {Object} note подсказка (опционально)
 * @param {String} note.title (opt)
 * @param {String} note.extra (opt)
 * @param {String} submitTitle название кнопки сабмита
 * @param {String} cancelTitle название кнопки отмены
 * @param {Function} handleOnCancel хэндлер отмены
 * @param {Function} handleOnSubmit хэндлер сабмита
 */

function OverlayForm(props) {
  return (
    <div className="overlayForm">
      <div className="overlayForm__content">
        <div className="overlayForm__content__wrapper">
          {props.note && 
            <Notice 
              type='info'
              title={props.note.title}
              extra={props.note.extra}
            />
          }
          {props.children}
        </div>
      </div>
      <div className="overlayForm__actions">
        <button 
          className="btn btn--secondary btn--block" 
          type="submit"
          onClick={props.handleOnSubmit}
        >
          {props.submitTitle}
        </button>
        <button 
            className="btn btn--secondary btn--block btn--red"
            onClick={props.handleOnCancel}
          >
            Cancel
          </button>
      </div>
    </div>
  )
}

export default OverlayForm;