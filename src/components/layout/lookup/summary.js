import React, { useState, useRef } from 'react';
import { useSpring, useTransition, animated } from 'react-spring';
import Dim from '../../misc/dim';
import IconSummary from '../../svg/summary';
import IconClose from '../../svg/close';
import IconUnequal from '../../svg/unequal';
import IconAppEqual from '../../svg/appequal';

/**
 * View попапа с деталями слова в лукапе
 *
 * @param {String} wordTitle
 * @param {String} wordPart
 * @param {String} wordDef
 * @param {String} wordDomains
 * @param {String} wordSyn
 * @param {String} wordAnt
 * @param {String} wordTranslations
 * @param {String} wordExample
 * @param {String} wordNextExample
 */
const Summary = ({
  wordTitle,
  wordPart,
  wordDef,
  wordDomains,
  wordSyn,
  wordAnt,
  wordTranslations,
  wordNextExample,
  currentTranslations,
}) => {
  /* show оркестрирует всё */
  const [show, setShow] = useState(false);

  /* саммари */
  const contentRef = useRef();
  const contentHeight = contentRef.current
    ? contentRef.current.clientHeight
    : 0;

  /* общий спринг */
  const { s, h } = useSpring({
    s: show ? 1 : 0,
    h: show ? contentHeight : 0,
  });

  /* появление dim */
  const dimTransition = useTransition(show, null, {
    from: { opacity: 0 },
    enter: { opacity: 1 },
    leave: { opacity: 0 },
  });

  return (
    <>
      {dimTransition.map(
        ({ item, key, props }) =>
          item && (
            <animated.div
              key={key}
              style={props}
              className="dim__transition-wrapper"
            >
              <Dim handleClose={() => setShow(false)} />
            </animated.div>
          )
      )}
      <animated.div
        className="lookup__summary"
        style={{
          height: h,
        }}
      >
        <div className="lookup__summary__content" ref={contentRef}>
          <h4>
            <span>{wordTitle}</span>
            {wordPart && (
              <span className={'pill pill--' + wordPart}>{wordPart}</span>
            )}
          </h4>
          {(wordSyn || wordAnt) && (
            <div className="lookup__summary__thesaurus">
              {wordSyn && (
                <span>
                  <IconAppEqual />
                  {wordSyn}
                </span>
              )}
              {wordAnt && (
                <span>
                  <IconUnequal />
                  {wordAnt}
                </span>
              )}
            </div>
          )}
          {wordDef && (
            <div className="lookup__summary__def">
              {wordDomains && (
                <div className="lookup__summary__def__domains">
                  {wordDomains.join(', ')}
                </div>
              )}
              <div className="lookup__summary__def__def">{wordDef}</div>
              {wordNextExample && wordNextExample.length && (
                <div className="lookup__summary__def__example">
                  {wordNextExample.map((word) => {
                    if (word.isActive) {
                      return <span key={word.id}>{word.word}</span>;
                    }
                  })}
                </div>
              )}
            </div>
          )}
          {!!wordTranslations.length && (
            <ul className="lookup__summary__translations">
              {wordTranslations.map((tr, i) => {
                return (
                  <li key={i}>
                    <span>{tr}</span>
                  </li>
                );
              })}
            </ul>
          )}
          {!!currentTranslations.length && (
            <div className="lookup__summary__available-translations">
              <h4>All Suitable Translations</h4>
              <p>
                {currentTranslations.map((t, i) => {
                  return <span key={i}>{t}</span>;
                })}
              </p>
            </div>
          )}
        </div>
      </animated.div>
      <div className="lookup__summmary-trigger">
        <button onClick={() => setShow(!show)}>
          <animated.span
            style={{
              opacity: s.interpolate([0, 1], [1, 0]),
              transform: s
                .interpolate([0, 1], [0, 90])
                .interpolate((s) => `rotate(${s}deg)`),
            }}
          >
            <IconSummary />
          </animated.span>
          <animated.span
            style={{
              opacity: s,
              transform: s
                .interpolate([0, 1], [0, 90])
                .interpolate((s) => `rotate(${s}deg)`),
            }}
          >
            <IconClose />
          </animated.span>
        </button>
      </div>
    </>
  );
};

export default Summary;
