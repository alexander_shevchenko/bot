import React from 'react';
import IconUnequal from '../../svg/unequal';
import IconAppEqual from '../../svg/appequal';

/**
 * View шага Review в Оксфорд-добавлении
 * @param {String} wordTitle
 * @param {String} wordPart
 * @param {String} wordSyn
 * @param {String} wordAnt
 * @param {String} wordDef
 * @param {String} wordDomains
 * @param {Array} wordTranslations
 * @param {Array} wordExample
 *
 * @param {String} hint
 * @param {Function} handleHintShow
 * @param {String} targetLanguage язык перевода, если есть
 */
const Review = ({
  wordTitle,
  wordPart,
  wordSyn,
  wordAnt,
  wordDef,
  wordDomains,
  wordExample,
  wordTranslations,

  hint,
  handleHintShow,

  targetLanguage,
}) => {
  const isTranslation = wordTranslations && !!wordTranslations.length;

  return (
    <div className="review">
      <div className="review__front">
        {wordPart && (
          <>
            <span
              data-testid="finalStep-part"
              className={'pill pill--' + wordPart}
            >
              {wordPart}
            </span>
            <br />
          </>
        )}
        <h1>
          <span data-testid="finalStep-front">{wordTitle}</span>
        </h1>
        <div className="review__front__ancillary">
          {(wordSyn || wordAnt) && (
            <div className="review__front__thesaurus">
              {wordSyn && (
                <span data-testid="finalStep-synonym">
                  <IconAppEqual />
                  {wordSyn}
                </span>
              )}
              {wordAnt && (
                <span data-testid="finalStep-antonym">
                  <IconUnequal />
                  {wordAnt}
                </span>
              )}
            </div>
          )}
          {!hint && (
            <button
              className="btn btn--medium btn--link btn--pseudo btn--blue"
              onClick={handleHintShow}
            >
              add hint
            </button>
          )}
          {hint && (
            <div className="review__front__hint">
              <span>
                <span className="icon-lightbulb-o"></span>
                {hint}
              </span>
              <button
                className="btn btn--medium btn--link btn--pseudo btn--blue"
                onClick={handleHintShow}
              >
                change
              </button>
            </div>
          )}
        </div>
      </div>
      <div
        className={
          'review__definition ' + (isTranslation ? '' : '--defHighlight')
        }
      >
        {isTranslation && (
          <h2
            className="review__definition__translation"
            data-testid="finalStep-translation"
            lang={targetLanguage ? targetLanguage : ''}
          >
            {wordTranslations.join(', ')}
          </h2>
        )}

        {wordDef && (
          <div
            data-testid="finalStep-definition"
            className="review__definition__def"
          >
            {wordDomains && (
              <p className="review__definition__domains">
                {wordDomains.join(', ')}
              </p>
            )}
            {wordDef}
          </div>
        )}
      </div>
      {wordExample && (
        <div className="review__example">
          {wordExample && wordExample.length && (
            <p data-testid="finalStep-example">
              {wordExample.map((word) => {
                if (word.isActive) {
                  return (
                    <span
                      key={word.id}
                      className={word.isWordTitle ? '--wordTitle' : ''}
                    >
                      {word.word}
                    </span>
                  );
                }
              })}
            </p>
          )}
        </div>
      )}
    </div>
  );
};

export default Review;
