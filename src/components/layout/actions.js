import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

// TODO: вынести либо в css переменные, либо завести конфиг-файл
// в любом случае — не хранить тут
const enterTimeout = 250;
const leaveTimeout = 150;

/**
 * Модуль отвечает за работу попап навигации в стиле айфончиков.
 *  
 * @param {Function}  handleClose               callback который модуль исполнит на клик по «Отмена»
 * типичный пример — переменная в стэйте родителя, которая отвечает за рендер данного модуля,
 * колбэк просто меняет переменную на false
 * handleClose() {
 *  this.setState({
 *    isTemp: false,
 *  })
 * }
 * @param {Object}    items                             массив из экшенов и соответствующих коллбеков
 * @param {String}    items[item].type                  тип экшена -- regular/danger (во втором случае кнопка будет красной)
 * @param {String}    items[item].title                 текст кнопки, например «Удалить слово»
 * @param {Function}  items[item].handler               коллбэк который будет вызван по клику на кнопку
 * @param {Boolean}   items[item].confirm               нужно ли подтверждение
 * @param {String}    items[item].confirmText           **OPTIONAL** текст для окошка подтверждения, например «Вы уверены?»
 * @param {String}    items[item].confirmSecondaryText  **OPTIONAL** второстепенный текст для окошка подтверждения, например «Все вложенные слова также будут удалены.»
 * @param {String}    items[item].confirmTitle          **OPTIONAL** текст кнопки в окошке с подтверждением, например «Удалить»
 * 
 * @example 
 *  <Actions 
 *    handleClose={handlePopupClose} 
 *    items={[
 *      {
 *        type: 'regular', 
 *        title: 'Изменить словарь', 
 *        confirm: false, 
 *        handler: (e) => console.log('Изменить')
 *      },
 *      {
 *        type: 'danger', 
 *        title: 'Удалить словарь', 
 *        confirm: true, 
 *        confirmText: 'Удалить словарь?', 
 *        confirmSecondaryText: 'Все вложенные слова также будут удалены.',
 *        confirmTitle: 'Удалить', 
 *        handler: (e) => console.log('Удалить')
 *      },
 *    ]}
 *  />
 */

class Actions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      isConfirm: false,
      confirmAction: {},
    }

    this.handleCallback = this.handleCallback.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.openConfirm = this.openConfirm.bind(this);
    this.closeConfirm = this.closeConfirm.bind(this);
  }

  // базовый флоу — 
  // по клику на любой экшен я должен выполнить оригинальный 
  // коллбэк из родительского компонента
  // а затем вызвать родительский метод handleClose
  // который по идее должен спрятать данный компонент за ненадобностью
  handleCallback(callback) {
    callback();
    this.props.handleClose();
  }

  // флоу закрытия основного меню на клик на отмену
  // обрати внимание, что я завел отдельную переменную в скоупе
  // которая отвечает за показ менюшки первого уровня
  // она нужна для того, чтобы успевала отработать анимация перед тем как закроется попап
  handleCancel() {
    this.setState({
      isOpen: false,
    }, () => {
      setTimeout(
        this.props.handleClose,
        leaveTimeout
      )
    })
  }

  // флоу с конфирмом -
  // по клику на экшен с конфирмом, я должен спросить у пользователя
  // подтверждение — действительно ли он хочет выполнить данное действие
  // таким образом на клик:
  // 1. я скрываю основное меню 
  // 2. показываю конфирмейшен
  // 3. по клику на отмену -- откатываю обратно
  // 4. по клику на подтверждение -- выполняю оригинальный коллбэк
  openConfirm(action) {
    this.setState({
      isConfirm: true,
      confirmAction: action,
    });
  }

  closeConfirm() {
    this.setState({
      isConfirm: false,
      confirmAction: {},
    });
  }

  render() {
    const {items} = this.props;
    const {isOpen, isConfirm, confirmAction} = this.state;

    return (
      <div className="actions">
        <div
          className="dim"
          onClick={this.handleCancel}
        ></div>
        
        <ReactCSSTransitionGroup
          transitionName="popup-transition-actions"
          transitionEnterTimeout={enterTimeout}
          transitionLeaveTimeout={leaveTimeout}
          transitionAppear={true}
          transitionAppearTimeout={enterTimeout}
        >
          {isOpen && !isConfirm &&
            <div className="popup popup--actions">
              <ul className="popup__section">
                {items.map((action, index) => 
                  <li key={index}>
                    <button 
                      className={"btn btn--link btn--block " + (action.type === 'danger' ? 'btn--red' : '')}
                      onClick={ action.confirm ? () => {this.openConfirm(action)} : () => {this.handleCallback(action.handler)}}
                    >
                      {action.title}
                    </button>
                  </li>
                )}
              </ul>
              
              <ul className="popup__section">
                <li>
                  <button
                    className="btn btn--link btn--block"
                    onClick={this.handleCancel}
                  ><strong>Отмена</strong>
                  </button>
                </li>
              </ul>
            </div>
          }
        </ReactCSSTransitionGroup>

        <ReactCSSTransitionGroup
          transitionName="popup-transition-confirmation"
          transitionEnterTimeout={enterTimeout}
          transitionLeaveTimeout={leaveTimeout}
        >
          {isConfirm && 
            <div className="popup popup--confirmation">
              <div className="popup__section">
                <h6>
                  {confirmAction.confirmText}
                  {confirmAction.confirmSecondaryText &&
                    <small>{confirmAction.confirmSecondaryText}</small>
                  }
                </h6>
                <ul>
                  <li>
                    <button 
                      className="btn btn--link btn--block"
                      onClick={this.closeConfirm}
                      ><strong>Отмена</strong>
                    </button>
                  </li>
                  <li>
                    <button 
                      className="btn btn--link btn--red btn--block"
                      onClick={() => this.handleCallback(confirmAction.handler)}
                      >{confirmAction.confirmTitle}
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          }
        </ReactCSSTransitionGroup>
      </div>
    )
  }
}

export default Actions;