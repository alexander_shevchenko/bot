import React from 'react';
import Overlay from './Overlay';
import { connect } from 'react-redux';
import { resetFailure, resetGeneral } from '../../actions';
import PropTypes from 'prop-types';

// TODO: проптайпс
// TODO: убрать консоли, причесать стиль кода
//

/**
 * Shows Overlay with a slot for Alert component
 * and primary button with a variety of actions
 *
 * @param {Object} history react-router 'history' object - required
 * @param {String} action expected action — 'back', 'close', 'reset', 'router'
 * @param {String} pathname required if action === 'router'
 */

class OverlayAlert extends React.Component {
  constructor(props) {
    super(props);

    this.handleGoBack = this.handleGoBack.bind(this);
    this.handleBackToMsngr = this.handleBackToMsngr.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleRouter = this.handleRouter.bind(this);
    this.getSubmitMethod = this.getSubmitMethod.bind(this);
  }

  render() {
    return (
      <Overlay>
        <div className="overlay__alert">{this.props.children}</div>
        <footer className="overlay__footer">
          <button
            className="btn btn--secondary btn--block"
            onClick={this.getSubmitMethod()}
          >
            Close
          </button>
        </footer>
      </Overlay>
    );
  }

  handleGoBack() {
    console.log('back');
    this.props.history.goBack();
  }

  handleBackToMsngr() {
    console.log('close');
  }

  handleReset() {
    console.log('reset');
    this.props.resetFailure();
  }

  handleRouter() {
    console.log('reset');

    // Reset
    this.props.resetGeneral();

    // Redirect
    this.props.history.push({ pathname: this.props.pathname });
  }

  getSubmitMethod() {
    const { action } = this.props;

    switch (action) {
      case 'back':
        return this.handleGoBack;
      case 'close':
        return this.handleBackToMsngr;
      case 'reset':
        return this.handleReset;
      case 'router':
        return this.handleRouter;
      default:
        return null;
    }
  }
}

OverlayAlert.propTypes = {
  history: PropTypes.object.isRequired,
  action: PropTypes.string.isRequired,
  // TODO: добить кастомный валидатор?
  // https://www.npmjs.com/package/prop-types
  pathname: PropTypes.string,
};

OverlayAlert = connect(null, { resetFailure, resetGeneral })(OverlayAlert);
export default OverlayAlert;
