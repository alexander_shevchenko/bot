/**
 * Appearance From Top animation
 * wrap some element with the component and pass an optional ref, get an animated component
 * 
 * <AppearanceTop ref={someRef}>
 *  ...
 * </AppearanceTop>
 *  
 * @param {Object} ref - ref is optional, if you pass ref, you have to use chain in parent component
 * otherwise, animation wouldn't play
 */

import React from 'react';
import { useSpring, animated } from 'react-spring';

const AppearanceTop = React.forwardRef((props, ref) => {
  const { x } = useSpring({x: 1, from: {x: 0}, config: {mass: 2, tension: 400}, ref});

  return (
    <animated.div style={{
      transform: x.interpolate( [0, 1], [-35, 0] ).interpolate( x => `translate3d(0, ${x}px, 0)` ),
      opacity: x,
    }}>
      {props.children}
    </animated.div>
  )
});

export default AppearanceTop;