/**
 * Disappearance to Opacity 0
 * <FadeOpacity ref={someRef}>
 *  ...
 * </FadeOpacity>
 *
 * @param {Object} ref - ref is optional, if you pass ref, you have to use chain in parent component
 * otherwise, animation wouldn't play
 */

import React from 'react';
import { useSpring, animated } from 'react-spring';

const FadeOpacity = React.forwardRef((props, ref) => {
  const { x } = useSpring({ x: 0, from: { x: 1 }, ref });

  return (
    <animated.div
      style={{
        opacity: x,
      }}
      {...props}
    >
      {props.children}
    </animated.div>
  );
});

export default FadeOpacity;
