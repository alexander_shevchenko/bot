/**
 * Counter animation
 * 
 * @param {Number} start - number counter starts from
 * @param {Number} end - number counter ends with 
 * @param {Object} ref - ref is optional, if you pass ref, you have to use chain in parent component
 * otherwise, animation wouldn't play
 */

import React from 'react';
import { useSpring, animated } from 'react-spring';

export const Counter = React.forwardRef((props, ref) => {
  const { start, end } = props;
  const springProps = useSpring({number: end, from: {number: start}, config: {tension: 175, friction: 40}, ref});

  return (
    <animated.span {...props}>{springProps.number.interpolate(n => n.toFixed(0))}</animated.span>
  )
});

export default Counter;