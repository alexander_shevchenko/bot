/**
 * Appearance From Opacity 0 animation
 * wrap some element with the component and pass an optional ref, get an animated component
 * 
 * <AppearanceOpacity ref={someRef}>
 *  ...
 * </AppearanceOpacity>
 *  
 * @param {Object} ref - ref is optional, if you pass ref, you have to use chain in parent component
 * otherwise, animation wouldn't play
 */

import React from 'react';
import { useSpring, animated } from 'react-spring';

const AppearanceOpacity = React.forwardRef((props, ref) => {
  const { x } = useSpring({x: 1, from: {x: 0}, ref});

  return (
    <animated.div 
      style={{opacity: x}}
      {...props}
    >
      {props.children}
    </animated.div>
  )
});

export default AppearanceOpacity;