import React from 'react';
import { Link } from 'react-router-dom';
import { paths } from '../consts/paths';

const styles = {
  display: 'block',
  padding: '10px',
  borderTop: '1px solid #ccc',
  borderBottom: '1px solid #ccc',
};

const Dummy = () => {
  return (
    <div className="dummy">
      <ul>
        <li>
          <Link to={paths.study} style={styles}>
            Тренировка
          </Link>
        </li>
        <li>
          <Link to={paths.addCard} style={styles}>
            Добавить оксфорд-карточку
          </Link>
        </li>
        <li>
          <Link to={paths.addCustomCard} style={styles}>
            Добавить вручную
          </Link>
        </li>
        <li>
          <Link to={paths.editCard} style={styles}>
            Редактирование карточек
          </Link>
        </li>
        <li>
          <Link to={paths.cards} style={styles}>
            Новый список слов
          </Link>
        </li>
        <li>
          <Link to={paths.settings} style={styles}>
            Настройки
          </Link>
        </li>
        <li>
          <Link to={paths.references} style={styles}>
            !! UI Библиотека !!
          </Link>
        </li>
        <li>
          <Link to={paths.playground} style={styles}>
            Песочница упражнений
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default Dummy;
