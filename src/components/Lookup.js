import React from 'react';
import Definition from './misc/definition';
import cloneDeep from 'lodash/cloneDeep';
import { connect } from 'react-redux';
import { uuid } from 'uuidv4';
import {
  fetchOxfordThesaurus,
  resetThesaurus,
} from '../actions/fetchOxfordThesaurus';
import { fetchOxford } from '../actions/fetchOxford';
import { addCard } from '../actions/addCard';
import { ioMessages } from '../actions/consts';
import { Link } from 'react-router-dom';
import { paths } from '../consts/paths';
import { pluralize_en } from '../helpers/pluralize';
import { occurenceArray } from '../helpers/occurenceArray';
import SingleTextarea from './layout/overlayForm/singleTextarea';
import SingleInput from './layout/overlayForm/singleInput';
import Shorten from './layout/overlayForm/shorten';
import Notice from './misc/notice';
import Summary from './layout/lookup/summary';
import Review from './layout/lookup/review';
import Audio from './misc/audio';

import Alert from './misc/alert';
import Overlay from './layout/Overlay';
import OverlayAlert from './layout/OverlayAlert';
import Spinner from './misc/spinner';

import { fetchFreeDictionary } from '../actions/fetchFreeDictionary';

const matchTranslations = (raw, part) => {
  let translations = [];
  if (raw && raw.length) {
    raw.forEach((tr) => {
      if (tr.lexicalCategory === part) {
        translations = translations.concat(tr.translations);
      }
    });
  }

  return translations;
};

const findSense = (word, id) => word.senses.find((sense) => sense.id === id);

const Word = ({
  isFetching,
  isFailure,
  isDone,
  history,

  wordMode,
  handleCloseOverlay,
  handleBack,
  handleOneMore,
  handleFinalSubmit,

  /* word data */
  wordTitle,
  wordPart,
  wordTranscription,
  wordAudio,
  wordSenses,
  wordSense,
  wordDomains,
  wordDef,
  wordExample,
  wordNextExample,
  wordSyn,
  wordAnt,
  wordTranslations,
  wordHint,

  /* definition */
  handleSenseSelect,

  /* example */
  handleExamplesSubmit,
  handleAddExtraShow,
  handleAddExtraSubmit,
  handleShortenShow,
  exampleIndex,
  isExampleAddExtra,
  isShortenShown,
  shortenExampleTokens,
  shortenExampleIndex,
  handleShortenOnSubmit,
  nextExampleIndex,
  handleExampleSelect,

  /* thesaurus */
  synonymsRaw,
  antonymsRaw,
  isSynonymAdd,
  isAntonymAdd,
  handleAddSynSubmit,
  handleAddAntSubmit,
  handleAddSynShow,
  handleAddAntShow,
  handleThesaurusToggle,
  handleThesaurusSubmit,

  /* translation */
  isTranslationAdd,
  currentTranslations,
  handleTransToggle,
  handleAddTransShow,
  handleAddTransSubmit,
  handleTransSubmit,

  /* hint */
  hint,
  handleHintSubmit,
  handleHintShow,
  isHintAdd,

  /* User */
  targetLanguage,
}) => {
  // в целом компонент Word имеет три режима/шага:
  // defs (default) - выбор определения/примеров
  // thesaurus - выбор синонимов/антонимов
  // final - ввод перевода/подсказки, финальный сабмит

  return (
    <div className="page page--zero-pp page--flex lookup">
      {isFetching && (
        <Overlay>
          <Spinner>
            {wordMode === 'examples'
              ? 'Searching oxford thesaurus'
              : 'SAVING CARD'}
          </Spinner>
        </Overlay>
      )}

      {isFailure && (
        <OverlayAlert history={history} action="reset">
          {isFailure === 'Api Down' && (
            <Alert
              type="serverError"
              title={ioMessages.oxford.apiDown}
              description={ioMessages.oxford.apiDownSecondary}
            />
          )}
          {(isFailure === 'Api Error' ||
            isFailure === ioMessages.connection.serverError) && (
            <Alert
              type="serverError"
              title={ioMessages.connection.serverError}
              description={ioMessages.connection.serverErrorSecondary}
            />
          )}
          {isFailure === ioMessages.connection.connectionError && (
            <Alert
              type="connectionFailed"
              title={ioMessages.connection.connectionError}
              description={ioMessages.connection.connectionErrorSecondary}
            />
          )}
          {isFailure === ioMessages.validation.duplicate && (
            <Alert
              type="duplicateFound"
              title={ioMessages.validation.duplicate}
              description={ioMessages.validation.duplicateWordSecondary}
            />
          )}
          {isFailure === ioMessages.validation.validationError && (
            <Alert
              type="validationError"
              title={ioMessages.validation.validationError}
              description={ioMessages.validation.validationErrorSecondary}
            />
          )}
        </OverlayAlert>
      )}
      {isDone && (
        <OverlayAlert history={history} action="close">
          <Alert
            type="done"
            title={ioMessages.oxford.addedPrimary}
            description={ioMessages.oxford.addedSecondary}
            actionTitle="Add one more"
            actionType="secondary--gray"
            actionHandler={handleOneMore}
          />
        </OverlayAlert>
      )}
      <div className="lookup__word">
        <div className="lookup__word__top">
          <Notice
            type="info"
            title={
              wordMode === 'defs'
                ? 'Select definition'
                : wordMode === 'examples'
                ? 'Select example'
                : wordMode === 'thesaurus'
                ? 'Select synonym/antonym'
                : wordMode === 'translation'
                ? 'Select translation'
                : wordMode === 'review'
                ? 'Review'
                : ''
            }
          />
          <div className="btn--back">
            <button className="btn btn--link" onClick={handleBack}>
              <span className="icon icon--angle icon--angle-left icon--primary2"></span>
              Back
            </button>
          </div>
        </div>
        {wordMode === 'defs' && (
          <div className="lookup__word__definitions">
            {wordSenses.map((sense) => {
              const domains =
                sense.domains && sense.domains.length ? sense.domains : null;
              const examples =
                sense.examples && sense.examples.length ? sense.examples : null;

              return (
                <Definition
                  title={sense.def}
                  domains={domains}
                  examples={examples}
                  handler={handleSenseSelect}
                  key={sense.id}
                  id={sense.id}
                />
              );
            })}
          </div>
        )}
        {wordMode === 'examples' && (
          <React.Fragment>
            <div className="lookup__word__examples">
              <div className="lookup__word__examples__contentWrapper">
                <div className="picker">
                  <h2 className="picker__title">{wordSense.def}</h2>
                  {!wordSense.examples.length && (
                    <p className="picker__nothing">
                      We could not find any examples for this definition. You
                      can add your own example or continue as is.
                    </p>
                  )}
                  {!!wordSense.examples.length && (
                    <ul className="picker__list">
                      {wordSense.examples.map((example, index) => {
                        const checked =
                          nextExampleIndex === index ? true : false;

                        return (
                          <li key={index}>
                            {checked && (
                              <button
                                className="btn btn--plain icon__wrapper picker__list__shorten"
                                onClick={(e) =>
                                  handleShortenShow(e, example, index)
                                }
                              >
                                <span className="icon__wrapper__container">
                                  <span className="icon icon-scissors"></span>
                                </span>
                              </button>
                            )}
                            <div className="form-control radio radio--inverted">
                              <label>
                                {example.map((word) => {
                                  return (
                                    <span
                                      key={word.id}
                                      className={
                                        'picker__list__token ' +
                                        (word.isActive ? '--active' : '')
                                      }
                                    >
                                      {word.word}
                                    </span>
                                  );
                                })}
                                <input
                                  type="radio"
                                  value={example
                                    .reduce(
                                      (total, word) =>
                                        (total += word.word + ' '),
                                      ''
                                    )
                                    .trim()}
                                  checked={checked}
                                  onChange={(e) =>
                                    handleExampleSelect(e, index)
                                  }
                                  name="examplesRadio"
                                />
                                <span className="checkmark"></span>
                              </label>
                            </div>
                          </li>
                        );
                      })}
                    </ul>
                  )}
                  <button
                    className="btn btn--small btn--secondary btn--gray"
                    onClick={handleAddExtraShow}
                  >
                    <span className="icon icon--add icon--s"></span>
                    Custom example
                  </button>
                </div>
              </div>
            </div>
            <footer>
              <button
                data-testid="pickExampleButton"
                className="btn btn--secondary btn--block"
                onClick={handleExamplesSubmit}
              >
                Continue
              </button>
            </footer>
          </React.Fragment>
        )}
        {wordMode === 'thesaurus' && (
          <React.Fragment>
            <div className="lookup__word__thesaurus">
              <div className="lookup__word__thesaurus__contentWrapper">
                <section>
                  {!synonymsRaw.length && (
                    <Alert
                      type="plain"
                      size="small"
                      title="Synonyms are not found"
                      // в стилях pre-wrap
                      // https://forum.freecodecamp.org/t/newline-in-react-string-solved/68484/10
                      description={`You can add an synonym on your own\nor continue as is`}
                    />
                  )}
                  {!!synonymsRaw.length && (
                    <ul
                      className="lookup__word__options"
                      data-testid="synonyms"
                    >
                      {synonymsRaw.map((synonym, index) => {
                        const isActive =
                          wordSyn && wordSyn === synonym ? true : false;
                        return (
                          <li key={index}>
                            <button
                              data-testid="pickSynonym"
                              className={
                                'btn btn--link btn--large btn--600 btn--serif ' +
                                (isActive
                                  ? 'btn--link--cyan'
                                  : 'btn--link--semiblack')
                              }
                              onClick={() =>
                                handleThesaurusToggle('s', synonym)
                              }
                            >
                              {synonym}
                            </button>
                          </li>
                        );
                      })}
                    </ul>
                  )}
                  <button
                    className="btn btn--small btn--secondary btn--gray"
                    onClick={handleAddSynShow}
                  >
                    <span className="icon icon--add icon--s"></span>
                    Custom synonym
                  </button>
                </section>
                <section>
                  {!antonymsRaw.length && (
                    <Alert
                      type="plain"
                      size="small"
                      title="Antonyms are not found"
                      // в стилях pre-wrap
                      // https://forum.freecodecamp.org/t/newline-in-react-string-solved/68484/10
                      description={`You can add an antonym on your own\nor continue as is`}
                    />
                  )}
                  {!!antonymsRaw.length && (
                    <ul
                      className="lookup__word__options"
                      data-testid="antonyms"
                    >
                      {antonymsRaw.map((antonym, index) => {
                        const isActive =
                          wordAnt && wordAnt === antonym ? true : false;
                        return (
                          <li key={index}>
                            <button
                              data-testid="pickAntonym"
                              className={
                                'btn btn--link btn--large btn--600 btn--serif ' +
                                (isActive
                                  ? 'btn--link--cyan'
                                  : 'btn--link--semiblack')
                              }
                              onClick={() =>
                                handleThesaurusToggle('a', antonym)
                              }
                            >
                              {antonym}
                            </button>
                          </li>
                        );
                      })}
                    </ul>
                  )}
                  <button
                    className="btn btn--small btn--secondary btn--gray"
                    onClick={handleAddAntShow}
                  >
                    <span className="icon icon--add icon--s"></span>
                    Custom antonym
                  </button>
                </section>
              </div>
            </div>
            <footer>
              <button
                data-testid="thesaurusSubmit"
                className="btn btn--secondary btn--block"
                onClick={handleThesaurusSubmit}
              >
                Continue
              </button>
            </footer>
          </React.Fragment>
        )}
        {wordMode === 'translation' && (
          <React.Fragment>
            <div className="lookup__word__translation">
              <div className="lookup__word__translation__contentWrapper">
                <section>
                  {!currentTranslations.length && (
                    <Alert
                      type="plain"
                      size="small"
                      title="Translation is not found"
                      // в стилях pre-wrap
                      // https://forum.freecodecamp.org/t/newline-in-react-string-solved/68484/10
                      description={`You can add your own translation\nor continue as is`}
                    />
                  )}
                  {!!currentTranslations && (
                    <ul
                      className="lookup__word__options"
                      data-testid="translations"
                    >
                      {currentTranslations.map((el, index) => {
                        const isActive = wordTranslations.includes(el)
                          ? true
                          : false;
                        return (
                          <li key={index}>
                            <button
                              data-testid="pickTranslation"
                              className={
                                'btn btn--link btn--large btn--600 btn--serif ' +
                                (isActive
                                  ? 'btn--link--cyan'
                                  : 'btn--link--semiblack')
                              }
                              onClick={() => handleTransToggle(el)}
                            >
                              {el}
                            </button>
                          </li>
                        );
                      })}
                    </ul>
                  )}
                  <button
                    className="btn btn--small btn--secondary btn--gray"
                    onClick={handleAddTransShow}
                  >
                    <span className="icon icon--add icon--s"></span>
                    Custom translation
                  </button>
                </section>
              </div>
            </div>
            <footer>
              <button
                data-testid="translationSubmit"
                className="btn btn--secondary btn--block"
                onClick={handleTransSubmit}
              >
                Continue
              </button>
            </footer>
          </React.Fragment>
        )}
        {wordMode === 'review' && (
          <React.Fragment>
            <div className="lookup__word__final">
              <div className="lookup__word__final__contentWrapper">
                <Review
                  wordTitle={wordTitle}
                  wordPart={wordPart}
                  wordAudio={wordAudio}
                  wordSyn={wordSyn}
                  wordAnt={wordAnt}
                  wordDef={wordDef}
                  wordDomains={wordDomains}
                  wordExample={wordExample}
                  wordTranslations={wordTranslations}
                  hint={hint}
                  handleHintShow={handleHintShow}
                  targetLanguage={targetLanguage}
                />
                {wordAudio && (
                  <Audio url={wordAudio} transcription={wordTranscription} />
                )}
              </div>
            </div>

            <footer>
              <button
                className="btn btn--primary btn--block"
                onClick={handleFinalSubmit}
                data-testid="finaStep-submit"
              >
                Save Card
              </button>
            </footer>
          </React.Fragment>
        )}

        {wordMode !== 'review' && (
          <Summary
            wordTitle={wordTitle}
            wordPart={wordPart}
            wordDef={wordDef}
            wordDomains={wordDomains}
            wordSyn={wordSyn}
            wordAnt={wordAnt}
            wordTranslations={wordTranslations}
            wordNextExample={wordNextExample}
            currentTranslations={currentTranslations}
          />
        )}
      </div>

      {isExampleAddExtra && (
        <>
          {(() => {
            return (
              <SingleTextarea
                label="example"
                value=""
                submitTitle="Continue"
                handleOnCancel={(e) =>
                  handleCloseOverlay(e, 'isExampleAddExtra')
                }
                handleOnSubmit={handleAddExtraSubmit}
              />
            );
          })()}
        </>
      )}
      {isShortenShown && (
        <>
          {(() => {
            return (
              <Shorten
                tokenizedExample={shortenExampleTokens}
                exampleIndex={shortenExampleIndex}
                wordTitle={wordTitle}
                submitTitle="Cut"
                handleOnCancel={(e) => handleCloseOverlay(e, 'isShortenShown')}
                handleOnSubmit={handleShortenOnSubmit}
                note={{ title: 'Touch to toggle' }}
              />
            );
          })()}
        </>
      )}
      {isSynonymAdd && (
        <>
          {(() => {
            return (
              <SingleInput
                label="synonym"
                value=""
                submitTitle="Add"
                handleOnCancel={(e) => handleCloseOverlay(e)}
                handleOnSubmit={handleAddSynSubmit}
              />
            );
          })()}
        </>
      )}
      {isAntonymAdd && (
        <>
          {(() => {
            return (
              <SingleInput
                label="antonym"
                value=""
                submitTitle="Add"
                handleOnCancel={(e) => handleCloseOverlay(e)}
                handleOnSubmit={handleAddAntSubmit}
              />
            );
          })()}
        </>
      )}
      {isTranslationAdd && (
        <>
          {(() => {
            return (
              <SingleInput
                label="translation"
                value=""
                submitTitle="Add"
                handleOnCancel={(e) => handleCloseOverlay(e)}
                handleOnSubmit={handleAddTransSubmit}
              />
            );
          })()}
        </>
      )}
      {isHintAdd && (
        <>
          {(() => {
            return (
              <SingleInput
                note={{
                  title: 'Don’t overuse hints',
                  extra:
                    'We recommend using hints only in cases where it is possible to confuse two or more similar cards. If this is not the case, we suggest you go ahead without a hint.',
                }}
                label="hint"
                value={wordHint}
                submitTitle="Add"
                handleOnCancel={(e) => handleCloseOverlay(e)}
                handleOnSubmit={handleHintSubmit}
              />
            );
          })()}
        </>
      )}
    </div>
  );
};

const Result = ({
  item,
  index,
  translations,
  handleWordSubmit,
  handleSuggestion,
}) => {
  // часть речи
  const part = item.lexicalCategory;
  // homograph, если есть
  const hom = item.homographNumber ? item.homographNumber : null;
  // считаю количество определений
  const defsCounter = item.senses.length;
  // считаю количество примеров
  let examplesCounter = 0;
  item.senses.forEach((sense) => {
    if (sense.examples) {
      examplesCounter += sense.examples.length;
    }
  });
  // ИСКЛЮЧЕНИЕ 1 -- если нет определений, но есть derivativeOf, то даю ссылку на первый дериватив
  // и скрываю переводы
  let derivativeOf = null;
  if (item.senses.length === 0 && item.derivativeOf) {
    derivativeOf = item.derivativeOf[0];
  }
  // ИСКЛЮЧЕНИЕ 2 -- нет определений, но есть crossReference, даю ссылку на crossReference
  let crossReference = null;
  if (item.senses.length === 0 && item.crossReference) {
    crossReference = item.crossReference;
  }

  return (
    <li
      onClick={
        derivativeOf
          ? (e) => handleSuggestion(derivativeOf.id, e)
          : crossReference
          ? (e) => handleSuggestion(crossReference.id, e)
          : (e) => handleWordSubmit(index)
      }
      className="flex2col"
    >
      <div className="flex2col__leftCol">
        <h4>
          <span>{item.word}</span>
          {part && <span className={'pill pill--' + part}>{part}</span>}
          {hom && <span className={'pill pill--s'}>{hom}</span>}
        </h4>
        {!!defsCounter && (
          <p className="lookup__results__results__counter">
            {pluralize_en(defsCounter, 'definition')}
            {!!examplesCounter && (
              <span> и {pluralize_en(examplesCounter, 'example')}</span>
            )}
          </p>
        )}
        {translations &&
          !!translations.length &&
          !(derivativeOf || crossReference) && (
            <p
              className="lookup__results__results__translations"
              data-testid="lookupTranslations"
            >
              {translations.join(', ')}
            </p>
          )}
        {/*(!translations.length && !(derivativeOf || crossReference)) && <p className="text-muted">перевод не найден</p>*/}
      </div>
      <div className="flex2col__rightCol">
        {derivativeOf && (
          <div className="lookup__results__results__derivative">
            derivative of
            <button
              className="btn btn--link --pure-link"
              data-testid="linkToDerivative"
            >
              {derivativeOf.text}
            </button>
          </div>
        )}
        {crossReference && (
          <div className="lookup__results__results__derivative">
            {crossReference.type}
            <button className="btn btn--link --pure-link">
              {crossReference.text}
            </button>
          </div>
        )}
        {!(derivativeOf || crossReference) && (
          <button
            className="btn btn--plain icon__wrapper icon__wrapper--secondary lookup__results__results__add"
            data-testid="pickPartSpeechButton"
          >
            <span className="icon__wrapper__container">
              <span className="icon icon--angle icon--s icon--primary"></span>
            </span>
          </button>
        )}
      </div>
    </li>
  );
};

// Обернуто в forwardRef, чтобы передать референс на инпут для фокуса на clear
const Results = React.forwardRef((props, ref) => {
  const {
    history,
    isFetching,
    isFailure,
    isNothing,
    handleQueryChange,
    handleLookupSubmit,
    handleWordSubmit,
    handleSuggestion,
    handleQueryClear,
    query,
    clear,

    entriesData,
    translationsData,
    searchData,
  } = props;

  return (
    <div className="page page--zero-pp page--flex lookup">
      {isFetching && (
        <Overlay>
          <Spinner>searching oxford dictionary</Spinner>
        </Overlay>
      )}

      {isFailure && (
        <OverlayAlert history={history} action="reset">
          {isFailure === 'Api Down' && (
            <Alert
              type="serverError"
              title={ioMessages.oxford.apiDown}
              description={ioMessages.oxford.apiDownSecondary}
            />
          )}
          {(isFailure === 'Api Error' ||
            isFailure === ioMessages.connection.serverError) && (
            <Alert
              type="serverError"
              title={ioMessages.connection.serverError}
              description={ioMessages.connection.serverErrorSecondary}
            />
          )}
          {isFailure === ioMessages.connection.connectionError && (
            <Alert
              type="connectionFailed"
              title={ioMessages.connection.connectionError}
              description={ioMessages.connection.connectionErrorSecondary}
            />
          )}
        </OverlayAlert>
      )}
      <div className="lookup__results">
        <form action="#" className="lookup__results__form">
          <div className="form-control">
            <input
              type="text"
              maxLength="60"
              id="indexSearchField"
              name="indexSearchField"
              placeholder="Search Oxford Dictionary"
              value={query}
              onChange={handleQueryChange}
              ref={ref}
            />
          </div>
          {!clear && (
            <button
              className="btn btn--plain lookup__results__magnifier"
              disabled={!query}
              onClick={handleLookupSubmit}
            >
              <span className="icon-search"></span>
            </button>
          )}
          {clear && (
            <button
              className="btn btn--plain icon__wrapper icon__wrapper--transparent lookup__results__clear"
              onClick={(e) => handleQueryClear(e)}
            >
              <span className="icon__wrapper__container">
                <span className="icon icon--add --clear icon--m icon--primary"></span>
              </span>
            </button>
          )}
        </form>
        {isNothing && (
          <div
            className="lookup__results__nothingFound"
            data-testid="nothingFound"
          >
            <Alert
              type="plain"
              size="small"
              title="Nothing Found"
              description="Try to change search query"
            />
          </div>
        )}
        {!isNothing && !entriesData && !searchData && (
          <div className="lookup__results__nothingFound">
            <Alert
              type="plain"
              size="small"
              title="No entries"
              description="Type a word in the search field above and we will find it!"
            />
          </div>
        )}
        {entriesData && (
          <div className="lookup__results__results" data-testid="lookupResults">
            <div className="lookup__results__results__wrapper">
              <ul>
                {entriesData.map((item, index) => {
                  return (
                    <Result
                      item={item}
                      translations={matchTranslations(
                        translationsData,
                        item.lexicalCategory
                      )}
                      key={index}
                      index={index}
                      handleWordSubmit={handleWordSubmit}
                      handleSuggestion={handleSuggestion}
                    />
                  );
                })}
              </ul>
            </div>
          </div>
        )}
        {searchData && (
          <div
            className="lookup__results__suggestions"
            data-testid="suggestions"
          >
            <div>
              <div className="text-muted">Did you mean?</div>
              {searchData.map((item) => {
                return (
                  <button
                    className="btn btn--link btn--large btn--pseudo btn--pseudo--black"
                    key={item.id}
                    onClick={(e) => handleSuggestion(item.word, e)}
                  >
                    {item.word}
                  </button>
                );
              })}
            </div>
          </div>
        )}
        <div className="lookup__byhand">
          <Link
            className="btn btn--link --pure-link btn--medium"
            to={{
              pathname: paths.addCustomCard,
              state: {
                query,
              },
            }}
          >
            Add manually
          </Link>
        </div>
      </div>
    </div>
  );
});

const Index = React.forwardRef((props, ref) => {
  const {
    history,
    isFetching,
    isFailure,
    handleQueryChange,
    handleLookupSubmit,
    query,
    isIndexInputValid,
  } = props;

  return (
    <div className="page page--zero-pp page--flex lookup">
      {isFetching && (
        <Overlay>
          <Spinner>searching oxford dictionary</Spinner>
        </Overlay>
      )}

      {isFailure && (
        <OverlayAlert history={history} action="reset">
          {isFailure === 'Api Down' && (
            <Alert
              type="serverError"
              title={ioMessages.oxford.apiDown}
              description={ioMessages.oxford.apiDownSecondary}
            />
          )}
          {(isFailure === 'Api Error' ||
            isFailure === ioMessages.connection.serverError) && (
            <Alert
              type="serverError"
              title={ioMessages.connection.serverError}
              description={ioMessages.connection.serverErrorSecondary}
            />
          )}
          {isFailure === ioMessages.connection.connectionError && (
            <Alert
              type="connectionFailed"
              title={ioMessages.connection.connectionError}
              description={ioMessages.connection.connectionErrorSecondary}
            />
          )}
        </OverlayAlert>
      )}

      <div className="lookup__index">
        <form action="#">
          {!isIndexInputValid && (
            <Notice title="Enter a word and press “Search”" type="warning" />
          )}
          <div className="form-control lookup__index__input">
            <input
              type="text"
              maxLength="60"
              id="indexSearchField"
              name="indexSearchField"
              placeholder="Search Oxford Dictionary"
              value={query}
              onChange={handleQueryChange}
              ref={ref}
              data-testid="indexSearchInput"
            />
          </div>
          <button
            className="btn btn--primary btn--block"
            onClick={handleLookupSubmit}
            data-testid="indexSearchSubmit"
          >
            Search
          </button>
        </form>
        <div className="lookup__byhand">
          <Link
            className="btn btn--link --pure-link btn--medium"
            to={{
              pathname: paths.addCustomCard,
              state: {
                query,
              },
            }}
          >
            Add manually
          </Link>
        </div>
      </div>
    </div>
  );
});

/**
 * Компонент довольно комплексный, содержит в себе по сути три подкомпонента —
 * index - стартовая форма
 * results - результаты поиска и, после первого поиска, уже стартовая страница
 * word - интерфейс добавления части речи в словарь
 */
class Lookup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // пользовательские настройки (включены ли переводы и какой язык, если включены)
      isTranslationEnabled: props.userSettings.translationEnabled,
      targetLanguage: props.userSettings.targetLanguage
        ? props.userSettings.targetLanguage
        : null,

      // mode определяет в каком режиме мы находимся — index, results, word
      // логика следующая -
      // если саксесс серча еще ни разу не было -- index (саксесс === 200 или 404)
      // если был серч и нет активного слова в стэйте -- result
      // если был серч и есть активное слово в стэйте -- word
      mode: 'index',

      // режим определяющий view в компоненте Word
      // defs, examples, thesaurus, translation, review
      wordMode: '',

      // значение поискового поля
      query: '',

      // крестик в поисковом поле (появляется на добавить еще одно, пропадает на любой ввод)
      clear: false,

      // валидно ли индексное поисковое поле
      isIndexInputValid: true,

      // Результаты fetch-a
      entriesData: null,
      translationsData: null,
      searchData: null,

      // Результаты поиска в тезаурусе
      synonymsRaw: [],
      antonymsRaw: [],

      // активное слово/часть речи
      currentWord: null,

      // TODO: фактически у меня будет где-то (в сторе?) жить результат
      // запроса в билингво словарь и оттуда я уже буду забирать нужные мне переводы
      // хомографов? или же я на уровне парсера дополню объект с датой из обычного словаря
      // найденными переводами?

      // переводы для активной части речи
      // пока hardcoded
      currentTranslations: [],

      // набор флагов для различных оверлеев
      isExampleAddExtra: false,
      isSynonymAdd: false,
      isAntonymAdd: false,
      isTranslationAdd: false,
      isHintAdd: false,
      isShortenShown: false,

      // временное хранилище для индекса примера в пикере примеров
      nextExampleIndex: 0,
      // текущий пример для укорачивания
      shortenExampleTokens: null,
      // index текущего примера в рамках текущего определения
      shortenExampleIndex: null,

      // здесь аккумулируются данные пользовательской карточки
      // частично или полностью стираются при переходах «Назад»
      activeData: {
        // id текущего определения
        definitionId: null,
        // индекс текущего примера (у примеров нет id)
        exampleIndex: null,
        // синоним/антоним
        synonym: null,
        antonym: null,
        // перевод/подсказка
        translations: [],
        hint: '',
      },
    };

    this.searchInputRef = React.createRef();
  }

  componentDidMount() {
    // если мы маунтимся и в параметрах есть init поисковый запрос — то сразу делаю поиск в оксфорде
    if (this.props.initialQuery) {
      this.handleInitSearch(this.props.initialQuery);
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // по умолчанию mode === index
    // если хоть раз был успешный серч statusCode === 404 или хоть раз пришла data,
    // я переключаюсь в results и этот кусок кода больше не отрабатыват,
    // то есть если mode !== index, то забыли про index
    // переключение же между results и word осуществляется с помощью отдельных хендлеров
    if (this.state.mode === 'index') {
      if (
        nextProps.entriesData ||
        nextProps.translationsData ||
        nextProps.searchData ||
        nextProps.isFailure === 'Nothing Found Entries'
      ) {
        this.setState({
          mode: 'results',
        });
      }
    }

    // в wordMode=examples пользователь жмет на кнопку и мы дергаем для него thesaurus
    // если в следующих пропсах есть thesaurusData или failure === 404
    // то меняем wordMode
    if (this.state.wordMode === 'examples') {
      if (
        nextProps.thesaurusData ||
        nextProps.isFailure === 'Nothing Found Thesaurus'
      ) {
        this.setState({
          wordMode: 'thesaurus',
        });
      }
    }

    // отправляю в стэйт всю дату
    const entriesData =
      nextProps.entriesData && nextProps.entriesData.length
        ? nextProps.entriesData
        : null;
    const translationsData =
      nextProps.translationsData && nextProps.translationsData.length
        ? nextProps.translationsData
        : null;
    const searchData =
      nextProps.searchData && nextProps.searchData.length
        ? nextProps.searchData
        : null;
    const thesaurus = nextProps.thesaurusData ? nextProps.thesaurusData : null;
    const synonymsRaw =
      thesaurus && thesaurus.synonyms && thesaurus.synonyms.length
        ? thesaurus.synonyms
        : [];
    const antonymsRaw =
      thesaurus && thesaurus.antonyms && thesaurus.antonyms.length
        ? thesaurus.antonyms
        : [];

    this.setState({
      entriesData,
      searchData,
      translationsData,
      synonymsRaw,
      antonymsRaw,
    });
  }

  /* QUERY и ПОИСК */
  /* Изменение query */
  handleQueryChange = (e) => {
    this.setState({
      query: e.target.value,
      // всегда на ввод сбрасываю потенциальный clear
      clear: false,
      // тут же на ввод сбрасываю валидацию индексного инпута
      isIndexInputValid: true,
    });
  };
  /* Clear query */
  handleQueryClear = (e) => {
    e.preventDefault();
    this.setState(
      {
        query: '',
        clear: false,
        entriesData: null,
        searchData: null,
        translationsData: null,
      },
      () => this.searchInputRef.current.focus()
    );
  };
  /* Submit поиска*/
  handleLookupSubmit = (e) => {
    // поскольку я могу вызвать лукапсабмит из handleSuggestion,
    // а event по умолчанию обнуляется после отработки родительского колбэка
    // и чтобы его передать нужно использовать event.persist и прочая прочая, то
    // я решил просто делать preventDefault в обоих методах, ивент не передавать, и делать
    // проверку, есть ли эвент и нужно ли делать превент
    if (e) {
      e.preventDefault();
    }

    // индексная форма позволяет кликнуть по пустому поиску
    // если query пустой -- обламываю поиск и меняю флаг
    // если нет -- то поиск (флаг обнуляю на ввод в соотв. хендлере)
    if (this.state.query) {
      //this.props.fetchOxford({
      this.props.fetchFreeDictionary({
        word: this.state.query,

        translationEnabled: this.state.isTranslationEnabled,
        targetLanguage: this.state.targetLanguage,
      });

      if (this.searchInputRef.current) this.searchInputRef.current.blur();
    } else {
      this.setState({
        isIndexInputValid: false,
      });
    }
  };
  /* Клик по вхождению из API Search */
  handleSuggestion = (id, e) => {
    e.preventDefault();
    this.setState(
      {
        query: id,
      },
      () => this.handleLookupSubmit()
    );
  };
  /* Поиск на маунт, если есть initSearchQuery */
  handleInitSearch = (id) => {
    this.setState(
      {
        query: id,
      },
      () => this.handleLookupSubmit()
    );
  };

  /* ЧАСТИ РЕЧИ */
  /* Выбор части речи */
  handleWordSubmit = (index) => {
    // взять выбранную часть речи, wordTitle и клонировать все определения
    const currentWord = this.props.entriesData[index];
    const currentWordTitle = currentWord.word;
    const currentWordSenses = cloneDeep(currentWord.senses);

    // токенизировать все примеры и перезаписать все определения
    currentWord.senses = this.tokenizeAllExamples(
      currentWordTitle,
      currentWordSenses
    );

    // забрать все переводы к данной части речи
    // обновить mode на word
    // обновить wordMode
    this.setState({
      currentWord,
      currentTranslations: matchTranslations(
        this.state.translationsData,
        this.props.entriesData[index].lexicalCategory
      ),
      mode: 'word',
      wordMode: 'defs',
    });
  };

  /* ОПРЕДЕЛЕНИЯ */
  /* Выбор определения */
  handleSenseSelect = (e, id) => {
    // устанавливаю в стэйте айди определения
    // показываю список примеров, если таковые есть
    // или приглашение к вводу кастомного примера, если таковых нет
    e.preventDefault();

    const sense = findSense(this.state.currentWord, id);

    this.setState({
      ...this.state,
      wordMode: 'examples',
      nextExampleIndex: 0,
      activeData: {
        ...this.state.activeData,
        definitionId: id,
      },
    });
  };

  /* ПРИМЕРЫ */
  /* токенизация всех примеров */
  tokenizeAllExamples = (wordTitle, senses) => {
    senses.forEach((sense) => {
      if (sense.examples && sense.examples.length) {
        sense.examples = sense.examples.map((example) => {
          return this.tokenizeExample(wordTitle, example);
        });
      }
    });

    return senses;
  };

  /* Токенизация примера */
  tokenizeExample = (wordTitle, example) => {
    // проверка для случая add one more (переиспользуется data от последнего поиска и все примеры уже токенизированы)
    // проверяю, если example string -- ok, else -- return example
    if (typeof example !== 'string') {
      return example;
    }

    // разбиваю пример на слова попробельно, знаки препинания аттачатся к предыдущему слову
    let exampleWords = example.split(' ');

    // пример может иметь неожиданные последовательности символов,
    // например в случае с cake пример начинается с перевода каретки и ряда пробелова
    // поэтому я проверяю, есть ли в каждом слове из exampleWords буквенные символы или дефис
    // если нет -- выбрасываю слово из массива
    exampleWords = exampleWords.filter((word) => {
      const match = word.match(/[\w-]+/);
      return match && match.length;
    });

    // затем беру wordTitle и разбиваю аналогично
    const wordTitleWords = wordTitle.split(' ');

    // тут тонкий момент, слова в exampleWords могут иметь знак препинания в конце (например запятую)
    // если слово в wordTitle массиве, например - "bright", а в массиве из примера - "bright," то совпадения не будет
    // поэтому для рассчета occurence, я делаю копию массива примера где удаляю знаки препинания в конце слов
    const exampleToCompare = exampleWords.map((word) => {
      return word.match(/[\w-]+/)[0];
    });

    // затем ищу первое полное вхождение массива из wordTitle в массиве из example
    const occurence = occurenceArray(exampleToCompare, wordTitleWords);

    // помечаю все occurences (если есть occurence)
    const occurences = [];
    if (occurence !== -1) {
      for (let i = 0; i < wordTitleWords.length; i++) {
        occurences.push(occurence + i);
      }
    }

    // создаю результирующий массив из слов примера
    // помечаю isWordTitle, если wordTitle нашелся в example (есть occurence)
    const tokenizedExample = exampleWords.map((word, index) => {
      return {
        word,
        isActive: true,
        isWordTitle: occurences.includes(index) ? true : false,
        id: uuid(),
      };
    });

    return tokenizedExample;
  };

  /* Перещелкивание примеров в пикере */
  handleExampleSelect = (e, index) => {
    this.setState({
      nextExampleIndex: index,
    });
  };

  /* Сабмит примера из списка */
  handleExamplesSubmit = (e) => {
    // записываю индекс выбранного примера в стэйт
    // дальше, дергаю тезаурус
    e.preventDefault();

    this.setState(
      {
        ...this.hideOverlays(),
        activeData: {
          ...this.state.activeData,
          exampleIndex: this.state.nextExampleIndex,
        },
      },
      this.fetchThesaurus
    );
  };

  /* Add Extra показать */
  handleAddExtraShow = (e) => {
    e.preventDefault();
    this.setState({
      isExampleAddExtra: true,
    });
  };
  /* Add Extra сабмит */
  handleAddExtraSubmit = (e, example) => {
    // токенизировать пример
    // добавить в стэйт
    // выбрать индекс нового примера
    // закрыть оверлей
    e.preventDefault();

    if (example) {
      const word = cloneDeep(this.state.currentWord);
      const sense = findSense(word, this.state.activeData.definitionId);

      if (!sense.examples) {
        sense.examples = [];
      }

      // токенизирую
      example = this.tokenizeExample(word.word, example);

      sense.examples.push(example);

      this.setState({
        ...this.state,
        currentWord: word,
        ...this.hideOverlays('isExampleAddExtra'),
        nextExampleIndex: sense.examples.length - 1,
      });
    } else {
      this.setState({
        ...this.hideOverlays('isExampleAddExtra'),
      });
    }
  };

  /* Добавление в стэйт кастомного примера */
  handleAddExample = (example) => {
    // найти определение
    // если пример не нулевой => добавить пример в массив (создать массив, если его нет)
    if (!example) return;

    const word = cloneDeep(this.state.currentWord);
    const sense = findSense(word, this.state.activeData.definitionId);

    if (!sense.examples) {
      sense.examples = [];
    }

    // токенизировать пример
    example = this.tokenizeExample(word.word, example);

    sense.examples.push(example);

    this.setState({
      ...this.state,
      currentWord: word,
    });
  };

  /* Показ диалога укорачивания примера */
  handleShortenShow = (e, exampleTokens, index) => {
    e.preventDefault();
    this.setState({
      isShortenShown: true,
      shortenExampleTokens: exampleTokens,
      shortenExampleIndex: index,
    });
  };

  handleShortenOnSubmit = (e, shortenExampleTokens, index) => {
    e.preventDefault();

    // клонирую текущую дату
    // нахожу требуемое определение
    const word = cloneDeep(this.state.currentWord);
    const sense = findSense(word, this.state.activeData.definitionId);

    // заменяю пример по индексу
    sense.examples[index] = shortenExampleTokens;

    // сохраняю стэйт
    this.setState({
      currentWord: word,
      isShortenShown: false,
    });
  };

  /* ТЕЗАУРУС */
  /* Fetch */
  fetchThesaurus = () => {
    // достаю определение
    const sense = findSense(
      this.state.currentWord,
      this.state.activeData.definitionId
    );
    // проверяю есть ли ссылки на тезаурус
    // !! хотя в ссылка может быть множество вхождений и парсер умеет обрабатывать это самое множество,
    // !! но я закладывал модель, что все вхождения будут вести к одному entry_id, а это не так
    // !! таким образом, временно, я забираю только первое вхождение в тезаурус
    // !! TODO: пофиксить проблему, поддержать реквест нескольких entry_id (проверять на уникальность,
    // поскольку есть case одинаковый entry_id и разные sense_id + case когда всё разное)
    let entry_id;
    let sense_id;
    if (sense.thesaurus && sense.thesaurus.length) {
      ({ entry_id, sense_id } = sense.thesaurus[0]);
    }

    if (entry_id && sense_id) {
      this.props.fetchOxfordThesaurus({
        word: entry_id,
        senseIds: [sense_id],
      });
    } else {
      this.props.fetchOxfordThesaurus({
        word: '',
        senseIds: null,
      });
    }
  };
  // TODO: сделать дженерик метод открывающий оверлей, аналогично закрывающему
  /* Add Synonym Show */
  handleAddSynShow = (e) => {
    e.preventDefault();
    this.setState({
      isSynonymAdd: true,
    });
  };
  /* Add Antonym Show */
  handleAddAntShow = (e) => {
    e.preventDefault();
    this.setState({
      isAntonymAdd: true,
    });
  };
  /* Add Synonym Submit */
  handleAddSynSubmit = (e, value) => {
    e.preventDefault();
    this.setState({
      ...this.hideOverlays(),
      ...this.addThesaurus('syn', value),
    });

    this.handleThesaurusToggle('s', value);
  };
  /* Add Antonym Submit */
  handleAddAntSubmit = (e, value) => {
    e.preventDefault();
    this.setState({
      ...this.hideOverlays(),
      ...this.addThesaurus('ant', value),
    });
    this.handleThesaurusToggle('a', value);
  };
  /* Добавление Син/Ант в стэйт */
  addThesaurus = (type, value) => {
    // type = syn/ant
    if (!value) return;
    const entity = type === 'syn' ? 'synonymsRaw' : 'antonymsRaw';
    return {
      [entity]: [...this.state[entity], value],
    };
  };
  /* Выбор синонимов/антонимов */
  handleThesaurusToggle = (mode, word) => {
    // поскольку процесс идентичен, то один хендлер тогглит обе сущности независимо — синонимы и антонимы
    // флаг mode - 's'/'a', определяет текущую сущность
    // 1. если активные синонимы/антонимы есть
    // то проверяю есть ли в активных данный,
    // если есть -- удаляю
    // если нет -- добавляю
    // 2. если активных нет —- просто добавляю
    const entity = mode === 's' ? 'synonym' : 'antonym';
    const current = this.state.activeData[entity];

    if (current) {
      if (this.state.activeData[entity] === word) {
        this.setState({
          activeData: {
            ...this.state.activeData,
            [entity]: null,
          },
        });
      } else {
        this.setState({
          activeData: {
            ...this.state.activeData,
            [entity]: word,
          },
        });
      }
    } else {
      this.setState({
        activeData: {
          ...this.state.activeData,
          [entity]: word,
        },
      });
    }
  };
  /* Метод перехода из тезауруса к переводу */
  handleThesaurusSubmit = () => {
    // если переводы включены, то перехожу к переводам
    // иначе прямо в review
    this.setState({
      wordMode: this.state.isTranslationEnabled ? 'translation' : 'review',
    });
  };
  /* Cancel-ы через генерик */

  /* ПЕРЕВОД */
  /* Add Trans Show */
  handleAddTransShow = (e) => {
    e.preventDefault();
    this.setState({
      isTranslationAdd: true,
    });
  };
  /* Add Trans Submit */
  handleAddTransSubmit = (e, value) => {
    e.preventDefault();
    const currentTranslations = this.state.currentTranslations;
    currentTranslations.push(value);
    this.setState({
      ...this.hideOverlays(),
      currentTranslations,
    });

    this.handleTransToggle(value);
  };
  /* Выбор переводов из списка */
  handleTransToggle = (word) => {
    // человек может выбрать 1-N вариантов перевода
    // на клик я проверяю выбран ли такой перевод
    // если выбран — сбрасываю выбор
    // если нет — выбираю
    let translations = [...this.state.activeData.translations];
    if (translations.includes(word)) {
      translations = translations.filter((el) => el !== word);
    } else {
      translations.push(word);
    }

    this.setState({
      activeData: {
        ...this.state.activeData,
        translations,
      },
    });
  };
  /* Сабмит в транслейшене */
  handleTransSubmit = () => {
    this.setState({
      wordMode: 'review',
    });
  };

  /* РЕВЬЮ */
  /* Add Hint Show*/
  handleHintShow = (e) => {
    e.preventDefault();
    this.setState({
      isHintAdd: true,
    });
  };
  handleHintSubmit = (e, value) => {
    e.preventDefault();
    this.setState({
      ...this.hideOverlays(),
      activeData: {
        ...this.state.activeData,
        hint: value,
      },
    });
  };
  /* Review Submit */

  /* <<< ВОЗВРАТЫ <<< */
  handleBack = (e) => {
    e.preventDefault();

    // в зависимости от wordmode есть разные сценарии возврата назад
    const wordMode = this.state.wordMode;

    switch (wordMode) {
      // ревью > перевод
      case 'review':
        // если переводы отключены, то шага с переводом не существует
        // и пользователь будет отправляться сразу на тезаурус

        // обычный флоу, перевод включен
        if (this.state.isTranslationEnabled) {
          this.setState({
            wordMode: 'translation',
            activeData: {
              ...this.state.activeData,
              hint: '',
            },
          });
        }

        // флоу без перевода
        if (!this.state.isTranslationEnabled) {
          this.setState({
            wordMode: 'thesaurus',
            activeData: {
              ...this.state.activeData,
              hint: '',
            },
          });
        }

        break;
      // перевод > тезаурус
      case 'translation':
        this.setState({
          wordMode: 'thesaurus',
          activeData: {
            ...this.state.activeData,
            translations: [],
          },
        });
        break;
      // тезаурус > примеры
      case 'thesaurus':
        this.setState({
          wordMode: 'examples',
          activeData: {
            ...this.state.activeData,
            antonym: null,
            synonym: null,
          },
        });
        break;
      // примеры > определения
      case 'examples':
        this.setState({
          wordMode: 'defs',
          nextExampleIndex: 0,
          activeData: {
            ...this.state.activeData,
            definitionId: null,
            exampleIndex: null,
          },
        });
        break;
      // определения > части речи
      case 'defs':
        this.setState({
          currentWord: null,
          mode: 'results',
          wordMode: '',
          activeData: {
            ...this.state.activeData,
          },
        });
        break;
      default:
        break;
    }
  };
  /* Возврат из любой точки к результатам поиска */
  handleBackToResults = () => {
    this.setState(
      {
        ...this.state,
        currentWord: null,
        mode: 'results',
        wordMode: '',
        // обнуляю всю пользовательскую информацию
        activeData: {
          definitionId: null,
          exampleIndex: null,
          synonym: null,
          antonym: null,
          translations: [],
          hint: '',
        },
        // не забываю обнулить тезаурус в сторе
      },
      this.handleThesaurusReset
    );
  };

  /* Возврат из done к началу (Добавить еще одно) */
  handleOneMore = () => {
    this.setState(
      {
        clear: true,
      },
      this.handleBackToResults
    );
  };

  /* Обнуление данных тезауруса */
  handleThesaurusReset = () => {
    this.props.resetThesaurus();
  };

  /* >>> ПЕРЕХОДЫ ВПЕРЁД >>> */
  /* Метод финального сабмита */
  handleFinalSubmit = () => {
    const { currentWord, activeData } = this.state;

    // достаю транскрипцию
    // TODO: сдублировано в header-e Word, надо бы мне все элементы касающиеся карточки хранить в activeData?
    let transcription;
    let audioUrl;
    if (currentWord.pronunciations && currentWord.pronunciations[0]) {
      transcription = currentWord.pronunciations[0].spelling
        ? currentWord.pronunciations[0].spelling
        : null;
      audioUrl = currentWord.pronunciations[0].audio
        ? currentWord.pronunciations[0].audio
        : null;
    }

    // достаю смысл
    const sense = findSense(currentWord, activeData.definitionId);

    // отдельно достаю пример
    // проверяю на null поскольку при численном преобразовании null даёт 0
    let tokenizedExample =
      activeData.exampleIndex !== null
        ? sense.examples[activeData.exampleIndex]
        : null;

    // поскольку пример у меня токенизирован, то для финального сабмита перегоняю массив в строку
    let example = '';

    if (tokenizedExample && tokenizedExample.length) {
      example = tokenizedExample.reduce((total, word) => {
        if (word.isActive) {
          return (total += word.word + ' ');
        } else {
          return total;
        }
      }, '');

      // отрезаю последний пробел
      example = example.trim();
    }

    this.props.addCard({
      // TODO: избавиться от словарей в целом и убрать соответствующий аттрибут
      // FIXME: case для оверлея больше не нужен, сходи и почисти экшен
      type: 'oxford',
      userId: this.props.userId,
      front: currentWord.word,
      wordId: currentWord.id,
      part: currentWord.lexicalCategory,
      transcription,
      audioUrl,
      definition: sense.def,
      defId: sense.id,
      example,
      domains: sense.domains,
      synonyms: this.state.synonymsRaw,
      synonym: activeData.synonym,
      antonyms: this.state.antonymsRaw,
      antonym: activeData.antonym,
      back: this.state.activeData.translations.join(', '),
      hint: this.state.activeData.hint,
    });
  };

  /* РАЗНОЕ */
  /* Универсальный метод закрытия оверлеев */
  handleCloseOverlay = (e, flag) => {
    e.preventDefault();
    this.setState({
      ...this.hideOverlays(flag),
    });
  };
  /* Объект для закрытия одного или нескольких оверлеев */
  hideOverlays = (flag) => {
    if (flag) {
      return {
        [flag]: false,
      };
    } else {
      return {
        isExampleAddExtra: false,
        isSynonymAdd: false,
        isAntonymAdd: false,
        isTranslationAdd: false,
        isHintAdd: false,
        isShortenShown: false,
      };
    }
  };

  render() {
    /* Достаю данные о слове/определение (если есть) */
    let wordTitle,
      wordPart,
      wordTranscription,
      wordAudio,
      wordSenses,
      wordSense,
      wordDomains,
      wordDef,
      wordExample,
      wordNextExample,
      wordSyn,
      wordAnt,
      wordTranslations = [],
      wordHint;

    const { currentWord, activeData, nextExampleIndex } = this.state;

    if (currentWord) {
      wordTitle = currentWord.word;
      wordPart = currentWord.lexicalCategory;
      // пока сюда приходят только произношения у которых транскрипция в ipa нотации
      // для простоты я беру первый элемент из этого массива и пробую достать всё из него
      // на практике посмотрим, работает ли такая тактика, если нет то нужно будет перебирать
      // элементы массива в поиске элемента у которого есть оба поля
      if (currentWord.pronunciations && currentWord.pronunciations[0]) {
        wordTranscription = currentWord.pronunciations[0].spelling
          ? currentWord.pronunciations[0].spelling
          : null;
        wordAudio = currentWord.pronunciations[0].audio
          ? currentWord.pronunciations[0].audio
          : null;
      }
      wordSenses = currentWord.senses;
    }

    if (currentWord && activeData.definitionId) {
      wordSense = findSense(currentWord, activeData.definitionId);
      wordDef = wordSense.def;
      wordDomains =
        wordSense.domains && wordSense.domains.length
          ? wordSense.domains
          : null;
      wordExample = wordSense.examples[activeData.exampleIndex];
      // нужен чтобы показывать в саммари пример отвечающий твоему текущему выбору на шаге выбора примеров
      wordNextExample = wordSense.examples[nextExampleIndex];
      wordSyn = activeData.synonym;
      wordAnt = activeData.antonym;
      wordTranslations = activeData.translations;
      wordHint = activeData.hint;
    }

    // ничего не найдено
    const isNothingEntries =
      this.props.isFailure === 'Nothing Found Entries' ? true : false;

    if (this.state.mode === 'index') {
      return (
        <Index
          history={this.props.history}
          isFetching={this.props.isFetching}
          isFailure={this.props.isFailure}
          handleQueryChange={this.handleQueryChange}
          handleLookupSubmit={this.handleLookupSubmit}
          query={this.state.query}
          ref={this.searchInputRef}
          isIndexInputValid={this.state.isIndexInputValid}
        />
      );
    }

    if (this.state.mode === 'results') {
      return (
        <Results
          history={this.props.history}
          isFetching={this.props.isFetching}
          isFailure={this.props.isFailure}
          isNothing={isNothingEntries}
          handleQueryChange={this.handleQueryChange}
          handleLookupSubmit={this.handleLookupSubmit}
          handleWordSubmit={this.handleWordSubmit}
          handleSuggestion={this.handleSuggestion}
          query={this.state.query}
          clear={this.state.clear}
          handleQueryClear={this.handleQueryClear}
          ref={this.searchInputRef}
          entriesData={this.state.entriesData}
          translationsData={this.state.translationsData}
          searchData={this.state.searchData}
        />
      );
    }

    if (this.state.mode === 'word') {
      return (
        <Word
          isFetching={this.props.isFetching}
          isFailure={this.props.isFailure}
          isDone={this.props.isDone}
          history={this.props.history}
          /* Mode/шаг компонента */
          wordMode={this.state.wordMode}
          overlayMode={this.state.overlayMode}
          handleBack={this.handleBack}
          handleCloseOverlay={this.handleCloseOverlay}
          handleOneMore={this.handleOneMore}
          handleFinalSubmit={this.handleFinalSubmit}
          /* word data */
          wordTitle={wordTitle}
          wordPart={wordPart}
          wordTranscription={wordTranscription}
          wordAudio={wordAudio}
          wordSenses={wordSenses}
          wordSense={wordSense}
          wordDomains={wordDomains}
          wordDef={wordDef}
          wordExample={wordExample}
          wordNextExample={wordNextExample}
          wordSyn={wordSyn}
          wordAnt={wordAnt}
          wordTranslations={wordTranslations}
          wordHint={wordHint}
          /* definition */
          handleSenseSelect={this.handleSenseSelect}
          /* example */
          handleExamplesSubmit={this.handleExamplesSubmit}
          handleAddExtraShow={this.handleAddExtraShow}
          handleAddExtraSubmit={this.handleAddExtraSubmit}
          handleShortenShow={this.handleShortenShow}
          exampleIndex={this.state.activeData.exampleIndex}
          isExampleAddExtra={this.state.isExampleAddExtra}
          isShortenShown={this.state.isShortenShown}
          shortenExampleTokens={this.state.shortenExampleTokens}
          shortenExampleIndex={this.state.shortenExampleIndex}
          handleShortenOnSubmit={this.handleShortenOnSubmit}
          nextExampleIndex={this.state.nextExampleIndex}
          handleExampleSelect={this.handleExampleSelect}
          /* thesaurus */
          synonymsRaw={this.state.synonymsRaw}
          antonymsRaw={this.state.antonymsRaw}
          isSynonymAdd={this.state.isSynonymAdd}
          isAntonymAdd={this.state.isAntonymAdd}
          handleAddSynSubmit={this.handleAddSynSubmit}
          handleAddAntSubmit={this.handleAddAntSubmit}
          handleAddSynShow={this.handleAddSynShow}
          handleAddAntShow={this.handleAddAntShow}
          handleThesaurusToggle={this.handleThesaurusToggle}
          handleThesaurusSubmit={this.handleThesaurusSubmit}
          /* translation */
          isTranslationAdd={this.state.isTranslationAdd}
          currentTranslations={this.state.currentTranslations}
          handleTransToggle={this.handleTransToggle}
          handleAddTransShow={this.handleAddTransShow}
          handleAddTransSubmit={this.handleAddTransSubmit}
          handleTransSubmit={this.handleTransSubmit}
          /* hint */
          handleHintShow={this.handleHintShow}
          handleHintSubmit={this.handleHintSubmit}
          hint={this.state.activeData.hint}
          isHintAdd={this.state.isHintAdd}
          /* User */
          targetLanguage={this.state.targetLanguage}
        />
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isFetching: state.lookup.isFetching,
    isFailure: state.lookup.isFailure,
    isDone: state.lookup.isDone,

    initialQuery: state.lookup.initialQuery,
    entriesData: state.lookup.entriesData,
    translationsData: state.lookup.translationsData,
    searchData: state.lookup.searchData,
    thesaurusData: state.lookup.thesaurusData,

    userId: state.user.id,
    userSettings: state.user.settings,
  };
};

Lookup = connect(mapStateToProps, {
  fetchOxford,
  fetchOxfordThesaurus,
  resetThesaurus,
  addCard,

  fetchFreeDictionary,
})(Lookup);

export default Lookup;
