// https://www.dictionaryapi.com/products/json
// learners api
// https://www.dictionaryapi.com/api/v3/references/learners/json/test?key=94233503-fc1f-4105-a8e3-1b193376e50c
// collegiate api
// https://www.dictionaryapi.com/api/v3/references/collegiate/json/test?key=120339b5-0e13-4728-bced-58e6f55f886d
// у нас на данный момент "Developer" аккаунт,
// дает 1000 запросов в день
// для некоммерческого использования
// когда начнем брать деньги, поменяем тип аккаунта
// Learners — вроде как наиболее подходящий для нашего случая API
// Collegiate — какой-то модный словарь, взял просто до кучи (разрешают два ключа к API)
//
// api не работает с клиент сайдом, поэтому нужен метод на бэкенде который будет запрашивать апи и возвращать клиенту результат
//
// Итерации внедрения —
// 1. прикручиваем Learners
// 2. прикручиваем Collegiate
// 3. тестируем, крутим, вертим

export const learnersApiKey = '94233503-fc1f-4105-a8e3-1b193376e50c';
export const learnersApiUrl = (word) => `https://www.dictionaryapi.com/api/v3/references/learners/json/${word}?key=${learnersApiKey}`;
export const collegiateApiKey = '120339b5-0e13-4728-bced-58e6f55f886d';
export const collegiateApuUrl = (word) => `https://www.dictionaryapi.com/api/v3/references/collegiate/json/${word}?key=${collegiateApiKey}`;