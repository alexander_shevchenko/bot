const bd = {
  queue: [
    {
      id: 'c01',
      front: 'map',
      back: 'карта, карты, картографирование',
      status: 'review',
      good: 0,
      again: 0
    },
    {
      id: 'c02',
      front: 'fealty',
      back: 'верность вассала феодалу',
      status: 'new',
      good: 0,
      again: 0
    },
    {
      id: 'c03',
      front: 'brisk',
      back: 'юркий, живой',
      status: 'review',
      good: 0,
      again: 0
    },
    {
      id: 'c04',
      front: 'spry',
      back: 'проворный',
      status: 'new',
      good: 0,
      again: 0
    },
    {
      id: 'c05',
      front: 'abundant',
      back: 'обильный',
      status: 'review',
      good: 0,
      again: 0
    },
    {
      id: 'c06',
      front: 'exuberant',
      back: 'буйный, порывистый, сильный',
      status: 'new',
      good: 0,
      again: 0
    },
    {
      id: 'c07',
      front: 'rambunctious',
      back: 'быстро раздражающийся, быстро приходящий в нервное возбуждение.',
      status: 'new',
      good: 0,
      again: 0
    },
    {
      id: 'c08',
      front: 'By default, flex items are laid out in the source order.',
      back: 'По умолчанию элементы Flex размещаются в исходном порядке.',
      status: 'new',
      good: 0,
      again: 0
    },
    {
      id: 'c09',
      front: 'flank',
      back: 'фланговый',
      status: 'new',
      good: 0,
      again: 0
    }
  ]
}


const delay = (ms) => 
  new Promise(resolve => setTimeout(resolve, ms));

export const fetchCards = () =>
  delay(500).then(() => {
    if (Math.random() > .9) {
      throw new Error('error!');
    }

    return bd.queue;
  });