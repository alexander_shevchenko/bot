/**
 * Метод получает массиив данных (entries) от merriam webster api,
 * и далее либо возвращает негативный ответ (если искомое слово не найдено ни в headwords
 * ни в dros), либо возвращает набор из данных (зависит от того, найдено ли вхождение в 
 * headwords или dros).
 *  
 * @docs https://www.dictionaryapi.com/products/json
 * 
 * @param {String} request  искомое слово
 * @param {Array}  response массив из entries
 * 
 * @return {Object} набор данных извлеченных из исходного массива entries
 * 
 *  @case Вхождение/я в Headwords
 *    @param {String} ipa транскрипция в ipa (International Phonetic Alphabet) формате
 *    @param {String} audio ссылка на аудио-файл с произношением (для первого headword)
 *    @param {Array}  entries список определений с, опционально, соответствующей частью речи 
 *                            и примерами использования
 *    @param {String} entries[i].fl функциональный лейбл, грамматическая функция слова 
 *                                  (в частности — часть речи)
 *    @param {Array}  entries[i].defs массив из определений (с, опционально, примерами)
 *    @param {Object} entries[i].defs[k] объект «определение»
 *    @param {String} entries[i].defs[k].dt defining text, определение слова
 *    @param {Array}  entries[i].defs[k].vis verbal illustrations, массив из примеров 
 *                                           использования данного headword 
 *                                           в рамках данного определения
 *    @param {String} entries[i].defs[k].sls subject/status label (предметная область)
 * 
 *  @case Вхождение/я в DROS
 *  @docs https://www.dictionaryapi.com/products/json#sec-2.dros
 *    @param {Array}  entries массив определений
 *    @param {Object} entries[i] объект «определение»
 *    @param {String} entries[i].dt определение
 *    @param {Array}  entries[i].vis массив из примеров использования в рамках данного DRO
 */
export const parser = (request, response) => {
  /*
    1. Ищу вхождение слова в headwords
    искомое слово является headword если оно совпадает со значением meta.id
    или meta.id + :HN (где HN - homograph number, цифра или цифры)
  */
  const headword_matches = response.filter( entry => {
    // если request совпадает с id полностью — бинго
    // иначе, регулярное выражение ищет на позиции 0 '${request}:'
    
    // https://learn.javascript.ru/regexp-sticky
    const regex = new RegExp(`${request}:`, 'y');
    regex.lastIndex = 0;
    
    // как оказалось, entry может не иметь секции def (только dros),
    // при текущем дизайне алгоритма, такие entries мне не интересны
    if ( !entry.def ) return false;
    if ( request === entry.meta.id ) return true;
    if ( regex.exec(entry.meta.id) ) return true;

    return false;
  });
  
  /*
    2. Ищу вхождение слова в DROS
  */
  let dros_list = [];
  let dros_matches;
  response.forEach(entry => {
    if (entry.dros) {
      entry.dros.forEach( dros => dros_list.push(dros) )
    }
  });
  dros_matches = dros_list.filter( dros => request === dros.drp );

  /*
    3. Если слово не попало ни в headwords ни в dros, возвращаю false;
  */
  if (!headword_matches.length && !dros_matches.length) {
    return false;
  }

  /*
    4. Если слово есть в headwords, то забираю все данные только из них
    headwords имеет приоритет над dros
  */
  if (headword_matches.length) {
    // как выяснилось, произношение для разных частей речи одного и того же слова может отличаться
    // таким образом я забираю произношения для всех entry (у которых оно есть)
    // дальше UI должен адекватно распорядиться этой информацией

    // 4.1 забираю информацию (для каждого entry)
    let entries = [];
    headword_matches.forEach(entry => {
      entries.push({
        // 4.2 грамматическая роль
        fl: entry.fl,
        // 4.3 определения и группы определений
        defs: getDefs( entry.def ),
        // 4.4 транскрипции и аудио ссылка
        prs: parsePrs( entry, 'headword' ),
      })
    });
    
    return [
      ...entries,
    ];
  }

  /* 
    5. Если мы дошли сюда, то точное вхождение не было найдено в headwords, пробуем поиск в DROS
    https://www.dictionaryapi.com/products/json#sec-2.dros
  */
  if (dros_matches.length) {
    // dros в целом это массив из объектов / run on phrases
    // каждый объект включает в себя:
    // drp - сама run on phrase
    // def - секция с определениями
    // prs - теоретически (на практике еще не видел) может включать в себя секцию с произношением
    // для меня, каждый drp (я буду так называть объект run on phrasы) это по сути отдельный entry
    // в котором есть массив определений, аналогичный таковому у headwords
    let entries = [];

    // 5.1 беру каждый матч
    dros_matches.forEach(entry => {
      entries.push({
        // 5.2 определения и группы определений
        defs: getDefs( entry.def ),
        // 5.3 транскрипции и аудио ссылка
        prs: parsePrs( entry, 'dros' ),
      })
    });

    return [
      ...entries,
    ]
  }
}

/**
 * Метод получает entry и флаг (headword или dros)
 * и возвращает объект {written: {ipa: '', mw: ''}, audio: ''}
 * @param {Object} entry headword или единица dros
 * @return {Object} объект c транскрипциями и аудио-ссылкой
 */
const parsePrs = (entry, type) => {
  // 1. проверяю, есть ли prs секция (не у каждого entry она есть)
  let prs;

  // headword хранит секцию prs в hwi > prs
  // dros хранит секцию prs на рут уровне
  if (type === 'headword') {
    prs = entry.hwi && entry.hwi.prs ? entry.hwi.prs[0] : null;
  } else if (type === 'dros') {
    prs = entry.prs ? entry.prs[0] : null;
  }

  // 2 забираю транскрипцию из prs (если есть)
  // есть, как минимум, два формата произношения 
  // ipa — https://www.dictionaryapi.com/products/json#sec-3.addlprs
  // mw - written pronunciation in Merriam-Webster format
  // я пока буду сохранять все что есть
  // ui разберется, что именно показывать
  const ipa = prs && prs.ipa ? prs.ipa : null;
  const mw = prs && prs.mw ? prs.mw : null;
  // 4.3 забираю ссылку на аудио-произношение из prs (если есть)
  const audio_url = (prs && prs.sound && prs.sound.audio) ? getAudioLink( prs.sound.audio ) : null;

  return {
    written: {
      ipa,
      mw,
    },
    audio: audio_url,
  }
}

/**
 * Метод получает сырой массив из определений для одного headword
 * и возвращает массив с определениями и примерами
 * @docs https://www.dictionaryapi.com/products/json#sec-2.def
 * @docs https://www.dictionaryapi.com/products/json#sec-2.sseq
 * 
 * @param {Array} def массив из определений
 * 
 * @return {Array} результирующий массив
 * @param {String} entries[i].fl функциональный лейбл, грамматическая функция слова 
 * (в частности — часть речи)
 * @param {Array}  entries[i].defs массив из определений (с, опционально, примерами)
 * @param {Object} entries[i].defs[k] объект «определение»
 * @param {String} entries[i].defs[k].dt defining text, определение слова
 * @param {Array}  entries[i].defs[k].vis verbal illustrations, массив из примеров 
 * использования данного headword в рамках данного определения
 * @param {String} entries[i].defs[k].sls subject/status label (предметная область)
 */
const getDefs = (def) => {
  // def: [{vd:'', sseq: []}, {vd:'', sseq: []}...]
  // sseq: [ [ [sense, {}], ... ], ... ]
  // sense, {dt: [ [text, '...'], ['vis', [{t: '...', t: '...'}]], ..., ['vis', [{t: '...', t: '...'}]] ]}
  // экстра кейсы — bs + senses, pseq > bs + senses

  // 1.1 мне пришел def, это массив из объектов, 
  // объект может включать свойство vd (опционально) и sseq (искомое свойство)
  // задача — забрать все sseq
  let sseqs = [];
  def.forEach(d => {
    if ( d.sseq && Array.isArray( d.sseq ) ) {
      sseqs.push( d.sseq );
    }
  });
  
  // 1.2 если есть sseqs, то достанем оттуда senses
  // sseq это массив, который включает себя группы (массивы) из senses (каждый из которых это массив)
  let senses = [];
  if (sseqs.length) {
    sseqs.forEach(sseq => {
      sseq.forEach(sensegroup => {
        sensegroup.forEach(child => {
          // на этом уровне могут быть pseq/bs/sense
          
          // если pseq то разбираю по очереди все элементы
          // bs, если есть и senses
          // пушу объект {bs: {dt: '...'}, senses: [{sense}, ...]
          if ( child[0] === 'pseq' ) {
            let pseq = {
              bs: {},
              senses: [],
            };
            
            // проверяю, это bs или sense
            child[1].forEach(child => {
              if ( child[0] === 'bs' ) {
                // беру сенсы и подсенсы и клонирую в pseq.bs
                pseq.bs = Object.assign({}, child[1]['sense']);
              } else if ( child[0] === 'sense' ) {
                // беру сенсы и подсенсы и пушу в pseq.senses
                pseq.senses.push( child[1] );
              }
            });

            senses.push( pseq );
          }

          // если bs
          // одинокий bs на первом уровне я пока обрабатываю как обычный sense
          if ( child[0] === 'bs' ) {
            // беру сенсы и подсенсы и пушу в senses
            senses.push( child[1].sense );
          }

          // основной кейс -- sense
          if ( child[0] === 'sense' ) {
            // беру сенсы и подсенсы и пушу в senses
            senses.push( child[1] );
          }
        });
      });
      
    });
  }
  
  // 1.3 у меня есть на руках набор смыслов из def > sseq > pseq/bs/sense > sense (опционально)
  // теперь нужно обработать каждый sense, достать определение (dt) и примеры для определения (vis)
  let definitions = [];
  if (senses.length) {
    senses.forEach(sense => {
      let definition = {};
      // sense у меня может быть как единичным смыслом, так и группой смыслов pseq с bs в виде тайтла
      // в обоих случаях это объект
      // если первый ключ объекта = bs (или, что проще — на первом уровне есть ключ bs), то это pseq
      if ( sense.bs ) {
        // если это pseq то в дефинишн я кладу объект вида
        // bs: {dt: ''}, defs: [{def}, {def}, ...]
        // ВАЖНО — pseq может быть без bs, в таком случае sense.bs = null
        // и в таком случае нет необходимости делать разбор
        if (sense.bs.dt) {
          definition.bs = {
            dt: sense.bs.dt[0][1],
          }
        } else {
          definition.bs = {
            dt: null,
          }
        }
        
        definition.defs = [];
        sense.senses.forEach(el => {
          // комментарий про разбор сенса смотри ниже
          if ( el.sdsense ) {
            definition.defs.push( Object.assign({}, {sls: getSls(el.sdsense)}, parseDt(el.sdsense.dt)) )
          } else {
            definition.defs.push( Object.assign({}, {sls: getSls(el)}, parseDt(el.dt)) )
          }
        })
      } else {
        // обычный sense
        // мне нужно достать объект вида
        // {dt: '', vis: ['','',...]}
        // sense может включать sdsense (более узкое определение), 
        // сейчас, для простоты, я считаю этот sdsense приоритетным смыслом
        // и парсю только его
        // кроме dt, который парсится в определение и примеры, sense обладает рядом свойств
        // среди которых есть например 'sls', описывающий предметную область или региональный статус определения
        // https://www.dictionaryapi.com/products/json#sec-2.sls
        if ( sense.sdsense ) {
          definition = Object.assign({}, {sls: getSls(sense.sdsense)}, parseDt(sense.sdsense.dt))
        } else {
          definition = Object.assign({}, {sls: getSls(sense)}, parseDt(sense.dt))
        }
      }

      definitions.push( definition );
    });
  }

  return definitions;
}

/**
 * Метод получает sense или sdsense
 * и возвращает массив из sls элементов, либо пустой массив
 * @docs https://www.dictionaryapi.com/products/json#sec-2.sls
 * @param {Object} sense sense или sdsense
 * @return {Array} массив из sls элементов или пустой
 */
const getSls = (sense) => sense.sls ? [...sense.sls] : [];

/**
 * Метод получает audio reference (строка)
 * и возвращает ссылку на аудиофайл
 * @docs https://www.dictionaryapi.com/products/json#sec-2.prs
 * @param {String} audio audio reference
 * @return {String} ссылка на аудио-файл
 */
const getAudioLink = (audio) => {
  let subdirectory;
  // https://learn.javascript.ru/regexp-sticky
  const regexp = /\w/y;
  regexp.lastIndex = 0;
  
  // если аудио стартует с bix, то сабдиректория = bix
  if ( audio.indexOf('bix') === 0 ) {
    subdirectory = 'bix';
  } else if ( audio.indexOf('gg') === 0 ) {
    // если аудио стартует с gg, то сабдиректория = gg
    subdirectory = 'gg';
    
  } else if ( !regexp.exec( audio ) ) {
    // если аудио стартует с number или с punctuation знака, то сабдиректория = number
    // тут я пошел от обратного, если это не буква — значит бинго (учитывая что в документации нет
    // списка знаков пунктуации)
    subdirectory = 'number';
  } else {
    // если ни одно из условий выше не отработало, значит subdirectory = первому символу
    // в строке audio
    subdirectory = audio.slice(0, 1);
  }

  return `https://media.merriam-webster.com/soundc11/${subdirectory}/${audio}.wav`;
}

/**
 * Метод получает массив dt
 * и возвращает объект {dt: '...', vis: ['','', {type: 'uns', dt: '', vis: ['',''...]}, ...]}
 * @param {Array} dt сырой массив
 * @return {Object} объект c определением и, опционально, примерами
 */
const parseDt = (dt) => {
  // dt, если верить документации, должен содержать определение в нулевом члене массива
  // хоть для большинства случаев это и правда, 
  // в некоторых случаях dt может быть без определения
  // в частности вариант без dt был замечен в случае когда единственным потомком был массив uns
  // что имеет смысл, поскольку у uns есть свое определение/ремарка
  
  // утиная типизация, я знаю, что dt, если есть, то живет по нулевому адресу
  // и это массив из двух членов, первый строка - 'text', второй строка с определением
  // если такой элемент не найден по нулевому адресу, предполагаю, что это тот случай когда dt нет
  const definition = ( Array.isArray( dt[0] ) && dt[0][0] === 'text' ) ? dt[0][1] : null;
  
  // dt может включать от 0 до нескольких массивов с примерами ['vis', {t: '...', ..., t: '...'}]
  // пример лежит под ключом t
  // dt также может включать в себя 'uns' секцию, которая может включать в себя примеры
  // https://www.dictionaryapi.com/products/json#sec-2.uns
  let examples = [];
  dt.forEach(el => {
    // основной кейс — vis, просто пушу
    if (el[0] === 'vis') {
      const vis = el[1];
      for (let i = 0; i < vis.length; i++) {
        examples.push(vis[i]);
      }
    } else if (el[0] === 'uns') {
      // второй кейс — uns
      // 'uns' это массив из массивов, каждый из которых включает в себя ремарку + какое-то количество примеров
      // ремарку я пока опускаю, забираю только примеры
      // TODO: пушить объект с ремаркой и экзамплами, если есть
      // TODO: разбирать на UI
      el[1].forEach( uns => {
        // в случае uns, я буду отдавать объект вида
        // {dt: '', vis: []}
        const parsedUns = {
          dt: null,
          vis: [],
        };

        // как и с dt на верхнем уровне, dt в uns я нахожу утиной типизацией
        // см. комментарии выше
        parsedUns.dt = ( Array.isArray( uns[0] ) && uns[0][0] === 'text' ) ? uns[0][1] : null;
        
        uns.forEach( unsChild => {
          // vis, по накатанной
          // а если не vis — игнорирую
          if ( unsChild[0] === 'vis' ) {
            const vis = unsChild[1];
            for (let i = 0; i < vis.length; i++) {
              parsedUns.vis.push(vis[i]);
            }
          }
        })

        // пушу в examples
        examples.push(parsedUns);
      });
    }
  });

  return {
    dt: definition,
    vis: examples,
  }
}
