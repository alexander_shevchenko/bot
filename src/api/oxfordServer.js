import cloneDeep from 'lodash/cloneDeep';

/**
 * Метод получает массив из слов из Oxford API Entries
 * и, опционально, переводы,
 * парсит
 * возвращает объект карточки
 *
 * @docs https://developer.oxforddictionaries.com/documentation#!/Entries/get_entries_source_lang_word_id
 *
 * @param {Object} entries ответ из Entries оксфорда
 * @param {Object} translations ответ из Translations оксфорда (опционален)
 * @return {Object} объект карточки
 *
 *   @param {String} id id слова (primary идентификатор в сервисах API)
 *   @param {String} word само слово
 *   @param {String} lexicalCategory часть речи первого вхождения
 *   @param {String} sense первое определение первого вхождения
 *   @param {Number} lexicalCounter сколько всего entries найдено
 *   @param {Number} defsCounter сколько всего определений найдено
 *   @param {Array} translations переводы
 */

export const parseEntries = (entries, translations) => {
  // console.log('parseEntries incoming entries: ');
  // console.log(entries.results);
  // console.log('parseEntries incoming translations: ');
  // console.log(translations.results);

  // КЛОНИРУЮ ВХОДЯЩИЕ ДАННЫЕ ИЗ ENTRIES И TRANSLATIONS
  const api_data_entries = cloneDeep(entries.results);
  const api_data_translations = cloneDeep(translations.results);

  // результирующий объект
  const result = {};

  // делаю ссылку на первое вхождение из входящих данных
  const firstEntry = api_data_entries[0];

  // забираю id слова из первого вхождения
  result.id = firstEntry.id;

  // забираю word из первого вхождения
  result.word = firstEntry.word;

  // забираю lexicalCategory из первого вхождения
  result.lexicalCategory = firstEntry.lexicalEntries[0].lexicalCategory.id;

  // забираю первый sense из первого вхождения
  // !! sense иногда имеет только определения
  // !! sense иногда имеет только короткое определение (shortDefinition) - забираю его
  // вместо основного
  // !!!! sense может не иметь ни коротких ни длинных определений — игнорирую, null
  // !!!! sense иногда не имеет ни определений ни примеров (см. done) но у него есть
  // сабсенсы - сам sense игнорирую, null, подсмыслы учитываю
  const senses = firstEntry.lexicalEntries[0].entries[0].senses;
  for (let i = 0; i < senses.length; i++) {
    const sense = senses[i];
    const parsedSense = parseSense(sense);

    // поддержка исключения 2, если в распарсенном сенсе есть только
    // кросс-референс то этот сенс я игнорирую
    // то есть фактически, если есть дефинишн — ок, нет — идем дальше
    if (parsedSense && parsedSense.def) {
      result.sense = parsedSense.def;
      break;
    }

    // если вижу свойство subsenses, то забираю первый подсмысл (как и выше, пропускаю
    // кросс-референсные подсмыслы)
    // костылек - делаю переменную которая если станет тру ыв цикле ниже, то
    // цикл выше делает break (что значит что я нашел подсмысл и задача выполнена)
    let isSubSenseFound = false;

    if (sense.subsenses) {
      for (let j = 0; j < sense.subsenses.length; j++) {
        const subsense = sense.subsenses[i];
        const parsedSubSense = parseSense(subsense);

        if (parsedSubSense && parsedSubSense.def) {
          result.sense = parsedSubSense.def;
          isSubSenseFound = true;
          break;
        }
      }
    }

    // если не нашлось даже подсмысла, то цикл идет дальше
    if (isSubSenseFound === true) break;
  }

  // считаю общеее количество определений
  result.defsCounter = calculateDefs(api_data_entries);

  // считаем количество записей (entries в lexical entries)
  result.lexicalCounter = calculateEntries(api_data_entries);

  // парсю переводы, получаю массив из объектов, вида часть речи + переводы
  // склеиваю все переводы в массив
  if (api_data_translations && api_data_translations.length) {
    let translationsSet = new Set();
    parseTranslations(api_data_translations).forEach((trs) => {
      trs.translations.forEach((tr) => {
        translationsSet.add(tr);
      });
    });

    result.translations = Array.from(translationsSet);
  }

  //console.log('parseEntries, result data: ');
  //console.log(result);

  return result;
};

/**
 * Метод считает total defs в lexical entries
 * возвращает число
 *
 * @param  {Array} data сырая дата из entries из оксфорда
 * @return {Number} количество defs
 */
const calculateDefs = (data) => {
  let counter = 0;
  data.forEach((result) => {
    result.lexicalEntries.forEach((lexical) => {
      lexical.entries.forEach((entry) => {
        entry.senses.forEach((sense) => {
          if (sense.definitions && sense.definitions.length)
            counter += sense.definitions.length;
          if (sense.subsenses && sense.subsenses.length)
            counter += sense.subsenses.length;
        });
      });
    });
  });

  return counter;
};

/**
 * Метод считает количество entries в lexical entries
 * возвращает число
 *
 * @param  {Array} data сырая дата из entries из оксфорда
 * @return {Number} количество entries
 */
const calculateEntries = (data) => {
  let counter = 0;
  data.forEach((res) => {
    res.lexicalEntries.forEach((res2) => (counter += res2.entries.length));
  });

  return counter;
};

/**
 * Метод получает объект sense
 * и возвращает объект с определением
 * предполагается, что встречаются случаи где есть только определение/я или только пример/ы
 * есть случай когда нет полного определения, но есть короткое (использую короткое)
 * есть и случаи когда нет ни примера ни определения (например есть только сабсенсы) в таком случае
 * я пока возвращаю null (пока у нас структура определений плоская, нет вложенности смыслов/подсмыслов)
 *
 * @param  {Object} sense смысл
 * @return {Object} объект c результатами
 * @return {Null} нет ни короткого ни длинного определения
 */
const parseSense = (sense) => {
  // в теории вероятно у определения может быть от нуля до N трактовок
  // я забираю нулевое определение
  let def;
  def = sense.definitions ? sense.definitions[0] : null;

  // если нет определения, пробую найти короткое определение
  def = !def && sense.shortDefinitions ? sense.shortDefinitions[0] : def;

  if (def) {
    return {
      def,
    };
  } else {
    return null;
  }
};

/**
 * Метод получает массив из результатов поиска из Oxford API Search, парсит в удобоваримый вид
 * возвращает массив из результатов
 *
 * @docs https://developer.oxforddictionaries.com/documentation#!/Search/get_search_source_lang
 *
 * @param {Array} data массив из вхождений в сыром виде
 * @return {Array} массив из отпарсенных результатов
 *
 *   каждый член массива имеет следующие свойства:
 *   @param {String} id id слова
 *   @param {String} word слово
 */

export const parseSearch = (data) => {
  const search_results = cloneDeep(data.results);

  const results = [];

  // я предполагаю, что раз мы дошли сюда, то элементы в массиве есть
  search_results.forEach((item) => {
    results.push({
      word: item.word,
      id: item.id,
    });
  });

  return results;
};

/**
 * МЕТОД 1 В 1 С ОРИГИНАЛЬНЫМ МЕТОДОМ, все пертурбации происходят на уровне parseEntries
 *
 * Метод получает массив из results из Oxford API Translations, парсит в удобоваримый вид
 * возвращает массив из Lexical Entries
 *
 * @docs https://developer.oxforddictionaries.com/documentation#!/Translations/get_translations_source_lang_translate_target_lang_translate_word_id
 *
 * @param {Array} data массив из слов в сыром виде
 * @return {Array} массив из Lexical Entries
 *
 *   каждый член массива имеет следующие свойства:
 *   @param {String} lexicalCategory часть речи
 *   @param {Array} translations переводы
 */
export const parseTranslations = (data) => {
  // КЛОНИРУЮ ВХОДЯЩИЕ ДАННЫЕ
  const api_data_entries = cloneDeep(data);

  // На первом уровне живут results, где меня интересует только массив с lexical entries
  //
  // Каждый lexical entry содержит в себе 1-N entries объединенных:
  // - lexicalCategory (часть речи)
  //
  // я прохожу через все results > вложенные lexical entries,
  // достаю оттуда entries и складываю их в результирующий массив, добавляя в каждый entry
  // родительский lexicalCategory

  // результирующий массив
  const entries = [];

  // иду по результатам
  api_data_entries.forEach((result) => {
    // перебираю все lexicalEntries
    if (result.lexicalEntries && result.lexicalEntries.length) {
      result.lexicalEntries.forEach((lexical) => {
        // перебираю все entries внутри конкретного lexicalEntry
        // нашёл случай где не было lexicalEntries — [fuck]
        // на всякий случай теперь проверяю кругом
        if (lexical.entries && lexical.entries.length) {
          lexical.entries.forEach((entry) => {
            // забираю родительскую часть речи
            entry.lexicalCategory = lexical.lexicalCategory;
            // забираю родительское произношение (с условием, что оно есть и нет своего произношения)
            entries.push(entry);
          });
        }
      });
    }
  });

  // беру по очереди элементы из сырого массива entries
  // и распарсиваю их в удобоваримый вид
  // ПЕРЕГОНЯЮ СЫРЫЕ РЕЗУЛЬТАТЫ В КОНЕЧНЫЙ МАССИВ
  const results = [];

  entries.forEach((entry) => {
    const result = {};

    // LEXICAL CATEGORY
    // перевожу в нижний регистр
    if (entry.lexicalCategory) {
      result.lexicalCategory = entry.lexicalCategory.text.toLowerCase();
    }

    // ПЕРЕВОДЫ
    result.translations = [];

    if (entry.senses && entry.senses.length) {
      entry.senses.forEach((sense) => {
        if (sense.translations && sense.translations.length) {
          sense.translations.forEach((tr) => {
            // https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize
            // https://unicode-table.com/en/
            let text = tr.text;
            text = text.normalize('NFD').replace(/[\u0301]/g, '');

            result.translations.push(text);
          });
        }
      });
    }

    // Некоторые части речи повторяются в отдельных entry
    // исходя из данных на руках, я никак не могу их заматчить
    // с соответствующими частями речи из Entries
    // поэтому на текущий момент я просто мерджу все повторяющиеся части речи
    const duplicateIndex = results.findIndex(
      (rs) => rs.lexicalCategory === result.lexicalCategory
    );
    if (duplicateIndex !== -1) {
      // если такая часть речи уже есть то мердж
      results[duplicateIndex].translations.concat(result.translations);
    } else {
      // иначе добавляю новую запись
      results.push(result);
    }
  });

  return results;
};
