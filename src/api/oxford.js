import cloneDeep from 'lodash/cloneDeep';

/**
 * Метод получает массив из слов из Oxford API Entries, парсит в удобоваримый вид
 * возвращает массив из Lexical Entries
 *
 * @docs https://developer.oxforddictionaries.com/documentation#!/Entries/get_entries_source_lang_word_id
 *
 * @param {Array} data массив из слов в сыром виде
 * @return {Array} массив из Lexical Entries
 *
 *   каждый член массива имеет следующие свойства:
 *   @param {String} id id слова (primary идентификатор в сервисах API)
 *   @param {String} word само слово
 *   @param {String} lexicalCategory часть речи
 *   @param {Array}  pronunciations произношения слова
 *   @param {Object} pronunciations[i].transcription транскрипция
 *   @param {String} pronunciations[i].transcription.notation тип транскрипции
 *   @param {String} pronunciations[i].transcription.spelling сама траннскрипция
 *   @param {String} pronunciations[i].audio ссылка на аудио-файл с произношением
 *   @param {Array}  senses список определений c примерами и доменами
 *   @param {String} senses[i].def определение
 *   @param {Array}  senses[i].domains домены использования
 *   @param {String} senses[i].domains[i] домен
 *   @param {Array}  senses[i].examples примеры использования
 *   @param {String} senses[i].examples[i] пример
 */
export const parseEntries = (data) => {
  //console.log('parseEntries incoming data: ');
  //console.log(data);

  // КЛОНИРУЮ ВХОДЯЩИЕ ДАННЫЕ
  const api_data = cloneDeep(data);

  // На первом уровне живут results, где меня интересуют
  // - id айди слова
  // - word само слово
  // - массив с lexical entries
  //
  // Каждый lexical entry содержит в себе 1-N entries объединенных:
  // - lexicalCategory (часть речи)
  // - pronunciations (произношение !!опционально, может жить на уровне ниже!!)
  // - word (само слово)
  //
  // я прохожу через все результаты, забираю id/word, далее прохожу по вложенным lexical
  // entries, достаю оттуда entries и складываю их в результирующий массив, добавляя в каждый entry
  // родительские id/word

  // результирующий массив
  const entries = [];

  // иду по результатам
  api_data.forEach((result) => {
    // id и word результата шарятся на всех потомков
    const { id, word } = result;

    // перебираю все lexicalEntries
    result.lexicalEntries.forEach((lexical) => {
      // перебираю все entries внутри конкретного lexicalEntry
      lexical.entries.forEach((entry) => {
        // забираю из результата id и слово
        entry.id = id;
        entry.word = word;
        // забираю родительскую часть речи
        entry.lexicalCategory = lexical.lexicalCategory;
        // забираю родительское произношение (с условием, что оно есть и нет своего произношения)
        if (!entry.pronunciations && lexical.pronunciations) {
          entry.pronunciations = lexical.pronunciations;
        }
        // для исключения номер 1 (см. ниже) забираю свойство derivativeOf из lexicalEntry, если есть
        if (lexical.derivativeOf) {
          entry.derivativeOf = lexical.derivativeOf;
        }

        entries.push(entry);
      });
    });
  });

  // беру по очереди элементы из сырого массива entries
  // и распарсиваю их в удобоваримый вид
  // ПЕРЕГОНЯЮ СЫРЫЕ РЕЗУЛЬТАТЫ В КОНЕЧНЫЙ МАССИВ
  const results = [];

  entries.forEach((entry) => {
    const result = {};

    // ID
    if (entry.id) {
      result.id = entry.id;
    }

    // СЛОВО
    if (entry.word) {
      result.word = entry.word;
    }

    // LEXICAL CATEGORY
    // перевожу в нижний регистр
    if (entry.lexicalCategory) {
      result.lexicalCategory = entry.lexicalCategory.text.toLowerCase();
    }

    // ТРАНСКРИПЦИИ и АУДИО-ССЫЛКИ
    // если есть
    const pronunciations = [];

    // забираю все IPA произношения
    // если есть
    if (entry.pronunciations && entry.pronunciations.length) {
      // pronunciations это массив, может включать в себя как выяснилось довольно большое множество
      // нам пока интересны все варианты произношения в IPA нотации (практика показывает, что эта нотация чаще всего сопровождается
      // аудио-файлами)
      entry.pronunciations.forEach((pron) => {
        if (
          pron.phoneticNotation &&
          pron.phoneticSpelling &&
          pron.phoneticNotation === 'IPA'
        ) {
          const res = {};
          res.notation = pron.phoneticNotation;
          res.spelling = pron.phoneticSpelling;

          if (pron.audioFile) res.audio = pron.audioFile;

          pronunciations.push(res);
        }
      });
    }

    result.pronunciations = pronunciations;

    // ОПРЕДЕЛЕНИЯ / ПРИМЕРЫ / ДОМЕНЫ / РЕГИСТРЫ / ID / THESAURUS
    // каждый entry имеет свойство senses, где на первом уровне лежат смыслы
    // у которых могут быть опциональные подсмыслы
    result.senses = [];

    if (entry.senses && entry.senses.length) {
      // прохожу каждое определение в цикле
      entry.senses.forEach((sense) => {
        // экстрагирую определение + примеры + домен + id
        // !! sense иногда имеет только определения
        // !! sense иногда имеет только короткое определение (shortDefinition) - забираю его вместо основного
        // !!!! sense может не иметь ни коротких ни длинных определений — игнорирую, null
        // !!!! sense иногда не имеет ни определений ни примеров (см. done) но у него есть сабсенсы - сам sense игнорирую, null, подсмыслы учитываю

        // поддержка исключения 2, если в распарсенном сенсе есть только кросс-референс то этот сенс я игнорирую
        // и записываю кросс-референс в result (пока я видел только случаи где кросс-референс был единственным дочерним
        // элементом в массиве смыслов)
        let parsedSense = parseSense(sense);
        if (parsedSense && parsedSense.def) {
          result.senses.push(parsedSense);
        } else if (parsedSense && parsedSense.crossReference) {
          result.crossReference = parsedSense.crossReference;
        }

        // если вижу свойство subsenses, обрабатываю каждое определение в subsenses аналогично
        // за исключением случая отсутствующих определений и примеров (как на уровне выше)
        if (sense.subsenses) {
          sense.subsenses.forEach((subsense) => {
            let parsedSubSense = parseSense(subsense);
            if (parsedSubSense && parsedSubSense.def) {
              result.senses.push(parsedSubSense);
            } else if (parsedSubSense && parsedSubSense.crossReference) {
              result.crossReference = parsedSubSense.crossReference;
            }
          });
        }
      });
    }

    // ИСКЛЮЧЕНИЕ 1 — нет определений, но есть derivativeOf у родительского lexicalEntry
    // jocularly
    // interminably
    if (
      result.senses.length === 0 &&
      entry.derivativeOf &&
      entry.derivativeOf.length
    ) {
      result.derivativeOf = entry.derivativeOf;
    }

    // ИСКЛЮЧЕНИЕ 2 — нет определений, но есть crossReference как нулевое определение
    // в этом случае парсер смыслов возвращает кроссреференс, я записываю его в свойства результата (см. выше)
    // и если, в итоге, нет определений но есть кроссреференс -- бинго, я оставляю кросс-референс и UI отработает как нужно
    // иначе, если определения есть — удаляю кросс-референс
    // jook
    // draught
    if (result.senses.length === 0 && result.crossReference) {
      // все как нужно, оставляю кросс-референс
    } else {
      delete result.crossReference;
    }

    results.push(result);
  });

  //console.log('parseEntries, results data: ');
  //console.log(results);

  return results;
};

/**
 * Метод получает объект sense
 * и возвращает объект с определением, примерами, доменами, регистрами, id, ссылками на тезаурус
 * предполагается, что встречаются случаи где есть только определение/я или только пример/ы
 * есть случай когда нет полного определения, но есть короткое (использую короткое)
 * есть и случаи когда нет ни примера ни определения (например есть только сабсенсы) в таком случае
 * я пока возвращаю null (пока у нас структура определений плоская, нет вложенности смыслов/подсмыслов)
 *
 * поддержка исключения 2 — если вместо определения есть crossReference, возвращаю его
 *
 * FIXME: тут скользкий момент, потенциально может быть случай, когда у части речи не будет определений
 * и её нельзя будет добавить
 * @param  {Object} sense смысл
 * @return {Object} объект c результатами
 * @return {Null} нет ни короткого ни длинного определения
 */
const parseSense = (sense) => {
  // в теории вероятно у определения может быть от нуля до N трактовок
  // я забираю нулевое определение
  let def;
  def = sense.definitions ? sense.definitions[0] : null;

  // если нет определения, пробую найти короткое определение
  def = !def && sense.shortDefinitions ? sense.shortDefinitions[0] : def;

  // забираю ID определения
  let id;
  id = sense.id ? sense.id : null;

  // забираю домены, если есть
  // перевожу домены в нижний регистр
  const domains = [];
  if (sense.domains) {
    sense.domains.forEach((domain) => {
      domains.push(domain.text.toLowerCase());
    });
  }

  // FIXME: разведи домены и регистры в два массива и объединяй, если так нравится, на UI
  // TODO: по идее это отдельная сущность и мне бы как-то выводить её по другому
  // добавляю регистры в домены
  if (sense.registers) {
    sense.registers.forEach((register) => {
      domains.push(register.text.toLowerCase());
    });
  }

  // забираю примеры, если есть
  const examples = [];
  if (sense.examples) {
    sense.examples.forEach((example) => {
      examples.push(example.text);
    });
  }

  // забираю ссылки на тезаурус если есть
  // ссылок как выясняется может быть несколько, я пока просто забираю все, какие использовать решу на UI
  // entry_id это ключ по которому дальше нужно осуществлять поиск в тезаурусе
  // sense_id это id по которому я вычленяю результат для данного определения из ответа по ключу (в ответе будут скопом все определения для ключа)
  const thesaurus = [];
  if (sense.thesaurusLinks) {
    sense.thesaurusLinks.forEach((th) => {
      thesaurus.push(th);
    });
  }

  // исключение 2, кросс-референс
  let crossReference;
  if (sense.crossReferences && sense.crossReferences.length) {
    crossReference = sense.crossReferences[0];
  }

  if (def) {
    return {
      id,
      def,
      domains,
      examples,
      thesaurus,
    };
  } else if (crossReference) {
    return {
      crossReference,
    };
  } else {
    return null;
  }
};

/**
 * Метод получает массив из результатов поиска из Oxford API Search, парсит в удобоваримый вид
 * возвращает массив из результатов
 *
 * @docs https://developer.oxforddictionaries.com/documentation#!/Search/get_search_source_lang
 *
 * @param {Array} data массив из вхождений в сыром виде
 * @return {Array} массив из отпарсенных результатов
 *
 *   каждый член массива имеет следующие свойства:
 *   @param {String} id id слова
 *   @param {String} word слово
 */

export const parseSearch = (data) => {
  const search_results = cloneDeep(data);

  const results = [];

  // я предполагаю, что раз мы дошли сюда, то элементы в массиве есть
  search_results.forEach((item) => {
    results.push({
      word: item.word,
      id: item.id,
    });
  });

  return results;
};

/**
 * Метод получает массив из результатов поиска в Oxford API Thesaurus
 * и айди требуемых определений.
 * достает синонимы и антонимы, возвращает
 *
 * @docs https://developer.oxforddictionaries.com/documentation#!/Thesaurus/get_thesaurus_lang_word_id
 *
 * @param {Array} data массив из вхождений в сыром виде
 * @param {Array} senseIds массив из айди искомых определений (почему их может быть много я пока не понимаю)
 * !! хоть парсер и принимает массив из айди и обрабатывает данные соответственно !!
 * !! на UI я решил пока запрашивать син/ант только для одного, первого вхождения в thesaurusLinks !!
 * !! поскольку для разных вхождений может быть разный entry_id !!
 * !! TODO: пофиксить на стороне экшена и функции на стороне бэкенда !!
 *
 * @return {Object} объект результатами
 *   @param {Array} synonyms массив из синонимов, если есть
 *   @param {Array} antonyms массив из антонимов, если есть
 */

export const parseThesaurus = (data, senseIds) => {
  const thesaurus_results = cloneDeep(data);

  // пройти последовательно по всем элементам массива,
  // свести все определения в массив,
  // найти определения с требуемыми айди
  // забрать все синонимы/антонимы, конкатенировать результаты

  // СОБИРАЮ ВСЕ СМЫСЛЫ В МАСССИВ
  const senses = [];

  thesaurus_results.forEach((result) => {
    result.lexicalEntries.forEach((lexical) => {
      lexical.entries.forEach((entry) => {
        entry.senses.forEach((sense) => {
          // Забрать айди
          const id = sense.id ? sense.id : null;
          // Забрать антонимы
          const antonyms = [];

          if (sense.antonyms && sense.antonyms.length) {
            sense.antonyms.forEach((antonym) => {
              antonyms.push(antonym.text);
            });
          }
          // Забрать синонимы
          const synonyms = [];
          if (sense.synonyms && sense.synonyms.length) {
            sense.synonyms.forEach((synonym) => {
              synonyms.push(synonym.text);
            });
          }

          senses.push({
            id,
            antonyms,
            synonyms,
          });
        });
      });
    });
  });

  // ПРОЧЕСЫВАЮ МАССИВ НА СОВПАДЕНИЕ С ИСКОМЫМИ АЙДИ, ЗАБИРАЮ ДАННЫЕ
  // если sense.id совпадает с одним из айди в senseIds
  // забираю синонимы и антонимы
  const result = {};
  result.synonyms = [];
  result.antonyms = [];

  senseIds.forEach((id) => {
    const index = senses.findIndex((sense) => {
      return sense.id === id;
    });

    // если есть совпадение, забираю, иначе ничего не делаю
    if (index !== -1) {
      result.synonyms = [...result.synonyms, ...senses[index].synonyms];
      result.antonyms = [...result.antonyms, ...senses[index].antonyms];
    }
  });

  return result;
};

/**
 * Метод получает массив из results из Oxford API Translations, парсит в удобоваримый вид
 * возвращает массив из Lexical Entries
 *
 * @docs https://developer.oxforddictionaries.com/documentation#!/Translations/get_translations_source_lang_translate_target_lang_translate_word_id
 *
 * @param {Array} data массив из слов в сыром виде
 * @return {Array} массив из Lexical Entries
 *
 *   каждый член массива имеет следующие свойства:
 *   @param {String} lexicalCategory часть речи
 *   @param {Array} translations переводы
 */
export const parseTranslations = (data) => {
  //console.log('parseTranslations incoming data: ');
  //console.log(data);

  // КЛОНИРУЮ ВХОДЯЩИЕ ДАННЫЕ
  const api_data = cloneDeep(data);

  // На первом уровне живут results, где меня интересует только массив с lexical entries
  //
  // Каждый lexical entry содержит в себе 1-N entries объединенных:
  // - lexicalCategory (часть речи)
  //
  // я прохожу через все results > вложенные lexical entries,
  // достаю оттуда entries и складываю их в результирующий массив, добавляя в каждый entry
  // родительский lexicalCategory

  // результирующий массив
  const entries = [];

  // иду по результатам
  api_data.forEach((result) => {
    // перебираю все lexicalEntries
    if (result.lexicalEntries && result.lexicalEntries.length) {
      result.lexicalEntries.forEach((lexical) => {
        // перебираю все entries внутри конкретного lexicalEntry
        // нашёл случай где не было lexicalEntries — [fuck]
        // на всякий случай теперь проверяю кругом
        if (lexical.entries && lexical.entries.length) {
          lexical.entries.forEach((entry) => {
            // забираю родительскую часть речи
            entry.lexicalCategory = lexical.lexicalCategory;
            // забираю родительское произношение (с условием, что оно есть и нет своего произношения)
            entries.push(entry);
          });
        }
      });
    }
  });

  // беру по очереди элементы из сырого массива entries
  // и распарсиваю их в удобоваримый вид
  // ПЕРЕГОНЯЮ СЫРЫЕ РЕЗУЛЬТАТЫ В КОНЕЧНЫЙ МАССИВ
  const results = [];

  entries.forEach((entry) => {
    const result = {};

    // LEXICAL CATEGORY
    // перевожу в нижний регистр
    if (entry.lexicalCategory) {
      result.lexicalCategory = entry.lexicalCategory.text.toLowerCase();
    }

    // ПЕРЕВОДЫ
    result.translations = [];

    if (entry.senses && entry.senses.length) {
      entry.senses.forEach((sense) => {
        if (sense.translations && sense.translations.length) {
          sense.translations.forEach((tr) => {
            // https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize
            // https://unicode-table.com/en/
            let text = tr.text;
            text = text.normalize('NFD').replace(/[\u0301]/g, '');

            result.translations.push(text);
          });
        }
      });
    }

    // Некоторые части речи повторяются в отдельных entry
    // исходя из данных на руках, я никак не могу их заматчить
    // с соответствующими частями речи из Entries
    // поэтому на текущий момент я просто мерджу все повторяющиеся части речи
    const duplicateIndex = results.findIndex(
      (rs) => rs.lexicalCategory === result.lexicalCategory
    );
    if (duplicateIndex !== -1) {
      // если такая часть речи уже есть то мердж
      results[duplicateIndex].translations.concat(result.translations);
    } else {
      // иначе добавляю новую запись
      results.push(result);
    }
  });

  //console.log('parseTranslations, parsed data: ');
  //console.log(results);

  return results;
};
