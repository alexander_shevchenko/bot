// TODO: ревью промисов, не понимаю этот момент

// reflects the same variable in the css
const transitionTime = 275;

document.addEventListener("DOMContentLoaded", function(event) { 
  // show overlays
  const openTriggers = Array.prototype.slice.call( document.querySelectorAll('[data-overlayid]') );
  openTriggers.forEach(function(trigger){
    trigger.addEventListener('click', showOverlay.bind(trigger, document.getElementById(trigger.dataset.overlayid)))
  });

  // hide overlays
  const closeTriggers = Array.prototype.slice.call( document.getElementsByClassName('overlay__close') );
  closeTriggers.forEach(function(trigger) {
    trigger.addEventListener('click', hideOverlay.bind(trigger, trigger.closest('.overlay')));
  });

  // on esc -- hide overlays
  document.addEventListener('keydown', function(event) {
    if (event.code === 'Escape') {
      const overlay = document.querySelector('.overlay:not(.hidden)');
      if (overlay) hideOverlay(overlay, event)
    };
  });
});

/**
 * Shows an overlay
 * 
 * @param {Object} overlay dom node
 * @param {Object} event event object
 */
function showOverlay(overlay, event) {
  event.preventDefault();
  document.getElementsByTagName('body')[0].classList.add('modal-open');
  enter(overlay, 'fade', transitionTime);
}

/**
 * Hides an overlay
 * @param {Object} overlay dom node
 * @param {Object} event event object
 */
function hideOverlay(overlay, event) {
  event.preventDefault();
  document.getElementsByTagName('body')[0].classList.remove('modal-open');
  leave(overlay, 'fade', transitionTime);
}

/**
 * Enter and Leave transitions based on
 * https://sebastiandedeyne.com/javascript-framework-diet/enter-leave-transitions/
 */

/**
 * Enter animation
 * element must have a 'hidden' class on it
 * 
 * @param {Object} element dom node
 * @param {String} transition animation type's classes prefix
 * @param {Number} duration transition time
 */
async function enter(element, transition, duration) {
  element.classList.remove('hidden');
  element.classList.add(`${transition}-enter`);
  await nextFrame();

  element.classList.add(`${transition}-enter-active`);
  await afterTransition(duration);

  element.classList.remove(`${transition}-enter`);
  element.classList.remove(`${transition}-enter-active`);
}

/**
 * Leave animation
 * 
 * @param {Object} element dom node
 * @param {String} transition animation type's classes prefix
 * @param {Number} duration transition time
 */
async function leave(element, transition, duration) {
  element.classList.remove('hidden');
  element.classList.add(`${transition}-leave`);
  await nextFrame();

  element.classList.add(`${transition}-leave-active`);
  await afterTransition(duration);

  element.classList.remove(`${transition}-leave`);
  element.classList.remove(`${transition}-leave-active`);
  element.classList.add('hidden');
}

/**
 * Requests new frame
 * little twist for chrome
 * https://bugs.chromium.org/p/chromium/issues/detail?id=675795 
 */
function nextFrame() {
  return new Promise(resolve => {
    requestAnimationFrame(() => {
      requestAnimationFrame(resolve);
    });
  });
}

/**
 * Wait until the transition is over
 */
function afterTransition(duration) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, duration);
  });
}