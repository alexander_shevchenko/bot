'use strict';

// версия приложения
const version_app = 1.013;

const express = require('express');
const app = express();
//const app = require('axios');

// инициализация firebase
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: functions.config().app.database_url,
});

// класс для бд и инициализация бд
const base = require('./class/bd-firebase-realtime.js');
const bd = new base(admin);

// класс для работы с TG
const botTelegram = require('./class/bot-tg.js');
const botTG = new botTelegram(
    functions.config().tg.token,
    functions.config().app.webview_url
);

// класс для работы с Oxford
const oxfordDictionaries = require('./class/oxfordDictionaries.js');
const oxford = new oxfordDictionaries(
    functions.config().oxford.application_id,
    functions.config().oxford.application_keys
);

// работа с карточками
const fetchCardsBD = require('./card/fetchCardsBD.js');
const postUpdateCardBD = require('./card/postUpdateCardBD.js');
const postAddCardBD = require('./card/postAddCardBD.js');
const postDeleteCardBD = require('./card/postDeleteCardBD.js');
const postGradeBD = require('./card/postGradeBD.js');
const fetchAllCardsBD = require('./card/fetchAllCardsBD.js');
const fetchCardBD = require('./card/fetchCardBD.js');
const postDoneBD = require('./card/postDoneBD.js');
const fetchUserAttributesBD = require('./card/fetchUserAttributesBD.js');
const postUserAttributesBD = require('./card/postUserAttributesBD.js');
const sendReminder = require('./card/sendReminder.js');
const postCardPreferencesBD = require('./card/postCardPreferencesBD.js');

// диалоги TG
const tgAddCard = require('./dialog/tgAddCard.js');
const tgPostDone = require('./dialog/tgPostDone.js');
const tgGetStarted = require('./dialog/tgGetStarted.js');
const tgFindInOxford = require('./dialog/tgFindInOxford.js');

// lending
const postAddEmailLendingBD = require('./postAddEmailLending.js');

// это костыль, чтобы стартануть гугловский крон, ждем исправления ошибки
// https://github.com/firebase/firebase-functions/issues/437
// process.env.GCLOUD_PROJECT = process.env.GCP_PROJECT;
process.env.GCLOUD_PROJECT = JSON.parse(process.env.FIREBASE_CONFIG).projectId; // <- правильный костыль

console.log('start!');

// https://console.cloud.google.com/cloudscheduler?project=repeat-a738a&jobs-tablesize=50 - тут наш крон
exports.scheduledTest = functions.pubsub
    .schedule('every 1 hours from 00:00 to 23:00')
    .onRun(async (context) => {
        // exports.scheduledTest = functions.pubsub.schedule('every 1 minutes').onRun( async (context) => {

        await sendReminder(bd, botTG);
    });

// для теста
app.get('*/test', async (request, response) => {
    response.send('Test -> yes. ');
    response.status(200);
    console.log('test');
});

// Обработка сообщений от TG
app.post('*/tg-application-1035367941', async (request, response) => {
    try {
        const result = await botTG.route(request.body);

        // начальный диалог
        if (result.getStarted === true) {
            botTG.text(result.tgid, 'Hello ' + result.name);
        }

        // поиск в оксфорде слов
        if (result.findInOxford === true) {
            tgFindInOxford(result.tgid, result.text, bd, botTG, oxford);
        }

        response.status(200);
        return response.end();
    } catch (error) {
        console.log(error);
        response.status(200);
        return response.end();
    }
});
exports.application = functions.https.onRequest(app);

// функция создания очереди карточек
exports.fetchCards = functions.https.onCall((data, context) => {
    return fetchCardsBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция редактирования карточки
exports.postUpdateCard = functions.https.onCall((data, context) => {
    return postUpdateCardBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция редактирования карточки
exports.postCardPreferences = functions.https.onCall(async (data, context) => {
    try {
        const result = await postCardPreferencesBD(data, bd);
        return result;
    } catch (error) {
        throw new functions.https.HttpsError('unknown', error);
    }
});

// функция создания карточки
exports.postAddCard = functions.https.onCall(async (data, context) => {
    try {
        // добавим карточку
        const result = await postAddCardBD(data, bd);

        // получим атрибуты пользователя (дело в том, что userId и psid это все таки разные вещи)
        const attr = await bd.getAttributesUser(data.userId);

        // проверим наличие tgid, это значит, что студент пользуется TG
        if (attr.tgid !== undefined) {
            // отправим сообщение пользователю, что карточка создана
            tgAddCard(attr.tgid, attr.firstTrain, data.front, botTG);
        }

        return result;
    } catch (error) {
        throw new functions.https.HttpsError('unknown', error);
    }
});

// функция удаления карточки
exports.postDeleteCard = functions.https.onCall((data, context) => {
    return postDeleteCardBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция получения оценки пользователя
exports.postGrade = functions.https.onCall((data, context) => {
    return postGradeBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция получения отмашки конца тренировки
exports.postDone = functions.https.onCall(async (data, context) => {
    try {
        // добавим карточку
        const result = await postDoneBD(data, bd);

        // получим атрибуты пользователя
        const attr = await bd.getAttributesUser(data.userId);

        // проверим наличие tgid, это значит, что студент пользуется TG
        if (attr.tgid !== undefined) {
            // отправим сообщение пользователю, что карточка создана
            tgPostDone(attr.tgid, result.isFirstDoneEver, data, botTG);
        }

        return result;
    } catch (error) {
        throw new functions.https.HttpsError('unknown', error);
    }
});

// функция получения списка всех карточек хэша
exports.fetchAllCards = functions.https.onCall((data, context) => {
    return fetchAllCardsBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция получения карточки у студента
exports.fetchCard = functions.https.onCall((data, context) => {
    return fetchCardBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция получения клиентских аттрибутов студента по его psid (или что там у него есть)
exports.fetchUserAttributes = functions.https.onCall((data, context) => {
    // Если запрос на пользователя ТГ, то проверим авторизацию
    if (data.id !== undefined) {
        if (!botTG.checkTG(data)) {
            throw `Data with id=${data.id} from TG is corrupted!`;
        }
    }

    return fetchUserAttributesBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция записи клиентских аттрибутов студента по его userId
exports.postUserAttributes = functions.https.onCall((data, context) => {
    return postUserAttributesBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// функция работы с Lending, получает и запоминает email в базе
exports.postAddEmailLending = functions.https.onCall((data, context) => {
    return postAddEmailLendingBD(data, bd)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            throw new functions.https.HttpsError('unknown', error);
        });
});

// работа со словарем оксфорда
exports.fetchOxfordAll = functions.https.onCall(async (data, context) => {
    try {
      return await oxford.getAll(data);
    } catch (error) {
        throw new functions.https.HttpsError('unknown', error);
    }
});

// работа со словарем оксфорда
exports.fetchOxfordThesaurus = functions.https.onCall(async (data, context) => {
    try {
        return await oxford.getThesaurus(data);
    } catch (error) {
        throw new functions.https.HttpsError('unknown', error);
    }
});

// Free Dictionary API
const FreeDictionary = require('./class/freeDictionary');
const freeDictionary = new FreeDictionary();

exports.fetchFreeDictionary = functions.https.onCall(async (data, context) => {
    try {
      const result = await freeDictionary.getDefinitions(data.word);
      // console.log(`result=${JSON.stringify(result)}`);
      return result;
    } catch (error) {
        throw new functions.https.HttpsError('unknown', error);
    }
});
