/*
 * Диалог с пользователем в случае если у него есть очередь на тренировку
 */

// склонение английских слов
const pluralize_en = require('../class/declension_en.js');

// тексты, используемые в диалогах
const dialog = require('./tgText.json');

async function tgDialogYesTrain(tgid, name, numCards, bot) {

	try {

		// покажем фразу напоминания о тренировке
		const count = Math.floor(Math.random() * 5);
		await bot.text(tgid, dialog.sendReminder.phrases[count].replace('{name}', name));

		// склоняем слово 'word'
		const noun = pluralize_en(numCards, dialog.sendReminder.noun);

		// сообщение пользователю формируем в заисимости от числа карточек
		const count2 = numCards > 40 ? 4 : (numCards > 20 ? 3 : (numCards > 10 ? 2 : (numCards > 5 ? 1 : 0)));

		// сообщение пользователю с предложением потренироваться
		await bot.dot(tgid, 1000);
		await bot.text(tgid, dialog.sendReminder.phrases2[count2].replace('{numCards}', numCards).replace('{noun}', noun));
	}
	catch (error) {

		console.log( `Server Error in dialog 'tgDialogYesTrain': ${error}` );
		// throw 'Server Error';
	}
}

module.exports = tgDialogYesTrain;