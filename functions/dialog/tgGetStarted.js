// 
// Cтартовый диалог, выводится после кнопки 'Начать'
// Входные параметры:
// 					tgid - кому отправляем сообщения (пользователь TG)
// 

// тексты, используемые в диалогах
const dialog = require('./tgText.json');

async function tgGetStarted( tgid, firstName, bd, bot ) {

	try {

		// проверим, вдруг такой пользователь у нас уже есть
		const users = await bd.getUserFromTGid( tgid );
		let userId = null;
		let userName = '';
		if( users !== null ) {
			// возьмем первый объект 
			const keys = Object.keys( users );
			userId = keys[0];
			userName = users[keys[0]].name;
		}
		
		// это абсолютно новый пользователь
		if ( userId === null ) {

			bot.dot( tgid, 1000 );

			// создадим пользователя
			userId = await bd.createUser();

			// нужны таймзона пользователя TG
			const timezone = 0;

			// если у пользователя TG не определено имя, то зададим значения по умолчанию
			if ( firstName == null ) firstName = 'anonymous';
			if ( firstName == undefined ) firstName = 'anonymous';

			// запишем в базу данные пользователя
			bd.setAttributeUser( userId, 'tgid', tgid );
			bd.setAttributeUser( userId, 'countCard', 20 );
			bd.setAttributeUser( userId, 'firstTrain', false );
			bd.setAttributeUser( userId, 'targetLanguage', null );
			bd.setAttributeUser( userId, 'translationEnabled', true );
			bd.setAttributeUser( userId, 'name', firstName );
			bd.setAttributeUser( userId, 'timezone', timezone );
			bd.setAttributeUser( userId, 'autoPlayEnabled', false );

			// сообщения пользователю + кнопки
			await bot.text( tgid, dialog.getStarted.answerNew1.replace('{name}', firstName ) );
			
			await bot.dot( tgid, 200 );
			await bot.text( tgid, dialog.getStarted.answerNew2 );
			
			await bot.dot( tgid, 200 );
			await bot.buttonTemplate( tgid, dialog.getStarted.answerNew3,
									{
										'text': dialog.getStarted.buttonNew1,
										'callback_data': 'sendMySettings'
									});
			
			await bot.dot( tgid, 1000 );
			await bot.buttonTemplate( tgid, dialog.getStarted.answerNew4,
									{
										'text': dialog.getStarted.buttonNew2,
										'callback_data': 'sendAddCard'
									});
		}

		// пользователь уже есть в базе
		else {

			// получим количество карточек пользователя
			const stat = await bd.getStatCommon( userId );

			// приветственное сообщение пользователю и две кнопки - тренироваться сейчас или тренироваться позже
			await bot.buttonTemplate( tgid, dialog.getStarted.answerInBase.replace('{name}', firstName ).replace( '{totalCards}', stat.totalCards),
				{
					'text': dialog.getStarted.buttonInBase1,
					'callback_data': 'sendStudy'
				},
				{
					'text': dialog.getStarted.buttonInBase2,
					'callback_data': 'trainLaterInlineButton'
				});
		}
	}

	catch( error ) {
		console.log( 'Server Error in tgGetStarted. ' + error );
		throw 'Server Error';
	}
}

module.exports = tgGetStarted;
