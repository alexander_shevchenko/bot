/*
 * Диалог в напоминалке с пользователем в случае если у нет карточек для тренировки на сегодня
 */

// тексты, используемые в диалогах
const dialog = require('./tgText.json');

async function tgDialogNoTrain(tgid, name, bot) {

	try {
		
		await bot.text(tgid, dialog.sendReminder.noTrain[0].replace('{name}', name));

		await bot.dot(tgid, 1000);
		await bot.text(tgid, dialog.sendReminder.noTrain[1]);

		await bot.dot(tgid, 1000);
		await bot.text(tgid, dialog.sendReminder.noTrain[2]);
	}
	catch (error) {
		
		console.log( `Server Error in dialog 'tgDialogNoTrain': ${error}` );
		// throw 'Server Error';
	}
}
module.exports = tgDialogNoTrain;