/*
 * Диалог с пользователем TG поле окончания тренировки
 *
 */

// склонение английских слов
const pluralize_en = require( '../class/declension_en.js' );

// тексты, используемые в диалогах
const dialog = require('./tgText.json');

tgPostDone = async function( id, isFirstDoneEver, data, bot ) {

	try {

	
		// если значение isFirstDoneEver === true, то это означает, что сейчас закончилась первая тренировка
		// в этом случае показываем отдельный диалог и трех фраз
		if ( isFirstDoneEver === true ) { 

			// диалоги первой тренировки
			await bot.dot( id, 1000 );
			await bot.text( id, dialog.postDone.answer1 );
			await bot.dot( id, 1000 );
			await bot.text( id, dialog.postDone.answer2 );
			await bot.dot( id, 1000 );
			await bot.text( id, dialog.postDone.answer3 );
		}

		// если сейчас закончилась обычная (не первая) тренировка, 
		// то из массива фраз dialog.postDone.phrases выбираем одно предложение к показу в чате
		// во фразу вставим сколько слов студент сегодня изучил
		else {

			// в чате может быть показана фраза со склоением слова word или словосочетания 'neural connection'

			// определим склонение слова 'word'
			const noun = pluralize_en(data.length, dialog.postDone.noun);
			// определим склонение слова 'neural connection'
			const noun2 = pluralize_en(data.length, dialog.postDone.noun2);

			// выбрем фразу к показу 
			const count = Math.floor( Math.random() * 5 );

			// диалог
			await bot.dot( id, 1000 );
			await bot.text( id, dialog.postDone.phrases[count].replace( '{length}', data.length ).replace( '{noun}', noun ).replace( '{noun2}', noun2 ) );
		}

		// если пользователь удалил все карточки во время тренировки, то мы сообщим о проблеме
		if( data.length === 0 ) {

			await bot.text( id, dialog.postDone.cardsWasDelete );
		}
		
		return true; 
	}
	catch( error ) {
		
		console.log( `Server Error in dialog: 'tgPostDone': ${error}` );
		// throw 'Server Error';
	}
}

module.exports = tgPostDone;