/*
 * Осуществляем поиск текста написанного студентом в оксфорде. 
 * По результатам поиска ведем со студентом диалог.
 *
 * 
 * Логика работы:
 *              1. По tgid определим студента и получим его настройки из базы
 *              2. Отправим в оксфорд запрос того, что написал студент
 *              3. Полученный из оксфорда результат распарсиваем и
 *                  3.1 Если в нем ничего путного нет, то сообщим об этом пользователю
 *                  3.2 Если в нем результаты из оксфордского поиска, то покажем не более 10 реузльтатов
 *                  3.3 Если в нем конкретное слово, то покажем слово и перевод 
 */

const parseOxford = require('../class/parseOxford');

async function tgFindInOxford(tgid, text, bd, botTG, oxford) {

	try {

		// запустим точки, нехай мелькают
		botTG.dot(tgid, 1000);

		// поищем пользователя в базе и получим его атрибуты
		const users = await bd.getUserFromTGid(tgid);
		const user = Object.keys(users)[0];

		// Проверим слово, если там есть слэш в начале слова - уберем его
		if (text[0] == '/') text = text.substring(1);

		// отправим слово в оксфорд
		const result = await oxford.getAll({
			word: text,
			targetLanguage: users[user].targetLanguage,
			translationEnabled: users[user].translationEnabled
		});

		// Если нашли слово в entries или дошли до translations (значит и в entries все ок), 
		// то предложим студенту игру с добавлением найденного слова
		if (result.entries.statusCode === 200) {

			// парсим результат
			const data = await parseOxford.parseEntries(result.entries, result.translations);

			// создадим сообщение пользователю 

			// собственно слово
			let message = `<b>${data.word}</b>\n`;

			//  добавим в сообщение все найденные переводы 
			if ( data.translations !== null && data.translations[0] !== undefined ) {
				message = `${message}<i>${data.translations[0]}</i>`;
				for (i = 1; i < data.translations.length; i++) {
					message = `${message}<i>, ${data.translations[i]}</i>`;
				}
				message = `${message}\n`;
			}
			message = `${message}\n`;

			// добавим часть часть речи
			message = `${message}${data.lexicalCategory === null ? '' : `[${data.lexicalCategory}]\n`}`;

			// добавим смысл
			message = `${message}${data.sense === null ? '' : `${data.sense}\n`}`;
			message = `${message}\n`;

			// добавим количество смыслов, если таковые еще есть
			message = `${message}${data.lexicalCounter > 1 ? `+${data.lexicalCounter - 1} more lexical entries\n` : ''}`;

			// добавим количество определений, если таковые еще есть
			message = `${message}${data.defsCounter > 1 ? `+${data.defsCounter - 1} more definitions\n` : ''}`;

			// отправим сообщение студенту и добавим кнопку добавить найденное слово
			await botTG.buttonTemplate(tgid, message,
				{
					'text': `Search Oxford "${data.word}"`, //!!! тут передавать id или word?
					'callback_data': 'sendAddCard 0'
				});

			return true;
		}

		// Если нашли что-то в search
		if (result.endpoint === 'search' && result.statusCode === 200) {

			// парсим результат
			const data = await parseOxford.parseSearch(result.search);

			// сформируем сообщение пользователю с кнопками
			let message = `I couldn't find '<b>${text}</b>'...\n\n Может быть это то, что вы ищете?\n `;
			let buttons = [];
			for (i = 0; i < 10; i++) {
				if (data[i] === undefined) break;
				// важно, обрамление кавычками должно быть, так я вычисляю поисковое слово
				buttons.push([{ text: `Search Oxford "${data[i].word}"`, 'callback_data': `sendAddCard ${i}` }]);
			}
// !!! решить, какой из методов оставить 
			// общаемся со студентом
			await botTG.buttonsTemplate(tgid, message, buttons);

			return true;
		}

		// ошибки 404, 400, 414, 601
		if (result.statusCode === 404 || result.statusCode === 400 || result.statusCode === 414 || result.statusCode === 601) {

			// сформируем сообщение пользователю
			let message = `I couldn't find anything related to "<b>${text}</b>"...`;

			// общаемся со студентом
			await botTG.text(tgid, message);

			return true;
		}

		// ошибки 500, 502, 503, 504
		if (result.statusCode === 500 || result.statusCode === 502 || result.statusCode === 503 || result.statusCode === 504) {

			// сформируем сообщение пользователю
			let message = 'Looks like the Oxford Dictionary API is down. Try again later.';

			// общаемся со студентом
			await botTG.text(tgid, message);

			return true;
		}

		// если дошли до сей точки, значит что-то пошло не так - Server Error
		throw 'Server Error';
	}

	catch (error) {
		// сформируем сообщение пользователю
		let message = 'Server Error. Try again later.';

		// общаемся со студентом
		await botTG.text(tgid, message);

		return true;
	}
}

module.exports = tgFindInOxford;
