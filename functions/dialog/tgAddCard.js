// Диалог при добавлении карточки 
// Входные параметры:
// 					id			- id пользователя в TG, для отправки сообщения
//					firstTrain	- была ли первая тренировка у данного пользователя (true - была, false - нет)
// 					word		- добавленное слово
//					bot 		- класс бота

// тексты, используемые в диалогах
const dialog = require('./tgText.json');

async function tgAddCard( id, firstTrain, word, bot ) {

	try {
		
		await bot.dot( id, 200 );
		await bot.text( id, dialog.addCard.answer.replace('{word}', word ) );

		// Если не было первой тренировки
		if ( firstTrain === false ) {
			
			await bot.text( id, dialog.addCard.firstTrain );
		}

		return true;
	}
	catch( error ) { 
		
		console.log( `Server Error in dialog 'tgAddCard': ${error}` );
		// throw 'Server Error';
	}
}

module.exports = tgAddCard;
