/**
 * Plural forms for english words
 * @param {Integer} count quantity for word
 * @param {String}  noun word
 * @return {String} Count + plural form for word
 */
const pluralize_en = (count, noun, suffix = 's') => `${noun}${count !== 1 ? suffix : ''}`;

module.exports = pluralize_en;