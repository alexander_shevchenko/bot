'use strict'
/*
 * Класс для работы со словарями оксфорда.
 *
 * Документация: https://developer.oxforddictionaries.com/documentation/making-requests-to-the-api
 * 
 * Входные данные для большинства методов:
 * 		data - объект
 *			.word 				- слово для запроса в словарь
 *			.translationEnabled - true или false
 *			.targetLanguage 	- направление перевода слова
 *
 * Логика работы:
 *		Любой метод класса (thesaurus, entries, translations и search) оформляет запрос 
 * 		в оксфорд в виде нужной ссылки (get-запрос) и опций с помощью this.axiosOptions
 *		Этот get-запрос исполняется с помощью this.queryOxford 
 *
 */

const axios = require( 'axios' );

class oxfordDictionaries {

	// конструктор получает id и ключ оксфорда для дальнейшей авторизации
	constructor( applicationID, applicationKeys ) {

		this.applicationID = applicationID;
		this.applicationKeys = applicationKeys;
		this.baseUrl = 'https://od-api.oxforddictionaries.com/api/v2';
	}

	// Метод формирует объект для запроса в оксфорд через модуль axios, указываем порт и параметры авторизации
	axiosOptions( ) {

		return {
			port: '443',
			headers: {
				'app_id': this.applicationID, 
				'app_key': this.applicationKeys
			}
		};
	}

	/* Универсальный запрос в оксфордский словарь
 	 * Входные данные: 
	 *		url - get запрос со всеми данными (в какой словарь, сколько результатов, направление перевода), 
	 *		options - парметры запроса (смотри axiosOptions)
	 * Выходные данне:
	 * 		объект содержащий положительный ответ от Оксфорда + поле statusCode (200 - все хорошо, любое другое число - https://developer.oxforddictionaries.com/documentation/response-codes)
	 * 		объект с кодом состояния и сопровождающим текстом в случае негативного ответа от оксфорда ( коды 6XX - мы добавляем самостоятельно)
	 * 		ошибка Server Error в случае нештатной ситуации
	 */ 
	async queryOxford ( url, options ){

		try {

			// запрос в оксфорд с нормальным ответом
			const result = await axios.get( url, options );
			result.data.statusCode = result.status ;
			return result.data;
		}

		// нештатная ситцуация
		catch( error ) { 
			
			const result = {};

			// нет ни HTTP кода, ни текстового кода от оксфорда - выбрасываем ошибку Server Error с записью в лог
			if( error.code === undefined && error.response === undefined ) {
				console.log( `Server Error in Oxford->queryOxford. ${error}` );
				throw 'Server Error';
			}

			// какой-то ответ от оксфорда есть, начинаем его разбирать и первым делом запомним сообщение об ошибке для клиента
			result.message = error.message;

			// если от оксфорда получен HTTP код, вернем его
			if( error.response !== undefined ) {
				result.statusCode = error.response.status;
			}

			// если нет HTTP кода, но есть текстовый код ошибки, тогда сами присвоим некий код и верне его
			else {

				// в ряде случаев оксфорд возвращает ошибку не в виде числового кода, а в виде текстового, в этом случае мы сами назначаем код из обладсти 6XX
				switch( error.code ) {

					// неизвестные символы
					case 'ERR_UNESCAPED_CHARACTERS':
						result.statusCode = 601;
						break;

					// не знаю, что это за ошибка, но она случаестя
					case 'ECONNRESET':
						result.statusCode = 602;
						break;

					// случилась некая ошибка, но я не знаю что это такое
					default:
						result.statusCode = 699;
						break;
				}
			}		
			return result;
		}
	}

	// запрос в thesaurus
	async getThesaurus ( data ) {

		try {
			return this.queryOxford( `${this.baseUrl}/thesaurus/en/${data.word.toLowerCase()}?strictMatch=false`, this.axiosOptions() );
		}
		catch( error ) {
			console.log( `The error happened in the Oxford->getThesaurus. ${error}` );
			throw 'Server Error';
		}
	}

	// запрос в entries
	async getEntries ( data ) {

		try {
			return this.queryOxford( `${this.baseUrl}/entries/en-us/${data.word.toLowerCase()}?strictMatch=false`, this.axiosOptions() );
		}
		catch( error ) {
			console.log( `The error happened in the Oxford->getEntries. ${error}` );
			throw 'Server Error';
		}	
	}

	// запрос в translations
	async getTranslations ( data ) {
		
		try {
			return this.queryOxford( `${this.baseUrl}/translations/en/${data.targetLanguage}/${data.word.toLowerCase()}?strictMatch=false&fields=translations`, this.axiosOptions() );
		}
		catch( error ) {
			console.log( `The error happened in the Oxford->getTranslations. ${error}` );
			throw 'Server Error';
		}
	}

	// запрос в search
	async getSearch ( data ) {

		try {

			let result = await this.queryOxford( `${this.baseUrl}/search/en-us?q=${data.word.toLowerCase()}&limit=15`, this.axiosOptions() );
			
			// если поиск не дал результатов, то сами назначим код 404 (результата нет)
			if( Array.isArray( result.results ) && result.results.length === 0 ) {
				result.statusCode = 404;
			}

			return result; 
		}
		catch( error ) {
			console.log( `The error happened in the Oxford->getSearch. ${error}` );
			throw 'Server Error';
		}
	}

	// запрос в entries (search) + translations
	async getAll ( data ) {

		// объект с результатом
		const results = {
			endpoint: null,
			statusCode: null,
			message: null,

			entries: null,
			translations: null,
			search: null,
		};

		try {

			// сначала делаем запрос в entries и пытаемся найти слово, по итогом заполняем объект с результатами
			const entries = await this.getEntries( data );

			results.endpoint = 'entries';
			results.entries = entries;
			results.statusCode = entries.statusCode;
			results.message = entries.message;

			// если слово не найдено в entries, воспользуемся запоросом в search
			if( entries.statusCode === 404 ) {

				const search = await this.getSearch( data );
				results.endpoint = 'search';
				results.search = search;
				results.statusCode = search.statusCode;
				results.message = search.message;
				return results;
			}
			
			// если хоть что-то было найдено в entries попробуем перевести это на клиентский язык
			if( results.statusCode === 200 && data.translationEnabled === true && data.targetLanguage !== "other" ){

				const translations = await this.getTranslations( data );
				results.endpoint = 'translations';
				results.translations = translations;
				results.statusCode = translations.statusCode;
				results.message = translations.message;
			}

			return results;
		}
		catch( error ) {
			console.log( `The error happened in the Oxford->getSearch. ${error}` );
			throw 'Server Error';
		}
	}

}

module.exports = oxfordDictionaries;

