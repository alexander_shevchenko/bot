'use strict'

const admin = require('firebase-admin');
//const functions = require('firebase-functions');

class FireBaseBD {

	//
	constructor( admin ) {

		this.db = admin.firestore();
	}

	// получить дату и время последней тренировки
	getLastTraining( psid ) {

		const self = this;

		return new Promise( function(resolve, reject) {
			
			// запрос из базы слова, у которого еще нет правильного ответа
			self.db.collection("students").doc(psid).get()
			.then( (doc) => {

				if (!doc.exists) {
					console.log("getLastTraining: No such document.");
					resolve(0);
				} else {
					console.log("getLastTraining: Last training in: ", doc.data().lastTraining.toDate());
					resolve(doc.data().lastTraining.toDate());
				}
			})
			.catch( (error) => {

				console.log("getLastTraining: Error read last training. ", error);
				reject(error);
			});
		});
	}

	// установим время последнего тестирования
	setLastTraining( psid ) {

		const self = this;

		return new Promise( function(resolve, reject) {
			
			const now = new Date();
			const stmp = new admin.firestore.Timestamp(Math.floor(now/1000),0);

			// запрос из базы слова, у которого еще нет правильного ответа
			self.db.collection("students").doc(psid).update( { lastTraining: stmp } )
			.then( (result) => {

				console.log("setLastTraining: Set last training to " + psid);
				resolve(1);
			})
			.catch( (error) => {

				console.log("setLastTraining: Error save last training. ", error);
				reject(error);
			});
		});
	}

	// установим в базе у всех слов поле answer = false
	setZero( psid ) {

		const self = this;

		return new Promise( function(resolve,reject) {

			// запрос слов у которых уже есть ответ 
			//self.db.collection("words").where("answer","==",true).get()
			self.db.collection("students").doc(psid).collection("words").get()
			.then((snapshot) => {

				if (snapshot.empty) {
					console.log("setZero: No documents to set in zero.");
				} 
				else {
					let answers = [];
					let i = 1;
					snapshot.forEach( (doc) => {
						const promise = self.db.collection("students").doc(psid).collection("words").doc(doc.id).update( { answer: false, number: i++ } );
						answers.push(promise)
					});
					Promise.all(answers);
					console.log("setZero: " + answers.length + " documents set in zero.");
				}
				
				const stmp = new admin.firestore.Timestamp(10,10);

				self.db.collection("students").doc(psid).update( { lastTraining: stmp } )
				.then( (result) => {
					console.log("setZero: Last Training set in zero. ");
					resolve(true);	
				})
				.catch((error) => {

					console.log("setZero: Error to set last Training in zero ", error);
					reject(error);
				});

				
			})
			.catch((error) => {

				console.log("setZero: Error to set documents in zero ", error);
				reject(error);
			});
		});
	}

	// получим все слова из базы, у которого поле anser = false
	getWords( psid ) {

		const self = this;

		return new Promise( function(resolve, reject) {
			
			// запрос из базы слова, у которого еще нет правильного ответа
			self.db.collection("students").doc(psid).collection("words").where("answer","==",false).get()

			.then((snapshot) => {

				if (snapshot.empty) {
					console.log("getWords: No words in base for repetitors.");
					resolve(0);
				} 
				else {
					console.log("getWords: " + snapshot.size + " words for training .");
					resolve(snapshot.size);
				}
			})
			.catch((error) => {

				console.log("getWords: Error getting documents", error);
				reject(error);
			});
		});
	}

	// получим 1 слово из базы, у которого поле anser = false
	getWord( psid ) {

		const self = this;

		return new Promise( function(resolve, reject) {
			
			// запрос из базы слова, у которого еще нет правильного ответа
			self.db.collection("students").doc(psid).collection("words").where("answer","==",false).orderBy("number").limit(1).get()

			.then( (snapshot) => {

				if (snapshot.empty) {
					console.log("getWord: No words in base for repetitors.");
					resolve(0);
				} 
				else {
					console.log("getWord: Words " + snapshot.docs[0].data().wordEn + " get from bd.");
					resolve(snapshot.docs[0].data());
				}
			})
			.catch( (error) => {

				console.log("getWord: Error getting documents", error);
				reject(error);
			});
		});
	}

	// получим слово из базы по id
	getWordID( psid, id ) {

		const self = this;

		return new Promise( function(resolve, reject) {
			
			// запрос из базы слова, у которого еще нет правильного ответа
			self.db.collection("students").doc(psid).collection("words").doc(id).get()

			.then( (doc) => {

				if (!doc.exists) {
					console.log("getWordID: No word for this id.");
					resolve(0);
				} else {
					console.log("getWordID: Found word for id: " + id);
					resolve(doc.data());
				}
			})
			.catch( (error) => {

				console.log("getWordID: Error read word for ID ", error);
				reject(error);
			});
		});
	}

	// получим слово из базы по id
	setAnswerTrue( psid, id ) {

		const self = this;

		return new Promise( function(resolve, reject) {
			
			// запрос из базы слова, у которого еще нет правильного ответа
			self.db.collection("students").doc(psid).collection("words").doc(id).update( {answer:true} )
			.then( (result) => {

				console.log("setAnswerTrue: True answer for id: " + id);
				resolve(1);
			})
			.catch( (error) => {

				console.log("setAnswerTrue: Error read word for ID ", error);
				reject(error);
			});
		});
	}
}

module.exports = FireBaseBD;
