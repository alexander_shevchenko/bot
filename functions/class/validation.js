/*
 * Функции проверяет data на наличие необходимых полей
 * В случае их отсутствия возвращает Validation Error
 */

function isData( data, fields ) {

    // проверим существование data
    if( data === undefined ){
        return 'Validation Error';
    }

    // проверим существование полей
    for ( let i = 1 ; i < arguments.length ; i++ ) {
        if( data[arguments[i]] === null || data[arguments[i]] === undefined ){
            return 'Validation Error';
        }
    }

    return true;
}

/*
 * Проверим, что переменная существует, то есть не равна undefined или null
 * Если empty = true, то проверим что значение переменной не равно ''
 * Вернет значение переменной или null
 */
function nullOrNot( variable, empty ) {

    if( empty === true )
        return ( variable === undefined || variable === null || variable === '' ) ? null : variable;
    else 
        return ( variable === undefined || variable === null ) ? null : variable;
}

module.exports.isData = isData;
module.exports.nullOrNot = nullOrNot;