'use strict'

class FireBaseBD {

	//
	constructor( firebase ) {

		this.db = firebase.database();
		this.dbf = firebase.database;
	}



	/*
	 * Работа со студентами
	 */ 

	// получить список атрибутов пользователей, вернет Object или null
	async getAllUsers() {
		
		try {
			
			const snapshot = await this.db.ref("students-attributes/").once("value");
			return snapshot.val();
		}
		catch( error ) {
			throw Error( "FireBaseBD->getAllUsers: Error get list of students. " + error );
		}
	}

	// получим атрибуты студента (userId), вернет Object или null
	async getAttributesUser( userId ) {

		try {
			const snapshot = await this.db.ref("students-attributes/" + userId ).once("value");
			return snapshot.val();
		}
		catch( error ) {
			throw Error( "FireBaseBD->getAttributesUser: Error to read attributes from 'students' = " + userId + ". " + error );
		}
	}

	// установим у студента (userId) атрибут, вернет true или null
	async setAttributeUser( userId, attribute, value, attributes, values ) {

		try {
			await this.db.ref( `students-attributes/${userId}` ).update({ [attribute]: value });

			for ( let i = 3 ; i < arguments.length ; i+=2 ) {
				await this.db.ref( `students-attributes/${userId}` ).update({ [arguments[i]]: arguments[i+1] });
			}

			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->setAttributeUser: Error to set attribute '${attribute}: ${value}' for student '${userId}'. ${error}` );
		}
	}

	// установим у студента (userId) атрибуты, вернет true или error
	async setAttributesUser( userId, attributes ) {

		try {
			await this.db.ref( 'students-attributes/' ).update({ [userId]: attributes});
			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->setAttributesUser: Error to set attributes ${attributes} for student ${userId}. ${error}` );
		}
	}

	// создаем нового пользователя, возвращает id, который можно использовать, например, в setAttributeUser
	// физически в базу функция записей не вносит
	async createUser() {

		try {
			// генерируем id 
			return await this.db.ref("students/").push().key;
		}
		catch( error ) {
			throw Error( "FireBaseBD->createUesr: Error to create new user." + error );
		}
	}

	// получение аттрибутов пользователя и его атрибутов по его id (psid) в FB
	// используется для идентификации пользователя
	async getUserFromPsid( psid ) {

		try {
			const snapshot = await this.db.ref( 'students-attributes/' ).orderByChild( 'psid' ).equalTo( psid ).once( 'value' );
			return snapshot.val();
		}
		catch( error ) {
			throw Error( `FireBaseBD->getUserFromPsid: Error to read user with psid = ${psid}. ${error}` );
		}
	}

	// получение аттрибутов пользователя по tgid (chat_id в телеграм)
	// используется для идентификации пользователя
	async getUserFromTGid( tgid ) {

		try {
			const snapshot = await this.db.ref( 'students-attributes/' ).orderByChild( 'tgid' ).equalTo( Number(tgid) ).once( 'value' );
			return snapshot.val();
		}
		catch( error ) {
			throw Error( `FireBaseBD->getUserFromTGid: Error to read user with tgid = ${tgid}. ${error}` );
		}
	}

	// получить список пользователей, у которых подошел alarmClock, вернет Object или null
	async getUsersWithAlarm() {
		
		const time = new Date();
		try {
			const snapshot = await this.db.ref('students-attributes/').orderByChild( 'alarmClock' ).startAt(1).endAt( time*1 ).once( 'value' );
			return snapshot.val();
		}
		catch( error ) {
			throw Error( `FireBaseBD->getAllUsers: Error get list of students. ${error}` );
		}
	}


	
	/*
	 * Работа с карточками
	 */ 

	// получить список карточек пользователя, вернет Object или null
	async getUserAllCards( userId ) {

		try {
			const snapshot = await this.db.ref("students/" + userId + "/cards/").once("value");
			return snapshot.val();
		}
		catch( error ) {
			throw Error( "FireBaseBD->getUserAllCards: Error get list of students. " + error );
		}
	}

	// Создать очередь карточек
	async createQueue( userId, countCard, currentDay ) {

		let countNew = 0;
		let queueNew = new Object();
		let queueRelearning = new Object();
		let queueInactive = new Object();
		let queueReview = new Object();

		try {

			// получим карточки со статусом new - это недоученные карточки с прошлых дней, то есть это бывшая inactive карточка, на которую был хотя бы один ответ, но это не привело к ее переходу в статус review
			const snapshotNew = await this.db.ref( `students/${userId}/cards/` ).orderByChild( 'status' ).equalTo( 'new' ).once( 'value' );
			if( snapshotNew.val() ) {
				queueNew = snapshotNew.val();
				countNew = Object.keys(queueNew).length;
			}

			// запросим relearning карточки
			const snapshotRelearning = await this.db.ref( `students/${userId}/cards/` ).orderByChild( 'status' ).equalTo( 'relearning' ).once( 'value' );
			if( snapshotRelearning.val() ) {
				queueRelearning = snapshotRelearning.val();
			}

			// запросим inactive карточки
			if( countNew < countCard ) {
				const snapshotInactive = await this.db.ref( `students/${userId}/cards/` ).orderByChild( 'status' ).equalTo( 'inactive' ).limitToFirst( countCard - countNew ).once( 'value' );
				if( snapshotInactive.val() ) {
					queueInactive = snapshotInactive.val();
				}
			}

			// получим список карточек со статусом review у которых сегодня due date или уже прошла
			// ВАЖНО !!!!! тут я считаю, что dueData есть только у review карточек 
			const snapshotReview = await this.db.ref( `students/${userId}/cards/` ).orderByChild( 'dueDate' ).endAt( currentDay ).once( 'value' );
			if( snapshotReview.val() ) {
				queueReview = snapshotReview.val();
			}

			return Object.assign( queueReview, queueNew, queueRelearning, queueInactive );
		}
		catch( error ) {
			throw Error( `FireBaseBD->createQueue: Error create queue for student '${userId}'. ${error}` );
		}
	}
	
	// создать custom карточку
	async createCard( userId, front, back = "", definition = "", example = "") {

		try {

			// генерируем id карточки
			const cardId = await this.db.ref( `students/${userId}/cards/` ).push().key;

			// создаем карточку 
			await this.db.ref( `students/${userId}/cards/${cardId}` ).set({ 
				id: cardId,
				type: "custom",
				status: "inactive",
				dueDate: 999999, // по идее это поле должно появиться только при смене статуса карточки на review
				Ef: 250,
				period: 0,
				definition: definition,
				front: front,
				back: back,
				example: example
			});

			return cardId;
		}
		catch( error ) {
			throw Error( `FireBaseBD->createCard: Error create cart '${front}' for student '${userId}'. ${error}` );
		}
	}

	// создать oxford карточку
	async createOxfordCard( userId, wordId, front, part, definition, defId, 
		audioUrl = '', example = '', synonym = '', antonym = '', synonyms = '', antonyms = '', 
		back = '', transcription = '', domains = '', hint = '' ) {
		
 		try {

			// генерируем id карточки
			const cardId = await this.db.ref(`students/${userId}/cards/`).push().key;

			// создаем карточку 
			await this.db.ref( `students/${userId}/cards/${cardId}`).set({ 
				id: cardId,
				type: 'oxford',
				status: 'inactive',
				dueDate: 999999, // по идее это поле должно появиться только при смене статуса карточки на review
				Ef: 250,
				period: 0,
				wordId: wordId,
				front: front,
				part: part,
				definition: definition,
				defId: defId,
				audioUrl: audioUrl, 
				example: example,
				synonym: synonym,
				antonym: antonym,
				synonyms: synonyms,
				antonyms: antonyms,
				back: back,
				transcription: transcription,
				domains: domains,
				hint: hint
			});

			return cardId;
		}
		catch( error ) {
			throw Error( `FireBaseBD->createOxfordCard: Error create cart '${front}' for student '${userId}'. ${error}` );
		}
	}

	// удалить карточку
	async removeCard( userId, cardId ) {

		try {
			await this.db.ref( "students/" + userId + "/cards/" + cardId ).remove();
			return true;
		}
		catch( error ) {
			throw Error( "FireBaseBD->removeCard: Error remove card '" + cardId + "' for student '" + userId + "'. " + error );
		}
	}

	// проверяет есть ли уже карточка с таким front/back или с таким fron/definition
	async isDoubleCard( userId, front, back, definition, cardId ) {	

		try {

			cardId = cardId || null;

			const ref = this.db.ref( `students/${userId}/cards/` ).orderByChild( 'front' ).equalTo( front );
			const snapshot = await ref.once( 'value' );
			const result = snapshot.val();

			for ( let key in result ) {

				// пропустим все оксфорд-карточки
				if( result[key].type === 'oxford' ) continue;
			
				// еслт id задана, то мы проверяем карточки у которой нет такого id (мы в режиме редактирования карточкий)
				if( cardId ) {
					if ( key != cardId && result[key].front === front && 
						 ( back !== null && result[key].back === back || 
						   definition !== null && result[key].definition === definition ) ) {
						return true;
					}
				}
				
				// если id не задана, то мы проверяем все карточки (мы в режиме создания новой карточки)
				else {
					if ( result[key].front === front && 
						 ( back !== null && result[key].back === back || 
						   definition !== null && result[key].definition === definition ) ) {
						return true;
					}
				}
			}
			return false;
		}
		catch( error ) {
			throw `FireBaseBD->isDoubleCard: error find double for card '${cardId}' for student '${userId}'. ${error}`;
		}
	}

	// проверяет есть ли уже карточка с таким defId
	async isDoubleOxfordCard( userId, defId, cardId ) {

		cardId = cardId || null;

		try {

			const snapshot = await this.db.ref( "students/" + userId + "/cards/" ).orderByChild( "defId" ).equalTo( defId ).once( "value" );
	
			const result = snapshot.val();

			for ( let key in result ) {

				// если id задана, то мы проверяем карточки у которой нет такого id (мы в режиме редактирования карточкий)
				if( cardId ) {
					if ( key != cardId && result[key].defId === defId ) {
						return true;
						break;
					}
				}
				
				// если id не задана, то мы проверяем все карточки (мы в режиме создания новой карточки)
				else {
					if ( result[key].defId === defId ) {
						return true;
						break;
					}
				}
			}
			return false;
		}
		catch( error ) {
			throw Error( "FireBaseBD->isDoubleOxfordCard: error find double for card '" + cardId + "' for student '" + userId + "'. " + error );
		}
	}

	// обновить параметры карточки 
	async setAttributeCard( userId, cardId, attribute, value, attributes, values ) {

		try {

			await this.db.ref( `students/${userId}/cards/${cardId}/` ).update({ [attribute]: value });
			
			for ( let i = 4 ; i < arguments.length ; i+=2 ) {
				await this.db.ref( `students/${userId}/cards/${cardId}/` ).update({ [arguments[i]]: arguments[i+1] });
			}

			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->setAttributeCard: Error set attribute of card '${cardId}' for student '${userId}'. ${error}` );
		}
	}

	// установим у карточки (cardId студента (userId) ВСЕ атрибуты, вернет true или error
	async setAttributesCard( userId, cardId, attributes ) {

		try {
			await this.db.ref( `students/${userId}/cards/` ).update({ [cardId]: attributes});
			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->setAttributesCard: Error to set attributes ${attributes} for card ${cardId} students ${userId}. ${error}` );
		}
	}

	// получим все атрибуты у карточки (cardId) студента (userId), вернет null или Object
	async getAttributesCard( userId, cardId ) {

		try {
			const snapshot = await this.db.ref( "students/" + userId + "/cards/" + cardId + "/" ).once( "value" );
			return snapshot.val();
		}
		catch( error ) {
			throw "FireBaseBD->getAttributesCard: Error read attribute of card '" + cardId + "' for student '" + userId + "'. " + error;
		}
	}

	// проверим наличие карточки у пользователя, возвращает true или false
	async isCardExist( userId, cardId ) {

		try {
			const snapshot = await this.db.ref( `students/${userId}/cards/${cardId}/` ).once( 'value' );
			if( snapshot.val() === null ) return false;
			return true;
		}
		catch( error ) {
			throw `FireBaseBD->getAttributesCard: Error read attribute of card '${cardId}' for student '${userId}'. ${error}`;
		}
	}

	async setLogCard( userId, cardId, string ) {

		try {

			// получим время на сервере
			let serverTime = Date.now();
			while (1) {
				// проверим, что в логе нет записи с таким-же временем (костыль, но как смог)
				const snapshot = await this.db.ref( `students/${userId}/cards/${cardId}/logs/${serverTime}` ).once( "value" )
				if( snapshot.val() === null ) break;
				serverTime = Date.now();
			}

			// запишем строчку в лог
			await this.db.ref(`students/${userId}/cards/${cardId}/logs/` ).update({
				[ serverTime ]: string
			});

			return true;
		}
		catch( error ) {
			throw `FireBaseBD->setLogCard: Error set log of card '${cardId}' for student '${userId}'. ${error}`;
		}
	}
	// добавить запись в лог карточки
	/*async setLogCard( userId, cardId, string, timestamp ) {

		try {

			// переведем время создания лога в формат yy-mm-dd
			const time = new Date( timestamp*1 );
			const yy = time.getFullYear() % 100;
			const mm = ( time.getMonth()+1 < 10 ) ? `0${time.getMonth()+1}` : time.getMonth()+1 ;
			const dd = ( time.getDate() < 10 ) ? `0${time.getDate()}` : time.getDate() ;
			const hrs = ( time.getHours() < 10 ) ? `0${time.getHours()}` : time.getHours() ;
			const min = ( time.getMinutes() < 10 ) ? `0${time.getMinutes()}` : time.getMinutes() ;
			const sec = ( time.getSeconds() < 10 ) ? `0${time.getSeconds()}` : time.getSeconds() ;	
			//const mss = time.getMilliseconds();	
			
			// получим ключ для формирования лога 
			//const key = `${yy}-${mm}-${dd}_${hrs}:${min}:${sec}_${mss}`;
			const key = `${yy}${mm}${dd}-${hrs}${min}${sec}`;

			// запишем строчку в лог
			await this.db.ref(`students/${userId}/cards/${cardId}/logs/` ).update({
				[ key ]: string
			});

			return true;
		}
		catch( error ) {
			throw Error(`FireBaseBD->setLogCard: Error set log of card '${cardId}' for student '${userId}'. ${error}`);
		}
	}*/

	// получить лог карточки
	async getLogCard( userId, cardId ) {

		try {
			const snapshot = await this.db.ref( `students/${userId}/cards/${cardId}/logs/` ).once( 'value' )
			return snapshot.val();
		}
		catch( error ) {
			throw Error( `FireBaseBD->getLogCard: Error read log of card '${cardId}' for student '${userId}'. ${error}` );
		}
	}


	/*
	 * Lending
	 */

	// запомнить email
	async createEmail( email ) {

		try {
			// генерируем id для новой записи
			const emialId = await this.db.ref( "emailsLending/" ).push().key;

			// сохраняем email в базу
			await this.db.ref( "emailsLending/" + emialId + "/" ).set({ 
				email: email,
				serverTimestamp: this.dbf.ServerValue.TIMESTAMP
			});

			return emialId;
		}
		catch( error ) {
			throw Error( "FireBaseBD->createEmail: Error save email '" + email + "'. " + error );
		}
	}

	// проверяет есть ли уже такой email в базе
	async isDoubleEmail( email ) {

		try {
			const snapshot = await this.db.ref( "emailsLending/" ).orderByChild( "email" ).equalTo( email ).once( "value" );
			const result = snapshot.val();

			for ( let key in result ) {

				if ( result[key].email === email ) {
					return true;
					break;
				}
			}
			return false;
		}
		catch( error ) {
			throw Error( "FireBaseBD->isDoubleEmail: error find double for email '" + email + "'. " + error );
		}
	}



	/*
	 * ХЭШ 
	 */

	// получить список хэш-записей карточек пользователя
	async getUserAllHashCards( userId ) {

		try {
			const snapshot = await this.db.ref( `students/${userId}/hash/` ).once( 'value' );
			return snapshot.val();
		}
		catch( error ) {
			throw Error( `FireBaseBD->getUserAllHashCards: Error get hashes of students. ${error}` );
		}
	}

	// удаляем хэш-запись карточки, вернет true или исключение
	async removeHash( userId, hashId ) {

		try {
			await this.db.ref( `students/${userId}/hash/${hashId}` ).remove();
			return true;
		}
		catch( error ) {
			throw `FireBaseBD->removeHash: Error remove hash '${hashId}' for student '${userId}'. ${error}`;
		}
	}

	// создать хэши для карточек пользователя
	async createUserHashs( userId ) {
		
		try {
			
			// получим список хэш-записей карточек у пользователя
			const hashes = await this.getUserAllHashCards( userId );

			// переберем все записи и если найдем несуществующую, то удалим ее
			for ( const hash in hashes ) {

				if ( await this.getAttributesCard( userId, hash ) === null ) {
					await this.removeHash( userId, hash );
				}
			}

			// получим список карточек у пользователя
			const cards = await this.getUserAllCards( userId );
			
			// перебираем все карточки и создаем для каждой хэш-запись
			for ( const card in cards ) {

				await this.createCardHash( userId, card, cards[card].front, cards[card].part );
			}
		}
		catch( error ) {
			throw Error( "FireBaseBD->createUserHashs: Error create card hashs for student '" + userId + "'. " + error );
		}
	}

	// создать хэш-запись карточки
	async createCardHash( userId, cardId, front, part ) {

		try {

			// проверим тип карточки, если part === undefined, то это обычная, иначе оксфордская
			if( part === undefined ){
				await this.db.ref( `students/${userId}/hash/${cardId}/` ).set({ 
					f: front
				});
			}
			else {
				await this.db.ref( `students/${userId}/hash/${cardId}/` ).set({ 
					f: front,
					p: part
				});
			}

			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->createCardHash: Error create card hash '${cardId}' for student '${userId}'. ${error}` );
		}
	}

	// получить хэш-запись карточки (метод нужен для тестирования, проверяю с помощью него, что хэш создан)
	async getHashCard( userId, hashId ) {

		try {
			const snapshot = await this.db.ref( "students/" + userId + "/hash/" + hashId + "/" ).once( "value" );
			return snapshot.val();
		}
		catch( error ) {
			throw `FireBaseBD->getHashCard: Error read hash of card '${hashId}' for student '${userId}'. ${error}`;
		}
	}

	// обновить параметры хэш-записи карточки 
	async setAttributeHash( userId, cardId, attribute, value) {

		try {
			await this.db.ref( "students/" + userId + "/hash/" + cardId + "/").update({ [attribute]: value });
			return true;
		}
		catch( error ) {
			throw Error( "FireBaseBD->setAttributeHash: Error set attribute of hash '" + cardId + "' for student '" + userId + "'. " + error );
		}
	}

	
	/*
	 * Статистика
	 */

	// записать общую статистику пользователя
	async setStatCommon( userId, stat ) {

		try {
			await this.db.ref( `students-statistics/` ).update({ [`${userId}`]: stat});
		}
		catch( error ) {
			console.log(`FireBaseBD->setStatCommon: Error updating statistics for students '${userId}'. ${error}`);
			throw Error( `FireBaseBD->setStatCommon: Error updating statistics for students '${userId}'. ${error}` );
		}
	}

	// получить общую статистику пользователя
	async getStatCommon( userId ) {

		try {

			// получим статистику пользователя
			const res = await this.db.ref( `students-statistics/${userId}` ).once('value');

			// если статистики нет, то задим поля = 0
			const stat = res.val() === null ? { totalCards:0 , currentStreak:0, bestStreak: 0 } : res.val() ;
			
			// статистика какая-то есть, но не все поля заполнены
			stat.totalCards = stat.totalCards === undefined ? 0 : stat.totalCards;
			stat.currentStreak = stat.currentStreak === undefined ? 0 : stat.currentStreak;
			stat.bestStreak = stat.bestStreak === undefined ? stat.currentStreak : stat.bestStreak;
			
			return stat;
		}
		catch( error ) {
			throw Error( `FireBaseBD->getStatCommon: Error getting statistics for students '${userId}'. ${error}` );
		}
	}

	// меняем счетчик количества карточек
	// userId - id пользователя
	// delta - на сколько изменить количество карточек, полагаю тут должно быть +1 или -1
	async setTotalCards( userId, delta ) {

		try {
			
			// увеличим счетчик карточек
			const snapshot = await this.db.ref(`students-statistics/${userId}/totalCards/` ).once('value');
			await this.db.ref( `students-statistics/${userId}` ).update({ ['totalCards']: snapshot.val() + delta });

			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->newStatInc: Error for increase statistics of new cards for students/day '${userId}/${day}'. ${error}` );
		}
	}

	// увеличиваем счетчик статистики изученных новых карточек пользователя за указанную дату 
	// userId - id пользователя
	// day - день в формате yy-mm-dd
	async incStatNewCard( userId, day ) {

		try {
			
			// увеличим счетчик новых карточек за сегодня
			const snapshot = await this.db.ref( `students-statistics/${userId}/${day}/trainedNew/` ).once('value');
			await this.db.ref( `students-statistics/${userId}/${day}` ).update({ ['trainedNew']: (snapshot.val() + 1) });

			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->incStatNewCard: Error for increase statistics of new cards for students/day '${userId}/${day}'. ${error}` );
		}
	}

	// увеличиваем счетчик статистики изученных всех карточек пользователя за указанную дату 
	// userId - id пользователя
	// day - день в формате yy-mm-dd
	async incStatTotalCard( userId, day ) {

		try {

			// увеличим счетчик карточек за сегодня			
			const snapshot = await this.db.ref(`students-statistics/${userId}/${day}/trainedTotal/` ).once('value');
			await this.db.ref( `students-statistics/${userId}/${day}` ).update({ ['trainedTotal']: (snapshot.val() + 1) });

			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->incStatTotalCard: Error for increase statistics of total cards for students/day '${userId}/${day}'. ${error}` );
		}
	}

	// увеличим счетчит статистики тренировок без перерыва
	// проверяем наличие рекорда по статистике и запоминаем его
	async incStatCurrentStreak( userId ) {

		try {

			// увеличим на 1 статистику тренировок без перерыва
			const snapshotСurrentStreak = await this.db.ref(`students-statistics/${userId}/currentStreak/` ).once('value');
			const currentStreak = snapshotСurrentStreak.val() == undefined ? 1 : snapshotСurrentStreak.val() + 1;
			await this.db.ref( `students-statistics/${userId}` ).update({ ['currentStreak']: currentStreak });

			// если у нас рекорд, то запомним его
			const snapshotBestStreak = await this.db.ref(`students-statistics/${userId}/bestStreak/` ).once('value');
			const bestStreak = snapshotBestStreak.val() == undefined ? 0 : snapshotBestStreak.val() ;
			if( currentStreak > bestStreak ) {
				await this.db.ref( `students-statistics/${userId}` ).update({ ['bestStreak']: currentStreak });
			}			

			return true;
		}
		catch( error ) {
			throw Error( `FireBaseBD->incStatCurrentStreak: Error for increase statistics of streal for students '${userId}'. ${error}` );
		}	
	}
}

module.exports = FireBaseBD;
