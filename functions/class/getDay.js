// константы
const config = require('./config.js');

/*
 * Функции работы с датами
 */

// Получим текущий день у клиента в числовом формате YYMMDD
// Время перехода на новые сутки задано в hourStart в config.js
// Таким образом текущий день определяется как времяСервера + таймЗона - hourStart
// Если задана delta, то от текущего дня отнимаем delta
function getDay( timezone, delta = 0 ) {

    let timeClient = new Date();
    
    timeClient.setHours( timeClient.getHours() + timezone - config.hourStart );
    
    if( delta > 0 ) {
        timeClient.setDate( timeClient.getDate() - delta );
    }

    const dd = (timeClient.getDate() < 10 ) ? `0${timeClient.getDate()}` : timeClient.getDate();
    const mm = (timeClient.getMonth()+1 < 10 ) ? `0${timeClient.getMonth()+1}` : timeClient.getMonth()+1;
    const yy = timeClient.getFullYear() % 100;

    return 1*`${yy}${mm}${dd}`;
}

// Рассчитаем разницу в днях между двумя датами day1-day2
// Даты записаны в числовом формате yymmdd
function deltaDay( day1, day2 ) {

    const d1 = new Date( 2000+(day1-day1%10000)/10000 , (day1%10000-day1%100)/100-1 , day1%100 )
    const d2 = new Date( 2000+(day2-day2%10000)/10000 , (day2%10000-day2%100)/100-1 , day2%100 )
    return (d1-d2)/86400000;
}

module.exports.getDay = getDay;
module.exports.deltaDay = deltaDay;