'use strict';

/*
 * Класс для работы с Free Dictionary
 *
 * Документация: https://dictionaryapi.dev
 * 
 */

const axios = require('axios');

class FreeDictionary {

    constructor() {}

    async getDefinitions(word) {

        try {
          const result = await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${word}`);
          return result.data;
        
        } catch (error) {

            if( global.test === undefined )
                console.log( `The error happened in the FreeDictionary->getDefinitions. ${error}` );
            
            throw 'Server Error';
        }
    }
}

module.exports = FreeDictionary;
