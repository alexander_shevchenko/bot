'use strict'
/*
 * Класс для Телеграмм бота
 * https://core.telegram.org/bots/api
 */

// инфо о вебхуке
// https://api.telegram.org/bot1035367941:AAHJlM1rE-hq7kSV0xnK3UXEiVI1sUqmepo/getWebhookInfo


const axios = require('axios');
const crypto = require('crypto');

class BotTG {

	// конструктор бота 
	constructor( token, webView ) {

		// ключ 
		this.token = token;
		
		// url
		this.baseurl = `https://api.telegram.org/bot${token}`;

		// адрес страниц, где работаем 
		this.webView = webView;

		// время жизни хэш из ТГ в секундах
		this.timeHash = 30;

		// настраиваемая клавиатура, которая всегда внизу
		this.customKeyboards = [ [ 'Study Now', 'Search Oxford' ], [ 'My Cards', 'My Settings' ] ];

		// версия приложения
		this.versionApp = 1.0112;
	}
	
	/* Метод получает обновления от ТГ и решает, что с ним делать
	 *	 update - сообщение от ТГ 
	 * 
	 * Логика работы:
	 * 	Если получили сообщение от инлайн-кнопки, то это однозначная инструкция к выполнению
	 *	Если получили текст, то либо класс сам реагирует на него, либо возвращает для дальнейшей обработки
	 *		getStarted - начало общения с ботом
	 * 		findInOxford - поиск слова в оксфорде и соответствующий диалог 
	 */
	async route( update ) {

		try {

			// если присутствует поле callback_query, значит нажали какую-то инлайн кнопку
			if( update.callback_query !== undefined ) {

				// будем обрабатывать поле callback_query
				const callback_query = update.callback_query;

				// callback_query содержит game_short_name, значит отправляем в чат приглашение в игру
				if( callback_query.game_short_name ) {

					await this.startGame( 
						callback_query.id,				// id каллбак запроса
						callback_query.from.id,			// чат с ботом
						callback_query.game_short_name,	// короткое имя игры (Practice, ...)
						callback_query.message			// исходное сообщение
						);
				}

				// callback_query содержит data, значит запускаем игру
				if( callback_query.data ) {
					
					await this.sendGame( 
						callback_query.from.id,	// чат с ботом
						callback_query.data,	// присланные данные от нажатой инлайн кнопки (practiceInlineButton, ...)
						callback_query.message	// исходное сообщение, там в тексте кнопки могут быть данные
					);
      			}
			}

			// пришло текстовое сообщение (набрали текст, вызвали команду /... или нажали кнопку на клавиатуре)
			if( update.message !== undefined ) {

				// будем обрабатывать поле message
				const message = update.message;

				// парсим присланный текст
				const result = await this.getText( message );

				// прислали что хотят начать общение
				if( result === 'getStarted' ) {
					
					return {
						getStarted: true,
						tgid: message.from.id,
						name: message.from.first_name
					};
				}

				// прислали некий текст (не команду) - значит будет делать запрос в оксфорд
				if( result === 'findInOxford' ) {

					return {
						findInOxford: true,
						tgid: message.from.id,
						text: message.text
					};
				}

				return true;
			}
		}
		catch( error ) {
			throw `BotTG->route: There was an error processing messages from TG. ${error}`;
		}
	}

	/* Метод для отправки сообщения студенту. Заодно формируем стандартную клавиатуру.
	 *	chatId - id чата студента с ботом
	 *	text   - текст для студента
	 */
	async text( chatId, text ) {

		try {			
			const options = {
				method: 'get',
				url: `${this.baseurl}/sendMessage`,
				data: {
					chat_id: chatId,
					text: text,
					parse_mode: 'HTML',
					reply_markup: {
						keyboard: this.customKeyboards,
						resize_keyboard: true
					}
				}
			}
			return await axios( options );
		}
		catch( error ) { 
			throw `BotTG->text: Error to send message ${text} to chat ${chatId}. ${error}`;
		}
	}

	/* Метод для сообщает пользователю, что бот печатает текст
	 *	chatId - id чата студента с ботом
	 *	ms     - время в мс, за которое имитируется печать текста
	 */
	async dot( chatId, ms ) {

		try {

			const options = {
				method: 'get',
				url: `${this.baseurl}/sendChatAction`,
				data: {
					chat_id: chatId,
					action: 'typing'
				}
			}
			
			const res = await axios( options );

			// задержка в ms милисекунд
			const delay = t => new Promise(resolve => setTimeout( resolve, t ));
			await delay( ms );

			return res;
		}
		catch( error ) { 
			throw `BotTG->dot: Error send dots to chat ${chatId}. ${error}`;
		}
	}

	async buttonsTemplate( chatId, text, buttons ) {

		const options = {
			method: 'get',
			url: `${this.baseurl}/sendMessage`,
			data: {
				chat_id: chatId,
				text: text,
				parse_mode: 'HTML',
				reply_markup: {
					inline_keyboard: buttons
				}
			}
		}

		try {			
			return await axios( options );
		}
		catch( error ) { 
			console.log( `Server Error in BotTG.buttonsTemplate. ${error}` );
			throw error;
		}
	}
		
	// Шаблон инлайн кнопки: текст + кнопки
	async buttonTemplate( chatId, text, button) {
		// Поддерживает два вида кнопок: урл кнопки и кнопки обратной передачи
		//
		// Формат урл кнопки
		// {
		// "text": "text_on_button",
		// "url": "<URL_TO_OPEN>",
		// }
		//
		// Формат кнопки обратной передачи
		// {
		// "text": "text_on_button",
		// "callback_data": "data_send_callback"
		// } 

		let buttons = new Array();
		for ( let i = 2 ; i < arguments.length ; i++ ) {
			buttons.push([arguments[i]]);
		}

		const options = {
			method: 'get',
			url: `${this.baseurl}/sendMessage`,
			data: {
				chat_id: chatId,
				text: text,
				parse_mode: 'HTML',
				reply_markup: {
					inline_keyboard: buttons
				}
			}
		}

		try {			
			return await axios( options );
		}
		catch( error ) { 
			console.log( `Server Error in BotTG.buttonTemplate. ${error}` );
			throw error;
		}
	}

	/* Метод показывает в чате приглашение в игру
	 *
	 * Отправлять игру в чат будем после нажатия соответствующей инлайн кнопки.
	 * Кнопка содержит одно из следующих значений: sendStudy, sendAddCard, sendMyCard, sendMySettings.
	 * В данных кнопки через пробел может быть передано число, определяющее номер кнопки в исходном сообщении.
	 * Считываем текст с кнопки с данным номером и ищем там слово обрамленное "". 
	 * Это слово, которое надо передать в игру, аналагичным образом подставим его в текст кнопки, запускающей игру.
	 *
	 * Игра отправится также после того как подали соответсвующую текстовую команду (руками или с клавиатуры).
	 * В этом случае никаких параметров не передается.
	 * 
	 *	chatId  - id чата студента с ботом
	 *	data    - определяет игру, возможные значения: 'Study Now', 'Add Card', 'My Cards', 'My Settings'
	 *			- через пробел может передаваться параметр, указывающий номер нажатой кнопки
	 *  message - сообщение в чате, инлайн кнопка которого была нажата
	 * 			- в кавычках передается поисковое слово
	 * 			- если значение неопределено, то sendGame был вызван ручным вводом команды или с клавиатуры
	 */
	async sendGame( chatId, data, message ) {

		try {

			// проверим передали ли нам дополнительные параметры
			let searchWordId = null;
			if( message ) {

				// в data кнокпи может быть включен номер нажатой кнопки в исходном сообщении
				const indexButton = data.indexOf(' ');
				// разделим номер кнопки и data кнопки
				const buttonN = indexButton === -1 ? null : data.slice( indexButton+1 );
				data = indexButton === -1 ? data : data.slice( 0, indexButton );

				// в текст кнопки может быть включено поисковое слово, обрамленное ""
				const inlineButtonText = message.reply_markup.inline_keyboard[buttonN][0].text;
				// вычленим поисковое слово 
				const index1 = inlineButtonText.indexOf('"');
				const index2 = inlineButtonText.lastIndexOf('"');
				searchWordId = index2 === -1 ? null : inlineButtonText.slice( index1+1, index2 );
			}

			let text = '';
			let gameName = '';
	
			// data определяет какую инлайн кнопку нажал пользователь, постараемся запустить солответсвующую игру
			switch ( data ) {
				case 'sendStudy':
					text = 'Study Now';
					gameName = 'Practice';
					break;
				case 'sendAddCard':
					// если задан параметр, то значит хотим добавить конкретное слово, покажем это в тексте кнопки 
					text = searchWordId === null ? 'Search Oxford' : `Search Oxford "${searchWordId}"`
					gameName = 'Search';
					break;
				case 'sendMyCard':
					text = 'My Cards';
					gameName = 'Cards';
					break;
				case 'sendMySettings':
					text = 'My Settings';
					gameName = 'Settings';
					break;
				default:
					throw `Recive wrong data '${data}'.`;
					break;
			}

			// формируем запрос для вызова игры
			const options = {
				method: 'get',
				url: `${this.baseurl}/sendGame`,
				data: {
					chat_id: chatId,
					game_short_name: gameName,
					reply_markup: { inline_keyboard: [ [ { text: text, callback_game: gameName } ] ] }
				}
			}

			// вызов игры
			return await axios( options );
		}

		catch ( error ) {
			throw `BotTG->sendGame: Error to send game to chat '${chatId}'. ${error}`;
		}
	}

	/* Метод запускает игру
	 *	callbackQueryId - id запроса
	 *	chatId  		- id чата студента с ботом
	 *	gameName 		- имя игры
	 *  message         - исходное сообщение
	 *	version 		- поле, чтобы избегать кэширования
	 */
	async startGame( callbackQueryId, chatId, gameName, message ) {

		try {

			// в текст первой кнопки может быть включено поисковое слово, обрамленное ""
			const inlineButtonText = message.reply_markup.inline_keyboard[0][0].text;
			// вычленим поисковое слово 
			const index1 = inlineButtonText.indexOf('"');
			const index2 = inlineButtonText.lastIndexOf('"');
			const searchWordId = index2 === -1 ? null : inlineButtonText.slice( index1+1, index2 );
		
			// сформируем URL игры, он может содержать: auth_client, auth_tgid, search_wordid, version_app
			let params = 'auth_client=tg' + 
						 '&auth_tgid=' + chatId +
						 '&version_app=' + this.versionApp + 
						 (searchWordId === null ? '' : '&search_wordid=' + searchWordId);
			let urlGame = '';
		
			switch ( gameName ) {
				case 'Practice':
					urlGame = 'study';
				break;
				case 'Search':
					urlGame = 'add';
				break;
				case 'Cards':
					urlGame = 'cards';
				break;
				case 'Settings':
					urlGame = 'settings';
				break;
			}
			console.log(`${this.webView}/${urlGame}?${params}`);
			const options = {
				method: 'get',
				url: `${this.baseurl}/answerCallbackQuery`,
				data: {
					callback_query_id: callbackQueryId,
					url: encodeURI( `${this.webView}/${urlGame}?${params}` )
				}
			}
			return await axios( options );
		}
		catch( error ) { 
			throw `BotTG->startGame: Error start game ${gameName}. ${error}`;
		}
	}

	/* Метод обрабатывает текстовые сообщения из telegram
	 * 	message - сообщение (команда из настраиваемой клавиатуры; текст набранный в чате; команда набранная в чате)
	 * Результатом обработки является либо запуск приглашения в игру, 
	 * либо возврат наверх сообщения, что требуется запустить стартовый диалог или диалог поиска слова в окчфорде
	 */
	async getText( message ) {

		try {

			switch ( message.text ) {
				case '/practice':
				case 'Study Now':
					this.sendGame( message.from.id, 'sendStudy' );
					break;
				case '/add':
				case 'Search Oxford':
					this.sendGame( message.from.id, 'sendAddCard' );
					break;
				case '/cards':
				case 'My Cards':
					this.sendGame( message.from.id, 'sendMyCard' );
					break;
				case '/settings':
				case 'My Settings':
					this.sendGame( message.from.id, 'sendMySettings' );
					break;
				case '/start':
					// сообщим наверх, что необходимо запустить стартовый диалог
					return 'getStarted';
				default:
					// сообщим наверх, что необходимо запустить диалог поиска слова в оксфорде
					return 'findInOxford';
			  }
			  return true;
		}
		catch( error ) {
			throw `BotTG->getText: Error occeured. ${error}`;
		}
	}


	/*
	 * Сервисные методы
	 */


	// отправим сообщение боту, что мы собираемся использовать вебхук
	async setWebHook( url ) {
		// url - адрес, где расположен вебхук, рекомендуется использовать адрес https://yourwebhookserver.com/<token>.

		const options = {
			method: 'get',
			url: `${this.baseurl}/setWebhook?url=${url}`
		}
		
		try {			
			return await axios( options );
		}
		catch( error ) { 
			throw error;
		}
	}

	// Проверим данные, что они действительно из ТГ 
	// Для этого расчитаем хэш из полученных данных и сравнить с хэшем присланным из ТГ
	// Вторая проверка - по времени. Если время указанное в присланных данных отличается от текущей более 60 секунд, то данные устарели
	// Пример данных - https://repeat-a738a.firebaseapp.com/study?id=111112064&first_name=Stanislav&last_name=Shevchenko&username=rlyAtas&photo_url=https://t.me/i/userpic/320/xluB12MtkVzl5IEZl50okd2JDxsR_EIIedCp2bGcFc8.jpg&auth_date=1602832101&hash=3fb91a0be9b3306f706f1e5a907cc89b4010c30b07034844eb90af4d18d3463d
	checkTG( data ) {

		// текущая дата
		const curentTime = (new Date() * 1) /1000 - data.auth_date;

		// проверим время
		if( curentTime >= 0 && curentTime <= this.timeHash ) {

			// запомним hash 
			const hash = data.hash;

			// удалим hash из data
			delete data.hash;

			// подготовим строчку с данными
			const str = Object.keys(data)
				.sort()
				.map(k => `${k}=${data[k]}`)
				.join('\n');
			
			// получим хэш и сравним с первоисточником
			const secretKey = crypto.createHash('sha256').update( this.token ).digest();
			const myHash = crypto.createHmac('sha256', secretKey ).update( str ).digest('hex');
			if( hash == myHash ) {

				return true;
			}
		}

		return false;
	}

}

module.exports = BotTG;