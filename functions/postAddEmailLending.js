//
// Получаем email от страницы лендинга и записываем в базу
//
// Входные данные:
//					data - объект
//					.email - email для записи в базу
//
// Выходные параметры:
//		true - если все ок
//		"Validation Error" - title прилетело пустым
//		"Duplicate Found" - словарь с таким названием существует
// 		"Server Error" - если что-то пошло не так

postAddEmailLendingBD = async function( data, bd ) {

	//console.log( "DEBUG. data.email = " + data.email );
 
	// проверка на пустоту
	if ( data.email === "" ) {
		throw "Validation Error";
	}

	try {

		// проверка на дубликат
		const isDouble = await bd.isDoubleEmail( data.email );

		//console.log( "DEBUG. isDouble = " + isDouble );

		if ( isDouble === true ) {
			throw "Duplicate Found";
		}

		// создадим словарь
		await bd.createEmail( data.email );

		//console.log( "DEBUG. true" );
		return true;
	}
	catch( error ) { 
		if ( error === "Duplicate Found" ) {
			throw "Duplicate Found";
		}
		else {
			console.log( "Server Error in postAddEmailLendingBD. " + error );
			throw "Server Error";
		}
	}
}

module.exports = postAddEmailLendingBD;