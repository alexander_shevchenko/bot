// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require( 'firebase-admin') ;
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

// для получения сегодняшней даты и статистики
const funDay = require( '../class/getDay.js' );

global.test = true;

/*
 * Что тестируем:
 *				1. Пришел запрос на оценку карточки без указания id пользователя, вернем Validation Error
 *				2. Пришел запрос на оценку карточки без указания карточки, вернем Validation Error
 * 				3. Пришла оценка на отсутствующую карточку, получим Server Error
 * 				4. Пришла оценка на обычную карточку, изменилась дата последней тренировки
 * 				5. Пришла оценка на карточку с неким status, Ef, логом и так далее. 
 * 					Проверим, что эти параметры у карточки изменились на заданные.
 * 				6. Пришла оценка на новую карточку (todayNewCard=true).
 * 					Должен измениться счетчик статистики новых карточек и общих карточек за сегодня.
 * 					Должен измениться атрибут пользователя - количество изученых новых карточек за сегодня.
 * 				7. Пришла оценка на обычную карточку (todayTotlaCard=true). 
 * 					Должен яизмениться счетчик статистики общих карточек за сегодня
 * 				8. Пришла оценка на карточку и последняя тренировка была вчера, 
 * 					Должен увеличиться счетчик тренировок подряд (currentStreak) 
 * 					и счетчик лучшей серии тренировок подряд (bestStreak).
 */ 

describe( 'Test grade card. ', function () {

	// наш студент для теста
	const userId = 'test-postGrade';
	
    // 1. Пришел запрос на окончание тренировки без указания id пользователя, вернем Validation Error
    it( '1. Post done without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postGrade )( {
                card: { id: '-0' }
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 2. Пришел запрос на удаление карточки без указания id карточки, вернем Validation Error
    it( '2. Post done without length. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postGrade )( {
                userId: userId
            });
       }, 
       {
           message: 'Validation Error'
       });
    });

	// 3. Пришла оценка на отсутствующую карточку, получим Server Error
	it( '3. Grade nonexistent сard. Expect "Server Error".', async function() {

		await assert.rejects( async function() {

			// оцениваем карточку с несуществующим id = 1
 			await test.wrap( myFunctions.postGrade )( {
				userId: userId,
				card: {
					id: '-1',
					Ef: 250
				}
 			});
		}, 
		{
			message: 'Server Error'
		});
	});

	// 4. Пришла оценка на обычную карточку, изменилась дата последней тренировки
	it( '4. Grade card. Expect lastTraining = today.', async function() {

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// определим дату сегодняшнего дня у студента
		const day = funDay.getDay( userAttr.timezone );

		// внесем в атрибут студента lastTraining дату = 800101 и посмотрим, что она изменилась на сегодняшнюю
		await bd.setAttributeUser( userId, 'lastTraining', 800101)

		const result = await test.wrap( myFunctions.postGrade ) ({
			userId: userId,
			card: {
				id: '-2',
				Ef: 250,
				status: 'review',
				dueDate: 800101,
				period: 1,
				newlogs: [ { string: 'create' } ]
			}
		});
		
		// проверим, что закончили тест без ошибок
        assert.equal( result, true );

		// узнаем атрибут студента lastTraining
		const attr = await bd.getAttributesUser( userId );

		// убедимся, что lastTraining = сегодняшнему дгю
		assert.equal( attr.lastTraining, day );

		// удалим лог, чтобы не плодился
		await bd.db.ref( `students/${userId}/cards/-2/logs` ).remove();
	});

	// 5. Пришла оценка на карточку с неким status, Ef, логом и так далее. 
 	//	Проверим, что эти параметры у карточки изменились на заданные.
	it( '5. Grade card. Expect change attributes status, Ef, period, newlogs.', async function() {

		// удалим лог, мало ли там что-то есть
		await bd.db.ref( `students/${userId}/cards/-2/logs` ).remove();

		// запишем параметры у карточки, которые потом измененим с помощью postGrade
		await bd.setAttributeCard( userId, '-2', 'Ef', 250, 'status', 'new', 'period', 1, 'dueDate', 200101);
		
		// оценим карточку
		const result = await test.wrap( myFunctions.postGrade ) ({
			userId: userId,
			card: {
				id: '-2',
				Ef: 200,
				status: 'review',
				dueDate: 800101,
				period: 2,
				newlogs: [ { string: 'create' }, { string: 'review' } ]
			}
		});
		
		// проверим, что закончили тест без ошибок
        assert.equal( result, true );

		// проверим, что параметры карточки соответствуют измененным
		const card = await bd.getAttributesCard( userId, '-2' );

		assert.equal( card.Ef, 200 );
		assert.equal( card.status, 'review' );
		assert.equal( card.dueDate, 800101 );
		assert.equal( card.period, 2 );
		const keys = Object.keys( card.logs );	
		assert.equal( card.logs[keys[0]], 'create' );
		assert.equal( card.logs[keys[1]], 'review' );

		// удалим лог, чтобы не плодился
		await bd.db.ref( `students/${userId}/cards/-2/logs` ).remove();
	});

	// 6. Пришла оценка на новую карточку (todayNewCard=true).
 	//	Должен измениться счетчик статистики новых карточек и общих карточек за сегодня.
 	//	Должен измениться атрибут пользователя - количество изученых новых карточек за сегодня.
	it( '6. Grade card with card.todayNewCard=true. Expect change counts of statistic and attributes todayCard.', async function() {

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// определим дату сегодняшнего дня у студента 
		const day = funDay.getDay( userAttr.timezone );

		// выставим на сегодня статистику, новых карт = 2, всего карт = 6
		await bd.db.ref( `students-statistics/${userId}/${day}/trainedNew` ).set( 2 );
		await bd.db.ref( `students-statistics/${userId}/${day}/trainedTotal` ).set( 6 );

		// выставим атрибут пользовател todayCard = 2
		await bd.setAttributeUser( userId, 'todayCard', 2 );

		// оценим карточку
		const result = await test.wrap( myFunctions.postGrade ) ({
			userId: userId,
			card: {
				id: '-2',
				Ef: 200,
				status: 'review',
				dueDate: 800101,
				period: 2,
				todayNewCard: true,
				newlogs: [ { string: 'create' }, { string: 'review' } ]
			}
		});
		
		// проверим, что закончили тест без ошибок
        assert.equal( result, true );

		// получим статистику студента и проверим, что она правильная (сегодня trainedNew=3 и trainedTotal=7)
		const stat = await bd.getStatCommon( userId )
		assert.equal( stat[day].trainedNew, 3 );
		assert.equal( stat[day].trainedTotal, 7 );

		// получим атрибуты студента и проверим, что счетчик изученых сегодня новых карточек правильный (todayCard=3)
		const attr = await bd.getAttributesUser( userId );
		assert.equal( attr.todayCard, 3 );

		// удалим статистику, чтобы не плодилась
		await bd.db.ref( `students-statistics/${userId}/${day}` ).remove();
		// удалим лог, чтобы не плодился
		await bd.db.ref( `students/${userId}/cards/-2/logs` ).remove();
	});

	// 7. Пришла оценка на обычную карточку (todayTotlaCard=true), изменился счетчик статистики общих карточек за сегодня
	it( '7. Grade card with card.todayTotlaCard=true. Expect change count of statistic.', async function() {

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// определим дату сегодняшнего дня у студента 
		const day = funDay.getDay( userAttr.timezone );

		// выставим на сегодня статистику, всего изучено карт = 18
		await bd.db.ref( `students-statistics/${userId}/${day}/trainedTotal` ).set( 18 );

		// оценим карточку
		const result = await test.wrap( myFunctions.postGrade ) ({
			userId: userId,
			card: {
				id: '-2',
				Ef: 200,
				status: 'review',
				dueDate: 800101,
				period: 2,
				todayTotlaCard: true,
				newlogs: [ { string: 'create' }, { string: 'review' } ]
			}
		});
		
		// проверим, что закончили тест без ошибок
        assert.equal( result, true );

		// получим статистику студента и проверим, что она правильная (сегодня trainedTotal=19)
		const stat = await bd.getStatCommon( userId )
		assert.equal( stat[day].trainedTotal, 19 );

		// удалим статистику, чтобы не плодилась
		await bd.db.ref( `students-statistics/${userId}/${day}` ).remove();
		// удалим лог, чтобы не плодился
		await bd.db.ref( `students/${userId}/cards/-2/logs` ).remove();
	});

	// 8. Пришла оценка на карточку и последняя тренировка была вчера, 
 	// Должен увеличиться счетчик тренировок подряд (currentStreak) 
 	// и счетчик лучшей серии тренировок подряд (bestStreak)
	it( '8. Grade card. Last train was yersteday. Expect change count of statistic.', async function() {

		// выставим на сегодня статистику, currentStreak = 5, bestStreak = 5
		await bd.db.ref( `students-statistics/${userId}/currentStreak` ).set( 5 );
		await bd.db.ref( `students-statistics/${userId}/bestStreak` ).set( 5 );

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// выставим дату последней тренировки - вчера
		const yesterday = funDay.getDay( userAttr.timezone, 1 );
		await bd.setAttributeUser( userId, 'lastTraining', yesterday );

		// оценим карточку
		const result = await test.wrap( myFunctions.postGrade ) ({
			userId: userId,
			card: {
				id: '-2',
				Ef: 200,
				status: 'review',
				dueDate: 800101,
				period: 2,
				newlogs: [ { string: 'create' }, { string: 'review' } ]
			}
		});
		
		// проверим, что закончили тест без ошибок
        assert.equal( result, true );

		// получим статистику студента и проверим, что она правильная ( currentStreak=6, bestStreak=6 )
		const stat = await bd.getStatCommon( userId )
		assert.equal( stat.currentStreak, 6 );
		assert.equal( stat.bestStreak, 6 );

		// удалим лог, чтобы не плодился
		await bd.db.ref( `students/${userId}/cards/-2/logs` ).remove();
	});

});

test.cleanup();