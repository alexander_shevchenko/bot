const assert = require( 'assert' );

const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require( 'firebase-admin') ;
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

global.test = true;

/*
 * Важно. 
 * Для теста студента test-addCard не требуются атрибуты. 
 * Однако, при добавлении карточки, в том числе вызываются соответствующие диалоги.
 * А для их реализации требуются psid или tgid, которые храняться в атрибутах.
 * Поэтому формально ветка атрибутов создана, но без psid или tgid.
 *
 * Что тестируем:
 * 				1. Нет поля data.userId, вернет Validation Error 
 * 				2. Нет поля data.type, вернет Validation Error 
 * 				3. data.type не custom и не oxford, вернет Validation Error 
 * 				4. data.type='custom' и data.front неопределено или пустое, вернет Validation Error 
 * 				5. data.type='custom' и data.back неопределено или пустое и data.definition неопределено или пустое, вернет Validation Error 
 * 				6. data.type='oxford' и data.wordId неопределено или пустое, вернет Validation Error 
 * 				7. data.type='oxford' и data.front неопределено или пустое, вернет Validation Error 
 * 				8. data.type='oxford' и data.part неопределено или пустое, вернет Validation Error 
 * 				9. data.type='oxford' и data.definition неопределено или пустое, вернет Validation Error 
 * 				10. data.type='oxford' и data.defId неопределено или пустое, вернет Validation Error 
 * 				11. data.type='custom', data.front='test' и data.back='тест', вернет Duplicate Found,
 * 					поскольку у студента есть карточка c id='-1' с точно такими же полями
 * 				12. data.type='custom', data.front='test' и data.definition='definition', вернет Duplicate Found
 *					поскольку у студента есть карточка c id='-1' с точно такими же полями
 *				13. data.type='oxford' и data.defId='defId', вернет Duplicate Found
 *					поскольку у студента есть карточка c id='-3' с точно такими defId
 *				14. создадим custom карточку, проверим что карточка и ее хэш создались,
 *					все поля созданы верно, лог записался и счетчик карточки увеличился на 1
 *				15. создадим oxford карточку, проверим что карточка и ее хэш создались,
 *					все поля созданы верно, лог записался и счетчик карточки увеличился на 1
 */

describe( 'Test create card.', function () {

	// наш студент для теста 
	const userId = 'test-postAddCard';

	// 1. Нет поля data.userId, вернет Validation Error 
	it( '1. Card without field "userId". Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

 			await test.wrap( myFunctions.postAddCard )( {
				type: 'custom',
				front: 'front',
				back: 'back'
 			});
		}, 
		{
			message: "Validation Error"
		});
	});
	
	// 2. Нет поля data.type, вернет Validation Error 
	it( '2. Card without field "type". Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

 			await test.wrap( myFunctions.postAddCard )( {
				userId: userId,
				front: 'front',
				back: 'back'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 3. data.type не custom и не oxford, вернет Validation Error 
	it( '3. Field "type" not "custom" and not "oxford" . Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

 			await test.wrap( myFunctions.postAddCard )( {
				userId: userId,
				front: 'front',
				back: 'back',
				type: 'not_custom_and_not_oxford'
 			});
		}, 
		{
			message: "Validation Error"
		});
	});

	// 4. data.type='custom' и data.front неопределено или пустое, вернет Validation Error 
	it( '4. Field "type"="custom" and field "front" is undefined. Expect "Validation Error".', async function() {

		await assert.rejects( async () => {

 			await test.wrap( myFunctions.postAddCard )( {
 				userId: userId,
 				type: 'custom',
				back: 'тест'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 5. data.type='custom' и data.back неопределено или пустое и data.definition неопределено или пустое, вернет Validation Error 
	it( '5. Field "type"="custom" and fields "back" and "definition" are undefined. Expect "Validation Error".', async function() {

		await assert.rejects( async () => {

 			await test.wrap( myFunctions.postAddCard )( {
 				userId: userId,
 				type: 'custom',
				front: 'test'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});
	
	// 6. data.type='oxford' и data.wordId неопределено или пустое, вернет Validation Error 
	it( '6. Field "type"="oxford" and field "wordId" are undefined. Expect "Validation Error".', async function() {

		await assert.rejects( async () => {

 			await test.wrap( myFunctions.postAddCard )( {
 				userId: userId,
 				type: 'oxford',
				front: 'front',
				part: 'part',
				definition: 'definition',
				defId: 'defId'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 7. data.type='oxford' и data.front неопределено или пустое, вернет Validation Error 
	it( '7. Field "type"="oxford" and field "front" are undefined. Expect "Validation Error".', async function() {

		await assert.rejects( async () => {

 			await test.wrap( myFunctions.postAddCard )( {
 				userId: userId,
 				type: 'oxford',
				wordId: 'wordId',
				part: 'part',
				definition: 'definition',
				defId: 'defId'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 8. data.type='oxford' и data.part неопределено или пустое, вернет Validation Error 
	it( '8. Field "type"="oxford" and field "part" are undefined. Expect "Validation Error".', async function() {

		await assert.rejects( async () => {

 			await test.wrap( myFunctions.postAddCard )( {
 				userId: userId,
 				type: 'oxford',
				wordId: 'wordId',
				front: 'front',
				definition: 'definition',
				defId: 'defId'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 9. data.type='oxford' и data.definition неопределено или пустое, вернет Validation Error 
	it( '9. Field "type"="oxford" and field "definition" are undefined. Expect "Validation Error".', async function() {

		await assert.rejects( async () => {

 			await test.wrap( myFunctions.postAddCard )( {
 				userId: userId,
 				type: 'oxford',
				wordId: 'wordId',
				front: 'front',
				part: 'part',
				defId: 'defId'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 10. data.type='oxford' и data.defId неопределено или пустое, вернет Validation Error 
	it( '10. Field "type"="oxford" and field "defId" are undefined. Expect "Validation Error".', async function() {

		await assert.rejects( async () => {

 			await test.wrap( myFunctions.postAddCard )( {
 				userId: userId,
 				type: 'oxford',
				wordId: 'wordId',
				front: 'front',
				part: 'part',
				definition: 'definition'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 11. data.type='custom', data.front='test' и data.back='тест', вернет Duplicate Found,
 	// поскольку у студента есть карточка c id='-1' с точно такими же полями
	it( '11. Card { type:"custom", front:"test", back:"тест", ... }. Expect "Duplicate Found".', async function() {

		await assert.rejects( async () => {
 			await test.wrap( myFunctions.postAddCard )( {
 				 userId: userId,
 				 type: 'custom',
				 front: 'test',
				 back: 'тест'
 			});
		}, 
		{
			message: 'Duplicate Found'
		});
	});

	// 12. data.type='custom', data.front='test' и data.definition='definition', вернет Duplicate Found
	// поскольку у студента есть карточка c id='-1' с точно такими же полями
	it( '12. Card { type:"custom", front:"test", definition:"definition", ... }. Expect "Duplicate Found".', async function() {

		await assert.rejects( async () => {
 			await test.wrap( myFunctions.postAddCard )( {
				 userId: userId,
				 type: 'custom',
				 front: 'test',
				 back: 'не тест',
				 definition: 'definition'
 			});
		}, 
		{
			message: 'Duplicate Found'
		});
	});
   
	// 13. data.type='oxford' и data.defId='defId', вернет Duplicate Found
	// поскольку у студента есть карточка c id='-3' с точно такими defId
	it( '13. Card { type:"oxford", defid:"defid", ... }. Expect "Duplicate Found".', async function() {

		await assert.rejects( async () => {
 			await test.wrap( myFunctions.postAddCard )( {
				 userId: userId,
				 wordId: 'wordId',
				 type: 'oxford',
				 front: 'test2',
				 part: 'part2',
				 definition: 'definition2',
				 defId: 'defId'
 			});
		}, 
		{
			message: 'Duplicate Found'
		});
	});
   
	// 14. создадим custom карточку, проверим что карточка и ее хэш создались,
 	// все поля созданы верно, лог записался и счетчик карточки увеличился на 1
	it( '14. Card custom card. Expect added card with all fields, hash, logs, and statistic totalCards increase by 1.', async function() {

		// выставим счетчик статистики на 2 (на всякий случай, вдруг я его где-то собью)
		await bd.db.ref( `students-statistics/${userId}` ).update({ ['totalCards']: 2});

		// создаем карточку
		const cardId = await test.wrap( myFunctions.postAddCard )
		({
 			userId: userId,
 			type: 'custom',
 			front: 'test0',
			back: 'тест0',
			definition: 'definition0',
			example: 'example0'
		});

		// проверим, что карточка создана и все поля карточки соответствуют ожидаемым
		const card = await bd.getAttributesCard( userId, cardId );
		assert.equal( card.type, 'custom' );
		assert.equal( card.front, 'test0' );
		assert.equal( card.back, 'тест0' );
		assert.equal( card.definition, 'definition0' );
		assert.equal( card.example, 'example0' );
		assert.equal( card.dueDate, 999999 );
		assert.equal( card.id, cardId );
		assert.equal( card.period, 0 );
		assert.equal( card.Ef, 250 );
		assert.equal( card.status, 'inactive' );
		
		// проверим, что лог создан правильно
		assert.equal( card.logs[ Object.keys( card.logs ).shift() ], 'create' );
	
		// проверим, что создан  hash карточки
		const hash = await bd.getHashCard( userId, cardId );
		assert.equal( hash.f, 'test0' );

		// проверим, что счетчик статистики totalCards стал равен 3
		const totalCards = await bd.db.ref( `students-statistics/${userId}/totalCards` ).once( 'value' );
		assert.equal( totalCards.val(), 3 );

		// удалим созданную карточку
		await bd.db.ref( `students/${userId}/cards/${cardId}` ).remove();

		// удалим хэш карточки
		await bd.db.ref( `students/${userId}/hash/${cardId}` ).remove();
	});

	// 15. создадим oxford карточку, проверим что карточка и ее хэш создались,
 	// все поля созданы верно, лог записался и счетчик карточки увеличился на 1
	 it( '15. Card oxford card. Expect added card with all fields, hash, logs, and statistic totalCards increase by 1.', async function() {

		// выставим счетчик статистики на 2 (на всякий случай, вдруг я его где-то собью)
		await bd.db.ref( `students-statistics/${userId}` ).update({ ['totalCards']: 2 });

		// создаем карточку
		const cardId = await test.wrap( myFunctions.postAddCard )
		({
 			userId: userId,
			type: 'oxford',
			wordId: 'wordId0',			 
			front: 'test0',
			part: 'part0',
			definition: 'definition0',
			defId: 'defId0',
			audioUrl: 'https://audio.oxforddictionaries.com/en/mp3/dote_gb_1.mp3',
			example: 'example0',
			synonym: 'synonym0',
			antonym: 'antonym0',
			synonyms: ['a'],
			antonyms: ['c'],
			back: 'тест0',
			transcription: 'dəʊt',
			domains: ['domains0'],
			hint: 'hint0'
		});

		// проверим, что карточка создана и все поля карточки соответствуют ожидаемым
		const card = await bd.getAttributesCard( userId, cardId );
		assert.equal( card.type, 'oxford' );
		assert.equal( card.wordId, 'wordId0' );
		assert.equal( card.front, 'test0' );
		assert.equal( card.part, 'part0' );
		assert.equal( card.definition, 'definition0' );
		assert.equal( card.defId, 'defId0' );
		assert.equal( card.audioUrl, 'https://audio.oxforddictionaries.com/en/mp3/dote_gb_1.mp3' );
		assert.equal( card.example, 'example0' );
		assert.equal( card.synonym, 'synonym0' );
		assert.equal( card.antonym, 'antonym0' );
		assert.equal( card.synonyms[0], 'a' );
		assert.equal( card.antonyms[0], 'c' );
		assert.equal( card.back, 'тест0' );
		assert.equal( card.transcription, 'dəʊt' );
		assert.equal( card.domains[0], 'domains0' );
		assert.equal( card.hint, 'hint0' );
		assert.equal( card.dueDate, 999999 );
		assert.equal( card.id, cardId );
		assert.equal( card.period, 0 );
		assert.equal( card.Ef, 250 );
		assert.equal( card.status, 'inactive' );
		
		// проверим, что лог создан правильно
		assert.equal( card.logs[ Object.keys( card.logs ).shift() ], 'create' );
	
		// проверим, что создан  hash карточки
		const hash = await bd.getHashCard( userId, cardId );
		assert.equal( hash.f, 'test0' );

		// проверим, что счетчик статистики totalCards стал равен 3
		const totalCards = await bd.db.ref( `students-statistics/${userId}/totalCards` ).once( 'value' );
		assert.equal( totalCards.val(), 3 );

		// удалим созданную карточку
		await bd.db.ref( `students/${userId}/cards/${cardId}` ).remove();

		// удалим хэш карточки
		await bd.db.ref( `students/${userId}/hash/${cardId}` ).remove();
	});

});

test.cleanup();