// 
const assert = require( "assert" );
const acc = require( "../.config/acc.js" );
const test = require( "firebase-functions-test" ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require('firebase-admin');
// класс для бд и инициализация бд
const base = require("../class/bd-firebase-realtime.js");
const bd = new base( admin );

global.test = true;

/*
 * Что тестируем:
 *              1. Пришел запрос на редактирование атрибутов студента без указания id пользователя, вернем Validation Error
 *				2. Пришел запрос на редактирование атрибутов студента без указания translationEnabled пользователя, вернем Validation Error
 *              3. Пришел запрос на редактирование атрибутов несуществующего студента, вернем Server Error
 *              4. Пришел запрос на редактирование атрибутов, штатно их записали. 
 *                  У студента есть атрибут translationEnabled = false, targetLanguage отсутствует
 *                  Запишем targetLanguage = 'ru', а translationEnabled = true


 * Проверяем работу функции postUserAttributes
 * Проверяем три случая:
 * - случай, когда функция выдаст Validation Error
 * - случай, когда пользователь не существует
 * - случай, когда userId = "test-postUserAttributes", targetLanguage = "RU", translationEnabled = true
 */

describe( 'Test post attributes user', function () {

	// наш студент для теста 
	const userId = 'test-postUserAttributes';

    // 1. Пришел запрос на редактирование атрибутов студента без указания translationEnabled пользователя, вернем Validation Error
    it( '1. Set attributes for user without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postUserAttributes )( {
                targetLanguage: 'ru',
                translationEnabled: true
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 2. Пришел запрос на редактирование карточки без указания id пользователя, вернем Validation Error
    it( '2. Set attributes for user without translationEnabled. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postUserAttributes )( {
                userId: userId,
                targetLanguage: 'ru'
            });
       }, 
       {
            message: 'Validation Error'
       });
    });  

    // 3. Пришел запрос на редактирование атрибутов несуществующего студента, вернем Server Error
    it( '3. Set attributes for unexisted user. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postUserAttributes )( {
                userId: 'test-unexistedUser',
                targetLanguage: 'ru',
                translationEnabled: true
            });
       }, 
       {
            message: 'Server Error'
       });
    });  
   
    // 4. Пришел запрос на редактирование атрибутов, штатно их записали. 
    // У студента есть атрибут translationEnabled = false, targetLanguage отсутствует
    // Запишем targetLanguage = 'ru', а translationEnabled = true
	it( '4. Set attributes for user with userId = "test-postUserAttributes". Expect object with attributes.', async function() {

		const result = await test.wrap( myFunctions.postUserAttributes )
		({
            userId: userId,
            targetLanguage: "ru",
            translationEnabled: true
        });

        // проверим, что функция вернула атрибуты пользователя
        assert.equal( result.userId, 'test-postUserAttributes' );
        assert.equal( result.targetLanguage, 'ru' );
        assert.equal( result.translationEnabled, true );

        // проверим что атрибуты были targetLanguage и translationEnabled записаны верно 
        const attr = await bd.getAttributesUser( 'test-postUserAttributes' );
        assert.equal( attr.targetLanguage, 'ru' );
        assert.equal( attr.translationEnabled, true );
        
        // вернем атрибуты targetLanguage и translationEnabled в исходное состояние
        await bd.setAttributeUser( 'test-postUserAttributes', 'targetLanguage', null );
        await bd.setAttributeUser( 'test-postUserAttributes', 'translationEnabled', false );
    });
    
});

test.cleanup();