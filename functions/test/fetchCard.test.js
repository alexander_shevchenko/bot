// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

global.test = true;

/*
 * Что тестируем:
 * 				1. Пришел запрос на карточку без указания id пользователя, вернем Validation Error
 * 				2. Пришел запрос на карточку без указания id карточки, вернем Validation Error
 *              3. Пришел запрос на карточку, ее нет, вернем Server Error
 * 				4. Пришел запрос на карточку c id = '-1' верне ее
 */
describe( 'Test fetch card.', function () {

	// наш студент для теста 
	const userId = 'test-fetchCard';

    // 1. Пришел запрос на карточку без указания id пользователя, вернем Validation Error
    it( '1. Get card without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchCard )( {
                cardId: '-1'
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 2. Пришел запрос на карточку без указания id карточки, вернем Validation Error
    it( '2. Get card without cardId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchCard )( {
                userId: userId
            });
       }, 
       {
           message: 'Validation Error'
       });
    });
    
    // 3. Пришел запрос на карточку, ее нет, вернем Server Error
    it( '3. Get card with id="-2" for student with id="test-fetchCard". Expect "Server Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchCard )( {
                userId: userId,
                cardId: '-2'
            });
       }, 
       {
           message: 'Server Error'
       });
	});

    // 4. Пришел запрос на карточку c id = '-1' верне ее
	it( '4. Get card with id="-1" for student with id="test-fetchCard". Expect return card with front="test".', async function() {

		const result = await test.wrap( myFunctions.fetchCard )
		({
            userId: userId,
            cardId: '-1'
		});        
        assert.equal( result.front, 'test' );
	});
});

test.cleanup();