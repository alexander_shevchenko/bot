// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require( 'firebase-admin') ;
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

global.test = true;

/*
 * Что тестируем:
 * 				1. Пришел запрос на хэши без указания id пользователя, вернем Validation Error
 *              2. Пришел запрос на хэши, их нет, вернем Empty
 * 				3. Пришел запрос на хэши, вернем два хэша { '-2': { f: 'test2' }, '-1': { f: 'test1' } }
 */
describe( 'Test fetch all cards.', function () {

	// наш студент для теста 
	const userId = 'test-fetchAllCards';

    // 1. Пришел запрос на хэши без указания id пользователя, вернем Validation Error
    it( '1. Get hashes without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchAllCards )( { 
            });
       }, 
       {
           message: "Validation Error"
       });
	});

    // 2. Пришел запрос на хэши, их нет, вернем Empty
    it( '2. Get hashes for user without cards. Expect "Empty".', async function() {

        // удалим хэши из базы 
        await bd.removeHash( userId, '-1' );
        await bd.removeHash( userId, '-2' );

		// получаем в ответ на запрос хэшей Empty
		const result = await test.wrap( myFunctions.fetchAllCards )
		({
            userId: userId
		});
		assert.equal( result, 'Empty' );

       // вернем хэши в базу
       await bd.createCardHash( userId, '-1', 'test1' );
       await bd.createCardHash( userId, '-2', 'test2' );
	});

    // 3. Пришел запрос на хэши, вернем два хэша { '-2': { f: 'test2' }, '-1': { f: 'test1' } }
	it( '3. Get hashes for user. Expect return two hashes.', async function() {

		const result = await test.wrap( myFunctions.fetchAllCards )
		({
            userId: userId
		});        
        assert.equal( result['-1'].f, 'test1' );
        assert.equal( result['-2'].f, 'test2' );
	});
});

test.cleanup();