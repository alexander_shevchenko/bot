// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');
const admin = require('firebase-admin');
// класс для бд и инициализация бд
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

global.test = true;

/*
 * Что тестируем:
 *				1. Пришел запрос на редактирование карточки без указания id пользователя, вернем Validation Error
 *				2. Пришел запрос на редактирование карточки без указания id карточки, вернем Validation Error
 *				3. Пришел запрос на редактирование карточки без указания типа карточки, вернем Validation Error
 * 				4. Попытка редактировать карточку у несуществующего пользователя, вернет Validation Error 
 * 				5. Попытка редактировать несуществующую карточку, вернет Validation Error 
 * 				6. data.type не 'custom' и не 'oxford', вернет Validation Error 
 * 				7. data.type='custom' и data.front неопределено или пустое, вернет Validation Error 
 * 				8. data.type='custom' и data.back неопределено или пустое и data.definition неопределено или пустое, вернет Validation Error 
 * 				9. data.type='oxford' и data.defId неопределено или пустое, вернет Validation Error 
 * 				10. data.type='custom', data.cardId=1, data.front='test2' и data.back='тест2', вернет Duplicate Found,
 * 					поскольку у студента есть карточка c id='-2' с точно такими же полями
 * 				11. data.type='custom', data.cardId=1, data.front='test2' и data.definition='definition2', вернет Duplicate Found
 *					поскольку у студента есть карточка c id='-2' с точно такими же полями
 *				12. data.type='oxford' и data.defId='defId', вернет Duplicate Found
 *					поскольку у студента есть карточка c id='-4' с точно такими defId
 *				13. отредактируем custom карточку, проверим что поля карточки и ее хэш изменились, лог записался
 *				14. отредактируем oxford карточку, проверим что поля карточки и ее хэш изменились, лог записался
 */

describe( 'Test update card.', function () {

	// наш студент для теста 
	const userId = 'test-postUpdateCard';

    // 1. Пришел запрос на редактирование карточки без указания id пользователя, вернем Validation Error
    it( '1. Update сard without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postUpdateCard )( {
                cardId: '-1',
 				type: 'custom'
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 2. Пришел запрос на редактирование карточки без указания id карточки, вернем Validation Error
    it( '2. Update сard without cardId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postUpdateCard )( {
                userId: userId,
 				type: 'custom'
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 3. Пришел запрос на редактирование карточки без указания типа карточки, вернем Validation Error
    it( '3. Update сard without type. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postUpdateCard )( {
                userId: userId,
				cardId: '-1'
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

	// 4. Попытка редактировать карточку у несуществующего пользователя, вернет Validation Error 
	it( '4. Update сard of nonexistent user. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

 			await test.wrap( myFunctions.postUpdateCard )( {
				userId: 'test-unexistedUser',
				cardId: '-1',
 				type: 'custom',
				front: 'test',
				back: 'тест',
				definition: 'definition',
				example: 'example'
			  });
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 5. Попытка редактировать несуществующую карточку, вернет Validation Error 
	it( '5. Update nonexistent сard. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

 			await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-0',
				type: 'custom',
				front: 'test',
				back: 'тест',
				definition: 'definition',
				example: 'example'
 			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 6. data.type не 'custom' и не 'oxford', вернет Validation Error 
	it( '6. Update nonexistent сard. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

				await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-1',
				type: 'notCustomAndNotOxford',
				front: 'test',
				back: 'тест',
				definition: 'definition',
				example: 'example'
			});
		}, 
		{
			message: 'Validation Error'
		});
	});
	
	// 7. data.type='custom' и data.front неопределено или пустое, вернет Validation Error 
	it( '7. Update nonexistent сard. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

				await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-1',
				type: 'custom',
				back: 'тест',
				definition: 'definition',
				example: 'example'
			});
		}, 
		{
			message: 'Validation Error'
		});
	});
	
	// 8. data.type='custom' и data.back неопределено или пустое и data.definition неопределено или пустое, вернет Validation Error 
	it( '8. Update nonexistent сard. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

				await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-1',
				type: 'custom',
				front: 'test',
				example: 'example'
			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 9. data.type='oxford' и data.defId неопределено или пустое, вернет Validation Error 
	it( '9. Update nonexistent сard. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

				await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-3',
				type: 'oxford',
				example: 'example',
				synonym: 'synonym',
				antonym: 'antonym',
				back: 'тест',
				hint: 'hint'
			});
		}, 
		{
			message: 'Validation Error'
		});
	});

	// 10. data.type='custom', data.cardId="-1", data.front='test2' и data.back='тест2', вернет Duplicate Found,
 	//	поскольку у студента есть карточка c id="-2" с точно такими же полями
	it( '10. Update сard {data.type="custom", data.cardId="-1", data.front="test2" и data.back="тест2"}. Expect "Duplicate Found".', async function() {

		await assert.rejects( async function() {

				await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-1',
				type: 'custom',
				front: 'test2',
				back: 'тест2',
				definition: 'definition',
				example: 'example'
			});
		}, 
		{
			message: 'Duplicate Found'
		});
	});

	// 11. data.type='custom', data.cardId="-1", data.front='test2' и data.definition='definition2', вернет Duplicate Found
 	//	поскольку у студента есть карточка c id="-2" с точно такими же полями
	it( '11. Update сard {data.type="custom", data.cardId="-1", data.front="test2" и data.definition="definition2"}. Expect "Duplicate Found".', async function() {

		await assert.rejects( async function() {

				await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-1',
				type: 'custom',
				front: 'test2',
				back: 'тест',
				definition: 'definition2',
				example: 'example'
			});
		}, 
		{
			message: 'Duplicate Found'
		});
	});

	// 12. data.type='oxford', data.cardId="-3" и data.defId='defId4', вернет Duplicate Found
	//	поскольку у студента есть карточка c id="-4" с точно такими defId
	it( '12. Update сard {data.type="oxford", data.cardId="-3", data.defId=defId4 }. Expect "Duplicate Found".', async function() {

		await assert.rejects( async function() {

				await test.wrap( myFunctions.postUpdateCard )( {
				userId: userId,
				cardId: '-3',
				type: 'oxford',
				defId: 'defId4',
				example: 'example',
				synonym: 'synonym',
				antonym: 'antonym',
				back: 'тест',
				hint: 'hint'
			});
		}, 
		{
			message: 'Duplicate Found'
		});
	});

	// 13. отредактируем custom карточку, проверим что поля карточки и ее хэш изменились, лог записался
	it( '13. Update custom сard with cardId="-1" Expect updated card with all fields, hash, logs.', async function() {

		// вернет данные отредактированной карточки
		const cardNew = await test.wrap( myFunctions.postUpdateCard )( {
			userId: userId,
			cardId: '-1',
			type: 'custom',
			front: 'test',
			back: 'тест',
			definition: 'definition',
			example: 'example'
		} );

		// проверим, что данное поле действительно изменилось
		assert.equal( cardNew.front, 'test' );
		assert.equal( cardNew.back, 'тест' );
		assert.equal( cardNew.definition, 'definition' );
		assert.equal( cardNew.example, 'example' );
		
		// проверим, что записался лог
		const keys = Object.keys( cardNew.logs );	
		assert.equal( cardNew.logs[keys[0]], "update front:'test'" );
		assert.equal( cardNew.logs[keys[1]], "update back:'тест'" );
		assert.equal( cardNew.logs[keys[2]], "update example:'example'" );
		assert.equal( cardNew.logs[keys[3]], "update definition:'definition'" );
		
		// проверим, что изменился хэш
		const hash = await bd.getHashCard( userId, cardNew.id );
		assert.equal( hash.f, 'test' );

		// вернем поля карточки обратно
		await bd.setAttributeCard( userId, '-1', 'front', 'test1', 'back', 'тест1',
									'definition', 'definition1', 'example', 'example1' );

		// вернем хэш обратно
		await bd.setAttributeHash( userId, '-1', 'f', 'test1' );

		// удалим лог, чтобы не плодился
		await bd.db.ref( `students/${userId}/cards/-1/logs` ).remove();

	});

	// 14. отредактируем oxford карточку, проверим что поля карточки и ее хэш изменились, лог записался
	it( '14. Update oxford сard with cardId="-3". Expect updated card with all fields, hash, logs.', async function() {

		// вернет данные отредактированной карточки
		const cardNew = await test.wrap( myFunctions.postUpdateCard )( {
			userId: userId,
			cardId: '-3',
			type: 'oxford',
			defId: 'defId1',
			example: 'example',
			synonym: 'synonym',
			antonym: 'antonym',
			back: 'тест',
			hint: 'hint'
		} );

		// проверим, что данное поле действительно изменилось
		assert.equal( cardNew.example, 'example' );
		assert.equal( cardNew.synonym, 'synonym' );
		assert.equal( cardNew.antonym, 'antonym' );
		assert.equal( cardNew.back, 'тест' );
		assert.equal( cardNew.hint, 'hint' );
		
		// проверим, что записался лог
		const keys = Object.keys( cardNew.logs );	
		assert.equal( cardNew.logs[keys[0]], "update example:'example'" );
		assert.equal( cardNew.logs[keys[1]], "update synonym:'synonym'" );
		assert.equal( cardNew.logs[keys[2]], "update antonym:'antonym'" );
		assert.equal( cardNew.logs[keys[3]], "update back:'тест'" );
		assert.equal( cardNew.logs[keys[4]], "update hint:'hint'" );
		
		// вернем поля карточки обратно
		await bd.setAttributeCard( userId, '-3', 'example', 'example3', 'synonym', 'synonym3',
									'antonym', 'antonym3', 'back', 'тест3', 'hint', 'hint3' );

		// удалим лог, чтобы не плодился
		await bd.db.ref( `students/${userId}/cards/-3/logs` ).remove();
	});

});

test.cleanup();