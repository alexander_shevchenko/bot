// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require( 'firebase-admin') ;
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

const funDay = require('../class/getDay.js');

global.test = true;

/*
 * Что тестируем:
 *				1. Пришел запрос на очередь без указания id пользователя, вернем Validation Error
 * 				2. Пришел запрос на очередь, а студента в базе нет, вернем Server Error
 * 				3. Пришел запрос на первую на сегодня тренировку. У меня 6 карточек: 2 inactive,
 * 					1 new, 1 relearning, 2 review, причем однеа review с заниженной датой (110101), 
 * 					а вторая review с завышенной (390101). Получу следующий результат: 
 * 					- аттрибут todayCard станет равен 0 (количество новых карточек, тренированных сегодня)
 * 					- очередь очередь будет содержать 5 картотчек.
 * 				4. Пришел запрос на первую тренировку и очередь на сегодня пустая. Вернем Empty.
 * 				5. Пришел запрос на НЕ первую тренировку на сегодня. У меня 6 карточек: 2 inactive,
 * 					1 new, 1 relearning, 2 review, причем однеа review с заниженной датой (110101), 
 * 					а вторая review с завышенной (390101). Кроме того поле todayCard будет равно 19,
 * 					значит я уже тренировал 19 новых карточек и в очереди может быть только одна inactive.
 * 					Получу очередь их четырех карточек.
 * 				6. Пришел запрос на НЕ первую тренировку и очередь на сегодня пустая. Вернем Done.
 * 				7. Статистики не было, создалась. 
 * 					Получили 7 ключей за последнии дни + currentStreak, totalCards и bestStreak
 * 				8. Статистика, штатная работа. 
 * 					Получили из базы последние 7 записей, которые сформирую сам, пропустив сегодня.
 * 					Делаю запрос очереди и получаю обновленную статистику - 7 записей на актуальные даты + currentStreak, totalCards и bestStreak
 * 				9. Статистика, штатная работа. Страйк обнулился, потому что был пропущен день тренировки.
 */

describe( 'Test fetch cards.', function () {

	// наш студент для теста 
	const userId = 'test-fetchCards';

    // 1. Пришел запрос на карточку без указания id пользователя, вернем Validation Error
    it( '1. Fetch сards without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchCards )( {});
       }, 
       {
            message: 'Validation Error'
       });
	});

	// 2. Пришел запрос на очередь, а студента в базе нет, вернем Server Error
	it( '2. Fetch сards from nonexistent student. Expect "Server Error".', async function() {

		await assert.rejects( async function() {

			// оцениваем карточку с несуществующим userrId = _dont_know_this_user
 			await test.wrap( myFunctions.fetchCards )( {
				userId: 'i_dont_know_this_user',
				currentDay: '200101'
 			});
		}, 
		{
			message: 'Server Error'
		});
	});
	
	// 3. Пришел запрос на первую на сегодня тренировку. У меня 6 карточек: 2 inactive,
 	// 1 new, 1 relearning, 2 review, причем однеа review с заниженной датой (110101), 
 	// а вторая review с завышенной (390101). Получу следующий результат: 
 	// - аттрибут todayCard станет равен 0 (количество новых карточек, тренированных сегодня)
 	// - очередь очередь будет содержать 5 картотчек.
	it( '3. First train on today. Expect 5 cards in quee. Expect todayCard=0.', async function() {

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// получим вчерашний и сегодняшний дени в нужном формате
		const yesterday = funDay.getDay( userAttr.timezone, 1 );
		const today = funDay.getDay( userAttr.timezone );

		// установим в базе дату последней тренировки на вчерашнйи день
		await bd.setAttributeUser( userId, 'lastTraining', yesterday)

		// установим todayCard равным 3 (главное, чтобы не 0)
		await bd.setAttributeUser( userId, 'todayCard', 3)

		// получаем очередь и проверяем ее длину
		const result = await test.wrap( myFunctions.fetchCards )
		({
            userId: userId,
            currentDay: today
		});
		assert.equal( Object.keys(result.cards).length, 5 );
		
		// получим атрибуты студента и проверим, что todayCard = 0
		const attr = await bd.getAttributesUser( userId );
		assert.equal( attr.todayCard, 0 );
	});

	// 4. Пришел запрос на первую тренировку и очередь на сегодня пустая. Вернем Empty.
	// Изменю у карточек статус на review с dueDate = 390101
	// Таким образом на сегодня в очередь не попадет ни одна карточка
	it( '4. First train on today. Expect empty quee.', async function() {

		// выставим у карточек status = review и dueDate = 390101
		await bd.setAttributeCard( userId, '-MMLwqffcly6S2-RwNCm', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLwyARgTffXPMKMGvq', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLx-3mcvTgsxogE8ic', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLxDNkwFhehoZbeDB_', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLxHGZN33V_Zw7-FZJ', 'dueDate', 390101);

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// получим вчерашний и сегодняшний дни в нужном формате
		const yesterday = funDay.getDay( userAttr.timezone, 1 );
		const today = funDay.getDay( userAttr.timezone );

		// установим в базе дату последней тренировки на вчерашнйи день
		await bd.setAttributeUser( userId, 'lastTraining', yesterday)

		// получаем в ответ на запрос очереди Empty
		const result = await test.wrap( myFunctions.fetchCards )
		({
            userId: userId,
            currentDay: today
		});
		assert.equal( result, 'Empty' );

		// вернем атрибуты карточек в исходное состояние
		await bd.setAttributeCard( userId, '-MMLwqffcly6S2-RwNCm', 'status', 'inactive', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLwyARgTffXPMKMGvq', 'status', 'inactive', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLx-3mcvTgsxogE8ic', 'status', 'new', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLxDNkwFhehoZbeDB_', 'status', 'relearning', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLxHGZN33V_Zw7-FZJ', 'dueDate', 110101);
	});

	// 5. Пришел запрос на НЕ первую тренировку на сегодня. У меня 6 карточек: 2 inactive,
 	// 1 new, 1 relearning, 2 review, причем однеа review с заниженной датой (110101), 
 	// а вторая review с завышенной (390101). Кроме того поле todayCard будет равно 19,
 	// значит я уже тренировал 19 новых карточек и в очереди может быть только одна new.
 	// Получу очередь из трех карточек.
	it( '5. Not first train on today and todayCard=19. Expect 3 cards in quee.', async function() {

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// получим сегодняшний дени в нужном формате
		const today = funDay.getDay( userAttr.timezone );

		// установим в базе дату последней тренировки на сегодняшний день
		await bd.setAttributeUser( userId, 'lastTraining', today)

		// установим todayCard равным 19
		await bd.setAttributeUser( userId, 'todayCard', 19)

		// получаем очередь и проверяем ее длину
		const result = await test.wrap( myFunctions.fetchCards )
		({
            userId: userId,
            currentDay: today
		});
		assert.equal( Object.keys(result.cards).length, 3 );
	});

	// 6. Пришел запрос на НЕ первую тренировку и очередь на сегодня пустая. Вернем Done.
	// Изменю у карточек статус на review с dueDate = 390101, также выставлю lastTraining = сегодняшнему дню
	// Таким образом на сегодня в очередь не попадет ни одна карточка
	it( '6. Not first train on today. Expect result = "Done".', async function() {

		// выставим у карточек status = review и dueDate = 390101
		await bd.setAttributeCard( userId, '-MMLwqffcly6S2-RwNCm', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLwyARgTffXPMKMGvq', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLx-3mcvTgsxogE8ic', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLxDNkwFhehoZbeDB_', 'status', 'review', 'dueDate', 390101);
		await bd.setAttributeCard( userId, '-MMLxHGZN33V_Zw7-FZJ', 'dueDate', 390101);

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// получим сегодняшний день в нужном формате
		const today = funDay.getDay( userAttr.timezone );

		// установим в базе дату последней тренировки на сегодняшний день
		await bd.setAttributeUser( userId, 'lastTraining', today)

		// получаем в ответ на запрос очереди Done
		const result = await test.wrap( myFunctions.fetchCards )
		({
            userId: userId,
            currentDay: today
		});
		assert.equal( result, 'Done' );

		// вернем атрибуты карточек в исходное состояние
		await bd.setAttributeCard( userId, '-MMLwqffcly6S2-RwNCm', 'status', 'inactive', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLwyARgTffXPMKMGvq', 'status', 'inactive', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLx-3mcvTgsxogE8ic', 'status', 'new', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLxDNkwFhehoZbeDB_', 'status', 'relearning', 'dueDate', 999999);
		await bd.setAttributeCard( userId, '-MMLxHGZN33V_Zw7-FZJ', 'dueDate', 110101);
	});

	//  7. Статистики не было, создалась. Получили 7 ключей за последнии дни + currentStreak, totalCards и bestStreak
	it( '7. Get statistic. Expect statistic where all keys = 0.', async function() {
		
		// удалим статистику если она была
		await bd.db.ref( `students-statistics/${userId}` ).remove();

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// получим сегодняшний день в нужном формате
		const today = funDay.getDay( userAttr.timezone );

		// выполним запрос на получение очереди
		await test.wrap( myFunctions.fetchCards )
		({
            userId: userId,
            currentDay: today
		});

		// проверим, что создалась вся необходимая статистика
		const stat = await bd.getStatCommon( userId )
		assert.equal( stat.currentStreak, 0 );
		assert.equal( stat.totalCards, 0 );
		assert.equal( stat.bestStreak, 0 );
		for( i=0 ; i<7 ; i++ ) {
			let day = funDay.getDay( userAttr.timezone, i );
			assert.equal( stat[day].trainedNew, 0 );
			assert.equal( stat[day].trainedTotal, 0 );
		}
	});

	// 8. Статистика, штатная работа. 
	// Получили из базы последние 7 записей, которые сформирую сам, пропустив сегодня.
	// Делаю запрос очереди и получаю обновленную статистику - 7 записей на актуальные даты + currentStreak, totalCards и bestStreak
	it( '8. Get real statistic. Expect statistic with real key.', async function() {
		
		// удалим статистику если она была
		await bd.db.ref( `students-statistics/${userId}` ).remove();

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// сформирую семь дней статистики не включая сегодняшний день
		const stat2base = {};
		stat2base.currentStreak = 7;
		stat2base.totalCards = 6;
		stat2base.bestStreak = 51;
		for( i=1 ; i<8 ; i++ ) {
			let day = funDay.getDay( userAttr.timezone, i );
			stat2base[day] = {
				trainedNew: 3,
				trainedTotal: 7
			}
		}
		// запишем ее в базу
		await bd.setStatCommon( userId, stat2base );

		// получим сегодняшний и вчерашний дни в нужном формате
		const today = funDay.getDay( userAttr.timezone );
		const yesterday = funDay.getDay( userAttr.timezone, 1 );

		// установим в базе дату последней тренировки на вчерашний день
		await bd.setAttributeUser( userId, 'lastTraining', yesterday)

		// выполним запрос на получение очереди
		await test.wrap( myFunctions.fetchCards )
		({
            userId: userId,
            currentDay: today
		});

		// проверим, что создалась вся необходимая статистика
		const stat = await bd.getStatCommon( userId )
		assert.equal( stat.currentStreak, 7 );
		assert.equal( stat.totalCards, 6 );
		assert.equal( stat.bestStreak, 51 );

		// количество ключей должно быть равно 10 (7 дней статистики + currentStreak, totalCards и bestStreak)
		assert.equal( Object.keys(stat).length, 10 );

		// за сегодня должны стоять нули
		assert.equal( stat[today].trainedNew, 0 );
		assert.equal( stat[today].trainedTotal, 0 );
		// плюс за предыдущие 6 дней статистика не должна измениться trainedNew=3 trainedTotal=7
		for( i=1 ; i<7 ; i++ ) {
			let day = funDay.getDay( userAttr.timezone, i );
			assert.equal( stat[day].trainedNew, 3 );
			assert.equal( stat[day].trainedTotal, 7 );
		}
	});
   
	// 9. Статистика, штатная работа. Страйк обнулился, потому что был пропущен день тренировки.
	it( '9. Get real statistic. Expect currentStreak = 0.', async function() {
		
		// удалим статистику если она была
		await bd.db.ref( `students-statistics/${userId}` ).remove();

		// получим атрибуты студента, для определения таймзоны
		const userAttr = await bd.getAttributesUser( userId );

		// сформирую статистику 
		const stat2base = {};
		stat2base.currentStreak = 7;
		stat2base.totalCards = 6;
		stat2base.bestStreak = 51;
		// запишем ее в базу
		await bd.setStatCommon( userId, stat2base );

		// получим сегодняшний и позавчерашний дни в нужном формате
		const today = funDay.getDay( userAttr.timezone );
		const beforeYesterday = funDay.getDay( userAttr.timezone, 2 );

		// установим в базе дату последней тренировки на вчерашний день
		await bd.setAttributeUser( userId, 'lastTraining', beforeYesterday)

		// выполним запрос на получение очереди
		await test.wrap( myFunctions.fetchCards )
		({
            userId: userId,
            currentDay: today
		});

		// проверим, что создалась вся необходимая статистика
		const stat = await bd.getStatCommon( userId )
		assert.equal( stat.currentStreak, 0 );
		assert.equal( stat.totalCards, 6 );
		assert.equal( stat.bestStreak, 51 );

		// количество ключей должно быть равно 10 (7 дней статистики + currentStreak, totalCards и bestStreak)
		assert.equal( Object.keys(stat).length, 10 );

		// за все дни должны стоять нули
		for( i=0 ; i<7 ; i++ ) {
			let day = funDay.getDay( userAttr.timezone, i );
			assert.equal( stat[day].trainedNew, 0 );
			assert.equal( stat[day].trainedTotal, 0 );
		}
	});

});

test.cleanup();