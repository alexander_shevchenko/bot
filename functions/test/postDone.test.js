const assert = require( 'assert' );

const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require( 'firebase-admin') ;
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

global.test = true;

/*
 * Что тестируем:
 *				1. Пришел запрос на окончание тренировки без указания id пользователя, вернем Validation Error
 *				2. Пришел запрос на окончание тренировки без указания числа тренированных карточек, вернем Validation Error
 * 				3. Отправляем серверу сообщение об окончании тренировки, в ответ получаем флаг, что это была первая тренировка
 * 				4. Отправляем серверу сообщение об окончании тренировки, в ответ получаем флаг, что это была НЕ первая тренировка
 */ 

describe( 'Test post done.', function () {

	// наш студент для теста
	const userId = 'test-postDone';

    // 1. Пришел запрос на окончание тренировки без указания id пользователя, вернем Validation Error
    it( '1. Post done without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postDone )( {
                length: 10
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 2. Пришел запрос на удаление карточки без указания id карточки, вернем Validation Error
    it( '2. Post done without length. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postDone )( {
                userId: userId
            });
       }, 
       {
           message: 'Validation Error'
       });
    });

	// 3. Отправляем серверу сообщение об окончании тренировки, в ответ получаем флаг, что это была первая тренировка
	it( '3. Send post done. Expect return object with field isFirstDoneEver=true .', async function() {

		// поставим флаг, что у студента еще не было первой тренировки
		await bd.setAttributeUser( userId, 'firstTrain', false );

		const result = await test.wrap( myFunctions.postDone )
		({
			userId: userId,
			length: 1
		});

		// проверим, что вернули флаг, что прошла первая тренировка 
		assert.equal( result.isFirstDoneEver, true );
	});

	// 4. Отправляем серверу сообщение об окончании тренировки, в ответ получаем флаг, что это была НЕ первая тренировка
	it( '4. Send post done. Expect return object with field isFirstDoneEver=false .', async function() {

		// поставим флаг, что у студента еще не было первой тренировки
		await bd.setAttributeUser( userId, 'firstTrain', true );

		const result = await test.wrap( myFunctions.postDone )
		({
			userId: userId,
			length: 1
		});

		// проверим, что вернули флаг, что прошла НЕ первая тренировка 
		assert.equal( result.isFirstDoneEver, false );
	});

});

test.cleanup();