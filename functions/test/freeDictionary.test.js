// 
const assert = require( "assert" );
const acc = require( "../.config/acc.js" );
const test = require( "firebase-functions-test" ) ( {
    databaseURL: acc.databaseURL,
    storageBucket: acc.storageBucket,
    projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

global.test = true;

/*
 * Запрос в Free Dictionary для получения определений слова
 */
describe.only( "Query to Free Dictionary.", function () {

    // Нормальный запрос слова 'table'. Ожидаю нормальную реакцию с кодом состояния 200
    it( "Get definitions for word 'table'. Expect 'status=200'.", async function() {

        const result = await test.wrap( myFunctions.fetchFreeDictionary )
        ({
            word: 'table'
        });
        assert.equal( result.status, 200 );
        // console.log(result);

    });

	// Запрос с ошибочным словом. Ожидаю 'Server Error'
	it( "Get definitions for word 'tab2'. Expect 'Server Error'.", async function() {

        await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchFreeDictionary )( {
                word: 'tab2'
            });
        }, 
        {
            message: 'Server Error'
        });
    });
    
	// Запрос со словом с неверными символами. Ожидаю 'Server Error'
	it( "Get definitions for word 'тейбл'. Expect 'Server Error'.", async function() {

        await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchFreeDictionary )( {
                word: 'тейбл'
            });
        }, 
        {
            message: 'Server Error'
        });
    });
});

test.cleanup();
