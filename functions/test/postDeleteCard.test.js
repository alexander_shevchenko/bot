// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require( 'firebase-admin') ;
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

global.test = true;


/*
 * Что тестируем:
 * 				1. Пришел запрос на удаление карточки без указания id пользователя, вернем Validation Error
 * 				2. Пришел запрос на удаление карточки без указания id карточки, вернем Validation Error
 *              3. Пришел запрос на удаление карточки c id='-1', проверим, что она удалилась, 
 *                  удалился хэш и изменился счетчик карточек
 */
describe( 'Test delete card', function () {

	// наш студент для теста 
	const userId = 'test-postDeleteCard';

    // 1. Пришел запрос на удаление карточки без указания id пользователя, вернем Validation Error
    it( '1. Delete card without userId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postDeleteCard )( {
                cardId: '-1'
            });
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 2. Пришел запрос на удаление карточки без указания id карточки, вернем Validation Error
    it( '2. Deletet card without cardId. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.postDeleteCard )( {
                userId: userId
            });
       }, 
       {
           message: 'Validation Error'
       });
    });
    
    // 3. Пришел запрос на удаление карточки c id='-1', проверим, что она удалилась, удалился хэш и изменился счетчик карточек
	it( '3. Deletet card with id="-1" for student with id="test-postDeleteCard". Expect delete card, hash and decresse count of cards.', async function() {

        // отправим запрос на удаление карточки
		const result = await test.wrap( myFunctions.postDeleteCard )
		({
            userId: userId,
            cardId: '-1'
        });
        
        // в случае успеха вернется id удаленной карточки
        assert.equal( result, '-1' );

        // проверим, что карточки нет
        const card = await bd.getAttributesCard( userId, '-1' );
        assert.equal( card, null );

        // проверим, что нет хэша
        const hash = await bd.getHashCard( userId, '-1' );
        assert.equal( hash, null );

        // проверим, что счетчик карточек стал равен 1
        const stat = await bd.getStatCommon( userId );
        assert.equal( stat.totalCards, '1' );

        // вернем удаленную карточку, хэш и счетчик
        await bd.createCardHash( userId, '-1', 'test1' );
        await bd.setAttributeCard( userId, '-1', 
                                    'type', 'custom', 
                                    'status', 'review',
                                    'dueDate', 800101, 
                                    'Ef', 250,
                                    'period', 0,
                                    'definition', 'definition1',
                                    'front', 'test1',
                                    'back', 'тест1',
                                    'example', 'example1');
        await bd.setTotalCards( userId, 1 );
	});
});

test.cleanup();