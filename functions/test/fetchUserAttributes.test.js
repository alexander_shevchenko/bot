// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');

const admin = require( 'firebase-admin') ;
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

global.test = true;

/*
 * Что тестируем:
 * 				1. Пришел запрос на студента без указания timezone пользователя, вернем Validation Error
 *              2. Пришел запрос на студента без tgid и psid, вернем Validation Error
 *              3. Пришел запрос на студента с psid, а его нет в базе - вернем Server Error
 * 				4. Пришел запрос на студента с tgid, а его нет в базе - вернем Server Error
 * 				5. Пришел запрос на студента с psid, он есть в базе - вернули id и его языковые настройки 
 * 				6. Пришел запрос на студента с tgid, он есть в базе - вернули id и его языковые настройки 
 * 				7. Пришел запрос на студента, а его таймзона изменилась - перезапишем таймзону и будильник
 */

describe( 'Test fetch attributes user.', function () {

    // наш студент для теста 
    const userId = 'test-fetchUserAttributes';

    // 1. Пришел запрос на студента без указания timezone пользователя, вернем Validation Error
    it( '1. Get attributes students without timezone. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchUserAttributes )( {});
       }, 
       {
            message: 'Validation Error'
       });
    });
    
    // 2. Пришел запрос на студента без tgid и psid, вернем Validation Error
    it( '2. Get attributes students without psid and tgid. Expect "Validation Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchUserAttributes )( {});
       }, 
       {
            message: 'Validation Error'
       });
	});

    // 3. Пришел запрос на студента с psid, а его нет в базе - вернем Students not found for psid ...
	it( '3. Get attributes for user with psid = "i_dont_know_this_user". Expect "Server Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchUserAttributes )( {
                psid: 'i_dont_know_this_user',
                timezone: 0
            });
       }, 
       {
           message: 'Server Error'
       });

	});

    // 4. Пришел запрос на студента с tgid, а его нет в базе - вернем Students not found for tgid ...
	it( '4. Get attributes for user with tgid = "i_dont_know_this_user". Expect "Server Error".', async function() {

		await assert.rejects( async function() {

            await test.wrap( myFunctions.fetchUserAttributes )( {
                tgid: 'i_dont_know_this_user',
                timezone: 0
            });
       }, 
       {
           message: 'Server Error'
       });

	});

    // 5. Пришел запрос на студента с psid, он есть в базе - вернули id и его языковые настройки 
    it( '5. Get attributes for user with psid = 1234567890. Expect id = userId, targetLanguage = "ru", translationEnabled = true.', async function() {

		const result = await test.wrap( myFunctions.fetchUserAttributes )
		({
            psid: '1234567809',
            timezone: 0
		});
		
        assert.equal( result.userId, userId );
        assert.equal( result.targetLanguage, 'ru' );
        assert.equal( result.translationEnabled, true );
    });

    // 6. Пришел запрос на студента с tgid, он есть в базе - вернули id и его языковые настройки 
    it( '6. Get attributes for user with psid = 1234567890. Expect id = userId, targetLanguage = "ru", translationEnabled = true.', async function() {

		const result = await test.wrap( myFunctions.fetchUserAttributes )
		({
            tgid: 1234567890,
            timezone: 0
		});
		
        assert.equal( result.userId, userId );
        assert.equal( result.targetLanguage, 'ru' );
        assert.equal( result.translationEnabled, true );
    });

    // 7. Пришел запрос на студента, а его таймзона изменилась - перезапишем таймзону и будильник
    it( '7. Set timezone for user with psid = "1234567809". Expect timezone = -1.', async function() {

		const result = await test.wrap( myFunctions.fetchUserAttributes )
		({
            psid: '1234567809',
            timezone: -1
		});
        
        // получим атрибуты пользователя
        const check = await bd.getAttributesUser( result.userId );

        // проверми что таймзона выставилась в -1
        assert.equal( check.timezone, -1 );

        // вернем таймзону обратно на 0
        await bd.setAttributeUser( result.userId, 'timezone', 0)
    });

});

test.cleanup();