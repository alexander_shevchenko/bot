// 
const assert = require( "assert" );
const acc = require( "../.config/acc.js" );
const test = require( "firebase-functions-test" ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);
const myFunctions = require('../index.js');
const functions = require("firebase-functions");

/*
 * Запрос в thesaurus (синонимы и антонимы)
 */
describe( "Query to Oxford Thesaurus.", function () {

	// Нормальный запрос слова 'table'. Ожидаю нормальную реакцию с кодом состояния 200
	it( "Get in thesaurus for word 'table'. Expect 'statusCode=200'.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordThesaurus )
		({
			word: 'table',
			targetLanguage: 'ru',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 200 );

	});

	// Запрос с ошибочным словом. Ожидаю, что оксфорд ответит, что слово не найдено и вернет код 404
	it( "Get in thesaurus for word 'tab2'. Expect 'statusCode=404'.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordThesaurus )
		({
			word: 'tab2',
			targetLanguage: 'ru',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 404 );
	});

	// Запрос со словом с неверными символами. Ожидаю, что оксфорд ответит, что слово не найдено и вернет код 404
	it( "Get in thesaurus for word 'тейбл'. Expect 'statusCode=601'.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordThesaurus )
		({
			word: 'тейбл',
			targetLanguage: 'ru',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 601 );
	});

});

/*
 * Запрос в entries (search) + translations
 */
describe( "Query to Oxford Entries (Search) + Translations.", function () {

	// Нормальны запрос слова 'table'. 
	// Ожидаю нормальную реакцию с кодом состояния 200
	// Ожидаю наличие ключа translations != null
	it( "Get normal query in entries (search) + translations for word 'table'. Expect 'statusCode=200' and key 'translations'.", async function() {
		
		const result = await test.wrap( myFunctions.fetchOxfordAll )
		({
			word: 'table',
			targetLanguage: 'ru',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 200 );
		assert.notEqual( result.translations, null );
	});

	// Запрос с непереводимым языком перевода (targetLanguage = other). Ожидаю, что в ответе ключ translations будет равен null. 
	it( "Get in entries (search) + translations for word 'table' with targetLanguage='other'. Expect data.tranlstions=null.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordAll )
		({
			word: 'table',
			targetLanguage: 'other',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 200 );
		assert.equal( result.translations, null );
	});

	// Запрос без необходимости перевода (translationEnabled = false). Ожидаю, что в ответе ключ translations будет равен null. 
	it( "Get in entries (search) + translations for word 'table' with translationEnabled=false. Expect data.tranlstions=null.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordAll )
		({
			word: 'table',
			targetLanguage: null,
			translationEnabled: false,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 200 );
		assert.equal( result.translations, null );
	});
	
	// Запрос с ошибочным словом. Ожидаю, что оксфорд ответит, что слово не найдено и вернет код 404
	it( "Get in entries (search) + translations for word 'asssdfff'. Expect 'statusCode=404'.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordAll )
		({
			word: 'asssdfff',
			targetLanguage: 'ru',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 404 );
	});

	// Запрос с ошибочным словом из ????? символов. Ожидаю, что оксфорд ответит, что слово не найдено и вернет код 500
	it( "Get in entries (search) + translations for word 'trè'. Expect 'statusCode=500'.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordAll )
		({
			word: 'très',
			targetLanguage: 'ru',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 500 );
	});


	// Запрос со словом с неверными символами. Ожидаю, что оксфорд ответит, что слово не найдено и вернет код 404
	it( "Get in entries (search) + translations for word 'тейбл'. Expect 'statusCode=601'.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordAll )
		({
			word: 'тейбл',
			targetLanguage: 'ru',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 601 );
	});

	// Запрос с не переводимым языком перевода (targetLanguage = other). Ожидаю, что в ответе ключ translations будет равен null. 
	it( "Get in entries (search) + translations for word 'table' with targetLanguage='other' . Expect data.tranlstions=null.", async function() {

		const result = await test.wrap( myFunctions.fetchOxfordAll )
		({
			word: 'table',
			targetLanguage: 'other',
			translationEnabled: true,
			applicationID: functions.config().oxford.application_id,
			applicationKeys: functions.config().oxford.application_keys
		});
		assert.equal( result.statusCode, 200 );
		assert.equal( result.translations, null );
	});

});

test.cleanup();