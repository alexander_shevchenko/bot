// 
const assert = require( 'assert' );
const acc = require( '../.config/acc.js' );
const test = require( 'firebase-functions-test' ) ( {
	databaseURL: acc.databaseURL,
	storageBucket: acc.storageBucket,
	projectId: acc.projectId,
}, acc.json);

const myFunctions = require('../index.js');

const admin = require('firebase-admin');
// класс для бд и инициализация бд
const base = require('../class/bd-firebase-realtime.js');
const bd = new base( admin );

/*
 * Что тестируем:
 * 				1. Проверим, что время следующей тренировки перенеслось на 'завтра' на 11 часов
 * 				
 * 					В базе есть 1 inactive, 1 new, 1 relerning и 2 review карточки. 
 * 					У одной review карточки dueDate = 100101 (она всегда попадет в очередь.)
 * 					У второй review карточки dueDate = 800101 (она никогда не попадет в очередь.)
 * 
 * 				Что еще можно проверить не очень понятно, поскольку далее идут только диалоги
 * 				Либо замкнуть диалоги на себя и смотреть на них?
 */ 

const config = require('../class/config.js');

describe.skip( 'Test send reminder.', function () {

	// наш студент для теста 
	const userId = 'test-sendReminder';

	// 1. Проверим, что время следующей тренировки перенеслось на 'завтра' на 11 часов
	it( 'Run schedule. Expect seting new alarm on tomorrow on 11:00.', async function() {

		// Чтобы сработал будильник выставим запредельно ранний alarmClock
		await bd.setAttributeUser( userId, 'alarmClock', 1 );

		// Запустим ежедневное напоминание о тренировке
		await test.wrap( myFunctions.scheduledTest )();

		// получим аттрибуты студенты (узнаем таймзону и alarmClock)
		const attr = await bd.getAttributesUser( userId );

		// рассчитать новый alarmClock
		let newAlarmClock = new Date();

		// ЕСЛИ сейчас время у клиента больше чем hourStart, то подправим день
		if( newAlarmClock.getUTCHours() + attr.timezone >= config.hourStart ) {
			newAlarmClock.setUTCDate( newAlarmClock.getDate() + 1 );
		}
		
		// выставим время на сревере с учетом смещения timezone когда у студента будет hourAlarm 
		newAlarmClock.setUTCHours( config.hourAlarm - attr.timezone, 0, 0, 0 );

		// Проверим, что у пользователя новый будильник
		assert.equal( attr.alarmClock, newAlarmClock*1 );
		
		// Чтобы не сработал будильник при нормальном оповещении пользователей, выставим запредельный alarmClock
		await bd.setAttributeUser( userId, 'alarmClock', 9999999999999 );

	});

});

test.cleanup();