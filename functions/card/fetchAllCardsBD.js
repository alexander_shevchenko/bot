/*
 * Получить хэш-записи карточек пользователя
 *
 * Входные параметры:
 * 		data - объект
 *		.userId - id клиента, например '1985249568178314'
 *
 * Выходные параметры:
 * 		объект содержащий хэш-записи карточкк, например
 *		{
 *			'-1': { f: 'test1' },
 *			'-4': { f: 'test4', p: 'verb' },
 *			...
 *		}
 * 		либо
 *		'Empty' - если у студента нет ни одной карточки
 * 		исключение 
 * 			'Validation Error' - неполные входные данные
 * 			'Server Error' - если что-то пошло не так
 */ 

// функция проверки входных данных
const validation = require('../class/validation.js');

fetchAllCardsBD = async function( data, bd ) {

	try {

		// проверка на Validation Error		
		if ( validation.isData( data, 'userId' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// получим хэш-записи студента
		const hashes = await bd.getUserAllHashCards( data.userId );

		// карточек у студента нет
		if( hashes === null ) {
			return 'Empty';
		}

		// Отправим хэш-записи клиенту
		return hashes;
	}
	catch( error ) {

		if ( error === 'Validation Error' ) 
		throw 'Validation Error';

		if( global.test === undefined ) console.log( `Server Error in fetchAllCardsBD. ${error}` );
		throw 'Server Error';
	}
}

module.exports = fetchAllCardsBD;