/*
 * Возвращает id cтудента в базе и его языковые настройки
 * Устанавливает чавсовой пояс студента
 * Меняет время будильника, если изменился часовой пояс студента
 *
 * Выходные параметры:
 * 		data - объект
 *			.psid или .tgid - id клиента в messenger или телеграм
 *			.timezone - временная зона, вычсисленная в браузере
 *
 * Выходные параметры:
 * 		объект 
 *			.userId - id пользователя в базе 
 *			.translationEnabled - параметр, включающий перевод
 *			.targetLanguage - параметр, показывающий язык перевода
 * 		либо
 * 		исключение 
 * 			'Validation Error' - неполные входные данные
 * 			'Server Error' - студент не найден или любая другая ошибка 
 * 
 * Логика работы
 *      Проверяем откуда пришел запрос - из мессенджера или из телеграмма и находим студента в базе
 * 		ЕСЛИ присланная таймзона студента отличается от той, что в базе, 
 * 		ТО изменим таймзону в базе и перезавдем будильник
 *      Вернем id пользователя и его языковые настройки
 */

// метод рассчитывающий время будильника для тренировки на завтра
const newAlarmClock = require( '../class/newAlarmClock.js' );

// функция проверки входных данных
const validation = require('../class/validation.js');

fetchUserAttributesBD = async function (data, bd) {

	try {

		// проверка на Validation Error
		if ( validation.isData( data, 'timezone' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// дополнительная проверка на Validation Error, если отсутствуют psid и tgid
		data.psid = validation.nullOrNot( data.psid );
		data.tgid = validation.nullOrNot( data.tgid );
		if ( data.psid === null && data.tgid === null ) {
			throw 'Validation Error';
		}

		let users = null;
		
		// если запрос пришел из Messenger, то попытаемся получить атрибуты пользователя
		if ( data.psid !== null ) {

			users = await bd.getUserFromPsid( data.psid );
			if (users === null) {
				throw `Students not found for psid=${data.psid}`;
			}
		}

		// если запрос пришел из Telegram, то попытаемся получить атрибуты пользователя
		// if( data.id ) {

		// 	users = await bd.getUserFromTGid( data.id );
		// 	if( users === null ) {
		// 		throw `Students not found for tgid=${data.id}`;
		// 	}
		// }

		// если запрос пришел из Telegram, то попытаемся получить атрибуты пользователя
		if ( data.tgid !== null ) {
			
			users = await bd.getUserFromTGid( data.tgid );
			if (users === null) {
				throw `Students not found for tgid=${data.tgid}`;
			}
		}

		// вытащим первого(!) пользователя из набора объектов (например, некоторые тестовые пользователи имеют мой psid)
		const user = Object.keys(users)[0];

		// если таймзона клиента отличается от текущей
		if ( data.timezone !== users[user].timezone  ) {

			// то обновим таймзону у пользователя
			await bd.setAttributeUser( user, 'timezone', data.timezone );

			// и перезаведем будильник следующей тренировки - config.hourAlarm часов следующего дня
			const alarm = newAlarmClock( data.timezone );
			await bd.setAttributeUser( user, 'alarmClock', alarm );
		}
		
		// Вернем атрибуты студента
		return {
			userId: user,
			translationEnabled: users[user].translationEnabled,
			targetLanguage: users[user].targetLanguage ? users[user].targetLanguage : null,
			autoPlayEnabled: users[user].autoPlayEnabled
		}
	}
	catch (error) {

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if( global.test === undefined ) console.log( `Server Error in fetchUserAttributesBD. ${error}`);
		throw 'Server Error';
	}
}

module.exports = fetchUserAttributesBD;