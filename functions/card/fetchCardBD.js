/*
 * Получить карточку
 *
 * Входные параметры:
 * 		data - объект
 *		.userId - id клиента, например "1985249568178314"
 *      .cardId - id карточки, например "-LluEc0qYyaHwltv2YFu"
 *
 * Выходные параметры:
 * 		объект содержащий карточку, например
 *		{
 *			Ef: 130,
 *          back: "рассказывать",
 *          dueDate: 1579208400000,
 *			...
 *		}
 * 		либо
 *		исключение 
 *			'Validation Error' - неполные входные данные
 *			'Server Error' - нет такой карточки или любая другая ошибка
 */ 

// функция проверки входных данных
const validation = require('../class/validation.js');

fetchCardBD = async function( data, bd ) {

	try {

		// проверка на Validation Error		
		if ( validation.isData( data, 'userId', 'cardId' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// получим карточку
		const card = await bd.getAttributesCard( data.userId, data.cardId );

		// карточки нет
		if( card === null ) {
			throw `No card with id=${data.cardId} for user=${data.userId}`;
		}

		// Отправим поля карточки клиенту
		return card;
	}
	catch( error ) {

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';
		
		if( global.test === undefined ) console.log( `Server Error in fetchCardBD. ${error}` );
		throw 'Server Error';
	}
}

module.exports = fetchCardBD;