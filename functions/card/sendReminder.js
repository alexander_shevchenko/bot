/*
 * Напоминание о тренировке в ФБ
 * И расчет времени следующего напоминания о тренировке
 * 
 * Входные данные:
 *			bd - класс базы данных
 *			botFB - класс бота facebook
 *			botTG - класс бота telegram
 *
 * Алгоритм:
 * 			сделаем запрос в базу и получим всех пользователей, у которых время сервера > alarmClock
 * 			(напомню, что alarmClock это время на сервере, когда у клиента предположительно 11 часов дня тренировки)
 * 			перебираем всех пользователей, пропуская тестовых пользователей 
 * 				заводим будильник на следующий день
 *				делаем запрос очереди для тренировки
 * 					ЕСЛИ карточки для тренировки есть - то сообщаем пользователю об этом и предлагаем тренироваться
 *					ЕСЛИ карточек для тренировки нет, ТО проверяем, тренировался ли он сегодня 
 *						ЕСЛИ не тренировался - отправляем сообщение пользователю 
 *						ЕСЛИ тренировался, то ничего не делаем
 */

// метод рассчитывающий время будильника для тренировки на завтра
const newAlarmClock = require('../class/newAlarmClock.js');

// функция для получения даты в числовом формате yymmdd
const funDay = require('../class/getDay.js');

// диалоги
const tgDialogYesTrain = require('../dialog/tgDialogYesTrain.js');
const tgDialogNoTrain = require('../dialog/tgDialogNoTrain.js');

async function sendReminder ( bd, botTG ) {

	try {

		// получим пользователей, у которых сработал будильник, т.е. мы считаем что их время сейчас = hourAlarm
		const users = await bd.getUsersWithAlarm();

		// перебираем всех пользователей
		for ( user in users ) {

			// Если у пользователя нет psid и tgid то и не стоит с ним работать, мы их не сможем оповестить
			if( users[user].psid === undefined && users[user].tgid === undefined ) continue;

			// Исключим тестовых пользователей, их id начинается с 'test-', кроме test-sendReminder
			if( user.slice(0,5) === 'test-' && user !== 'test-sendReminder' ) continue;

			// определим сегодняшний день у студента
			const today = funDay.getDay( users[user].timezone );

			// Установим время следующей тренировки - config.hourAlarm часов следующего дня
			const alarm = newAlarmClock( users[user].timezone );
			await bd.setAttributeUser( user, 'alarmClock', alarm );

			// Получим количество карточек в очереди на ТЕКУЩИЙ момент времени 
			const tempQueue = await bd.createQueue( user, users[user].countCard, today );
			const numCards = Object.keys(tempQueue).length;

			// ЕСЛИ карточки для тренировки есть - то сообщаем пользователю об этом и предлагаем тренироваться
			if( numCards > 0 ) {

				// диалог для пользовтеля TG
				if( users[user].tgid !== undefined ) {
					await tgDialogYesTrain( users[user].tgid, users[user].name, numCards, botTG );
				}
			}

			// ЕСЛИ карточек для тренировки нет, ТО проверяем, тренировался ли он сегодня 
			else {

				// если еще не тренировались, значит у пользователя не хватает карточек для тренировки
				// скажем ему об этом, пусть добавляет карточки
				if( users[user].lastTraining !== today ) {

					// диалог для пользовтеля TG
					if( users[user].tgid !== undefined ) {
						await tgDialogNoTrain( users[user].tgid, users[user].name, botTG );
					}		
				}
				// а если уже тренировались сегодня, то ничего делать не надо
			}
		}
	}
	catch( error ) {
		// чтобы не обрывать напоминалку, в случае ошибки не будем генерировать исключение, лишь писать в лог
		console.log( `Server Error in sendReminder. ${error}` );
		// throw 'Server Error';
	}
}

module.exports = sendReminder;