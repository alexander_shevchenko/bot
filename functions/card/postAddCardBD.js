/* Создать новую карточку
 *
 * Входные параметры:
 * 		data - объект 
 *		.userId			- id пользователя
 *		.type 			- тип карточки обычная или оксфордская (custom/oxford)
 *
 *		Поля обычной карточки
 *		.front			- слово
 *		.back			- перевод (перевод и определение условно опциональны, одно из двух полей должно быть)
 *		.definition		- определение (перевод и определение условно опциональны, одно из двух полей должно быть)
 *		.example		- текст (опционально)
 *
 *		Поля оксфордской карточки
 *		.wordId			- id слова
 *		.front			- слово
 *		.part			- часть речи
 *		.definition 	- определение
 *		.defId			- айди определения
 *		.audioUrl		- ссылка на произношение (опционально)
 *		.example 		- определение (опционально)
 *		.synonym		- синоним (опционально)
 *		.antonym		- антоним (опционально)
 *		.synonyms		- массив синонимов (опционально)
 *		.antonyms		- массив антонимов (опционально)
 *		.back 			- перевод (опционально)
 *		.transcription	- транскрипция (опционально)
 *		.domains 		- массив из доменов и регистров
 *		.hint 			- подсказка (опционально)
 *
 * Выходные параметры:
 *		idCard 				- если все ок, возвращаем id созданной карточки
 *		исключения
 *		'Validation Error' 	- неполные входные данные
 *							- если type не custom и не oxford
 *							- если front прилетел пустым для custom карточки 
 *							- или back и definition прилетели пустыми для custom карточки
 *							- если front, part, definition или defId прилетели пустыми для oxford карточки
 *		'Duplicate Found' 	- если front и back для custom карточки совпадают с другой custom карточкой
 *							- если front и definition для custom карточки совпадают с другой custom карточкой
 *							- если defId для oxford карточки совпадают с другой oxford карточкой
 * 		'Server Error' 		- если что-то пошло не так
 */

// функция проверки входных данных
const validation = require('../class/validation.js');

postAddCardBD = async function( data, bd ) {

	try {

		// проверка на Validation Error
		if ( validation.isData( data, 'userId', 'type' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// не унифицированные проверки на Validation Error
		if ( data.type !== 'custom' && data.type !== 'oxford' ) {
			throw 'Validation Error';
		}

		// Проверка наличия полей у custom карточки 
		if ( data.type === 'custom' ) {

			if ( validation.isData( data, 'front' ) === 'Validation Error' ) {
				throw 'Validation Error';
			}
	
			// не унифицированные проверки на Validation Error
			data.back = validation.nullOrNot( data.back, true );
			data.definition = validation.nullOrNot( data.definition, true );
			if ( data.back === null && data.definition === null ) {
				throw 'Validation Error';
			}
		}

		// Проверка наличия полей у oxford карточки
		if ( data.type === 'oxford' ) {

			if ( validation.isData( data, 'wordId', 'front', 'part', 'definition', 'defId' ) === 'Validation Error' ) {
				throw 'Validation Error';
			}
		}

		// проверка на Duplicate Found для custom карточки
		if ( data.type === 'custom' && await bd.isDoubleCard( data.userId, data.front, data.back, data.definition ) === true ) 
			throw 'Duplicate Found';

		// проверка на Duplicate Found для oxford карточки
		if ( data.type === 'oxford' && await bd.isDoubleOxfordCard( data.userId, data.defId ) === true ) 
			throw 'Duplicate Found';
		
		// создаем новую custom карточку
		if ( data.type === 'custom' ) {

			const idCard = await bd.createCard( data.userId, data.front, data.back, data.definition, data.example );
			if ( idCard ) {	

				// создадим hash карточки
				await bd.createCardHash( data.userId, idCard, data.front );

				// запишем лог
				await bd.setLogCard( data.userId, idCard, 'create' );

				// увеличим счетчик карочек в статистике пользователя
				await bd.setTotalCards( data.userId, 1 );

				return idCard;
			}
		}

		// создаем новую oxford карточку
		if ( data.type === 'oxford' ) {

			const idCard = await bd.createOxfordCard( data.userId, 
				data.wordId, data.front, data.part, data.definition, data.defId, 
				data.audioUrl, data.example, data.synonym, data.antonym, data.synonyms, data.antonyms, 
				data.back, data.transcription, data.domains, data.hint );

			if ( idCard ) {	

				// создадим hash карточки
				await bd.createCardHash( data.userId, idCard, data.front, data.part );

				// запишем лог
				await bd.setLogCard( data.userId, idCard, 'create' );
				
				// увеличим счетчик карочек в статистике пользователя
				await bd.setTotalCards( data.userId, 1 );

				return idCard;
			}
		}
	}
	catch( error ) { 

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if ( error === 'Duplicate Found' ) 
			throw 'Duplicate Found';
		
		if( global.test === undefined ) console.log( `Server Error in postAddCardBD. ${error}` );
		throw 'Server Error';
	}
}

module.exports = postAddCardBD;	