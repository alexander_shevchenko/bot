/*
 * Получить сырую очередь из карточек
 *
 * Входные параметры:
 * 		data - объект
 *			.userId - id клиента, например "1985249568178314"
 *
 * Выходные параметры:
 * 		объект содержащий карточки, например
 * 		card: { '-L_1jXy1K75zbwT92ICZ':
 *			{ Ef: 0,
 *			  back: 'колесо',
 *			  dueDate: 0,
 *			  front: 'wheel',
 *			  id: '-L_1jXy1K75zbwT92ICZ',
 *			  status: 'new' }, ...
 *		и статистику
 *			stat: {
 *     			bestStreak: 12,
 *     			currentStreak: 1,
 *     			totalCards: 85,
 *     				'20-07-12': { trainedNew: 1, trainedTotal: 9 },
 *     				'20-07-11': { trainedNew: 2, trainedTotal: 5 }
 * 					...
 *   		}
 * 		либо
 *		'Done' - если пользователь сегодня уже оттренировался и новых карточек не добавлял
 *		'Empty' - если на сегодня в принципе ничего нет и не было
 * 		исключение 
 * 			'Validation Error' - неполные входные данные
 * 			'Server Error' - если что-то пошло не так
 * 
 * Логика работы:
 * 			1. Определяем тренировались сегодня или нет
 * 				Рассчитываем сегодняшнюю дату у клиента - currentDay 
 * 				На сервере хранится lastTraining - клиентская дата последней тренировки
 * 				Если lastTraining == currentDay, то это значит, что сегодня тренировка уже была
 * 			2. Определяем сколько надо запросить новых карточек для сегодняшней тренировки. 
 * 				Если сегодня тренировали новые карточки, то учтем их количсество.
 * 			3. Передаем в bd.createQueue текущий день у клиента, 
 * 				таким образом мы получим все review карточки у которых подошла dueDate
 * 			4. Запрашиваем статистику клиента, пересчитываем ее с учетом активности последних 7 дней.
 * 				Перезаписываем обновленную статистику на сервере.
 */ 

// функция для получения даты в числовом формате yymmdd
const funDay = require('../class/getDay.js');

// функция проверки входных данных
const validation = require('../class/validation.js');

fetchCardsBD = async function( data, bd ) {

	try {
		
		// проверка на Validation Error		
		if ( validation.isData( data, 'userId' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// получим атрибуты студента
		const userAttr = await bd.getAttributesUser( data.userId );
		if( userAttr === null ) {
			throw `fetchCardsBD: Error to read attribute from 'students-attributes/${data.userId}.`;
		}

		// получим текущий день клиента в нужном формате
		const currentDay = funDay.getDay( userAttr.timezone );

		// определим, первая ли тренировка это на сегодня
		const firstTrain = ( userAttr.lastTraining == currentDay ) ? false : true;
		
		// Определим сколько карточек new + inactive планируется к показу. Полный набор, если сегодня первая тренировка. Или только то, что осталось от уже начатой сегодня тренировки.
		const countCard = ( firstTrain ) ? userAttr.countCard : userAttr.countCard - userAttr.todayCard;

		// Запрос на формирование сырой очереди
		const cards = await bd.createQueue( data.userId, countCard, currentDay );

		// Если на сегодня это первая тренировка, то установим обнулим число тренированных за сегодня новых карточек (todayCard = 0)
		if ( firstTrain ) {
			
			await bd.setAttributeUser( data.userId, 'todayCard', 0 );
		}

		// Если это первая тренировка и очередь пуста, значит на сегодня ничего нет
		if ( firstTrain && Object.keys( cards ).length == 0 ) {

			return 'Empty';
		}
			
		// Если это НЕ первая тренировка и очередь пуста, значит на сегодня уже все что смогли, потренировали
		if ( !firstTrain && Object.keys( cards ).length == 0 ) {

			return 'Done';
		}

		// получим данные статистики
		const stat = await bd.getStatCommon( data.userId );

		// объект с результатом статистики для записи обратно в базу
		const stat2base = {};
		stat2base.currentStreak = stat.currentStreak;
		stat2base.totalCards = stat.totalCards;
		stat2base.bestStreak = stat.bestStreak;

		// объект с результатом статистики для выдачи во фронтенд
		const stat2front = {};
		stat2front.currentStreak = stat.currentStreak;
		stat2front.totalCards = stat.totalCards;
		stat2front.bestStreak = stat.bestStreak;
		stat2front.trainedTotal = [];
		stat2front.trainedNew = [];

		// сделаем выборку статистики за последние 7 дней
		for( i=0 ; i<7 ; i++ ) {
	
			day = funDay.getDay( userAttr.timezone, i);
			if ( stat[day] === undefined )  {
				stat[day] = { trainedNew: undefined, trainedTotal: undefined };
			} 
	
			stat2front.trainedTotal[i] = stat[day].trainedTotal === undefined ? 0 :stat[day].trainedTotal;
			stat2front.trainedNew[i] = stat[day].trainedNew === undefined ? 0 : stat[day].trainedNew;
	
			stat2base[day] = { 
				trainedNew: stat[day].trainedNew === undefined ? 0 : stat[day].trainedNew, 
				trainedTotal: stat[day].trainedTotal === undefined ? 0 :stat[day].trainedTotal 
			};
		} 

		// обновим статистику: сколько дней непрерывно тренировались, если был перерыв, то обнулим streak
		// const delta = currentDay - userAttr.lastTraining;
		const delta = funDay.deltaDay( currentDay, userAttr.lastTraining )
		if( delta > 1 ) {
			stat2front.currentStreak = 0;
			stat2base.currentStreak = 0;
		}

		// запишем выборку обратно в базу, тем самым удалив лишнюю статистику 
		bd.setStatCommon( data.userId, stat2base );
	
		// Отправим очередь и статистику клиенту
		const queue = {};
		queue.stat = stat2front;
		queue.cards = cards;

		return queue ;
	}
	catch( error ) {

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if( global.test === undefined ) console.log( `Server Error in fetchCardsBD. ${error}` );
		throw 'Server Error';
	}
}

module.exports = fetchCardsBD;