/* Отредактировать карточку
 * Редактируются не все поля, лишь перечисленные ниже
 *
 * Входные параметры:
 * 		data - объект 
 *
 *		Общие поля для всех карточек
 *		.userId			- id пользователя
 *		.cardId 		- id редактируемой карточки
 *		.type 			- тип карточки обычная или оксфордская (custom/oxford)
 *
 *		Поля обычной карточки
 *		.front			- слово (обязательное поле)
 *		.back			- перевод (перевод и определение условно опциональны, одно из двух полей должно быть)
 *		.definition		- определение (перевод и определение условно опциональны, одно из двух полей должно быть)
 *		.example		- текст (опционально)
 *
 *		Поля оксфордской карточки
 *		.defId			- (обязательное поле)
 *		.example 		- определение (опционально)
 *		.synonym		- синонимы (опционально)
 *		.antonym		- антонимы (опционально)
 *		.back 			- перевод (опционально)
 *		.hint 			- подсказка (опционально)
 *
 * Выходные параметры:
 *		true 				- если все ок
 * 		либо
 *  	исключение
 *		'Validation Error' 	- неполные входные данные
 *							- если type не custom и не oxford
 *							- если front прилетел пустым для custom карточки
 *							- если back или definition прилетели пустыми для custom карточки
 *							- если не существует карточки userId\cardId
 *							- если defId прилетел пустым для oxford карточки
 *		'Duplicate Found' 	- если front и back для custom карточки совпадают с другой custom карточкой
 *							- если front и definition для custom карточки совпадают с другой custom карточкой
 *							- если defId для oxford карточки совпадают с другой oxford карточкой
 * 		'Server Error' 		- если что-то пошло не так
 */

// функция проверки входных данных
const validation = require('../class/validation.js');

postUpdateCardBD = async function( data, bd ) {

	try {

		// проверка на Validation Error
		if ( validation.isData( data, 'userId', 'cardId', 'type' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// получим данные карточки, которую собираемся редактировать
		const card = await bd.getAttributesCard( data.userId, data.cardId );
		if ( card === null ) 
			throw 'Validation Error';

		if ( data.type !== 'custom' && data.type !== 'oxford' ) 
			throw 'Validation Error';

		// Проверка наличия полей у custom карточки
		if ( data.type === 'custom' ) {

			data.front = validation.nullOrNot( data.front, true );
			data.back = validation.nullOrNot( data.back, true );
			data.definition = validation.nullOrNot( data.definition, true );

			if ( data.front === null || data.back === null && data.definition === null ) {
				throw 'Validation Error';
			}
		}

		// Проверка наличия полей у oxford карточки
		data.defId = validation.nullOrNot( data.defId, true );
		if ( data.type === 'oxford' && data.defId === null ) {
			throw 'Validation Error';
		}

		// проверка на Duplicate Found для custom карточки
		if ( data.type === 'custom' && await bd.isDoubleCard( data.userId, data.front, data.back, data.definition, data.cardId ) === true ) 
			throw 'Duplicate Found';

		// проверка на Duplicate Found для oxford карточки
		if ( data.type === 'oxford' && await bd.isDoubleOxfordCard( data.userId, data.defId, data.cardId ) === true ) 
			throw 'Duplicate Found';

		// проверка полей custom карточки, изменяем нужные
		if ( data.type === 'custom' ) {

			if ( card.front !== data.front ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'front', data.front );
				await bd.setAttributeHash( data.userId, data.cardId, 'f', data.front );
				await bd.setLogCard(data.userId, data.cardId, `update front:'${data.front}'` );
			}
			if ( card.back !== data.back ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'back', data.back );
				await bd.setLogCard(data.userId, data.cardId, `update back:'${data.back}'` );
			}
			if ( data.example !== undefined && card.example !== data.example ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'example', data.example );
				await bd.setLogCard(data.userId, data.cardId, `update example:'${data.example}'` );
			}
			if ( card.definition !== data.definition ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'definition', data.definition );
				await bd.setLogCard(data.userId, data.cardId, `update definition:'${data.definition}'` );
			}
		}

		// проверка полей oxford карточки, изменяем нужные
		if ( data.type === 'oxford' ) {

			if ( data.example !== undefined && card.example !== data.example ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'example', data.example );
				await bd.setLogCard(data.userId, data.cardId, `update example:'${data.example}'` );
			}
			if ( data.synonym !== undefined && card.synonym !== data.synonym ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'synonym', data.synonym );
				await bd.setLogCard(data.userId, data.cardId, `update synonym:'${data.synonym}'` );
			}
			
			if ( data.antonym !== undefined && card.antonym !== data.antonym ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'antonym', data.antonym );
				await bd.setLogCard(data.userId, data.cardId, `update antonym:'${data.antonym}'` );
			}
			
			if ( data.back !== undefined && card.back !== data.back ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'back', data.back );
				await bd.setLogCard(data.userId, data.cardId, `update back:'${data.back}'` );
			}
			
			if ( data.hint !== undefined && card.hint !== data.hint ) {
				await bd.setAttributeCard( data.userId, data.cardId, 'hint', data.hint );
				await bd.setLogCard(data.userId, data.cardId, `update hint:'${data.hint}'` );
			}
		}

		// получим и вернем данные обновленной карточки
		const cardNew = await bd.getAttributesCard( data.userId, data.cardId );
		return cardNew;
	}
	catch( error ) {

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if ( error === 'Duplicate Found' ) 
			throw 'Duplicate Found';

		if( global.test === undefined ) console.log( `Server Error in postUpdateCardDB. ${error}` );
		throw 'Server Error';		
	}
}

module.exports = postUpdateCardBD;