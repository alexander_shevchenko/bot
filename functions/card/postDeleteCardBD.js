/* Удалим карточку
 *
 * Входные параметры:
 * 		data - объект
 *		.userId - id клиента, например '1985249568178314'
 *		.cardId - id карточки, например '-L_1jXy1K75zbwT92ICZ'
 *
 * Выходные параметры:
 * 		id карточки - в случае успеха
 *		исключение 
 *			'Validation Error' - неполные входные данные
 *			'Server Error' - если что-то пошло не так
 */

// функция проверки входных данных
const validation = require('../class/validation.js');

postDeleteCardBD = async function( data, bd) {

	try {

		// проверка на Validation Error
		if ( validation.isData( data, 'userId', 'cardId' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// удаление карточки
		await bd.removeCard( data.userId, data.cardId );

		// удалим hash карточки
		await bd.removeHash( data.userId, data.cardId );

		// уменьшим счетчик карочек в статистике пользователя
		await bd.setTotalCards( data.userId, -1 );

		return data.cardId;
	}
	catch( error ) { 

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if( global.test === undefined ) console.log( `Server Error in postDeleteCardBD. ${error}` );
		throw 'Server Error';
	}
}

module.exports = postDeleteCardBD;