/* ???
 * ???
 *
 * Входные параметры:
 * 		data - объект 
 *			.userId				- id пользователя
 *			.cardId 			- id редактируемой карточки
 *			.defFirst 			- ???
 *
 * Выходные параметры:
 *		обновленную карточку
 * 		либо исключение:
 *			'Validation Error' 	- неполные входные данные
 * 			'Server Error' 		- если что-то пошло не так
 */

// функция проверки входных данных
const validation = require('../class/validation.js');

postCardPreferencesBD = async function( data, bd ) {

	try {

		// проверка на Validation Error
		if ( validation.isData( data, 'userId', 'cardId', 'defFirst' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// изменим defFirst у карточки
		await bd.setAttributeCard( data.userId, data.cardId, 'defFirst', data.defFirst );

		// получим и вернем данные обновленной карточки
		const cardNew = await bd.getAttributesCard( data.userId, data.cardId );
		return cardNew;

	}
	catch( error ) {

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if( global.test === undefined ) console.log( `Server Error in postCardPreferencesBD. ${error}` );
		throw 'Server Error';		
	}
}

module.exports = postCardPreferencesBD;