/* 
 * Обработать отмашку конца тренировки
 *
 * Входные параметры:
 * 		data (объект): 
 *					.userId - id пользователя
 *					.length - сколько было карточек пройдено
 *
 * Выходные параметры:
 *					в случае успеха объект с полями:
 *						isFirstDoneEver: - флаг окончания первой тренировки true или false
 * 					либо
 *					исключение 
 * 					'Validation Error' - неполные входные данные
 * 			'		Server Error' - если что-то пошло не так
 *
 * Алгоритм:
 *          Проверить значение аттрибута firstTrain у пользователя.
 * 			Изменить firstTrain его на true, если прошла первая тренировка. 
 * 			Вернуть клиенту флаг первой тренировки.
 */

// функция проверки входных данных
const validation = require('../class/validation.js');

postDoneBD = async function( data, bd ) {

	try {

		// проверка на Validation Error
		if ( validation.isData( data, 'userId', 'length' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// получим атрибуты пользователя
		const attr = await bd.getAttributesUser( data.userId );
		
		// флаг для того, чтобы сообщить фронтенду о первой тренировке
		const isFirstDoneEver = !attr.firstTrain;

		// если значение firstTrain === false, то это означает, что сейчас закончилась первая тренировка
		if ( attr.firstTrain === false ) { 

			// изменим значение атрибута firstTrain на true
            bd.setAttributeUser( data.userId, 'firstTrain', true);
        }
        return { isFirstDoneEver: isFirstDoneEver };
    }
    catch( error ) {
		
		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if( global.test === undefined ) console.log( `Server Error in postDoneBD. ${error}` );
        throw 'Server Error';
	}
}

module.exports = postDoneBD;