/*
 * Обработать ответ на карточку
 *
 * Входные параметры:
 * 		data (объект): 
 *						.userId - id пользователя
 *						.card - объект карточка
 *							.id - id карточки
 *							.status - статус карточки 
 *							.Ef - фактор карточки, влияющий на период, когда она опять будет показана
 *							.period - период в днях, через который будет показана карточка
 *							.dueDate - дата, когда необходимо показать карточку, в формате yymmdd
 *							.newlogs - строчки лога в виде массива объектов {string, timestamp} (строчка лога, тайстамп клиента)
 *							.todayNewCard - если = true, значит получена положительнаяя оценка на inactive или new карточка
 *							.todayTotlaCard - если = true, значит получена положительная оценка на review карточку
 *
 * Выходные параметры:
 *				true - если все ок
 *				либо
 * 				исключение 
 * 					'Validation Error' - неполные входные данные
 * 					'Server Error' - такой карточки нет в базе или любая другая ошибка
 * 
 * Логика работы:
 *				1. Проверим, что карточка существует, вдруг ее удалили 
 *				2. Изменим атрибуты (Ef, status и т.д.) карточки и лог карточки
 *				3. Меняем статистику пользователя 
 *				4. Изменим параметры пользователя (день последней тренировки и счетчик новых карточек за сегодня)
 */

// функция проверки входных данных
const validation = require('../class/validation.js');

// функция для получения даты в числовом формате yymmdd
const  funDay = require('../class/getDay.js');

postGradedBD = async function( data, bd ) {

	try {

		// проверка на Validation Error
		if ( validation.isData( data, 'userId', 'card' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}

		// проверим существование такой карточки
		if( !await bd.isCardExist( data.userId, data.card.id) ) {
			throw `Card ${data.card.id} not exist`;
		}

		// получим атрибуты карточки, обновим поля Ef, status, dueDate, period и запишем их в базу
		const attributesCard = await bd.getAttributesCard( data.userId, data.card.id );
		attributesCard.Ef = data.card.Ef;
		attributesCard.status = data.card.status;
		attributesCard.dueDate = data.card.dueDate;
		attributesCard.period = data.card.period;
		await bd.setAttributesCard( data.userId, data.card.id, attributesCard); 

		// добавим записи в лог
		await bd.setLogCard( data.userId, data.card.id, data.card.newlogs[0].string );
		for (let i = 1; i < data.card.newlogs.length; i++) {
			await bd.setLogCard( data.userId, data.card.id, data.card.newlogs[i].string ); 
		}

		// получим атрибуты пользователя, нужно для определения таймзоны и даты последней тренировки
		const attributesUser = await bd.getAttributesUser( data.userId );

		// определим текущий день у клиента для записи статистики и даты последней тренировки
		const dayNow = funDay.getDay( attributesUser.timezone );

		// обновим статистику: сколько new или inactive карточек мы сегодня выучили
		if ( data.card.todayNewCard == true) {
			await bd.incStatNewCard( data.userId, dayNow );
			await bd.incStatTotalCard( data.userId, dayNow );
		}

		// обновим статистику: сколько всего мы сегодня выучили
		if( data.card.todayTotlaCard == true ) {
			await bd.incStatTotalCard( data.userId, dayNow );
		}

		// если разница между двумя последними тренировками = 1 день, то увеличим счетчик тренировок
		if( funDay.deltaDay( dayNow, attributesUser.lastTraining ) == 1 ) {
			await bd.incStatCurrentStreak( data.userId );
		}

		// При необходимости запомним в базе дату последней тренировки
		if ( attributesUser.lastTraining !== dayNow) { 
			await bd.setAttributeUser( data.userId, 'lastTraining', dayNow)
		}

		// Если была оценена new или inactive карточка, то обновить поле todayCard (увеличим счетчик изученых новых карточек сегодня на 1)
		if ( data.card.todayNewCard == true) { 
			await bd.setAttributeUser( data.userId, 'todayCard', attributesUser.todayCard + 1)
		}

		return true;
	}
	catch( error ) {

		if ( error === 'Validation Error' ) 
		throw 'Validation Error';

		if( global.test === undefined ) console.log( `Server Error in postGradedBD. ${error}` );
		throw 'Server Error';
	}
}

module.exports = postGradedBD;
