/*
 * Записать атрибуты cтудента в базу
 *
 * Входные параметры:
 * 		data - объект
 *			.userId - id клиента, например '1'
 *			.targetLanguage - параметр, определяющий язык перевода, например 'RU' или null
 *			.translationEnabled - параметр, включающий перевод = false | true
 *			.autoPlayEnabled - параметр, разрешающий автопроигрывание аудиофайлов = false | true
 * 
 * Выходные параметры:
 *		измененный объект свойств студента	- если все ок
 *		либо
 * 		исключение
 *		'Validation Error' 	- если входные параметры содержат ошибку или они не полные
 *		'Server Error' 		- если студент отсутствует или любая другая ошибка
 * 
 * Логика работы
 *      Проверяем переменные объекта дата на валидность.
 * 		Изменяем аттрибуты пользователя.
 */

// функция проверки входных данных
const validation = require('../class/validation.js');

postUserAttributesBD = async function( data, bd ) {

	try {
		
		// проверка на Validation Error
		if ( validation.isData( data, 'userId', 'translationEnabled' ) === 'Validation Error' ) {
			throw 'Validation Error';
		}
		
		// проверка существования пользователя
		if ( await bd.getAttributesUser( data.userId ) === null ) {
			throw 'Server Error';
		}

		// запись в базу атрибутов
		await bd.setAttributeUser( data.userId, 'targetLanguage', data.targetLanguage );
		await bd.setAttributeUser( data.userId, 'translationEnabled', data.translationEnabled );
		await bd.setAttributeUser( data.userId, 'autoPlayEnabled', data.translationEnabled );
		
		// запросим у базы чего туда записалось
		const user = await bd.getAttributesUser( data.userId );
		
		// что то пошло не так
		if( user === null ) {
            throw 'Server Error';
		}

		// Вернем атрибуты студента
		return {
			userId: data.userId,
			translationEnabled: user.translationEnabled,
			targetLanguage: user.targetLanguage ? user.targetLanguage : null,
			autoPlayEnabled: autoPlayEnabled
		}
	}
	catch( error ) {

		if ( error === 'Validation Error' ) 
			throw 'Validation Error';

		if ( error === 'Server Error' ) 
			throw 'Server Error';

		if( global.test === undefined ) console.log( `Server Error in postUserAttributesBD. ${error}` );
		throw 'Server Error';
	}
}

module.exports = postUserAttributesBD;